import facebook;

exports.uid = "";
exports.accessToken = "";

exports.GetLoginStatusCB = function(response) {
	console.log("Tiendv - getLoginStatus: " + JSON.stringify(response));
    if (response.status === 'connected') {
      // the user is logged in and has authenticated your
      // app, and response.authResponse supplies
      // the user's ID, a valid access token, a signed
      // request, and the time the access token 
      // and signed request each expire
      uid = response.authResponse.userID;
      accessToken = response.authResponse.accessToken;
      console.log("getLoginStatus = connected");
	  getUserInfo();
    } else if (response.status === 'not_authorized') {
      // the user is logged in to Facebook, 
      // but has not authenticated your app
      console.log("getLoginStatus = not_authorized");
    } else {
      // the user isn't logged in to Facebook.
      console.log("the user isn't logged in to Facebook.");
      login();
    }  
};

var login = exports.login = function(){
	facebook.login(function(response) {
        if (response && response.authResponse) {
          console.log('Successfully login');
        } else {
          console.log('User cancelled login or did not fully authorize.');
        }		
	}, {scope: 'public_profile, publish_actions'});
};

var getUserInfo = exports.getUserInfo = function() {
	facebook.api('/me', function(response) {
	console.log(JSON.stringify(response));
	}); 
};

