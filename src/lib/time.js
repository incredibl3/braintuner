var SEC = 1000,
	MIN = 60 * SEC,
	HOUR = 60 * MIN,
	DAY = 24 * HOUR,
	WEEK = 7 * DAY;

exports.units = {
	second: SEC,
	minute: MIN,
	hour: HOUR,
	day: DAY,
	week: WEEK
};

exports.sec2dur = function(s) {
	var h = 0, m = 0, str = '';
	while (s >= 3600) {
		s -= 3600;
		h += 1;
	}
	while (s >= 60) {
		s -= 60;
		m += 1;
	}
	if (h) {
		str += h + 'h';
	}
	if (m) {
		str += m + 'm';
	}
	if (s) {
		str += s + 's';
	}
	return str;
};

exports.parseDuration = function(dur) {
	var d = {
		days: ~~(dur / DAY),
		hours: ~~(dur / HOUR) % 24,
		mins: ~~(dur / MIN) % 60,
		secs: ~~(dur / SEC) % 60
	};
	d.countdown = (d.mins ? (d.mins + ':' + ((d.secs < 10) ? '0' : '')) : '') + d.secs;
	return d;
};