import device;
device.defaultFontFamily = "BPreplayBold";

exports = {
	REQUEST_RATING_PERIOD: 10,
	blocksFallTogether: false,
	specialEffects: true,

	incentives: {
		connectToFB: {
			rewards: {
				cash: 20
			}
		},
		livesTutorial: {
			rewards: {
				cash: 5,
				lives: 5
			}
		}
	},

	colors: {
		friendName: 'rgb(91, 101, 213)',
		friendScore: 'rgb(23, 34, 124)',
		friendRank: 'rgb(168, 83, 12)',
		friendBack: 'rgb(241, 255, 249)',
		giftName: 'rgb(8, 49, 25)',
		giftAcceptStroke: 'rgb(77, 151, 252)',
		moreLives: 'rgb(151, 78, 22)',
		pink: 'rgb(235, 71, 210)',
		blue: 'rgb(82, 132, 230)',
		green: 'rgb(30, 117, 33)',
		magenta: 'rgb(150, 42, 135)',
		blueGray: 'rgb(64, 101, 126)',
		brown: 'rgb(126, 64, 30)',
		lightBrown: 'rgb(219, 201, 167)',
		blueStroke: 'rgb(98, 140, 197)',
		darkBlue: 'rgb(46, 46, 106)',
		darkRed: 'rgb(138, 36, 29)',
		rankBrown: 'rgb(188, 102, 44)',
		cellSeparator: '#4978DE',
		cellLight: '#F1F8F6',
		cellDark: '#DCCBE2',
		white: 'rgb(255, 255, 255)',
		black: 'rgb(0, 0, 0)',
		achievementName: 'rgb(42, 45, 117)',
		achievementDesc: 'rgb(93, 100, 170)',
		achievementNameIncomplete: '#767676',
		achievementDescIncomplete: '#ACACAC'
	},

	sparkles: [
		"resources/images/GAME/sparkly.png",
		"resources/images/GAME/sparklyRed.png",
		"resources/images/GAME/sparklyOrange.png",
		"resources/images/GAME/sparklyPink.png",
		"resources/images/GAME/sparklyWhite.png",
		"resources/images/GAME/sparklyGreen.png",
		"resources/images/GAME/sparklyBlue.png",
		"resources/images/GAME/sparklyPurple.png"
	],

	itemSlotUnlocks: [0, 0, 0],

	special_item_ids : {
		'firework': 106,
		'blackhole': 107,
		'timebonus': 108
	},

	special_items: [
		{
			id: 106,
			name: "Firework Crystal",
			description: "Award random points at end of next game!",
			image: "resources/images/SPECIALS/icon_special_fireworks.png",
			cost: 250,
			costType: 'coins',
			hours: -1,
			uses: 1
		},
		{
			id: 107,
			name: "Black Hole Crystal",
			description: "Award 100,000 bonus points at end of next game!",
			image: "resources/images/SPECIALS/icon_special_blackhole.png",
			cost: 350,
			costType: 'coins',
			hours: -1,
			uses: 1
		},
		{
			id: 108,
			name: "Time Crystal",
			description: "Increase duration of games by 15 seconds!",
			image: "resources/images/SPECIALS/icon_special_timebonus.png",
			cost: 15,
			costType: 'cash',
			minutes: 24 * 60,
			uses: -1

		},
	],
	SPECIAL_ITEM_OFFER_PERIOD: 3,

	stores: {
		coinStore: {
			storeID: "coinStore",
			currencySpent: "BUCKS",
			currencyBought: "COINS",
			headerText: "MORE COINS",
			headerImage: "buycoins",
			items: [
				{
					name: "16,000 Coins",
					productID: "coins_16000",
					image: "resources/images/MAIN/icon_coinpiles_05.png",
					saleTag: "BEST VALUE!",
					saleInfo: "60% MORE FREE!",
					priceTag: "500",
					isBucks: true,
					cost: 500,
					value: 16000
				},
				{
					name: "8,000 Coins",
					productID: "coins_8000",
					image: "resources/images/MAIN/icon_coinpiles_04.png",
					saleTag: "POPULAR",
					saleInfo: "33% MORE FREE!",
					priceTag: "300",
					isBucks: true,
					cost: 300,
					value: 8000
				},
				{
					name: "2,400 Coins",
					productID: "coins_2400",
					image: "resources/images/MAIN/icon_coinpiles_03.png",
					saleTag: false,
					saleInfo: false,
					priceTag: "100",
					isBucks: true,
					cost: 100,
					value: 2400
				},
				{
					name: "1,100 Coins",
					productID: "coins_1100",
					image: "resources/images/MAIN/icon_coinpiles_02.png",
					saleTag: false,
					saleInfo: false,
					priceTag: "50",
					isBucks: true,
					cost: 50,
					value: 1100
				},
				{
					name: "200 Coins",
					productID: "coins_200",
					image: "resources/images/MAIN/icon_coinpiles_01.png",
					saleTag: false,
					saleInfo: false,
					priceTag: "10",
					isBucks: true,
					cost: 10,
					value: 200
				}
			]
		},
		buckStore: {
			storeID: "buckStore",
			currencySpent: "MONEY",
			currencyBought: "BUCKS",
			headerText: "MORE BUCKS",
			headerImage: "buybucks",
			items: [
				{
					name: "750 Bucks",
					productID: "bucks_750",
					image: "resources/images/MAIN/icon_buckpiles_05.png",
					saleTag: "BEST VALUE!",
					saleInfo: "50% MORE FREE!",
					priceTag: "$49.99",
					cost: 49.99,
					value: 750
				},
				{
					name: "400 Bucks",
					productID: "bucks_400",
					image: "resources/images/MAIN/icon_buckpiles_04.png",
					saleTag: "POPULAR",
					saleInfo: "33% MORE FREE!",
					priceTag: "$29.99",
					cost: 29.99,
					value: 400
				},
				{
					name: "120 Bucks",
					productID: "bucks_120a",
					image: "resources/images/MAIN/icon_buckpiles_03.png",
					saleTag: false,
					saleInfo: false,
					priceTag: "$9.99",
					cost: 9.99,
					value: 120
				},
				{
					name: "55 Bucks",
					productID: "bucks_55a",
					image: "resources/images/MAIN/icon_buckpiles_02.png",
					saleTag: false,
					saleInfo: false,
					priceTag: "$4.99",
					cost: 4.99,
					value: 55
				},
				{
					name: "10 Bucks",
					productID: "bucks_10",
					image: "resources/images/MAIN/icon_buckpiles_01.png",
					saleTag: false,
					saleInfo: false,
					priceTag: "$0.99",
					cost: 0.99,
					value: 10
				}
			]
		},
		lifeStore: {
			storeID: "lifeStore",
			currencySpent: "BUCKS",
			currencyBought: "LIVES",
			headerText: "MORE LIVES",
			headerImage: "buylives",
			items: [
				{
					name: "150 Lives",
					productID: "lives_150",
					image: "resources/images/MAIN/icon_heartpiles_05.png",
					saleTag: "BEST VALUE!",
					saleInfo: "50% MORE FREE!",
					priceTag: "100",
					isBucks: true,
					cost: 100,
					value: 150
				},
				{
					name: "55 Lives",
					productID: "lives_55",
					image: "resources/images/MAIN/icon_heartpiles_04.png",
					saleTag: "POPULAR",
					saleInfo: "38% MORE FREE!",
					priceTag: "40",
					isBucks: true,
					cost: 40,
					value: 55
				},
				{
					name: "25 Lives",
					productID: "lives_25",
					image: "resources/images/MAIN/icon_heartpiles_03.png",
					saleTag: false,
					saleInfo: false,
					priceTag: "20",
					isBucks: true,
					cost: 20,
					value: 25
				},
				{
					name: "12 Lives",
					productID: "lives_12",
					image: "resources/images/MAIN/icon_heartpiles_02.png",
					saleTag: false,
					saleInfo: false,
					priceTag: "10",
					isBucks: true,
					cost: 10,
					value: 12
				},
				{
					name: "5 Lives",
					productID: "lives_5",
					image: "resources/images/MAIN/icon_heartpiles_01.png",
					saleTag: false,
					saleInfo: false,
					priceTag: "5",
					isBucks: true,
					cost: 5,
					value: 5
				}
			]
		}
	}
};