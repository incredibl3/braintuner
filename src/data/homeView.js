import device;

var BG_WIDTH = 576;
var BG_HEIGHT = 1024;

//Reference aspect ratio as long/short
var delta = 0.01;
var _16x9 = 16/9;
var _3x2 = 3/2;
var _4x3 = 4/3;
var ASPECT_RATIO = device.screen.height / device.screen.width;
logger.log ("aspect ratio " + ASPECT_RATIO + " | width: " + device.screen.width + ", height: " + device.screen.height);

//Default is 16:9
//MainMenu configuration
var hometitleScale = 0.8;
var hometitleTop = 10;
var homebrainguy_x = 180;
var homebrainguy_y = 140;
var hometext_y = 230;
//Choosemode linear layout configuration
var modeScale = 0.9;
var startButtonY = 0;
var modeLayoutY = 200;
var bestScoreFrameLayoutY = 340;
var bestScroreFrameTextY = -18;
var bestScoreTextSize = 50;
var resetButtonLeft = 30;
var resetButtonY = 25;
// var shareFrameLayoutY = 450;
// var shareTextSize = 35;
// var shareButtonScale = 0.6;
// var shareFrameLeft = 10;
// var shareButtonY = 0;
//Freelayout
var FreelayoutY = 550;

//GameScreen configuration
var PlayBackButtonY = 10;
var chosenButtonScale = 0.8;
var AdHeight = 100;

//3:2
if(ASPECT_RATIO < (_16x9-delta) ) {
	logger.log ("aspect ratio less than 16:9");
	modeLayoutY = 170;
	bestScoreFrameLayoutY = 300;
	// shareFrameLayoutY = 340;
	shareTextSize = 35;
	FreelayoutY = 520;
}

//4:3
if(ASPECT_RATIO < (_3x2-delta) ){
	logger.log ("aspect ratio less than 3:2");
	hometitleTop = 0;
	modeLayoutY = 160;
	bestScoreFrameLayoutY = 280;
	// shareFrameLayoutY = 320;
	// shareTextSize = 35;
	// shareButtonScale = 0.4;
	// shareButtonY = -10;	
	FreelayoutY = 450;
}

exports = { 
	name: 'layout',
	width: BG_WIDTH,
	height: BG_HEIGHT,
	children: [
		{
			name: "mainmenu",
			cls: 'src.view.home.MainMenu',
			x: 0,
			y: 0,
			zIndex: 1,
			width: BG_WIDTH,
			height: BG_HEIGHT,
			children: [
				{
					name: 'bgImage',
					cls: 'ui.ImageView',
					x: 0,
					y: 0,
					width: BG_WIDTH,
					height: BG_HEIGHT,
					image: 'resources/images/Background.png',
					canHandleEvents: false
				},
				{
					name: 'hometitle',
					cls: 'ui.ImageView',
					x: (BG_WIDTH - 591 * hometitleScale) / 2,
					y: hometitleTop,
					width: 591 * hometitleScale,
					height: 209 * hometitleScale,
					image: 'resources/images/braintunerx/MainScene/HomeTitle.png',
					canHandleEvents: false
				},			
				{
					name: 'modeLayout',
					layout: 'linear',
					direction: 'horizontal',
					x: 36,
					y: modeLayoutY,
					width: 540,
					height: 160,
					children: [
						{
							name: 'button20',
							cls: 'ui.ImageView',
							width: 190,
							height: 143,
							anchorX: 190 / 2,
							anchorY: 143 / 2,
							image: 'resources/images/braintunerx/MainScene/Button20Unpress.png',
							scale: modeScale,
						},
						{
							name: 'button60',
							cls: 'ui.ImageView',
							width: 190,
							height: 143,
							anchorX: 190 / 2,
							anchorY: 143 / 2,							
							image: 'resources/images/braintunerx/MainScene/Button60Unpress.png',
							scale: modeScale,
						},
						{
							name: 'button100',
							cls: 'ui.ImageView',
							width: 190,
							height: 143,
							anchorX: 190 / 2,
							anchorY: 143 / 2,							
							image: 'resources/images/braintunerx/MainScene/Button100Unpress.png',
							scale: modeScale,
						},						
						// {
						// 	name: 'startButton',
						// 	cls: 'ui.widget.ButtonView',
						// 	y: startButtonY,
						// 	left: modeLeftSpacing + 10,
						// 	anchorX: 190 / 2,
						// 	anchorY: 143 / 2,
						// 	width: 190,
						// 	height: 143,
						// 	images: {
						// 		"up": "resources/images/braintunerx/MainScene/HomeStartButtonUnpress.png",
						// 		"down": "resources/images/braintunerx/MainScene/HomeStartButtonPress.png"
						// 	},
						// 	scale: modeScale
						// }
					]
				},
				{
					name: "bestScoreFrame",
					layout: 'linear',
					direction: 'horizontal',
					x: 36,
					y: bestScoreFrameLayoutY,
					width: 530,
					height: 320,
					children: [
						{
							name: "bestScoreLabel",
							cls: 'ui.TextView',
							fontFamily: 'Denne Kitten Heels',
							y: bestScroreFrameTextY,
							size: bestScoreTextSize,
							left: 20,
							width: 150,
							height: 180,
							autoFontSize: false,
							text: "Best:"
						},
						{
							name: "bestScoreText",
							cls: 'ui.TextView',
							fontFamily: 'Denne Kitten Heels',
							y: bestScroreFrameTextY,
							size: bestScoreTextSize,
							left: 0,
							width: 150,
							height: 180,
							autoFontSize: false,
							text: "None yet"
						},						
						{
							name: 'resetButton',
							cls: 'ui.widget.ButtonView',
							y: resetButtonY,
							left: resetButtonLeft,
							anchorX: 156 / 2,
							anchorY: 117 / 2,
							width: 156,
							height: 117,
							images: {
								"up": "resources/images/braintunerx/MainScene/ResetUnpress.png",
								"down": "resources/images/braintunerx/MainScene/ResetPress.png"
							},
							scale: modeScale
						}						
					]					
				},
				// {
				// 	name: "shareFrame",
				// 	layout: 'linear',
				// 	direction: 'horizontal',
				// 	x: 46,
				// 	y: shareFrameLayoutY,
				// 	width: 530,
				// 	height: 160,
				// 	children: [
				// 		{
				// 			name: "shareLabel",
				// 			cls: 'ui.TextView',
				// 			fontFamily: 'Denne Kitten Heels',
				// 			size: shareTextSize,
				// 			left: 20,
				// 			width: 100,
				// 			height: 120,
				// 			autoFontSize: false,
				// 			text: "Share:"
				// 		},						
				// 		{
				// 			name: 'facebookButton',
				// 			cls: 'ui.widget.ButtonView',
				// 			y: shareButtonY,
				// 			left: shareFrameLeft,
				// 			anchorX: 148 / 2,
				// 			anchorY: 148 / 2,
				// 			width: 148,
				// 			height: 148,
				// 			images: {
				// 				"up": "resources/images/braintunerx/MainScene/Facebook.png",
				// 				"down": "resources/images/braintunerx/MainScene/FacebookPress.png"
				// 			},
				// 			scale: shareButtonScale
				// 		},
				// 		{
				// 			name: 'twitterButton',
				// 			cls: 'ui.widget.ButtonView',
				// 			y: shareButtonY,
				// 			left: shareFrameLeft,
				// 			anchorX: 148 / 2,
				// 			anchorY: 148 / 2,
				// 			width: 148,
				// 			height: 148,
				// 			images: {
				// 				"up": "resources/images/braintunerx/MainScene/Twitter.png",
				// 				"down": "resources/images/braintunerx/MainScene/TwitterPress.png"
				// 			},
				// 			scale: shareButtonScale
				// 		},
				// 		// {
				// 		// 	name: 'sendMailButton',
				// 		// 	cls: 'ui.widget.ButtonView',
				// 		// 	y: 0,
				// 		// 	left: shareFrameLeft,
				// 		// 	anchorX: 227 / 2,
				// 		// 	anchorY: 229 / 2,
				// 		// 	width: 227,
				// 		// 	height: 229,
				// 		// 	images: {
				// 		// 		"up": "resources/images/braintunerx/MainScene/SendEmailUnpress.png",
				// 		// 		"down": "resources/images/braintunerx/MainScene/SendEmailPress.png"
				// 		// 	},
				// 		// 	scale: shareButtonScale
				// 		// }																		
				// 	]					
				// },			
				{
					name: 'moreButton',
					cls: 'ui.widget.ButtonView',
					x: 30,
					y: FreelayoutY - 30,
					width: 190,
					height: 143,
					anchorX: 190 / 2,
					anchorY: 143 / 2,						
					images: {
						"up": "resources/images/braintunerx/MainScene/MoreButtonUnpress.png",
						"down": "resources/images/braintunerx/MainScene/MoreButtonPress.png"
					},
					scale: 0.7
				},
				{
					name: 'accountButton',	// leaderboard
					cls: 'ui.widget.ButtonView',
					x: 50,
					y: FreelayoutY + 115,
					width: 156,
					height: 131,
					anchorX: 156 / 2,
					anchorY: 131 / 2,						
					images: {
						"up": "resources/images/braintunerx/MainScene/HomeAccountUnpress.png",
						"down": "resources/images/braintunerx/MainScene/HomeAccountPress.png"
					},
					scale: 0.7
				},
				{
					name: 'settingButton',
					cls: 'ui.widget.ButtonView',
					x: 172,
					y: FreelayoutY + 120,
					width: 153,
					height: 127,
					anchorX: 153 / 2,
					anchorY: 127 / 2,						
					images: {
						"up": "resources/images/braintunerx/MainScene/HomeAboutUnpress.png",
						"down": "resources/images/braintunerx/MainScene/HomeAboutPress.png"
					},
					scale: 0.7
				},
				{
					name: 'hometext',
					cls: 'ui.ImageView',
					x: 185,
					y: FreelayoutY - 50,
					width: 317,
					height: 187,
					anchorX: 317 / 2,
					anchorY: 187 / 2,
					image: 'resources/images/braintunerx/MainScene/HomeLongText.png',
					scale: 0.9
				},
				{
					name: 'brainguy',
					cls: 'ui.ImageView',
					x: 400,
					y: FreelayoutY + 100,
					width: 320,
					height: 339,
					image: 'resources/images/braintunerx/MainScene/BrainGuy.png',
					scale: 0.5
				}					
			]
		},
		{
			name: "gamescreen",
			cls: 'src.view.home.GameScreen',
			x: 0,
			y: 0,
			width: BG_WIDTH,
			height: BG_HEIGHT,
			children: [
				{
				 	name: "bg",
				 	cls: "ui.View",
				 	backgroundColor: "#FFF",
				 	x: 0,
				 	y: 0,
				 	width: BG_WIDTH,
				 	height: BG_HEIGHT,
				 	children: [
				 		{
				 			name: "gameBoard",
				 			cls: "ui.View",
				 			x: 0,
				 			y: 141 * BG_WIDTH / 768,
				 			width: BG_WIDTH,
				 			height: BG_HEIGHT - (2 * 141 * BG_WIDTH / 768)
				 		}
				 	]
				},
				{
					name: "header",
					layout: "linear",
					direction: "horizontal",
					x: 0,
					y: 0,
					width: BG_WIDTH,
					height: 141 * BG_WIDTH / 768,
					children: [
						{
							name: "topbarbg",
							cls: "ui.ImageView",
							x: 0,
							y: 0,
							width: BG_WIDTH,
							height: 141 * BG_WIDTH / 768,
							image: 'resources/images/braintunerx/PlayScene/BottomBar_Ipad.png',
							inLayout: false
						},
						{
							name: "backBtn",
							cls: "ui.widget.ButtonView",
							left: 20,
							y: (141 * BG_WIDTH / 768 - 83) / 2,
							width: 84,
							height: 83,
							images: {
								"up": "resources/images/braintunerx/PlayScene/PlayBackUnpress.png",
								"down": "resources/images/braintunerx/PlayScene/PlayBackPress.png"
							}
						},
						{
							name: "HeaderText",
							cls: 'ui.TextView',
							fontFamily: 'Denne Kitten Heels',
							size: 60,
							left: 20,
							width: 330,
							height: 141 * BG_WIDTH / 768,
							color: "white",
							text: "Brain Tuner"
						},		
						{
							name: "ProgressText",
							cls: 'ui.TextView',
							fontFamily: 'Denne Kitten Heels',
							left: 0,
							width: BG_WIDTH - 84 - 350 - 60,
							height: 141 * BG_WIDTH / 768,
							color: "grey",
							text: "0 / 100"
						}										
					]			
				},
				{
					name: "footer",
					layout: "linear",
					direction: "horizontal",
					x: 0,
					y: BG_HEIGHT - 141 * BG_WIDTH / 768,
					width: BG_WIDTH,
					height: 141 * BG_WIDTH / 768,
					justifyContent: 'space-outside',
					children: [				
						{
							name: "bottombarbg",
							cls: "ui.ImageView",
							x: 0,
							y: 0,
							width: BG_WIDTH,
							height: 141 * BG_WIDTH / 768,
							image: 'resources/images/braintunerx/PlayScene/BottomBar_Ipad.png',
							inLayout: false
						},
						{
							name: "wrongBtn",
							cls: "ui.widget.ButtonView",
							y: (141 * BG_WIDTH / 768 - 118 * chosenButtonScale) / 2 - 10,
							width: 260 * chosenButtonScale,
							height: 118 * chosenButtonScale,
							images: {
								"up": "resources/images/braintunerx/PlayScene/PlayWrongUnpress.png",
								"down": "resources/images/braintunerx/PlayScene/PlayWrongPress.png"
							},
							on: {
								// up: GC.app.buttonClickNoSound,
								// down: GC.app.buttonClickNoSound
							}							
						},
						{
							name: "rightBtn",
							cls: "ui.widget.ButtonView",
							y: (141 * BG_WIDTH / 768 - 118 * chosenButtonScale) / 2 - 10,
							width: 260 * chosenButtonScale,
							height: 118 * chosenButtonScale,
							images: {
								"up": "resources/images/braintunerx/PlayScene/PlayRightUnpress.png",
								"down": "resources/images/braintunerx/PlayScene/PlayRightPress.png"
							},
							on: {
								// up: GC.app.buttonClickNoSound,
								// down: GC.app.buttonClickNoSound
							}							
						},						
					]			
				}						
			]
		},
		{
			name: "optionscreen",
			cls: 'src.view.home.OptionScreen',
			x: 0,
			y: 0,
			zIndex: 2,
			backgroundColor: "white",
			width: BG_WIDTH,
			height: BG_HEIGHT,
			children: [	
				{
					name: "header",
					cls: 'ui.View',
					layout: "linear",
					direction: "horizontal",
					justifyContent: "space-outside",
					backgroundColor: "#525252",
					x: 0,
					y: 0,
					width: BG_WIDTH,
					height: 80,
					children: [
						{
							name: "version",
							cls: 'ui.TextView',
							left: 0,
							width: BG_WIDTH / 6,
							color: 'white',
							fontWeight: 'bold',
							size: 40,
							height: 60,
							text: "v 3.1"
						},
						{
							name: "title",
							cls: 'ui.TextView',
							left: 0,
							width: BG_WIDTH * 3 / 5,
							fontWeight: 'bold',
							color: 'white',
							size: 40,
							height: 60,
							text: "Options"
						},
						{
							name: "done",
							cls: 'ui.TextView',
							left: 0,
							width: BG_WIDTH / 6,
							size: 36,
							height: 60,
							color: 'blue',
							text: "Done"
						},						
					]
				},
				{
					name: "body",
					layout: "linear",
					cls: 'ui.View',
					direction: "vertical",
					justifyContent: "space-outside",
					x: 0, 
					y: 120,
					width: BG_WIDTH,
					height: BG_HEIGHT - AdHeight - 140 - 120,
				},
				{
					name: "tip",
					cls: 'ui.TextView',
					width: BG_WIDTH,
					height: 140,
					size: 30,
					x: 0,
					y: BG_HEIGHT - AdHeight - 140,
					backgroundColor: "black",
					color: "white",
					wrap: "true",
					text: "Best Times are saved separately for some option combinations."
				},
				{
					name: "ads",
					cls: 'ui.View',
					width: BG_WIDTH,
					height: AdHeight,
					x: 0,
					y: BG_HEIGHT - AdHeight,
				}
			]		
		},
		{
			name: "leaderboard",
			cls: 'src.view.home.LeaderboardScreen',
			x: 0,
			y: 0,
			zIndex: 2,
			backgroundColor: "white",
			width: BG_WIDTH,
			height: BG_HEIGHT,
			children: [
				{
					name: "header",
					cls: 'ui.View',
					layout: "linear",
					direction: "horizontal",
					justifyContent: "space-outside",
					backgroundColor: "#525252",
					x: 0,
					y: 0,
					width: BG_WIDTH,
					height: 80,
					children: [
						{
							name: "close",
							cls: 'ui.TextView',
							left: 0,
							width: BG_WIDTH / 6,
							color: 'blue',
							fontWeight: 'bold',
							size: 40,
							height: 60,
							text: "Close"
						},
						{
							name: "title",
							cls: 'ui.TextView',
							left: 0,
							width: BG_WIDTH * 3 / 5,
							fontWeight: 'bold',
							color: 'white',
							size: 40,
							height: 60,
							text: "Score Board"
						},
						{
							name: "reload",
							cls: 'ui.TextView',
							left: 0,
							width: BG_WIDTH / 6,
							size: 36,
							height: 60,
							color: 'blue',
							text: "Reload"
						},						
					]
				},
				{
					name: "ads",
					cls: 'ui.View',
					width: BG_WIDTH,
					height: AdHeight,
					x: 0,
					y: BG_HEIGHT - AdHeight,
				}				
			]
		}		
	]
};

exports.quesview = {
				fontFamily: "Helvetica",
				fontWeight: "bold",
				x: 30,
				width: 576,
				horizontalAlign: "left",
			};