import animate;
import effects;
import ui.View as View;
import src.data.homeView as homeView;
import src.lib.uiInflater as uiInflater;

import nativeframework.admob as admob;
import nativeframework.parse as parse;

exports = Class(View, function(supr) {
	
	var imageEnabled = [];
	var imageDisabled = [];
	imageEnabled[0] = "resources/images/braintunerx/MainScene/Button20Unpress.png";
	imageDisabled[0] = "resources/images/braintunerx/MainScene/Button20Unpress.png";
	imageEnabled[1] = "resources/images/braintunerx/MainScene/Button60Unpress.png";
	imageDisabled[1] = "resources/images/braintunerx/MainScene/Button60Unpress.png";
	imageEnabled[2] = "resources/images/braintunerx/MainScene/Button100Unpress.png";
	imageDisabled[2] = "resources/images/braintunerx/MainScene/Button100Unpress.png";

	this.init = function(opts) {
		supr(this, 'init', [opts]);

		this.currentText = 0;
		this.canHandleInput = true;
		this.chosenMode = 0;	// 0: 20 questions,	1: 60 questions, 2: 100 questions
	};

	this.populateUI = function(homeView) {
		this.homeView = homeView;

		this.animateModeView();
		this.animateMoreView();

		
		this.modeLayout.button20.on('InputSelect', bind(this, function() {
			this.changeGameMode(0);
			this.updateBestScore();
			this.homeView.emit('startGame', this.chosenMode);
		}));

		this.modeLayout.button60.on('InputSelect', bind(this, function() {
			this.changeGameMode(1);
			this.updateBestScore();
			this.homeView.emit('startGame', this.chosenMode);
		}));

		this.modeLayout.button100.on('InputSelect', bind(this, function() {
			this.changeGameMode(2);
			this.updateBestScore();
			this.homeView.emit('startGame', this.chosenMode);
		}));

		this.moreButton.onClick = bind(this, function() {
			this.Adfrom = "more";
			this.showFullScreenAd(true);
		});

		this.accountButton.onClick = bind(this, function() {
			this.Adfrom = "account";
			this.showFullScreenAd(true);
		});

		this.bestScoreFrame.resetButton.on('InputSelect', bind(this, function() {
			this.resetBestScore();
		}));

		this.settingButton.onClick = bind(this, function() {
			console.log("Tiendv-Setting click");
			this.homeView.emit('settingclicked');
		});

		// Update Best Score
		this.updateBestScore();

		// Register callback
		// admob.registerHandler(this.admobPluginCB);
		admob.registerHandler(bind(this, function(e) {
			console.log("TiendvAds - close admob: " + e.eventID + ": " + this.Adfrom);
			if (e.eventID == 1 || e.eventID == 2) { // interstitial closed or not ready
				if (this.Adfrom == "account") {
					this.homeView.emit('scoreboardclicked');
				} else if (this.Adfrom == "more") {
					this.showWeebyGames();
				}
			}		
		}))

		// Show Banner
		this.showBanner(false);

		// Load Interestitial
		this.loadInterstitial();

		// send a parse test event
		parse.registerHandler(bind(this, function(e){
			console.log("receive rank: " + e.rank + " name: " + e.player_name + " problem: " + e.problems);
			this.homeView.RetrieveLeaderboard(e);
		}))
		parse.reloadData({problems: -1});
		parse.reloadData({problems: 20});
		parse.reloadData({problems: 60});
		parse.reloadData({problems: 100});
	};

	this.showWeebyGames = function() {
		admob.showWeebyGames();
	};

	this.showBanner = function(_reload) {
		admob.showAdView({adSize: "banner", horizontalAlign: "center", verticalAlign: "bottom", reload: _reload});
	};

	this.loadInterstitial = function() {
		admob.loadInterstitial({});
	};

	this.showFullScreenAd = function(_reload) {
		// admob.showAdView({adSize: "large_banner", reload: _reload});
		admob.showInterstitial();
	};

	this.hideBanner = function() {
		admob.hideAdView();
	};

	this.updateBestScore = function(){
		switch (this.chosenMode) {
			case 0:
				this.bestScoreFrame.bestScoreText.setText(this.homeView.app._settings.best20);
				break;
			case 1:
				this.bestScoreFrame.bestScoreText.setText(this.homeView.app._settings.best60);
				break;
			case 2:
				this.bestScoreFrame.bestScoreText.setText(this.homeView.app._settings.best100);
				break;
		};
	};

	this.resetBestScore = function() {
		switch (this.chosenMode) {
			case 0:
				this.homeView.app._settings.best20 = "None yet";
				break;
			case 1:
				this.homeView.app._settings.best60 = "None yet";
				break;
			case 2:
				this.homeView.app._settings.best100 = "None yet";
				break;
		};

		this.updateBestScore();
	};

	this.getActiveModeView = function() {
		if (this.chosenMode == 0)
			return this.modeLayout.button20;
		else if (this.chosenMode == 1)
			return this.modeLayout.button60;
		else 
			return this.modeLayout.button100;
	};

	this.animateModeView = function() {
		this._modeScale = this.getActiveModeView().style.scale;
		animate(this.getActiveModeView()).now({scale: this._modeScale * 1.2}, 250, animate.easeOut).then({scale: this._modeScale}, 250, animate.easeIn).then(bind(this, function() {
			this.animateModeView();
		}));
	};

	this.animateMoreView = function() {
		this._moreScale = this.moreButton.style.scale;
		animate(this.moreButton).now({r: Math.PI / 12}, 200, animate.easeOut)
								.then({r: 0}, 200, animate.easeIn)
								.then({r: -Math.PI / 12}, 200, animate.easeOut)
								.then({r: 0}, 200, animate.easeIn)
								.then({r: Math.PI / 12}, 200, animate.easeOut)
								.then({r: 0}, 200, animate.easeIn)
								.then({r: -Math.PI / 12}, 200, animate.easeOut)
								.then({r: 0}, 200, animate.easeIn)
								.then({scale: this._moreScale * 1.2}, 350, animate.easeOut)
								.then({scale: this._moreScale}, 350, animate.easeIn)
								.then({scale: this._moreScale * 1.2}, 350, animate.easeOut)
								.then({scale: this._moreScale}, 350, animate.easeIn)
								.wait(5000)
								.then(bind(this, function() {
									this.animateMoreView();
								}));
	};

	this.changeGameMode = function(newMode) {
		if (this.chosenMode == newMode)
			return;

		// Clear the current animation and reset it to disable 
		animate(this.getActiveModeView()).clear();
		this.getActiveModeView().setImage(imageDisabled[this.chosenMode]);
		this.getActiveModeView().style.scale = this._modeScale;

		// Assign newMode value to this.chosenMode
		this.chosenMode = newMode;

		// Start the animation
		this.getActiveModeView().setImage(imageEnabled[this.chosenMode]);
		this.animateModeView();
	};
});
