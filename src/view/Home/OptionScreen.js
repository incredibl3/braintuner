import animate;
import ui.View as View;
import ui.TextView as TextView;

var BG_WIDTH = 576;
var ITEMHEIGHT = 70;

var settings = [
	"Sound",
	"Countdown Timer",
	"Addition",
	"Subtraction",
	"Multiplication",
	"Division",
	"Review Answers"
]

var defaultVal = [
	1, 1, 1, 1, 1, 0, 0
]

var initialOpts = {
	x: 0, 
	y: 0,
	width: 576
}

var option;

exports = Class(View, function(supr) {
	this.init = function(opts) {
		option = this;
		supr(this, 'init', [opts]);

		for (var i = 0; i < settings.length; i++) {
		    if (NATIVE.localStorage) {
      			var val = NATIVE.localStorage.getItem(settings[i]);

      			if (val != null)
      				defaultVal[i] = val;
		    }
		}
	}

	this.populateUI = function(homeView) {
		this.homeView = homeView;

		this.header.done.on('InputSelect', bind(this, function() {
			this.header.done.updateOpts({color: 'white'});
			animate(this.header.done).wait(100).then(bind(this, function() {
				this.header.done.updateOpts({color: 'blue'});
				this.homeView.emit('settingclicked');
			}));
		}));

		this.ads.style.y = this.ads.style.y - (homeView.getRedundantY());
		this.tip.style.y = this.tip.style.y - (homeView.getRedundantY());
		this.body.style.height = this.body.style.height - (homeView.getRedundantY());

		var ITEMHEIGHT = this.body.style.height / settings.length;
		for (var i = 0; i < settings.length; i++) {
			itemViewOpts = merge({index: i, superview: this.body, y: ITEMHEIGHT * i, itemtext: settings[i], txtOnOff: defaultVal[i] == 1 ? "On" : "Off", height: ITEMHEIGHT, itemheight: ITEMHEIGHT}, initialOpts);
			var item = new ItemView(itemViewOpts);

		// 	item.onoff.on('InputSelect', bind(this, function() {
		// 		console.log("Tiendv ItemSelect");
		// 		defaultVal[this.index] = defaultVal[this.index] == 1 ? 0 : 1;
		// 		item.onoff.setText(defaultVal[this.index] == 1 ? "On" : "Off")
		// 	}));			
		}
	}

	this.save = function(index) {
		if (NATIVE.localStorage) {
			NATIVE.localStorage.setItem(settings[index], defaultVal[index]);
		}
	}

	this.getAllowedOperators = function() {
		var ret = [];

		for (var i = 2; i < 6; i++) {
			if (defaultVal[i] == 1) {
				ret[ret.length] = settings[i];
				console.log("operator: " + settings[i] + " is allowed, size: " + ret.length);
			}
		}

		return ret;
	}
})

var ItemView = Class(View, function(supr) {
	this.init = function(opts) {
		supr(this, 'init', [opts]);

		this.index = opts.index;
		this.buildView(opts);
	}

	this.buildView = function(opts) {
		this.textview = new TextView({
			superview: this,
			x: 10,
			y: 0,
			width: BG_WIDTH * 3 / 4,
			height: opts.itemheight,
			horizontalAlign: 'left',
			verticalAlign: 'top',
			fontWeight: 'bold',
			size: 30,
			text: opts.itemtext,
			color: 'black'
		});

		this.onoff = new TextView({
			superview: this,
			x: BG_WIDTH * 3 / 4,
			y: 0,
			size: 30,
			width: BG_WIDTH / 4,
			height: opts.itemheight,
			verticalAlign: 'top',
			horizontalAlign: 'center',
			text: opts.txtOnOff,
			color: 'black'
		});

		this.onoff.on('InputSelect', bind(this, function() {
			console.log("Tiendv ItemSelect");
			defaultVal[this.index] = defaultVal[this.index] == 1 ? 0 : 1;
			this.onoff.setText(defaultVal[this.index] == 1 ? "On" : "Off")

			option.save(this.index);
		}));
	}
});