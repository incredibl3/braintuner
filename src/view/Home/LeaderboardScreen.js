import ui.View as View;
import animate;
import ui.ScrollView as ScrollView;
import ui.TextView as TextView;
import src.lib.uiInflater as uiInflater;
import nativeframework.nativedialog as nativedialog;

var BG_WIDTH = 576;
var BG_HEIGHT = 1024;
var ITEMHEIGHT = 70;
var AdHeight = 100;
var ITEMHEIGHT = 80;

var leaderboard;

var bodyOpts =	{
				name: "body",
				layout: "linear",
				direction: "vertical",
				justifyContent: "space-outside",
				x: 0, 
				y: 80,
				width: BG_WIDTH,
				height: BG_HEIGHT,
				scrollX: false,
				scrollBounds: {
					minY: 0,
					maxY: BG_HEIGHT
				}
			};

var problemOpts = {
				x: 10,
				y: 0,
				width: BG_WIDTH - 10,
				height: ITEMHEIGHT,
				horizontalAlign: 'left',
				verticalAlign: 'bottom',
				handleEvents: false,
				ignoreSubviews: true,
				color: 'grey',
				size: 25,
				height: 60,
				text: "ALL-TIME HIGH SCORE"
				};

var LabelOpts = {
				x: 0,
				y: 0,
				width: BG_WIDTH,
				height: ITEMHEIGHT,
				handleEvents: false,
				ignoreSubviews: true,
				layout: "linear",
				direction: "horizontal",
				children: [
					{
						name: "rankLabel",
						cls: 'ui.TextView',
						left: 0,
						width: BG_WIDTH / 6,
						color: 'white',
						fontWeight: 'bold',
						size: 30,
						height: 80,
						horizontalAlign: 'center',
						verticalAlign: 'middle',
						text: "Rank"
					},
					{
						name: "nameLabel",
						cls: 'ui.TextView',
						left: 0,
						width: BG_WIDTH * 3 / 5,
						color: 'white',
						fontWeight: 'bold',
						size: 30,
						height: 80,
						horizontalAlign: 'center',
						verticalAlign: 'middle',
						text: "Name"
					},
					{
						name: "timeLabel",
						cls: 'ui.TextView',
						left: 0,
						width: BG_WIDTH / 6,
						fontWeight: 'bold',
						size: 30,
						height: 80,
						color: 'white',
						horizontalAlign: 'center',
						verticalAlign: 'middle',
						text: "Time"
					},						
				]
				};

var EntryOpts = {
				x: 0,
				y: 0,
				width: BG_WIDTH,
				height: ITEMHEIGHT,
				handleEvents: false,
				ignoreSubviews: true,				
				layout: "linear",
				direction: "horizontal",
				children: [
					{
						name: "rank",
						cls: 'ui.TextView',
						left: 0,
						width: BG_WIDTH / 6,
						color: 'black',
						size: 30,
						height: 80,
						horizontalAlign: 'center',
						verticalAlign: 'middle',
						text: "1"
					},
					{
						name: "player_name",
						cls: 'ui.TextView',
						left: 0,
						width: BG_WIDTH * 3 / 5,
						color: 'black',
						size: 30,
						height: 80,
						horizontalAlign: 'center',
						verticalAlign: 'middle',
						text: "Name"
					},
					{
						name: "score",
						cls: 'ui.TextView',
						left: 0,
						width: BG_WIDTH / 6,
						size: 30,
						height: 80,
						color: 'black',
						horizontalAlign: 'center',
						verticalAlign: 'middle',
						text: "0.000"
					},						
				]
				};				

exports = Class(View, function(supr) {
	this.init = function(opts) {
		leaderboard = this;
		supr(this, 'init', [opts]);
		this.setMaxListeners(600);
	}

	this.populateUI = function(homeView) {
		this.homeView = homeView;

		this.header.close.on('InputSelect', bind(this, function() {
			this.header.close.updateOpts({color: 'white'});
			animate(this.header.close).wait(100).then(bind(this, function() {
				this.header.close.updateOpts({color: 'blue'});
				this.homeView.emit('scoreboardclicked');
			}));
		}));

		this.header.reload.on('InputSelect', bind(this, function() {
			var msg = "Your data is up-to-date."
			this.header.reload.updateOpts({color: 'white'});
			animate(this.header.reload).wait(100).then(bind(this, function() {
				this.header.reload.updateOpts({color: 'blue'});
				nativedialog.showDialog({title: "", messages: [msg], buttons: ["Ok"]}, bind(this, function(e) {
					logger.log("{nativedialog} Got response:", e.buttonPressed);
				}));
			}))
		}));

		this.ads.style.y = this.ads.style.y - (homeView.getRedundantY());

		console.log("TIendv: " + bodyOpts.height);
		console.log("TIendv: " + homeView.getRedundantY());
		newOpts = merge(
			{ superview: this, height: bodyOpts.height - homeView.getRedundantY(), 
				scrollBounds: merge({ maxY: bodyOpts.scrollBounds.maxY - homeView.getRedundantY()}, bodyOpts.scrollBounds)
			}, bodyOpts);
		this.body = new Body(newOpts);
	}

	this.reloadData = function() {
		this.body.reloadData();
	}
});

var Body = Class(ScrollView, function(supr) {
	this.init = function(opts) {
		supr(this, 'init', [opts]);

		this.setMaxListeners(600);
		this.buildView();
	};

	this.buildView = function() {
		this.currentIndex = 0;
		this.removeAllSubviews();

		// All Time leaderboard
		this.allTimeLabel = new TextView(merge({superview: this}, problemOpts));
		this.currentIndex++;

		this.allTimeLabelView = new View(merge({
				superview: this, y: (this.currentIndex * ITEMHEIGHT), backgroundColor: 'green'}, LabelOpts));
		uiInflater.addChildren(LabelOpts.children, this.allTimeLabelView);
		this.currentIndex++;

		// 20 problems leaderboard
		this.twentyProbLabel = new TextView(merge({superview: this, y: this.currentIndex * ITEMHEIGHT}, problemOpts));
		this.twentyProbLabel.setText("20 PROBLEMS");
		this.twentyProbLabel.offsetY = this.twentyProbLabel.style.y;
		this.currentIndex++;

		this.twentyProbLabelView = new View(merge({
				superview: this, y: (this.currentIndex * ITEMHEIGHT), backgroundColor: 'green'}, LabelOpts));
		this.twentyProbLabelView.offsetY = this.twentyProbLabelView.style.y;
		uiInflater.addChildren(LabelOpts.children, this.twentyProbLabelView);
		this.currentIndex++;

		// 60 problems leaderboard
		this.sixtyProbLabel = new TextView(merge({superview: this, y: this.currentIndex * ITEMHEIGHT}, problemOpts));
		this.sixtyProbLabel.setText("60 PROBLEMS");
		this.sixtyProbLabel.offsetY = this.sixtyProbLabel.style.y;
		this.currentIndex++;

		this.sixtyProbLabelView = new View(merge({
				superview: this, y: (this.currentIndex * ITEMHEIGHT), backgroundColor: 'green'}, LabelOpts));
		this.sixtyProbLabelView.offsetY = this.sixtyProbLabelView.style.y;
		uiInflater.addChildren(LabelOpts.children, this.sixtyProbLabelView);
		this.currentIndex++;

		// 100 problems leaderboard
		this.hundredProbLabel = new TextView(merge({superview: this, y: this.currentIndex * ITEMHEIGHT}, problemOpts));
		this.hundredProbLabel.setText("100 PROBLEMS");
		this.hundredProbLabel.offsetY = this.hundredProbLabel.style.y;
		this.currentIndex++;

		this.hundredProbLabelView = new View(merge({
				superview: this, y: (this.currentIndex * ITEMHEIGHT), backgroundColor: 'green'}, LabelOpts));
		this.hundredProbLabelView.offsetY = this.hundredProbLabelView.style.y;
		uiInflater.addChildren(LabelOpts.children, this.hundredProbLabelView);
		this.currentIndex++;
	};

	this.allTimeList = [];
	this.twentyList = [];
	this.sixtyList = [];
	this.hundredList = [];
	this.updateBound = 0;
	this.reloadData = function() {
		var totalEntry = 0;
		var lb = leaderboard.homeView.getLeaderboard(20);
		for (var i = 1; i < lb.length; i++) {
			if (this.allTimeList[i]) {
				this.allTimeList[i].player_name.setText(lb[i].player_name);
				this.allTimeList[i].score.setText(lb[i].score);
				this.allTimeList[i].style.y = this.allTimeLabelView.style.y + i * ITEMHEIGHT;
			} else {
				var entry = new View(merge({
						superview: this, y: (this.allTimeLabelView.style.y + this.allTimeLabelView.style.height + (i-1) * ITEMHEIGHT), backgroundColor: 'white'}, EntryOpts));
				uiInflater.addChildren(EntryOpts.children, entry);
				totalEntry++; this.updateBound++;

				console.log("lb[i].score: " + lb[i].score + " of: " + lb[i].player_name);
				entry.rank.setText(i);
				entry.player_name.setText(lb[i].player_name);
				entry.score.setText(lb[i].score);
				this.allTimeList[i] = entry;
			}
		}
		this.twentyProbLabel.style.y = this.twentyProbLabel.style.y + totalEntry * ITEMHEIGHT;
		this.twentyProbLabelView.style.y = this.twentyProbLabelView.style.y + totalEntry * ITEMHEIGHT;

		// totalEntry = 0;
		var lb = leaderboard.homeView.getLeaderboard(20);
		for (var i = 1; i < lb.length; i++) {
			if (this.twentyList[i]) {
				this.twentyList[i].player_name.setText(lb[i].player_name);
				this.twentyList[i].score.setText(lb[i].score);
				this.twentyList[i].style.y = this.twentyProbLabelView.style.y + i * ITEMHEIGHT;
			} else {
				var entry = new View(merge({
						superview: this, y: (this.twentyProbLabelView.style.y + i * ITEMHEIGHT), backgroundColor: 'white'}, EntryOpts));
				uiInflater.addChildren(EntryOpts.children, entry);
				totalEntry++; this.updateBound++;

				console.log("lb[i].score: " + lb[i].score + " of: " + lb[i].player_name);
				entry.rank.setText(i);
				entry.player_name.setText(lb[i].player_name);
				entry.score.setText(lb[i].score);
				this.twentyList[i] = entry;
			}
		}
		this.sixtyProbLabel.style.y = this.sixtyProbLabel.style.y + totalEntry * ITEMHEIGHT;
		this.sixtyProbLabelView.style.y = this.sixtyProbLabelView.style.y + totalEntry * ITEMHEIGHT;

		// totalEntry = 0;
		var lb = leaderboard.homeView.getLeaderboard(60);
		for (var i = 1; i < lb.length; i++) {
			if (this.sixtyList[i]) {
				this.sixtyList[i].player_name.setText(lb[i].player_name);
				this.sixtyList[i].score.setText(lb[i].score);
				this.sixtyList[i].style.y = this.sixtyProbLabelView.style.y + i * ITEMHEIGHT;
			} else {
				var entry = new View(merge({
						superview: this, y: (this.sixtyProbLabelView.style.y + i * ITEMHEIGHT), backgroundColor: 'white'}, EntryOpts));
				uiInflater.addChildren(EntryOpts.children, entry);
				totalEntry++; this.updateBound++;

				console.log("lb[i].score: " + lb[i].score + " of: " + lb[i].player_name);
				entry.rank.setText(i);
				entry.player_name.setText(lb[i].player_name);
				entry.score.setText(lb[i].score);
				this.sixtyList[i] = entry;
			}
		}
		this.hundredProbLabel.style.y = this.hundredProbLabel.style.y + totalEntry * ITEMHEIGHT;
		this.hundredProbLabelView.style.y = this.hundredProbLabelView.style.y + totalEntry * ITEMHEIGHT;

		// totalEntry = 0;
		var lb = leaderboard.homeView.getLeaderboard(100);
		for (var i = 1; i < lb.length; i++) {
			if (this.hundredList[i]) {
				this.hundredList[i].player_name.setText(lb[i].player_name);
				this.hundredList[i].score.setText(lb[i].score);
				this.hundredList[i].style.y = this.hundredProbLabelView.style.y + i * ITEMHEIGHT;
			} else {
				var entry = new View(merge({
						superview: this, y: (this.hundredProbLabelView.style.y + i * ITEMHEIGHT), backgroundColor: 'white'}, EntryOpts));
				uiInflater.addChildren(EntryOpts.children, entry);
				totalEntry++; this.updateBound++;

				console.log("lb[i].score: " + lb[i].score + " of: " + lb[i].player_name);
				entry.rank.setText(i);
				entry.player_name.setText(lb[i].player_name);
				entry.score.setText(lb[i].score);
				this.hundredList[i] = entry;
			}
		}

		this.updateOpts({scrollBounds:{minY: 0, maxY: BG_HEIGHT + this.updateBound * ITEMHEIGHT}});
	};
});