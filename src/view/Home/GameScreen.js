import animate;
import effects;
import ui.View as View;
import ui.ViewPool as ViewPool;
import ui.TextView as TextView;
import ui.ImageView as ImageView;
import src.data.homeView as homeView;
import src.lib.uiInflater as uiInflater;

// plugin import
import nativeframework.nativedialog as nativedialog;

var floor = Math.floor;
var random = Math.random;
var rollFloat = function (mn, mx) { return mn + random() * (mx - mn); };
var rollInt = function (mn, mx) { return floor(mn + random() * (1 + mx - mn)); };

var min = -10;
var max = 100;
var QUESTION_HEIGHT = 100;
var BG_WIDTH = 576;
var quesviewOpts = homeView.quesview;
var gamescreen;

exports = Class(View, function(supr) {
	var numOfQuestion = [20, 60, 100];
	var gameboard;
	var operatorText = [];
	var OFFSETY;
	operatorText["Addition"] = "+";
	operatorText["Subtraction"] = "-";
	operatorText["Multiplication"] = "*";
	operatorText["Division"] = "/";

	this.init = function(opts) {
		gamescreen = this;
		supr(this, 'init', [opts]);
	};

	this.populateUI = function(homeView) {
		this.homeView = homeView;

		this.footer.style.y -= this.homeView.importantY;
		
		gameboard = this.bg.gameBoard;
		gameboard.style.height -= this.homeView.importantY;

		this.equationHeight = gameboard.style.height / 6;
		OFFSETY = this.equationHeight * 3;

		this.selectedBG = new ImageView({
			superview: gameboard,
			x: 10,
			y: OFFSETY + this.equationHeight * 0.25 - 0.5 * 112 * 556 / 616,
			width: 556,
			height: 112 * 556 / 616,
			image: 'resources/images/braintunerx/PlayScene/PlaySelectionBar.png'
		})

		this.quesViewPool = new ViewPool({
			ctor: QuesView
		});

		this.footer.wrongBtn.onClick = bind(this, function() {
			if (this.currentQuestion < numOfQuestion[this.gamemode]) {
				this.answerQuestion('wrong');
			}
		});

		this.footer.rightBtn.onClick = bind(this, function() {
			if (this.currentQuestion < numOfQuestion[this.gamemode]) {
				this.answerQuestion('right');
			}
		});

		this.header.backBtn.onClick = bind(this, function() {
			this.quesViewPool.releaseAllViews();
			this.homeView.emit('leftGame');
		});
	};

	this.generateLevel =function(opts) {
		this.gamemode = opts.mode || 0;
		this.allowOperator = opts.operator || [];
		
		var progressText = "0 / " + numOfQuestion[this.gamemode];
		this.header.ProgressText.setText(progressText);
		this.correctAnswer = 0;
		this.totalTime = 0;

		for (var i = 0; i < numOfQuestion[this.gamemode]; i++) {
			// Is this question true or false ?
			var bool = rollInt(0, 1) == 1;

			// Random the operator
			var operatorInt = rollInt(0, opts.operator.length - 1);
			var operatorStr = opts.operator[operatorInt];

			// Random left operand
			var isMultiplication = operatorStr == "multiplication";
			var leftOperand = isMultiplication ? rollInt(1, 10) : rollInt(0, 10);
			// Random right operand
			var isDivision = operatorStr == "division";
			var rightOperand = isDivision || isMultiplication ? rollInt(1, 10) : rollInt(0, 10);
			var result = 0;
			switch (operatorStr) {
				case "Addition":
					{
						if (bool) 
							result = leftOperand + rightOperand;
						else {
							var done = false;
							while (!done) {
								result = rollInt(leftOperand + rightOperand - 5, leftOperand + rightOperand + 5);

								done = result != (leftOperand + rightOperand);
							}
						}
						break;
					}
				case "Subtraction":
					{
						if (bool) 
							result = leftOperand - rightOperand;
						else {
							var done = false;
							while (!done) {
								result = rollInt(leftOperand - rightOperand - 5, leftOperand - rightOperand + 5);

								done = result != (leftOperand - rightOperand);
							}
						}
						break;
					}				
				case "Multiplication":
					{
						if (bool) 
							result = leftOperand * rightOperand;
						else {
							var done = false;
							while (!done) {
								result = rollInt(leftOperand * rightOperand - 5, leftOperand * rightOperand + 5);

								done = result != (leftOperand * rightOperand);
							}
						}
						break;
					}		
				case "Division":
					{
						if (bool) 
							result = leftOperand - rightOperand;
						else {
							var done = false;
							while (!done) {
								result = rollInt(leftOperand - rightOperand - 5, leftOperand - rightOperand + 5);

								done = result != (leftOperand - rightOperand);
							}
						}
						break;
					}															
			}

			var _text = leftOperand + " " + operatorText[operatorStr] + " " + rightOperand + " = " + result;
			var quesview = this.quesViewPool.obtainView(merge({
				superview: gameboard,
				y: OFFSETY + i * this.equationHeight,
				height: this.equationHeight * 0.5,
			}, quesviewOpts));
			quesview.createView({text: _text, correct: bool, num: i});
		}
	};

	this.startGame = function(opts) {
		this._time = Date.now();
		console.log("start game with mode: " + opts.mode + " at: " + this._time);
		this.generateLevel(opts);
		this.currentQuestion = 0;
	};

	this.getQuestionAt = function(num) {
		var view;
		this.quesViewPool.forEachActiveView(function(child, i) {
			if (child.number == num) {
				view = child;		
			}
		}, this);

		return view;
	};

	this.answerQuestion = function(answer) {
		var correct;
		var shouldEndGame = false;
		if (this.getQuestionAt(this.currentQuestion).isCorrect) {
			this.homeView.playRight();
			correct = 'right';	
		} else {
			this.homeView.playWrong();
			correct = 'wrong'; 
		}

		if (correct == answer) {
			this.correctAnswer++;
			var progressText = this.correctAnswer + " / " + numOfQuestion[this.gamemode];
			this.header.ProgressText.setText(progressText);			
		}

		this.quesViewPool.forEachActiveView(function(view, i) {
			if (view.number == this.currentQuestion)
				view.endAnswer(correct == answer);

			if (this.currentQuestion == numOfQuestion[this.gamemode] - 1) {
				shouldEndGame = true;
			} else {
				view.MoveUp(this.equationHeight);
			}
		}, this);
		this.currentQuestion++;

		if (shouldEndGame) {
			// Calculate score
			var _time = ((Date.now() - this._time) / 1000).toFixed(2);
			var _penalty = (numOfQuestion[this.gamemode] - this.correctAnswer) * 5;
			var _totalTime = parseFloat(_time) + parseFloat(_penalty);
			var line1 = _time + " sec. + " + _penalty + " penalty =";
			var line2 = _totalTime.toFixed(2) + " seconds.";
			var Accuracy = Math.round((this.correctAnswer / numOfQuestion[this.gamemode])* 100);
			var line3 = Accuracy + "% Accuracy.";

			var _title = "Not Very Good";
			var line4 = "Get your brain in-tune by trying again!";
			if (Accuracy >= 50 && Accuracy < 75) {
				_title = "Good Job";
				line4 = "That's a good time! Keep improving!";
			}
			else if (Accuracy >= 75 && Accuracy < 100) {
				_title = "Great Speed";		
				line4 = "Nice Job!"				
			} else if (Accuracy == 100) {
				_title = "Congratulations";
				line4 = "You're Amazing!"
			}

			this.totalTime = _totalTime.toFixed(2)
			this.checkBestScore(this.totalTime);

			this.homeView.playComplete();

			nativedialog.showDialog({title: _title, messages: [line1, line2, line3, line4], buttons: ["Share Facebook", "Submit Score", "Ok"]}, bind(this, function(e) {
				logger.log("{nativedialog} Got response:", e.buttonPressed);
				this.quesViewPool.releaseAllViews();

				if (e.buttonPressed == 0) { // Share Facebook button
					this.homeView.shareFacebook(this.gamemode, this.totalTime, line4);
					this.homeView.emit('endGame');
				} else if (e.buttonPressed == 1) { // submit button
					this.homeView.emit('endGame');
				} else {	// OK Button
					this.homeView.emit('endGame');
				}
			}));
		}
	};

	this.checkBestScore = function(score) {
		switch (this.gamemode) {
			case 0:
				if (this.homeView.app._settings.best20 == "None yet" || parseFloat(score) < parseFloat(this.homeView.app._settings.best20)) {
					NATIVE.localStorage.setItem("best20", score);
					this.homeView.app._settings.best20 = score;
					console.log("Tiendv - set best20: " + this.homeView.app._settings.best20);
				}
				break;
			case 1:
				if (this.homeView.app._settings.best60 == "None yet" || parseFloat(score) < parseFloat(this.homeView.app._settings.best60)) {
					NATIVE.localStorage.setItem("best60", score);
					this.homeView.app._settings.best20 = score;
					console.log("Tiendv - set best60: " + this.homeView.app._settings.best60);
				}
				break;
			case 2:
				if (this.homeView.app._settings.best100 == "None yet" || parseFloat(score) < parseFloat(this.homeView.app._settings.best100)) {
					NATIVE.localStorage.setItem("best100", score);
					this.homeView.app._settings.best20 = score;
					console.log("Tiendv - set best100: " + this.homeView.app._settings.best100);
				}
				break;
		}

		this.homeView.updateBestScore();
	};
});

var QuesView = exports.QuesView = Class(View, function () {
	var sup = View.prototype;

	this.init = function (opts) {
		sup.init.call(this, opts);

		this.targetY = opts.y;
		
		this.textView = new TextView({
			superview: this,
			x: 30,
			y: 0,
			width: 576,
			height: opts.height,
			text: "",
			horizontalAlign: opts.horizontalAlign
		});

		this.rightView = new ImageView({
			superview: this,
			x: 450,
			y: (this.style.height - 41) / 2,
			width: 40,
			height: 41,
			anchorX: 0,
			anchorY: 41,
			image: 'resources/images/braintunerx/PlayScene/PlayRightSymbol.png'
		});
		this.rightView.style.visible = false;

		this.wrongView = new ImageView({
			superview: this,
			x: 450,
			y: (this.style.height - 42) / 2,
			width: 37,
			height: 42,
			anchorX: 0,
			anchorY: 42,
			image: 'resources/images/braintunerx/PlayScene/PlayWrongSymbol.png'
		});
		this.wrongView.style.visible = false;

	};

	this.updateOpts = function(opts) {
		sup.updateOpts.call(this, opts);
		this.targetY = opts.y;
	}

	this.createView = function(opts) {
		this.textView.setText(opts.text);
		this.isCorrect = opts.correct;
		this.number = opts.num;
		this.rightView.style.visible = false;
		this.wrongView.style.visible = false;
	};

	this.endAnswer = function(correct) {
		if (correct) {
			this.rightView.style.visible = true;
			animate(this.rightView).now({scale: 1.5}, 200).then({scale: 1}, 200).then(bind(this, function() {
				// effects.spin(this.rightView, {duration: 200});
				animate(this.rightView).now({r: Math.PI / 12}, 100).then({r: -Math.PI/12}, 100).then({r: 0}, 100);
			}));
		} else {
			this.wrongView.style.visible = true;
			animate(this.wrongView).now({scale: 2}, 200).then({scale: 1}, 200).then(bind(this, function() {
				// effects.spin(this.wrongView, {duration: 200});
				animate(this.wrongView).now({r: Math.PI / 18}, 50).then({r: -Math.PI/18}, 50).then({r: 0}, 50);
			}));			
		}
	};

	this.MoveUp = function(deltaY) {
		this.targetY -= deltaY;
		animate(this).now({y: this.targetY}, 300).then(bind(this, function() {
			if (this.targetY < 0) {
				gamescreen.quesViewPool.releaseView(this);
			}
		}));
	};
});