import animate;
import ui.View as View;
import src.data.homeView as homeView;
import src.lib.uiInflater as uiInflater;
import facebook;

// plugin import
import nativeframework.nativedialog as nativedialog;

var BG_HEIGHT = 1024;
exports = Class(View, function(supr) {
	var mainmenu,
		gamescreen,
		optionscreen,
		leaderboardscreen;

	this.alltimeLb = [];
	this.twentyLb = [];
	this.sixtyLb = [];
	this.hundredLb = [];
	this.Lb = [this.alltimeLb, this.twentyLb, this.sixtyLb, this.hundredLb];

	this.init = function(opts) {
		this.importantY = opts.y;		
		this.app = opts.app;	

		supr(this, 'init', [merge(opts, homeView)]);
		uiInflater.addChildren(homeView.children, this);

		this.getSubviews().forEach(bind(this, function(child, i) {
			child.populateUI && child.populateUI(this);
		}));

		mainmenu = this.mainmenu;
		mainmenu.style.visible = true;

		gamescreen = this.gamescreen;
		gamescreen.style.visible = false;

		optionscreen = this.optionscreen;
		optionscreen.style.visible = false;
		optionscreen.style.y = BG_HEIGHT;

		leaderboardscreen = this.leaderboard;
		leaderboardscreen.style.visible = false;
		leaderboardscreen.style.y = BG_HEIGHT;		

		this.on('startGame', bind(this, function(chosenmode) {
			// build operators
			var allowedOperator = optionscreen.getAllowedOperators();
			if (allowedOperator.length == 0) {
				var _title = "Error";
				var msg1 = "You haven't chosen any operator!";
				var msg2 = "Please choose at least 1 in Options";
				console.log("Tiendv showDialog");
				nativedialog.showDialog({title: _title, messages: [msg1, msg2], buttons: ["OK", "Cancel"]}, bind(this, function(e) {
					if (e.buttonPressed == 0) { 
						this.emit('settingclicked');
					} else if (e.buttonPressed == 1) {
						// Nothing happens
					} 
				}));
			} else {
				mainmenu.style.visible = false;

				// Hide banner
				mainmenu.hideBanner();

				gamescreen.style.visible = true;
				gamescreen.startGame({
					mode: chosenmode,
					operator: allowedOperator
				});
			}
		}));

		this.on('endGame', function(pressed) {
			mainmenu.style.visible = true;
			gamescreen.style.visible = false;
			mainmenu.showBanner(true);
		});

		this.on('leftGame', function() {
			mainmenu.style.visible = true;
			gamescreen.style.visible = false;
			mainmenu.showBanner(true);
		});

		this.on('settingclicked', function() {
			console.log("setting click");
			mainmenu.hideBanner();
			if (optionscreen.style.visible == false) {
				console.log("Tiendv-click not visible");
				optionscreen.style.visible = true;
				animate(optionscreen).now({y: 0}, 600, animate.easeIn).then(function() {
					mainmenu.showBanner();
				});
			}
			else{
				console.log("Tiendv-click is visible");
				animate(optionscreen).now({y: BG_HEIGHT}, 600, animate.easeOut).then(function() {
					optionscreen.style.visible = false;
					mainmenu.showBanner();
				});
			}
		});

		this.on('scoreboardclicked', function() {
			console.log("scoreboard click");
			mainmenu.hideBanner();
			// leaderboardscreen.reloadData();
			if (leaderboardscreen.style.visible == false) {
				console.log("Tiendv-click not visible");
				leaderboardscreen.style.visible = true;
				animate(leaderboardscreen).now({y: 0}, 600, animate.easeIn).then(function() {
					mainmenu.showBanner();
				});
			}
			else{
				console.log("Tiendv-click is visible");
				animate(leaderboardscreen).now({y: BG_HEIGHT}, 600, animate.easeOut).then(function() {
					leaderboardscreen.style.visible = false;
					mainmenu.showBanner();
				});
			}
		});
	};

	this.playWrong = function() {
		this.app.playWrong();
	};

	this.playRight = function() {
		this.app.playRight();
	};

	this.playComplete = function() {
		this.app.playComplete();
	};

	this.onStartButtonClick = function() {
		animate(this).wait(800).then(bind(this, function() {
			this.onEnterChooseMode();
		}));
	};

	this.transitionToScreen = function() {

	};

	this.updateBestScore = function() {
		mainmenu.updateBestScore();
	};

	this.shareFacebook = function(mode, time, text) {
		facebook.api('/me/feed', 'post', { message: text }, function(response) {
		  if (!response || response.error) {
		    console.log(JSON.stringify(response));
		  } else {
		    console.log('Tiendv - Post ID: ' + response.id);
		  }
		});
	};

	this.getRedundantY = function() {
		console.log("Tiendv getRedundantY: " + this.style.y);
		return this.style.y;
	};

	this.RetrieveLeaderboard = function(e) {
		var lb = this.getLeaderboard(e.problems);
		console.log("Add " + e.rank + " of problems " + e.problems + " name " + e.player_name + " score " + e.score);
		lb[e.rank] = {player_name: e.player_name, score: e.score.toFixed(3)};
		leaderboardscreen.reloadData();
	};

	this.getLeaderboard = function(problems) {
		var lb = this.alltimeLb;
		switch (problems) {
			case 0:
				lb = this.alltimeLb;
				break;
			case 20:
				lb = this.twentyLb;
				break;
			case 60:
				lb = this.sixtyLb;
				break;
			case 100:
				lb = this.hundredLb;
				break;
		} 
		return lb;
	}
});
