exports.resolve = function() {
	return [
		'.HomeView',
		".MainMenu",
		".GameScreen",
		'.OptionScreen',
		'.LeaderboardScreen'
	];
};
