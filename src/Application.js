import device;
import AudioManager;
import ui.View as View;
import src.view.home.HomeView as HomeView;
import src.lib.FacebookController as FacebookController;
import facebook;
import event.Emitter as Emitter;

var BG_WIDTH = 576;
var BG_HEIGHT = 1024;
var emitter = new Emitter();
emitter.setMaxListeners(600);

exports = Class(GC.Application, function () {

  this.initUI = function () {
    this._settings = {
      showFPS: false,
      clearEachFrame: true,
      alwaysRepaint: true,
      preload: [
        // "resources/sounds/fx",
        "resources/images",
        "resources/images/home"
      ],
      best20: "None yet",
      best60: "None yet",
      best100: "None yet",
    };
    
    this.setScreenDimensions(BG_WIDTH > BG_HEIGHT);

    // Device's back button
    if (GLOBAL.NATIVE) {
      GLOBAL.NATIVE.onBackButton = bind(this, function() {
        logger.log ("onBackButton clicked!!!");
        GLOBAL.NATIVE.sendActivityToBack();
      });
    };

    facebook.onReady.run(function() {
      facebook.init({
        appId: CONFIG.modules.facebook.facebookAppID,
        displayName: CONFIG.modules.facebook.facebookDisplayName,
        // other config
      }); 

      facebook.getLoginStatus(FacebookController.GetLoginStatusCB);  
    });

    // blocks player input to avoid traversing game elements' view hierarchy
    this.bgLayer = new View({
      parent: this.view,
      y: this.view.style.height - BG_HEIGHT,
      width: BG_WIDTH,
      height: BG_HEIGHT
    });

    this.buildSound();
  };

  this.buildSound = function() {
    this._sound = new AudioManager({
      path: "resources/audio/",
      files: {
        button: {
          volume: 0.5
        },
        buttonclick: {
          volume: 0.5
        },
        completegame: {
          volume: 0.5
        },
        countdown: {
          volume: 0.5
        },
        right: {
          volume: 0.5
        },          
        wrong: {
          volume: 0.5
        }
      }
    }); 

    this.buttonClick = bind(this, function() {
      this._sound.play('buttonclick');
    });        
  };

  this.playButtonClick = function() {
    this._sound.play("buttonclick");
  };

  this.playRight = function() {
    this._sound.play("right");
  };

  this.playWrong = function() {
    this._sound.play("wrong");
  }; 

  this.playComplete = function() {
    this._sound.play("completegame");
  } 

  this.launchUI = function () {
    // Retrive Best Score
    this._settings.best20 = this.retrieveBestScore(["best20"]);
    this._settings.best60 = this.retrieveBestScore(["best60"]);
    this._settings.best100 = this.retrieveBestScore(["best100"]);
  
    this.homeView = new HomeView({
      parent: this.bgLayer,
      y: BG_HEIGHT - this.view.style.height,
      app: this 
    });    
  };

  this.retrieveBestScore = function(mode) {
    var best = "None Yet";
    if (NATIVE.localStorage)
      best = NATIVE.localStorage.getItem(mode);

    if (best != null) 
      return best;
    
    return "None yet";
  };

  /**
   * setScreenDimensions
   * ~ normalizes the game's root view to fit any device screen
   */
  this.setScreenDimensions = function(horz) {
    var ds = device.screen;
    console.log("device screen: " + ds.width + " " + ds.height);
    var vs = this.view.style;
    vs.width = horz ? ds.width * (BG_HEIGHT / ds.height) : BG_WIDTH;
    vs.height = horz ? BG_HEIGHT : ds.height * (BG_WIDTH / ds.width);
    vs.scale = horz ? ds.height / BG_HEIGHT : ds.width / BG_WIDTH;
  };

});
