//
//  NSString+URLEncoding.h
//  BrainTunerX
//
//  Created by Leon Yuu on 10/24/12.
//
//

#import <Foundation/Foundation.h>
@interface NSString (URLEncoding)
-(NSString *)urlEncodeUsingEncoding:(NSStringEncoding)encoding;
@end
