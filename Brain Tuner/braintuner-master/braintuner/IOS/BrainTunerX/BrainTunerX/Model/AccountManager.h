//
//  AccountManager.h
//  BrainTuner2
//
//  Created by silvercast on 5/18/11.
//  Copyright 2011 GreenGar studios. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <Facebook-iOS-SDK/FacebookSDK/FacebookSDK.h>
#import <Parse/Parse.h>
#import <ParseFacebookUtils/PFFacebookUtils.h>
#import "SA_OAuthTwitterEngine.h"
#import "SA_OAuthTwitterController.h"
#import "AppDelegate.h"
#import "GameManager.h"
#import "AccountManagerDelegate.h"

#define kTwitterControllerCanceled @"kTwitterControllerCanceled"

typedef void (^GSResultBlock)(BOOL succeed, NSError *error);

@interface AccountManager : NSObject <SA_OAuthTwitterControllerDelegate, UIAlertViewDelegate>
{
    BOOL                    shouldBroadcastAfterFacebookAuthorization;
    BOOL                    shouldGetUserInfoAfterFacebookAuthorization;
    
	SA_OAuthTwitterEngine	*twitterManager;
    
    UIViewController        *backViewController;
    
    UIAlertView             *loadingAlertView;
}

+ (AccountManager *)sharedManager;
@property (nonatomic,retain) id<AccountManagerDelegate> delegate;

- (void)connectWithFacebookFinished:(GSResultBlock)block;
- (void)registerWithFirstname:(NSString *)firstname lastname:(NSString *)lastname
                        email:(NSString *)email password:(NSString *)password finished:(GSResultBlock)block;
- (void)signInWithEmail:(NSString *)email password:(NSString *)password finished:(GSResultBlock)block;
- (void)signOut;

- (void)showForgotPasswordAlert;

- (void)broadcastOnFacebook:(NSString*)caption;
- (void)broadcastOnTwitter:(NSString*)caption;

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation;
- (void)signOutTwitter;

- (void)showLoadingAlertwithTitle:(NSString *)title andMessage:(NSString*)message;
- (void)dismissLoadingAlert;

- (BOOL)checkInternetConnection;

@end
