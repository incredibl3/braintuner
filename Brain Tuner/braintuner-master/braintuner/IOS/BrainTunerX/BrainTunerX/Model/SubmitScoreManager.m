//
//  SubmitScoreManager.m
//  BrainTunerX
//
//  Created by Leon Yuu on 10/11/12.
//
//

#import "SubmitScoreManager.h"
#import <Parse/Parse.h>

@interface SubmitScoreManager()
@property (nonatomic) BOOL locationLoadingTimeOut;
@property (nonatomic) BOOL scoreSubmitted;
@end

@implementation SubmitScoreManager

- (id)init
{
    if (self = [super init])
    {
        self.locationLoadingTimeOut = NO;
        self.scoreSubmitted = NO;
    }
    return self;
}

- (void)submitGlobalHighScorewithScore:(CFTimeInterval)score
                              accuracy:(int)accuracy
                         totalProblems:(int)totalProblems
                                userID:(NSString *)userID
                             userEmail:(NSString *)userEmail
{
    DLog(@"Score %f - UserID %@",score,userID);
    
    NSString *name = userID;
    NSString *email = userEmail;
    
    #if (__IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_6_0)
        NSString *uid = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
    #else
        NSString *uid = [[UIDevice currentDevice] uniqueIdentifier];
    #endif
    
    
    NSString *os = @"iOS";
    NSString *osVersion = [[UIDevice currentDevice] systemVersion];
    NSNumber *finishTime = @(score);
    NSNumber *accuracyDat = @(accuracy);
    NSNumber *numProblems = @(totalProblems);
    NSString *device = [[UIDevice currentDevice] model];
    NSString *code = [self sha1:[NSString stringWithFormat:@"%@-%@-%@", name, os, finishTime]];
    
    PFObject *object = [PFObject objectWithClassName:@"HighScore"];
    [object setObject:name forKey:@"name"];
    [object setObject:email forKey:@"email"];
    [object setObject:uid forKey:@"uid"];
    [object setObject:os forKey:@"os"];
    [object setObject:osVersion forKey:@"osVersion"];
    [object setObject:finishTime forKey:@"finishTime"];
    [object setObject:accuracyDat forKey:@"accuracy"];
    [object setObject:numProblems forKey:@"problems"];
    [object setObject:device forKey:@"device"];
    [object setObject:code forKey:@"code"];
    
    self.locationLoadingTimeOut = NO;
    self.scoreSubmitted = NO;
    
    [PFGeoPoint geoPointForCurrentLocationInBackground:^(PFGeoPoint *geoPoint, NSError *error) {
        
        if (NO == self.locationLoadingTimeOut
            && NO == self.scoreSubmitted)
        {
            self.scoreSubmitted = YES;
            
            if (geoPoint)
            {
                [object setObject:geoPoint forKey:@"location"];
            }
            
            [object saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                
                if (succeeded && !error)
                {
                    if (self.delegate && [(id) self.delegate respondsToSelector:@selector(onSubmitGlobalHighScoreFinished)]){
                        [self.delegate onSubmitGlobalHighScoreFinished];
                    }
                }
                else
                {
                    if (self.delegate && [(id) self.delegate respondsToSelector:@selector(onSubmitGlobalHighScoreFailed)]){
                        [self.delegate onSubmitGlobalHighScoreFailed];
                    }
                }
            }];
        }
    }];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(10.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        if (NO == self.scoreSubmitted)
        {
            self.scoreSubmitted = YES;
            self.locationLoadingTimeOut = YES;
            
            [object saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                
                if (succeeded && !error)
                {
                    if (self.delegate && [(id) self.delegate respondsToSelector:@selector(onSubmitGlobalHighScoreFinished)]){
                        [self.delegate onSubmitGlobalHighScoreFinished];
                    }
                }
                else
                {
                    if (self.delegate && [(id) self.delegate respondsToSelector:@selector(onSubmitGlobalHighScoreFailed)]){
                        [self.delegate onSubmitGlobalHighScoreFailed];
                    }
                }
            }];
        }
    });
}

- (void) submitFacebookwithScore:(CFTimeInterval)score{
    DLog();
}

- (void) submitTwitterwithScore:(CFTimeInterval)score{
    DLog();
}

#pragma mark - Support Functions
- (NSString *)sha1:(NSString*)input
{
    const char *cstr = [input cStringUsingEncoding:NSUTF8StringEncoding];
    NSData *data = [NSData dataWithBytes:cstr length:input.length];
    
    uint8_t digest[CC_SHA1_DIGEST_LENGTH];
    
    CC_SHA1(data.bytes, (int)data.length, digest);
    
    NSMutableString* output = [NSMutableString stringWithCapacity:CC_SHA1_DIGEST_LENGTH * 2];
    
    for (int i = 0; i < CC_SHA1_DIGEST_LENGTH; i++)
    {
        [output appendFormat:@"%02x", digest[i]];
    }
    
    return output;
    
}

- (NSString *)md5:(NSString *)input
{
    const char *cStr = [input UTF8String];
    unsigned char digest[16];
    CC_MD5( cStr, (int)strlen(cStr), digest ); // This is the md5 call
    
    NSMutableString *output = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH * 2];
    
    for (int i = 0; i < CC_MD5_DIGEST_LENGTH; i++)
    {
        [output appendFormat:@"%02x", digest[i]];
    }
    
    return  output;
}

@end
