//
//  Problem.h
//  BrainTuner3
//
//  Created by Elliot Lee on 7/14/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import "cocos2d.h"

typedef enum {
	kResultUnknown = 0,
	kResultWrong,
	kResultRight
} ProblemResult;

@interface Problem : NSObject {
	NSString *text;
	BOOL isRight;
    ProblemResult problemResult;
	CCSprite * image;
}

@property (nonatomic, retain) NSString *text;
@property (nonatomic) BOOL isRight;
@property (nonatomic) ProblemResult problemResult;
@property (nonatomic, retain) CCSprite *image;

- (void) randomize;
- (void) printOutProblem;
- (NSString *) getText;
- (ProblemResult) getResult;
- (void) invalidateProblemResult:(BOOL) inputResult;

@end
