//
//  SecurityManager.m
//  BrainTunerX
//
//  Created by Leon Yuu on 10/25/12.
//
//

#import "SecurityManager.h"

static SecurityManager * sharedManager = nil;

@implementation SecurityManager

+ (SecurityManager *) sharedManager {
    @synchronized(self) {
        if (sharedManager == nil) {
            [[self alloc] init];
        }
    }
    return sharedManager;
}

- (id) init{
    @synchronized(self) {
        [super init];
        return self;
    }
}

#pragma mark - Main Methods
- (void) createLocalStorageKey{
    DLog();
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    int randomcase = (int) (arc4random()%10);
    NSString * formatString = @"";
    switch (randomcase) {
        case 0:
            formatString = @"yyyyMMddHHmmss";
            break;
        case 1:
            formatString = @"MMddyyyymmHHss";
            break;
        case 2:
            formatString = @"ddyyyyMMmmHHss";
            break;
        case 3:
            formatString = @"yyyyMMddmmssHH";
            break;
        case 4:
            formatString = @"yyyyMMddssHHmm";
            break;
        case 5:
            formatString = @"HHmmssyyyyMMdd";
            break;
        case 6:
            formatString = @"mmssHHyyyyMMdd";
            break;
        case 7:
            formatString = @"HHssmmyyyyMMdd";            
            break;
        case 8:
            formatString = @"HHyyyymmddMMss";
            break;
        case 9:
            formatString = @"ssMMHHyyyyddmm";
            break;
        default:
            formatString = @"yyyyMMddHHmmss";
            break;
    }
    
    [dateFormatter setDateFormat:formatString];
    NSString *dateString = [dateFormatter stringFromDate:[NSDate date]];
    [dateFormatter release];
    
    NSString * localstoragekey  = [NSString stringWithFormat:@"BT%@%@BT",dateString,dateString];
    DLog(@"Local Key: %@",localstoragekey);
    
    [NSDEF setObject:localstoragekey forKey:kLocalStorageKey];
}

- (void) initDefaultValuesforBestTimeandAccuracy {
    DLog();
    [self storeBestTime:0.0f Accuracy:0 withNumofProblems:20];
    [self storeBestTime:0.0f Accuracy:0 withNumofProblems:60];
    [self storeBestTime:0.0f Accuracy:0 withNumofProblems:100];
}

- (NSString *) encryptBestTime:(double) bestTime {
    DLog();
    
    // Init Key, Text
    NSString *key = [NSDEF stringForKey:kLocalStorageKey];
    NSString *text = [NSString stringWithFormat:@"%f",bestTime];
    DLog(@"%@",text);

    // Get NSString encrypted result
    NSString *result = [AESCrypt encrypt:text password:key];
    
    DLog(@"%@",result);
    
    return result;
}

- (NSString *) encryptAccuracy:(int) accuracy {
    DLog();
    
    // Init Key, Text
    NSString *key = [NSDEF stringForKey:kLocalStorageKey];
    NSString *text = [NSString stringWithFormat:@"%d",accuracy];
    DLog(@"%@",text);
    
    // Get NSString encrypted result
    NSString *result = [AESCrypt encrypt:text password:key];
    
    DLog(@"%@",result);
    
    return result;
}

- (double) decryptBestTimewithNumofProblems:(int) numofProblems{
    DLog();
    
    // Get encrypted best time value from local storage
    NSString *besttimekey = @"";
    
    if(numofProblems == 20) besttimekey = [NSDEF stringForKey:kBestTime20];
    
    else if(numofProblems == 60) besttimekey = [NSDEF stringForKey:kBestTime60];
    
    else if(numofProblems == 100) besttimekey = [NSDEF stringForKey:kBestTime100];
    
    // Init Key, Text
    NSString *key = [NSDEF stringForKey:kLocalStorageKey];
    NSString *text = besttimekey;
    NSString *result = [AESCrypt decrypt:text password:key];
    DLog(@"%f",[result doubleValue]);
    
    return [result doubleValue];
}

- (int) decryptAccuracywithNumofProblems:(int) numofProblems{
    DLog();
    
    // Get encrypted accuracy value from local storage
    NSString *accuracy = @"";
    
    if(numofProblems == 20) accuracy = [NSDEF stringForKey:kBestAccuracy20];
    
    else if(numofProblems == 60) accuracy = [NSDEF stringForKey:kBestAccuracy60];
    
    else if(numofProblems == 100) accuracy = [NSDEF stringForKey:kBestAccuracy100];
    
    // Init Key, Text
    NSString *key = [NSDEF stringForKey:kLocalStorageKey];
    NSString *text = accuracy;
    NSString *result = [AESCrypt decrypt:text password:key];
    DLog(@"%d",[result intValue]);
    
    return [result intValue];
}

- (void) storeBestTime:(double) bestTime Accuracy:(int) accuracy withNumofProblems:(int) numofProblems{
    DLog();
    switch (numofProblems) {
        case 20:
            [NSDEF setObject:[self encryptBestTime:bestTime] forKey:kBestTime20];
            [NSDEF setObject:[self encryptAccuracy:accuracy] forKey:kBestAccuracy20];
            break;
        case 60:
            [NSDEF setObject:[self encryptBestTime:bestTime] forKey:kBestTime60];
            [NSDEF setObject:[self encryptAccuracy:accuracy] forKey:kBestAccuracy60];
            break;
        case 100:
            [NSDEF setObject:[self encryptBestTime:bestTime] forKey:kBestTime100];
            [NSDEF setObject:[self encryptAccuracy:accuracy] forKey:kBestAccuracy100];
            break;
        default:
            break;
    }
}

- (double) getBestTime:(int) numofproblems{
    DLog();
    double bestTime = 0.0f;
    bestTime = [self decryptBestTimewithNumofProblems:numofproblems];
    return bestTime;
}

- (int) getAccuracy:(int) numofproblems{
    DLog();
    int bestTime = 0;
    bestTime = [self decryptAccuracywithNumofProblems:numofproblems];
    return bestTime;
}

#pragma mark - Synthesize Singleton Methods
+ (id)allocWithZone:(NSZone *)zone {
    @synchronized(self) {
        if (sharedManager == nil) {
            sharedManager = [super allocWithZone:zone];
            return sharedManager;  // assignment and return on first allocation
        }
    }
    return nil; //on subsequent allocation attempts return nil
}

- (id)copyWithZone:(NSZone *)zone {
    return self;
}

- (id)retain {
    return self;
}

- (NSUInteger)retainCount {
    return NSUIntegerMax;  //denotes an object that cannot be released
}

- (oneway void)release {
    //do nothing
}

- (id)autorelease
{
    return self;
}



@end
