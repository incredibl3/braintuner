//
//  SubmitScoreManager.h
//  BrainTunerX
//
//  Created by Leon Yuu on 10/11/12.
//
//

#import <Foundation/Foundation.h>
#import "ASIHTTPRequest.h"
#import <CommonCrypto/CommonDigest.h>
#import "SBJsonParser.h"
#import "DeviceHelper.h"
#import "NSString+URLEncoding.h"
#import "NSString+Base64.h"

@protocol SubmitScoreDelegate
@optional
- (BOOL) onSubmitGlobalHighScoreFinished;
- (BOOL) onSubmitFaceBookFinished;
- (BOOL) onSubmitTwitterFinished;
- (BOOL) onSubmitGlobalHighScoreFailed;
- (BOOL) onSubmitFaceBookFailed;
- (BOOL) onSubmitTwitterFailed;

@end

@interface SubmitScoreManager : NSObject<ASIHTTPRequestDelegate> {
    
}

@property (nonatomic,retain) id<SubmitScoreDelegate> delegate;

- (void)submitGlobalHighScorewithScore:(CFTimeInterval)score
                              accuracy:(int)accuracy
                          totalProblems:(int)totalProblems
                                userID:(NSString *)userID
                              userEmail:(NSString*)userEmail;

- (void)submitFacebookwithScore:(CFTimeInterval)score;
- (void)submitTwitterwithScore:(CFTimeInterval)score;

@end
