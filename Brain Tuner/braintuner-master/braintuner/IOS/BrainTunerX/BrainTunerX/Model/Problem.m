//
//  Problem.m
//  BrainTuner3
//
//  Created by Elliot Lee on 7/14/08.
//  Copyright 2008-2009 GreenGar Studios. All rights reserved.
//

#import "Problem.h"

@implementation Problem

@synthesize text, isRight, image, problemResult;

- (void) randomize {
	
}

- (void) printOutProblem {
    
}

- (NSString*) getText{
    return  self.text;
}

- (ProblemResult) getResult{
    return  problemResult;
}

- (void) invalidateProblemResult:(BOOL)inputResult{
    if(isRight == inputResult){
        problemResult = kResultRight;
    }else{
        problemResult = kResultWrong;
    }
}

@end
