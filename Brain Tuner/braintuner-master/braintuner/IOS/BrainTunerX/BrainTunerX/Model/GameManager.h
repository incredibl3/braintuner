//
//  GameManager.h
//  BrainTunerX
//
//  Created by Leon Yuu on 10/10/12.
//
//

#import <Foundation/Foundation.h>
#import "GameUtils.h"
#import "EquityProblem.h"
#import "InequityProblem.h"
#import "SecurityManager.h"

@interface GameManager : NSObject{

    NSMutableArray *problemArray;
    
}

+ (GameManager *) sharedGameManager;
- (id) init;

#pragma mark - optional game control methods
- (void) initGameWithTotalProblems:(int) numberofproblems Difficulty:(int) difficulty;
- (void) prepareGame;
- (void) startGame;
- (void) endGame;

#pragma mark - problems source
- (NSString *)      getProblemTextatIndex:(int)index;
- (ProblemResult)   getProblemResultatIndex:(int)index;
- (ProblemResult)   updateProblemResult:(BOOL)result;
- (NSString *)      storeRecord:(CFTimeInterval) record Accuracy:(int) accuracy withTotalNumberProblems:(int) numofProblems;
- (NSString *)      getBestRecordwithTotalNumbers:(int) numofProblems;
- (void)            resetBestRecordwithTotalNumbers:(int) numofProblems;
- (NSString *)      getSocialNetWorkUploadMessageWithNumofProblems:(int) numofProblems;

@property (nonatomic, retain) NSMutableArray *problemArray;
@property (nonatomic) int currentIndex;
@property (nonatomic) int correctAnswer;
@property (nonatomic) int incorrectAnswer;
@property (nonatomic) int TotalNumberProblems;
@property (nonatomic) int Difficulty;
@property (nonatomic) int MaxDifficulty;
@property (nonatomic) BOOL isGameRunning;
@property (nonatomic) BOOL onDialog;
@property (nonatomic) CFTimeInterval startTime;
@property (nonatomic) CFTimeInterval playTime;
@property (nonatomic) CFTimeInterval finalTime;
@property (nonatomic) int Accuracy;

@end
