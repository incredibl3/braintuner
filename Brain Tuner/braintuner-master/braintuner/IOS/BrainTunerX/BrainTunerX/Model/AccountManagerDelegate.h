//
//  AccountManagerDelegate.h
//  BrainTunerX
//
//  Created by Leon Yuu on 11/6/12.
//
//

#import <Foundation/Foundation.h>

@protocol AccountManagerDelegate
@optional
- (void) fbRequestUserInfoFinished;
- (void) fbRequestUserInfoFailed;

@end
