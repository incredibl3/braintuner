//
//  SecurityManager.h
//  BrainTunerX
//
//  Created by Leon Yuu on 10/25/12.
//
//

#import <Foundation/Foundation.h>
#import "GameUtils.h"
#import "AESCrypt.h"

@interface SecurityManager : NSObject{
    
}

+ (SecurityManager *) sharedManager;
- (id) init;

// Create Local Key
- (void) createLocalStorageKey;
- (void) initDefaultValuesforBestTimeandAccuracy;

// Encrypt/Decrypt Data
- (NSString *) encryptBestTime:(double) bestTime;
- (NSString *) encryptAccuracy:(int) accuracy;
- (double) decryptBestTimewithNumofProblems:(int) numofProblems;
- (int) decryptAccuracywithNumofProblems:(int) numofProblems;

// Store and Get Data
- (void) storeBestTime:(double) bestTime Accuracy:(int) accuracy withNumofProblems:(int) numofProblems;
- (double) getBestTime:(int) numofproblems;
- (int) getAccuracy:(int) numofproblems;

@end
