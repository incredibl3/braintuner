//
//  AccountManager.m
//  BrainTuner2
//
//  Created by silvercast on 5/18/11.
//  Copyright 2011 GreenGar studios. All rights reserved.
//

#import "AccountManager.h"
#import <Parse/Parse.h>
#import <ParseUI/ParseUI.h>
#import <ParseFacebookUtils/PFFacebookUtils.h>
#import "cocos2d.h"
#import "UIAlertView+Blocks.h"

#define kFacebookAppID @"428745243840430"

#define kTwitterOAuthConsumerKey				@"T0wSTwmOx4NkJy07juPL5w"		
#define kTwitterOAuthConsumerSecret				@"7iA612acLweMGPh7iqSISIVcV3G6LAm8IvAMHT05I80"
#define kTwitterClientName						@"Brain Tuner"
#define kTwitterClientVersion					@"1.6"
#define kBrainTunerAppURL						@"http://www.greengar.com/apps/brain-tuner/"
#define kGGItunesLink                           @"http://bit.ly/GgComAllGreengarApps"

#define MIN_PASSWORD_LENGTH 6
#define VALID_STR(x) (x && [x length] > 0)

static AccountManager *sharedAccountManager = nil;

@implementation AccountManager
@synthesize delegate;

+ (AccountManager *)sharedManager {
	
	@synchronized(self) {
        if (sharedAccountManager == nil) {
            [[self alloc] init]; // assignment not done here
        }
    }
	return sharedAccountManager;
}

// Use getPostMessage Instead - Fix crash issue when using facebookMessage
- (NSString *)getPostMessage
{
    NSString *postmessage = [[GameManager sharedGameManager] getSocialNetWorkUploadMessageWithNumofProblems:(int)[NSDEF integerForKey:kCurrentNumberProblem]];
    DLog(@"Post Message: %@",postmessage);
    return postmessage;
}

- (void)initTwitterAndFacebook
{
    shouldBroadcastAfterFacebookAuthorization = NO;
    shouldGetUserInfoAfterFacebookAuthorization = NO;
    
    // twitter engine initialization
	twitterManager = [[SA_OAuthTwitterEngine alloc] initOAuthWithDelegate: self];
	twitterManager.consumerKey = kTwitterOAuthConsumerKey;
	twitterManager.consumerSecret = kTwitterOAuthConsumerSecret;
	[twitterManager setClientName:kTwitterClientName version:kTwitterClientVersion URL:kBrainTunerAppURL token:@""];
}

- (id) init {
    @synchronized(self) {
        [super init];
        [self initTwitterAndFacebook];
        return self;
    }
}

- (void)authorizeFacebook {
    
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    return [FBAppCall handleOpenURL:url sourceApplication:sourceApplication withSession:[PFFacebookUtils session]];
}

- (void)signOutTwitter
{
    [twitterManager setClearsCookies:YES];    
    [twitterManager clearsCookies];
    [twitterManager clearAccessToken];
    //DLog(@"%@", [[NSHTTPCookieStorage sharedHTTPCookieStorage] cookies]);
    
    NSArray *cookies = [NSArray arrayWithArray:[[NSHTTPCookieStorage sharedHTTPCookieStorage] cookies]];
    for (NSHTTPCookie *cookie in cookies)
    {
        if ([cookie.domain rangeOfString:@"twitter.com"].location != NSNotFound)
        {
            [[NSHTTPCookieStorage sharedHTTPCookieStorage] deleteCookie:cookie];
        }
    }
    //DLog(@"%@", [[NSHTTPCookieStorage sharedHTTPCookieStorage] cookies]);

    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"authData"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"twitterUsername"];    
    [[NSUserDefaults standardUserDefaults] synchronize];
    [twitterManager release];
    twitterManager = nil;
    
	// twitter engine initialization
	twitterManager = [[SA_OAuthTwitterEngine alloc] initOAuthWithDelegate: self];
	twitterManager.consumerKey = kTwitterOAuthConsumerKey;
	twitterManager.consumerSecret = kTwitterOAuthConsumerSecret;
	[twitterManager setClientName: kTwitterClientName version:kTwitterClientVersion URL:kBrainTunerAppURL token:@""];
}

- (void)broadcastOnFacebook:(NSString *)caption
{
    if ([PFUser currentUser] != nil) // Already log in
    {
        if ([PFFacebookUtils isLinkedWithUser:[PFUser currentUser]]) // And logged in with Facebook
        {
            if ([[[FBSession activeSession] permissions] indexOfObject:@"publish_actions"] == NSNotFound)
            {
                [[FBSession activeSession] requestNewPublishPermissions:@[@"publish_actions"]
                                                        defaultAudience:FBSessionDefaultAudienceFriends
                                                      completionHandler:^(FBSession *session, NSError *error) {
                                                          
                    if (!error)
                    {
                        [self readyToShareOnFacebook:caption];
                    }
                    else
                    {
                        [self shareOnFacebookFailed];
                    }
                }];
            }
            else
            {
                [self readyToShareOnFacebook:caption];
            }
        }
        else // And logged in not by using Facebook
        {
            [PFUser logOut]; // So log out first
            
            [self connectWithFacebookFinished:^(BOOL succeed, NSError *error) { // Then log in with Facebook
                
                if (succeed && !error)
                {
                    [self broadcastOnFacebook:caption]; // And share
                }
                else
                {
                    [self shareOnFacebookFailed];
                }
            }];
        }
    }
    else // Not logged in yet
    {
        [self connectWithFacebookFinished:^(BOOL succeed, NSError *error) { // So now log in with Facebook
            
            if (succeed && !error)
            {
                [self broadcastOnFacebook:caption]; // And share
            }
            else
            {
                [self shareOnFacebookFailed];
            }
        }];
    }
}

- (void)readyToShareOnFacebook:(NSString *)caption
{
    NSString *name = @"Brain Tuner X";
    NSString *descriptions = [NSString stringWithFormat:@"%@ with Brain Tuner, a fun math game for iOS, Android and Windows Phone. Can you beat this?\nGreengar Apps: %@", caption,kGGItunesLink];
    NSString *link = kAppiTunesLink;
    
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                   link, @"link",
                                   name, @"name",
                                   descriptions, @"description",
                                   descriptions,  @"message",
                                   nil];
    
    [FBRequestConnection startWithGraphPath:@"me/feed" parameters:params
                                 HTTPMethod:@"POST"
                          completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
        if (!error)
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Shared on Facebook!", nil)
                                                            message:NSLocalizedString(@"Please check your Facebook", nil)
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
            [alert release];
        }
        else
        {
            [self shareOnFacebookFailed];
        }
    }];
}

- (void)shareOnFacebookFailed
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Failed to share on Facebook!", nil)
                                                    message:NSLocalizedString(@"Please try again", nil)
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
    [alert release];
}

- (void)connectWithFacebookFinished:(GSResultBlock)block
{
    // Check Internet Connection
    if (![self checkInternetConnection])
    {
        DLog(@"No Internet Connection");
        if (block) {
            block(NO, nil);
        }
        return;
    }
    
    [self showLoadingAlertwithTitle:NSLocalizedString(@"Loading", nil)
                         andMessage:NSLocalizedString(@"Connecting to server...", nil)];
    
    [[FBSession activeSession] closeAndClearTokenInformation];
    
    NSArray *permissionsArray = @[@"email"];
    
    [PFFacebookUtils logInWithPermissions:permissionsArray block:^(PFUser *user, NSError *error) {
        
        [self dismissLoadingAlert];
        
        if (user && error == nil)
        {
            [self showLoadingAlertwithTitle:NSLocalizedString(@"Loading", nil)
                                 andMessage:NSLocalizedString(@"Connecting to server...", nil)];
            
            [self linkFacebookDataBlock:^(BOOL succeed, NSError *error) {
                
                [self dismissLoadingAlert];
                
                NSString *name = ([[PFUser currentUser] objectForKey:@"fullname"] ? [[PFUser currentUser] objectForKey:@"fullname"] : [[PFUser currentUser] username]);
                NSString *email = ([[PFUser currentUser] objectForKey:@"email"] ? [[PFUser currentUser] objectForKey:@"email"] : [NSString stringWithFormat:@"%@@greengar.com", [PFUser currentUser].username]);
                [NSDEF setObject:name forKey:kSubmitName];
                [NSDEF setObject:email forKey:kSubmitEmail];
                
                if (block) {
                    block (YES, nil);
                }
            }];
        }
        else
        {
            if ([DeviceHelper isIOS6OrHigher])
            {
                [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Facebook Login Failed", nil)
                                            message:NSLocalizedString(@"Make sure you've allowed Brain Tuner X to use Facebook in iOS Settings › Privacy › Facebook.", nil)
                                           delegate:nil
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil] show];
            }
            else
            {
                [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Facebook Login Failed", nil)
                                            message:nil
                                           delegate:nil
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil] show];
            }
            
            if (block) {
                block (NO, error);
            }
        }
    }];
}

- (void)registerWithFirstname:(NSString *)firstname lastname:(NSString *)lastname
                        email:(NSString *)email password:(NSString *)password
                     finished:(GSResultBlock)block
{
    // Check Internet Connection
    if (![self checkInternetConnection])
    {
        DLog(@"No Internet Connection");
        if (block) {
            block (NO, nil);
        }
        return;
    }
    
    // Check firstname
    if (![self checkValidName:firstname])
    {
        if (block) {
            block (NO, nil);
        }
        return;
    }
    
    // Check lastname
    if (![self checkValidName:lastname])
    {
        if (block) {
            block (NO, nil);
        }
        return;
    }
    
    // Check email
    if (![self checkValidEmailAddress:email])
    {
        if (block) {
            block (NO, nil);
        }
        return;
    }
    
    // Check password
    if (![self checkValidPassword:password])
    {
        if (block) {
            block (NO, nil);
        }
        return;
    }
    
    // Start to sign up
    [self showLoadingAlertwithTitle:NSLocalizedString(@"Sign Up", nil)
                         andMessage:NSLocalizedString(@"Connecting to server...", nil)];
    
    PFUser *user = [PFUser user];
    user.username = email;
    user.password = password;
    user.email = email;
    [user setObject:firstname forKey:@"firstname"];
    [user setObject:lastname forKey:@"lastname"];
    [user setObject:[NSString stringWithFormat:@"%@ %@", firstname, lastname] forKey:@"fullname"];
    
    [user signUpInBackgroundWithBlock:^(BOOL success, NSError *error) {
        
        [self dismissLoadingAlert];
        
        if (success)
        {
            [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Congratulations!", nil)
                                        message:NSLocalizedString(@"You have successfully created Brain Tuner X account", nil)
                                       delegate:nil
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil] show];
            
            [self finishWithUsername:email password:password silent:NO block:^(BOOL succeed, NSError *error) {
                if (block) {
                    block(succeed, error);
                }
            }];
        }
        else
        {
            if (error.code == 202 || error.code == 203)
            {
                // Error: username already taken (Code: 202, Version: 1.2.11) -> Attempt login
                [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Email address already in use", nil)
                                            message:nil
                                           delegate:nil
                                  cancelButtonTitle:NSLocalizedString(@"Whoops", nil)
                                  otherButtonTitles:nil] show];
                if (block) {
                    block(NO, error);
                }
            }
            else
            {
                NSString *reason = [[error userInfo] objectForKey:@"error"];
                
                [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Sign Up Failed", nil)
                                            message:reason
                                           delegate:nil
                                  cancelButtonTitle:NSLocalizedString(@"Darn", nil)
                                  otherButtonTitles:nil] show];
                
                if (block) {
                    block(NO, error);
                }
            }
        }
    }];
}

- (void)signInWithEmail:(NSString *)email password:(NSString *)password finished:(GSResultBlock)block
{
    // Check Internet Connection
    if (![self checkInternetConnection])
    {
        DLog(@"No Internet Connection");
        if (block) {
            block (NO, nil);
        }
        return;
    }
    
    // Check email
    if (![self checkValidEmailAddress:email])
    {
        if (block) {
            block (NO, nil);
        }
        return;
    }
    
    // Check password
    if (![self checkValidPassword:password])
    {
        if (block) {
            block (NO, nil);
        }
        return;
    }
    
    // Start to sign in
    [self finishWithUsername:email password:password silent:NO block:^(BOOL succeed, NSError *error) {
        if (block) {
            block(succeed, error);
        }
    }];
}

- (void)finishWithUsername:(NSString *)username password:(NSString *)password silent:(BOOL)silent block:(GSResultBlock)block
{
    if (silent == NO)
    {
        [self showLoadingAlertwithTitle:NSLocalizedString(@"Loading", nil)
                             andMessage:NSLocalizedString(@"Connecting to server...", nil)];
    }
    
    [PFUser logInWithUsernameInBackground:username password:password block:^(PFUser *user, NSError *error) {
        
        [self dismissLoadingAlert];
        
        if (user)
        {
            DLog(@"Looks like the user signed in to an existing account");
            NSString *name = ([[PFUser currentUser] objectForKey:@"fullname"] ? [[PFUser currentUser] objectForKey:@"fullname"] : [[PFUser currentUser] username]);
            NSString *email = ([[PFUser currentUser] objectForKey:@"email"] ? [[PFUser currentUser] objectForKey:@"email"] : [NSString stringWithFormat:@"%@@greengar.com", [PFUser currentUser].username]);
            [NSDEF setObject:name forKey:kSubmitName];
            [NSDEF setObject:email forKey:kSubmitEmail];
            
            if (block) {
                block(YES, nil);
            }
        }
        else
        {
            NSString *title = silent ?
                // Sign Up:
                NSLocalizedString(@"Username Already Taken", nil) :
                // Sign In:
                NSLocalizedString(@"Invalid username/password", nil);
            
            NSString *message = NSLocalizedString(@"Password didn't match. If this is your username, tap 'Forgot Password'", nil);
            RIButtonItem *cancelItem = [RIButtonItem itemWithLabel:NSLocalizedString(@"OK", nil)];
            RIButtonItem *okItem = [RIButtonItem itemWithLabel:NSLocalizedString(@"Forgot Password", nil) action:^{
                
                [self showForgotPasswordAlert];
                
            }];
            
            [[[UIAlertView alloc] initWithTitle:title message:message cancelButtonItem:cancelItem otherButtonItems:okItem, nil] show];
            
            if (block) {
                block (NO, error);
            }
        }
    }];
}

- (void)signOut
{
    [PFUser logOut];
}

#pragma mark - TWITTER
- (void)broadcastOnTwitter:(NSString *)caption
{
	UIViewController * controller = [SA_OAuthTwitterController controllerToEnterCredentialsWithTwitterEngine:twitterManager delegate:self];
	
	if (controller)
    {
		[[UIAppDelegate getRootViewController] presentViewController:controller animated:YES completion:nil];
		// we wil send status update in delegate method authenticatedWithUsername , after user authorization
	}
    else
    {
		[self performSelector:@selector(postInAppTwitter) withObject:nil afterDelay:1.0];
	}
}

- (void)postInAppTwitter
{
    [self showLoadingAlertwithTitle:NSLocalizedString(@"Loading", nil)
                         andMessage:NSLocalizedString(@"Connecting to Twitter...", nil)];
    
	//  already start send status update immediately.
    // Fix Crash Here in case use facebookMessage
    [twitterManager sendUpdate: [NSString stringWithFormat:@"%@ with #BrainTuner, a fun math game for iOS & Android developed by @greengar.", [self getPostMessage]]];
}

#pragma mark - SA_OAuthTwitterEngineDelegate
- (void)storeCachedTwitterOAuthData:(NSString *)data forUsername:(NSString *)username
{
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	[defaults setObject:data forKey:@"authData"];
    [defaults setObject:username forKey:@"twitterUsername"];
	[defaults synchronize];
}

- (NSString *)cachedTwitterOAuthDataForUsername:(NSString *)username
{
	return [[NSUserDefaults standardUserDefaults] objectForKey: @"authData"];
}

#pragma mark - SA_OAuthTwitterControllerDelegate
- (void) OAuthTwitterController:(SA_OAuthTwitterController *)controller authenticatedWithUsername:(NSString *)username
{
	DLog(@"Authenicated for %@", username);
    
    // Fix Crash Here in case use facebookMessage
	[twitterManager sendUpdate:[NSString stringWithFormat:@"%@ with #BrainTuner, a fun math game for iOS & Android developed by @greengar. Can you beat this?", [self getPostMessage]]];
}

- (void)OAuthTwitterControllerFailed:(SA_OAuthTwitterController *)controller
{
	DLog(@"Authentication Failed!");
}

- (void)OAuthTwitterControllerCanceled:(SA_OAuthTwitterController *)controller
{
	DLog(@"Authentication Canceled.");
    [[NSNotificationCenter defaultCenter] postNotificationName:kTwitterControllerCanceled object:nil];
}

#pragma mark - TwitterEngineDelegate
- (void)requestSucceeded:(NSString *)requestIdentifier
{
	// update user interface and prevent user from submitting again
    [self dismissLoadingAlert];
    
	DLog(@"Request %@ succeeded", requestIdentifier);
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Shared on Twitter!", nil)
                                                    message:NSLocalizedString(@"Please check your Twitter", nil)
                                                   delegate:nil
										  cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
	[alert show];
	[alert release];
}

- (void)requestFailed:(NSString *)requestIdentifier withError:(NSError *)error
{
    [self dismissLoadingAlert];
    
	DLog(@"Request %@ failed with error: %@", requestIdentifier, error);
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Failed to share on Twitter!", nil)
                                                    message:NSLocalizedString(@"Please try again", nil)
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
	[alert show];
	[alert release];
}

#pragma mark - Support
- (BOOL)checkValidEmailAddress:(NSString *)email
{
    if (!email || email.length < 5 || [self isValidEmail:email] == NO)
    {
        [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Invalid Email", nil)
                                    message:NSLocalizedString(@"We need your email in case you forget your password", nil)
                                   delegate:nil
                          cancelButtonTitle:@"OK"
                          otherButtonTitles:nil] show];
        return NO;
    }
    return YES;
}

- (BOOL)checkValidName:(NSString *)name
{
    if (!name || name.length == 0)
    {
        [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Name is invalid", nil)
                                    message:NSLocalizedString(@"Please don't leave it blank", nil)
                                   delegate:nil
                          cancelButtonTitle:@"OK"
                          otherButtonTitles:nil] show];
        return NO;
    }
    return YES;
}


- (BOOL)checkValidPassword:(NSString *)password
{
    if (!password || password.length < MIN_PASSWORD_LENGTH)
    {
        [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Password too short", nil)
                                    message:[NSString stringWithFormat:@"%@ %u %@", NSLocalizedString(@"Minimum", nil), MIN_PASSWORD_LENGTH, NSLocalizedString(@"characters", nil)]
                                   delegate:nil
                          cancelButtonTitle:@"OK"
                          otherButtonTitles:nil] show];
        return NO;
    }
    return YES;
}

- (BOOL)checkInternetConnection
{
    BOOL hasInternet = YES;
    
    if([[Reachability reachabilityForInternetConnection] currentReachabilityStatus] == NotReachable)
    {
        UIAlertView * alertView = [[UIAlertView alloc] initWithTitle:@"No Internet connection"
                                                             message:@"Please connect your device to Internet!"
                                                            delegate:nil
                                                   cancelButtonTitle:@"Ok"
                                                   otherButtonTitles:nil];
        alertView.tag = kInternetErrorDialogTag;
        [alertView show];
        [alertView release];
        
        hasInternet = NO;
    }
    return hasInternet;
}

- (BOOL)isValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = YES; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSString *laxString = @".+@.+\\.[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

- (void)showForgotPasswordAlert
{
    RIButtonItem *cancelItem = [RIButtonItem itemWithLabel:NSLocalizedString(@"Cancel", nil)];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Enter your email address.", nil)
                                                    message:NSLocalizedString(@"We will send an email with a link to reset your password", nil) cancelButtonItem:cancelItem otherButtonItems:nil];
    alert.alertViewStyle = UIAlertViewStylePlainTextInput;
    [alert textFieldAtIndex:0].keyboardType = UIKeyboardTypeEmailAddress;
    
    RIButtonItem *okItem = [RIButtonItem itemWithLabel:NSLocalizedString(@"OK", nil) action:^{
        
        NSString *email = [alert textFieldAtIndex:0].text;
        
        if ([self isValidEmail:email])
        {
            [self showLoadingAlertwithTitle:NSLocalizedString(@"Loading", nil)
                                 andMessage:NSLocalizedString(@"Connecting to server...", nil)];
            
            [PFUser requestPasswordResetForEmailInBackground: email block:^(BOOL success, NSError *error){
                
                [self dismissLoadingAlert];
                
                if (success)
                {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Email sent!", nil)
                                                                    message:NSLocalizedString(@"Check your mail inbox and reset your password.", nil)
                                                                   delegate:nil
                                                          cancelButtonTitle:@"OK"
                                                          otherButtonTitles:nil];
                    [alert show];
                }
                else
                {
                    DLog(@"Failed %@", [error description]);
                    NSString *errorMsg = nil;
                    switch (error.code)
                    {
                        case 125:
                            errorMsg = NSLocalizedString(@"Invalid email address. Try again!", nil);
                            break;
                        default:
                            errorMsg = NSLocalizedString(@"Invalid email address. Try again!", nil);
                            break;
                    }
                    
                    RIButtonItem *item = [RIButtonItem itemWithLabel:NSLocalizedString(@"OK", nil) action:^{
                        [self showForgotPasswordAlert];
                    }];
                    
                    [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:errorMsg cancelButtonItem:item otherButtonItems:nil] show];
                } // success == NO
            }];
        }
        else
        {
            // email is invalid
            RIButtonItem *item = [RIButtonItem itemWithLabel:NSLocalizedString(@"OK", nil) action:^{
                [self showForgotPasswordAlert];
            }];
            [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:NSLocalizedString(@"Invalid email address", nil) cancelButtonItem:item otherButtonItems:nil] show];
        }
    }];
    
    [alert addButtonItem:okItem];
    [alert show];
}

- (void)linkFacebookDataBlock:(GSResultBlock)block
{
    // Create request for user's Facebook data
    FBRequest *request = [FBRequest requestForMe];
    
    // Send request to Facebook
    [request startWithCompletionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
        if (!error)
        {
            // result is a dictionary with the user's Facebook data
            NSDictionary *userData = (NSDictionary *)result;
            
            NSString *facebookID = userData[@"id"];
            NSString *name = userData[@"name"];
            NSString *email = userData[@"email"];
            NSString *username = userData[@"username"];
            NSString *pictureURL = [NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?width=132&height=132", facebookID];
            NSString *gender = userData[@"gender"];
            
            if (name)
            {
                [[PFUser currentUser] setObject:name forKey:@"fullname"];
            }
            else
            {
                DLog(@"ERROR: name missing");
            }
            
            if (pictureURL)
            {
                NSString *existedURL = [[PFUser currentUser] objectForKey:@"avatar"];
                if ([existedURL rangeOfString:@"https://graph.facebook.com/"].location != NSNotFound)
                {
                    [[PFUser currentUser] setObject:pictureURL forKey:@"avatar"];
                }
                else
                {
                    // Avatar is not from Facebook anymore, so it should not be updated
                }
            }
            
            if (email)
            {
                NSString *existedEmail = [[PFUser currentUser] objectForKey:@"email"];
                
                if (!existedEmail || [existedEmail isEqualToString:@""] || ![existedEmail isEqualToString:email])
                {
                    // Only update email if the email field is not set/ or it is empty/
                    // or it's different from the email retrieved from Facebook
                    [[PFUser currentUser] setObject:email forKey:@"email"];
                    
                    NSString *message;
                    if (!existedEmail || [existedEmail isEqualToString:@""])
                    {
                        message = [NSString stringWithFormat:@"%@ '%@'. %@", NSLocalizedString(@"Your email will be updated to", nil), email, NSLocalizedString(@"Thanks for joining Brain Tuner X!", nil)];
                    }
                    else
                    {
                        message = [NSString stringWithFormat:@"%@ '%@' %@ '%@'. %@", NSLocalizedString(@"Your email will be updated from", nil), existedEmail, NSLocalizedString(@"to", nil), email, NSLocalizedString(@"Thanks for joining Brain Tuner X!", nil)];
                    }
                    
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Email updated!", nil)
                                                                    message:message
                                                                   delegate:nil
                                                          cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                                          otherButtonTitles:nil];
                    [alert show];
                }
                
            }
            else if ([[PFUser currentUser] objectForKey:@"email"] == nil)
            {
                DLog(@"email is missing");
            }
            
            [[PFUser currentUser] setObject:[NSNumber numberWithBool:YES] forKey:@"facebookLinked"];
            
            if (facebookID)
            {
                [[PFUser currentUser] setObject:facebookID forKey:@"facebookId"];
            }
            
            if (username)
            {
                [[PFUser currentUser] setObject:username forKey:@"facebookScreenName"];
            }
            
            if (gender)
            {
                [[PFUser currentUser] setObject:gender forKey:@"gender"];
            }
            
            [[PFUser currentUser] saveInBackground];
        }
        if (block) {
            block(YES, error);
        }
    }];
}

#pragma mark - UI AlertView Delegate
- (void)showLoadingAlertwithTitle:(NSString *)title andMessage:(NSString*)message
{
    if (!loadingAlertView)
    { // just being defensive
        loadingAlertView = [[UIAlertView alloc] initWithTitle:title
                                                      message:message
                                                     delegate:self
                                            cancelButtonTitle:nil
                                            otherButtonTitles:@"Cancel", nil];
        [loadingAlertView show];
    }
}

- (void)dismissLoadingAlert
{
    if (loadingAlertView)
    { // sending messages to nil is fine, but so is this
        [loadingAlertView dismissWithClickedButtonIndex:[loadingAlertView cancelButtonIndex] animated:NO];
        [loadingAlertView release];
        loadingAlertView = nil;
    }
}

+ (id)allocWithZone:(NSZone *)zone
{
    @synchronized(self)
    {
        if (sharedAccountManager == nil)
        {
            sharedAccountManager = [super allocWithZone:zone];
            return sharedAccountManager;  // assignment and return on first allocation
        }
    }
    return nil; //on subsequent allocation attempts return nil
}

- (id)copyWithZone:(NSZone *)zone
{
    return self;
}

- (id)retain
{
    return self;
}

- (NSUInteger)retainCount
{
    return NSUIntegerMax;  //denotes an object that cannot be released
}

- (oneway void)release
{
    //do nothing
}

- (id)autorelease
{
    return self;
}

@end
