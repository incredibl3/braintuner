//
//  InequityProblem.h
//  BrainTuner3
//
//  Created by Silvercast on 10/4/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Problem.h"
#import "GameManager.h"

@interface InequityProblem : Problem {

}

- (void) randomize;
- (void) printOutProblem;

@end
