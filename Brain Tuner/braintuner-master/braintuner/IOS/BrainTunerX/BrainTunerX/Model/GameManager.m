//
//  GameManager.m
//  BrainTunerX
//
//  Created by Leon Yuu on 10/10/12.
//
//

#import "GameManager.h"

static GameManager *sharedGameManager = nil;

@implementation GameManager
@synthesize problemArray;
@synthesize currentIndex;
@synthesize isGameRunning;
@synthesize onDialog;
@synthesize correctAnswer;
@synthesize incorrectAnswer;
@synthesize Difficulty;
@synthesize MaxDifficulty;
@synthesize TotalNumberProblems;
@synthesize startTime;
@synthesize playTime;
@synthesize finalTime;
@synthesize Accuracy;

+ (GameManager *) sharedGameManager {
    @synchronized(self) {
        if (sharedGameManager == nil) {
            [[self alloc] init];
        }
    }
    return sharedGameManager;
}

- (id) init{
    @synchronized(self) {
        [super init];
        [self initDefaultOptions];
        return self;
    }
}

- (void) initDefaultOptions{
    if(![NSDEF boolForKey:kFirstLaunch]){
        DLog(@"First Launch");
        
        [NSDEF setBool:YES forKey:kFirstLaunch];
        
        [NSDEF setBool:YES forKey:OPT_SOUND];
        [NSDEF setBool:YES forKey:OPT_COUNTDOWN];
        [NSDEF setBool:YES forKey:OPT_ADD];
        [NSDEF setBool:YES forKey:OPT_SUB];
        [NSDEF setBool:YES forKey:OPT_MUL];
        [NSDEF setBool:NO forKey:OPT_DIV];
        [NSDEF setBool:NO forKey:OPT_REV];
        [NSDEF setBool:NO forKey:OPT_POS];
        [NSDEF setBool:NO forKey:OPT_NEG];
        [NSDEF setBool:NO forKey:OPT_NSL];
        [NSDEF setBool:NO forKey:OPT_SWP];
        [NSDEF setBool:NO forKey:OPT_BIG];
        [NSDEF setObject:[self generateDeafultID] forKey:kUserID];
        
        [[SecurityManager sharedManager] createLocalStorageKey];
        [[SecurityManager sharedManager] initDefaultValuesforBestTimeandAccuracy];
        
        [NSDEF setObject:@"" forKey:kSubmitName];
        [NSDEF setObject:@"" forKey:kSubmitEmail];
    }
}

- (NSString *) generateDeafultID{
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyMMddHHmmss"];
    NSString *dateString = [dateFormatter stringFromDate:[NSDate date]];
    [dateFormatter release];
    
    NSString * defaultUserID  = [NSString stringWithFormat:@"BTX%@",dateString];
    return defaultUserID;
}

#pragma mark -
#pragma mark optional game control methods
- (NSString *) storeRecord:(CFTimeInterval)record Accuracy:(int) accuracy withTotalNumberProblems:(int)numofProblems{
    NSString * newRecord = @"";
    SecurityManager * securitymanager = [SecurityManager sharedManager];
    DLog(@"Play Time %f",playTime);
    DLog(@"Accuracy %d",accuracy);
    
    if([securitymanager getBestTime:numofProblems] == 0.0f){ // None Yet
        [securitymanager storeBestTime:record Accuracy:accuracy withNumofProblems:numofProblems];
        newRecord = @"\n(New Record!)";
    }else{
        double oldvalue = [securitymanager getBestTime:numofProblems];
        if(oldvalue > record){
            [securitymanager storeBestTime:record Accuracy:accuracy withNumofProblems:numofProblems];
            newRecord = @"\n(New Record!)";
        }
    }
    
    return newRecord;
}

- (NSString*) getBestRecordwithTotalNumbers:(int)numofProblems{
    NSString * best = nil;
    SecurityManager * securitymanager = [SecurityManager sharedManager];
    
    if([securitymanager getBestTime:numofProblems] == 0.0f){ // None Yet
        best = @"Best: None Yet";
    }else{
        best = [NSString stringWithFormat:@"Best: %.2f sec",[securitymanager getBestTime:numofProblems]];
    }
    return best;
}

- (void) resetBestRecordwithTotalNumbers:(int)numofProblems{
    [[SecurityManager sharedManager] storeBestTime:0.0f Accuracy:0 withNumofProblems:numofProblems];
}

- (NSString *) getSocialNetWorkUploadMessageWithNumofProblems:(int)numofProblems{
    double besttime = [[SecurityManager sharedManager] getBestTime:numofProblems];
    int accuracy = [[SecurityManager sharedManager] getAccuracy:numofProblems];
    
    NSString * message = [NSString stringWithFormat:@"Solved %i problems in %.2fs with %d%% Accuracy",numofProblems,besttime,accuracy];
    return message;
}

- (void) initGameWithTotalProblems:(int)numberofproblems Difficulty:(int)difficulty{
    DLog();
    TotalNumberProblems = numberofproblems;
    Difficulty = difficulty;
    
    // Create Problems Array
    if(problemArray){
        [problemArray removeAllObjects];
        [problemArray release];
        problemArray = nil;
    }
    
    // Init Values for Problems Array
    problemArray = [[NSMutableArray alloc] initWithObjects: nil];
    for (int i = 0; i < TotalNumberProblems; i++) {
		Problem * prob;
		if (Difficulty < 4) {
			prob = [[[EquityProblem alloc] init] autorelease];
		} else {
			BOOL useInequityProblem = (arc4random()%2 == 0);
			if (useInequityProblem) {
				prob = [[[InequityProblem alloc] init] autorelease];
			} else {
				prob = [[[EquityProblem alloc] init] autorelease];
			}
		}
        
		[prob randomize];
        [prob printOutProblem];
		[problemArray addObject:prob];
	}
    
}

- (void) prepareGame {
    DLog();
    currentIndex = 0;
    isGameRunning = NO;
    onDialog = NO;
    correctAnswer = 0;
    incorrectAnswer = 0;
    playTime = 0;
    startTime = 0;
    finalTime = 0;
    Accuracy = 0;
    
}

- (void) startGame {
    DLog();
    isGameRunning = YES;
    startTime = CACurrentMediaTime();
}

- (void) endGame {
    DLog();
    isGameRunning = NO;
    onDialog = YES;
    playTime = CACurrentMediaTime() - startTime;
    DLog(@"Correct: %i",self.correctAnswer);
    DLog(@"InCorrect: %i",self.incorrectAnswer);
    DLog(@"PlayTime: %f",self.playTime);
}

#pragma mark data sources
- (NSString*) getProblemTextatIndex:(int)index{
    return [[problemArray objectAtIndex:index] getText];
}

- (ProblemResult) getProblemResultatIndex:(int)index{
    return [[problemArray objectAtIndex:index] getResult];
}

- (ProblemResult) updateProblemResult:(BOOL)result {
	if (self.currentIndex >= [problemArray count]) {
		return kResultUnknown;
	}
    
	Problem * problem = [self.problemArray objectAtIndex:self.currentIndex];
	[problem invalidateProblemResult:result];
	self.currentIndex++;
    
    if(problem.problemResult == kResultRight)
        self.correctAnswer ++;
    if(problem.problemResult == kResultWrong)
        self.incorrectAnswer ++;
    
	return problem.problemResult;
}

- (void) dealloc{
    [problemArray release];
    [super dealloc];
}

#pragma mark - Synthesize Singleton Methods
+ (id)allocWithZone:(NSZone *)zone {
    @synchronized(self) {
        if (sharedGameManager == nil) {
            sharedGameManager = [super allocWithZone:zone];
            return sharedGameManager;  // assignment and return on first allocation
        }
    }
    return nil; //on subsequent allocation attempts return nil
}

- (id)copyWithZone:(NSZone *)zone {
    return self;
}

- (id)retain {
    return self;
}

- (NSUInteger)retainCount {
    return NSUIntegerMax;  //denotes an object that cannot be released
}

- (oneway void)release {
    //do nothing
}

- (id)autorelease
{
    return self;
}



@end
