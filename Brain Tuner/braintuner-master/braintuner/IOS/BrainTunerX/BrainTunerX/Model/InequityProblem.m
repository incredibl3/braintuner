//
//  InequityProblem.m
//  BrainTuner3
//
//  Created by Silvercast on 10/4/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "InequityProblem.h"

// a random number between min and max inclusive [min, max]
#define RANDOM_INT(__MIN__, __MAX__) ((__MIN__) + random() % ((__MAX__+1) - (__MIN__))) 

@implementation InequityProblem

- (void)randomize
{

	int multiplier = 1;
	if ([NSDEF boolForKey:OPT_BIG] == YES) {
		multiplier = 4; //3;
    }
	
	int minNumber;
	int maxNumber; //maxNumber = multiplier * 10;
	    
// Temporarily Not Used this Case
//	int difficulty = [GameManager sharedGameManager].Difficulty;
//	if (difficulty == 1) {
//		minNumber = 1;
//		maxNumber = 4;
//	} else if (difficulty >= 2 && difficulty <= 11) {
//		minNumber = difficulty - 1;
//		maxNumber = 8 + difficulty;
//	} else if (difficulty >= 12 && difficulty <= 21) {
//		minNumber = difficulty - 11;
//		maxNumber = 8 + difficulty;
//		//multiplier = 2;
//	} else if (difficulty >= 22 && difficulty <= 31) {
//		minNumber = difficulty - 21;
//		maxNumber = 8 + difficulty;
//		//multiplier = 3;
//	} else {
//		minNumber = difficulty - 31;
//		maxNumber = 8 + difficulty;
//		//multiplier = 4;
//	}
    
    minNumber = 1;
	maxNumber = 10; 
	
	//DLog(@"minNumber:%d maxNumber:%d", minNumber, maxNumber);
	int a = RANDOM_INT(minNumber, maxNumber);
	int b = RANDOM_INT(minNumber, maxNumber);
	int c;
	
	if ([NSDEF boolForKey:OPT_NEG] == YES) {
		// Only chance of first number being negative; looks weird otherwise
		// b/c negative sign looks like subtraction operator.
		if (RANDOM_INT(1, 2) == 1) {
			a = -a;
		}
	}
	
	NSString *divSign = @"/";
	if ([NSDEF boolForKey:OPT_NSL] == YES) {
		divSign = @"÷";
	}
	
	NSString *strOp;
    
	int numOps = [NSDEF boolForKey:OPT_ADD] + [NSDEF boolForKey:OPT_SUB] +
                 [NSDEF boolForKey:OPT_MUL] + [NSDEF boolForKey:OPT_DIV]; // Number of Operators We use
	int op;
	op = RANDOM_INT(1, numOps);
	
	// Find out which operator this problem shall use
	int addOp=0, subOp=0, mulOp=0, divOp=0;
	int nextOp = 1;
	if ([NSDEF boolForKey:OPT_ADD] == YES) { // Addition is enabled
		addOp = nextOp;
		nextOp++;
	}
	if ([NSDEF boolForKey:OPT_SUB] == YES) { // Subtraction is enabled
		subOp = nextOp;
		nextOp++;
	}
	if ([NSDEF boolForKey:OPT_MUL] == YES) {
		mulOp = nextOp;
		nextOp++;
	}
	if ([NSDEF boolForKey:OPT_DIV] == YES) {
		divOp = nextOp;
		nextOp++;
	}
	
	if (op == addOp) {
		strOp = @"+";
		c = a + b;
	} else if (op == subOp) {
		strOp = @"-";
		c = a - b;
		if ([NSDEF boolForKey:OPT_POS] == YES) {
			if (c < 0) {
				// Swap a and b
				int t = a;
				a = b;
				b = t;
				c = a - b;
			}
		}
	} else if (op == mulOp) {
		strOp = @"x";
		c = a * b;
	} else {
		strOp = divSign; //@"/"; // ÷ looks too much like +
		c = a * b;
		// Swap a and c
		int t = a;
		a = c;
		c = t;
	}
	
	/**
	 * Test Cases:
	 * ADD, SUB, MUL
	 * ADD, SUB, MUL, DIV
	 * MUL
	 * DIV
	 * ADD, DIV
	 * SUB, DIV
	 */
	
	/*
	 int opsCovered = op;
	 for (int curOp = 1; curOp <= numOps; curOp++) {
	 if ([app.optionsArray objectAtIndex:OPT_ADD] == 1 && opsCovered <= 1) {
	 strOp = @"+";
	 c = a + b;
	 opsCovered += 1;
	 } else if ([app.optionsArray objectAtIndex:OPT_SUB] == 1 && opsCovered <= 2) {
	 strOp = @"-";
	 c = a - b;
	 opsCovered += 2;
	 } else if ([app.optionsArray objectAtIndex:OPT_MUL] == 1 && opsCovered <= 3) {
	 strOp = @"x";
	 c = a * b;
	 opsCovered += 3;
	 } else if ([app.optionsArray objectAtIndex:OPT_DIV] == 1 && opsCovered <= 4) {
	 strOp = @"÷";
	 c = a * b;
	 // Swap a and c
	 int t = a;
	 a = c;
	 c = t;
	 opsCovered += 4;
	 } else {
	 DLog(@"Error: invalid operator options");
	 }
	 }
	 */
	
	int makeRight = RANDOM_INT(0, 1);
	int isMoreThan = RANDOM_INT(0, 1);
	// If multiplier is 0.3, offset is [1-3]
	int offset = RANDOM_INT(1, (int)(multiplier * 10));
	
	if (makeRight == 1) {
		if (isMoreThan == 1) {
			// we want something like 10 - 9 > x and x = 1 - offset. note that c = 10 - 9 = 1
			if ([NSDEF boolForKey:OPT_POS] == YES) {
				if (c - offset < 0) {							
					// special case! x < 0 . let's apply a 'dirty fix'.
					// change more than to less than and make it WRONG
					// e.g.  10 -  9 > 1 + offset, 9/9 > 1 + offset  
					self.isRight = NO;
					self.text = [NSString stringWithFormat:@"%d %@ %d > %d", a, strOp, b, c + offset];
				} else {
					self.isRight = YES;
					self.text = [NSString stringWithFormat:@"%d %@ %d > %d", a, strOp, b, c - offset];
				}
			} else {
				self.isRight = YES;
				self.text = [NSString stringWithFormat:@"%d %@ %d > %d", a, strOp, b, c - offset];
			}
		} else {
			// we want something like 10 - 9 < x and x = 1 + offset. note that c = 10 - 9 = 1
			self.isRight = YES;
			self.text = [NSString stringWithFormat:@"%d %@ %d < %d", a, strOp, b, c + offset];
		}
	} else {
		
		if (isMoreThan == 1) {
			// we want something like 10 - 9 > x and x = 1 + offset to make it WRONG. note that c = 10 - 9 = 1
			self.isRight = NO;
			self.text = [NSString stringWithFormat:@"%d %@ %d > %d", a, strOp, b, c + offset];
		} else {
			
			// we want something like 10 - 9 < x and x = 1 - offset. note that c = 10 - 9 = 1
			if ([NSDEF boolForKey:OPT_POS] == YES) {
				if (c - offset < 0) {
					// special case! x >= 0 . let's apply a 'dirty fix'.
					// change less than to more than and make it RIGHT
					// e.g.  10 -  9 < 1 + offset, 9/9 < 1 + offset  
					self.isRight = YES;
					self.text = [NSString stringWithFormat:@"%d %@ %d < %d", a, strOp, b, c + offset];
				} else {
					self.isRight = NO;
					self.text = [NSString stringWithFormat:@"%d %@ %d < %d", a, strOp, b, c - offset];
				}
			} else {
				self.isRight = NO;
				self.text = [NSString stringWithFormat:@"%d %@ %d < %d", a, strOp, b, c - offset];
			}
		}
	}
	
}

- (void) printOutProblem{
    DLog(@"%@  %i",self.text,self.isRight);
}

@end
