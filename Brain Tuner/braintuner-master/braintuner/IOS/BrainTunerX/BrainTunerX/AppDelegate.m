//
//  AppDelegate.m
//  BrainTunerX
//
//  Created by Leon Yuu on 10/5/12.
//  Copyright __MyCompanyName__ 2012. All rights reserved.
//

#import "AppDelegate.h"
#import <Parse/Parse.h>
#import <ParseCrashReporting/ParseCrashReporting.h>
#import <ParseFacebookUtils/PFFacebookUtils.h>

#define kRevmobLocalNotificationScheduleTime 7*24*3600

@implementation AppDelegate

@synthesize window;

- (void) removeStartupFlicker
{
	//
	// THIS CODE REMOVES THE STARTUP FLICKER
	//
	// Uncomment the following code if you Application only supports landscape mode
	//
#if GAME_AUTOROTATION == kGameAutorotationUIViewController
    
    //	CC_ENABLE_DEFAULT_GL_STATES();
    //	CCDirector *director = [CCDirector sharedDirector];
    //	CGSize size = [director winSize];
    //	CCSprite *sprite = [CCSprite spriteWithFile:@"Default.png"];
    //	sprite.position = ccp(size.width/2, size.height/2);
    //	sprite.rotation = -90;
    //	[sprite visit];
    //	[[director openGLView] swapBuffers];
    //	CC_ENABLE_DEFAULT_GL_STATES();
	
#endif // GAME_AUTOROTATION == kGameAutorotationUIViewController
}

- (void) applicationDidFinishLaunching:(UIApplication*)application
{
    // Init Flurry
    [Flurry startSession:kFlurryAppID];
    
    [ParseCrashReporting enable];
    
    [Parse setApplicationId:@"UJZ2OlusGRE01wEIuLkG4UjBAgzyQCB5l2Zf7fkL" clientKey:@"J3kEwPYFbU7G1h85RbPLHyOgMCQvofkZ5EinJiRS"];
    
    [PFFacebookUtils initializeFacebook];
    
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
    
    // Apple Push Notifications
    if ([DeviceHelper isIOS8OrHigher])
    {
        // Request permission for Badge
        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeBadge|UIUserNotificationTypeSound|UIUserNotificationTypeAlert
                                                                                 categories:nil];
        [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    }
    else
    {
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes: UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound |
     UIRemoteNotificationTypeAlert];
    }
    
    // Check and Update Version Index
    [self checkAndUpdateVersionIndex];
    
	// Init the window
	window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
	
	// Try to use CADisplayLink director
	// if it fails (SDK < 3.1) use the default director
	if( ! [CCDirector setDirectorType:kCCDirectorTypeDisplayLink] )
		[CCDirector setDirectorType:kCCDirectorTypeDefault];
	
	
	CCDirector *director = [CCDirector sharedDirector];
	
	// Init the View Controller
	viewController = [[RootViewController alloc] initWithNibName:nil bundle:nil];
	viewController.wantsFullScreenLayout = YES;
    [window  setRootViewController:viewController];
	
	//
	// Create the EAGLView manually
	//  1. Create a RGB565 format. Alternative: RGBA8
	//	2. depth format of 0 bit. Use 16 or 24 bit for 3d effects, like CCPageTurnTransition
	//
	//
	EAGLView *glView = [EAGLView viewWithFrame:[window bounds]
								   pixelFormat:kEAGLColorFormatRGB565	// kEAGLColorFormatRGBA8
								   depthFormat:0						// GL_DEPTH_COMPONENT16_OES
						];
	
	// attach the openglView to the director
	[director setOpenGLView:glView];
    if(IS_IPAD3){
        [director enableRetinaDisplay:NO]; // Temporarily disable iPad Retina display
    }else{
        [director enableRetinaDisplay:YES];
    }
	
    //	// Enables High Res mode (Retina Display) on iPhone 4 and maintains low res on all other devices
    //	if( ! [director enableRetinaDisplay:YES] )
    //		CCLOG(@"Retina Display Not supported");
	
	//
	// VERY IMPORTANT:
	// If the rotation is going to be controlled by a UIViewController
	// then the device orientation should be "Portrait".
	//
	// IMPORTANT:
	// By default, this template only supports Landscape orientations.
	// Edit the RootViewController.m file to edit the supported orientations.
	//
	[director setDeviceOrientation:kCCDeviceOrientationPortrait];

	
	[director setAnimationInterval:1.0/60];
	[director setDisplayFPS:NO];
	
	// Default texture format for PNG/BMP/TIFF/JPEG/GIF images
	// It can be RGBA8888, RGBA4444, RGB5_A1, RGB565
	// You can change anytime.
	[CCTexture2D setDefaultAlphaPixelFormat:kCCTexture2DPixelFormat_RGBA8888];
    
	// Removes the startup flicker
	[self removeStartupFlicker];
    
    // Load Textture Cache
    [self loadTextureToCache];
	
	// Run the intro Scene
	[[CCDirector sharedDirector] runWithScene: [MainScene node]];
    
    // make the OpenGLView a child of the view controller
	[viewController setView:glView];
	
	// make the View Controller a child of the main window
	[window addSubview: viewController.view];
	
	[window makeKeyAndVisible];
    
    // Add Observer for Send Email to Friends
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(sendEmailtoFriends)
                                                 name:kSendEmailFriend
                                               object:nil];
    
    // Add Observer for Show Options View
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(showOptionsView)
                                                 name:kShowOptionsView
                                               object:nil];
    
    // Add Observer for Show High Score Web View
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(showHighScoreWebView)
                                                 name:kShowHighScoreWebView
                                               object:nil];
    
    // Add Observer for Show Revmob Ad Link
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(showRevmobAdLink)
                                                 name:kShowRevmobAdsLink
                                               object:nil];
    
    // Init Ad View
    [viewController initAdView];
    [[NSNotificationCenter defaultCenter] postNotificationName:kShowAdViewNotification object:nil];
    
    // Init GameManager
    [GameManager sharedGameManager];
    
    // Promo Ads Start
    promoAds = [[PromoAds alloc] init];
    [self startPromoAds];
}

- (UIViewController *)getRootViewController
{
    return viewController;
}

- (void)startPromoAds
{
    [promoAds startPromoAds:viewController];
}

- (void)checkAndUpdateVersionIndex
{
    // Check update new version or not
    NSString * version = [NSDEF stringForKey:kVersionIndex];
    DLog(@"%@",version);
    if (version)
    {
        if (![version isEqualToString:[NSString stringWithFormat:@"%@",[[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"]]])
        { // Has New Version Update
            DLog(@"Has new update version");
            version = [NSString stringWithFormat:@"%@",[[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"]];
            [NSDEF setValue:version forKey:kVersionIndex];
            [NSDEF setBool:NO forKey:kL0SolicitReviewDoneAlreadyDefault];
            [NSDEF setInteger:0 forKey:kGameIndexNumber];
        }
    }
    else
    {
        // First Installed
        version = [NSString stringWithFormat:@"%@",[[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"]];
        [NSDEF setValue:version forKey:kVersionIndex];
    }
    DLog(@"%@",[NSDEF stringForKey:kVersionIndex]);
}

#pragma mark - Loading Texture Cache
- (void)loadTextureToCache
{
    CCSpriteFrameCache * cache = [CCSpriteFrameCache sharedSpriteFrameCache];
    
    float scale = 1.0;
    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)]) {
        scale=[[UIScreen mainScreen] scale];
    }
    
    if (IS_IPAD)
    {
        [cache addSpriteFramesWithFile:@"Buttons-hd.plist"];
    }
    else
    {
        if (scale == 1.0)
        {
            [cache addSpriteFramesWithFile:@"Buttons.plist"];
        }
        else
        {
            [cache addSpriteFramesWithFile:@"Buttons-hd.plist"];
        }
    }
    
}

#pragma mark - Show Options View
- (void) showOptionsView
{
    DLog();
    OptionsViewController * optionview = nil;
    
    if (IS_IPAD)
    {
        optionview = [[[OptionsViewController alloc] initWithNibName:@"OptionsViewiPad" bundle:nil] autorelease] ;
    }
    else if (IS_IPHONE5)
    {
        optionview = [[[OptionsViewController alloc] initWithNibName:@"OptionsViewiPhone5" bundle:nil] autorelease] ;
    }
    else
    {
        optionview = [[[OptionsViewController alloc] initWithNibName:@"OptionsViewiPhone" bundle:nil] autorelease] ;
    }
    
    [window.rootViewController presentViewController:optionview animated:YES completion:nil];
}

#pragma mark - Show High Score Web View
- (void)showHighScoreWebView
{
    DLog();
    HighScoreViewController * highscoreview = nil;
    
    if (IS_IPAD)
    {
        highscoreview = [[[HighScoreViewController alloc] initWithNibName:@"HighScoreViewControlleriPad"
                                                                   bundle:nil] autorelease];
    }
    else if(IS_IPHONE5)
    {
        highscoreview = [[[HighScoreViewController alloc] initWithNibName:@"HighScoreViewControlleriPhone5"
                                                                   bundle:nil] autorelease];
    }
    else
    {
        highscoreview = [[[HighScoreViewController alloc] initWithNibName:@"HighScoreViewControlleriPhone"
                                                                   bundle:nil] autorelease];
    }
    
    [window.rootViewController presentViewController:highscoreview animated:YES completion:nil];
    
}


#pragma mark - Send Email Function
- (void) sendEmailtoFriends
{
    int numofproblems = (int)[NSDEF integerForKey:kCurrentNumberProblem];
    float besttime = [[SecurityManager sharedManager] getBestTime:numofproblems];
    int bestAccuracy = [[SecurityManager sharedManager] getAccuracy:numofproblems];
    
    DLog(@"Number of Problems %i",numofproblems);
    DLog(@"Best Time %.2f",besttime);
    
    if (besttime <= 2.0)
    {
		DLog(@"No Record Yet");
        
        NSString *message = @"Play Brain Tuner, and then email your best time to your friends!";
        
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No Record Yet"
														message:message
													   delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        alert.tag = kNoRecordYetDialog;
		[alert show];
		[alert release];
		return;
	}
        
    if ([MFMailComposeViewController canSendMail])
    {
        
		MFMailComposeViewController *composeViewController = [[MFMailComposeViewController alloc] init];
        NSString *subject = [NSString stringWithFormat:@"My Brain Tuner record: %.2f seconds with %d%% Accuracy (%d problems)",
                                                 besttime,
                                                 bestAccuracy,
                                                 numofproblems];
        
        NSString *messageBody = [self emailBody:YES];
        
		[composeViewController setSubject:subject];
        
		[composeViewController setMessageBody:messageBody isHTML:YES];
        
		composeViewController.mailComposeDelegate = self;
        
		[window.rootViewController presentViewController:composeViewController animated:YES completion:nil];
        
		[composeViewController release];
	}
    else
    {
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Your device is unable to send email" message:@"Please check your Mail configuration" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[alert show];
		[alert release];
	}
    
}

- (NSString *)emailBody:(BOOL) isfriendstyle
{
    NSString * emailBody = @"";
    
    if (isfriendstyle)
    {
        // Fill out the email body text
        emailBody = [NSString stringWithFormat:@"Can you beat my record?<br><a href=\"%@\">Get Brain Tuner</a>, a free iPhone game, to find out!", kBrainTunerURLString];
        
        float timeFor20 = [[SecurityManager sharedManager] getBestTime:20];
        float timeFor60 = [[SecurityManager sharedManager] getBestTime:60];
        float timeFor100 = [[SecurityManager sharedManager] getBestTime:100];
        int accuracyFor20 = [[SecurityManager sharedManager] getAccuracy:20];
        int accuracyFor60 = [[SecurityManager sharedManager] getAccuracy:60];
        int accuracyFor100 = [[SecurityManager sharedManager] getAccuracy:100];
        
        if (timeFor20 > 2.0)
        {
            emailBody = [emailBody stringByAppendingFormat:@"<br><br>20 Problems: %.2f seconds with %d%% Accuracy", timeFor20,accuracyFor20];
        }
        if (timeFor60 > 6.0)
        {
            emailBody = [emailBody stringByAppendingFormat:@"<br><br>60 Problems: %.2f seconds with %d%% Accuracy", timeFor60,accuracyFor60];
        }
        if (timeFor100 > 10.0)
        {
            emailBody = [emailBody stringByAppendingFormat:@"<br><br>100 Problems: %.2f seconds with %d%% Accuracy", timeFor100,accuracyFor100];
        }
        
        emailBody = [NSString stringWithFormat:@"%@<br><br><a href=\"%@\">Brain Tuner</a> is a game for your iPhone, iPod touch, or iPad.", emailBody, kBrainTunerURLString];
    }
    else
    {
        NSString *applicationName = @"BrainTunerX.iOS";
        emailBody = [NSString stringWithFormat:@"\n\n\n=== Application information ===\n---    (please do not remove)   ---\n%@.v%@ %@\n%@\niOS %@\n=========================", applicationName, [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"], kBuildType, [[UIDevice currentDevice] model],[[UIDevice currentDevice] systemVersion]];
    }
	
	return emailBody;
}

#pragma mark - MFmail Controller Delegate
- (void)mailComposeController:(MFMailComposeViewController*)_controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
	[window.rootViewController dismissViewControllerAnimated:YES completion:nil];
}

- (void)showAdViewAgain
{
    [[NSNotificationCenter defaultCenter] postNotificationName:kShowAdViewNotification object:nil];
}

#pragma mark - Open Revmob Ad Link
- (void) showRevmobAdLink {
    DLog();
    [self showLoadingAlert];
    [RevMobAds session].testingMode = kTestRevmobAds ? RevMobAdsTestingModeWithAds:RevMobAdsTestingModeOff;
    [[RevMobAds session] openAdLinkWithDelegate:self];
}

#pragma mark - Revmob Ads Delegate
- (void)revmobAdDidReceive {
    DLog();
    [self dismissLoadingAlert];
}

- (void)revmobAdDidFailWithError:(NSError *)error {
    DLog();
    [self dismissLoadingAlert];
    
    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Can not link to More Games\nPlease try again"
                                                     message:nil
                                                    delegate:self
                                           cancelButtonTitle:@"Ok"
                                           otherButtonTitles:nil];
    [alert show];
    [alert release];
}

- (void)showLoadingAlert {
	if (!loadingAlert) { // just being defensive
		// show Loading... alert
		loadingAlert = [[UIAlertView alloc] initWithTitle:@"Loading..." message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:@"Hide", nil];
		[loadingAlert show];
	}
}

- (void)dismissLoadingAlert {
	if (loadingAlert) { // sending messages to nil is fine, but so is this
		[loadingAlert dismissWithClickedButtonIndex:[loadingAlert cancelButtonIndex] animated:NO];
        [loadingAlert release];
        loadingAlert = nil;
	}
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 0) {
        if (loadingAlert) {
            [loadingAlert release];
            loadingAlert = nil;
        }
    }
}


#pragma mark - FB Support Functions
- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url
{
    return [[AccountManager sharedManager] application:application
                                               openURL:url
                                     sourceApplication:nil
                                            annotation:nil];
}

// For iOS 4.2+ support
- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    return [[AccountManager sharedManager] application:application
                                               openURL:url
                                     sourceApplication:sourceApplication
                                            annotation:annotation];
}

// getting a call, pause the game
- (void)applicationWillResignActive:(UIApplication *)application
{
	[[CCDirector sharedDirector] pause];
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
	[[CCDirector sharedDirector] resume];
    
    // Configure Chartboost
    [Chartboost startWithAppId:@"50d40fa416ba477162000001" appSignature:@"a7badf55f71eccfddedee600bf8e0a0a0a177fd1" delegate:self];
    
    // Revmob Local Notification
    [RevMobAds startSessionWithAppID:@"507d1b611975420c0000004a"];
    [RevMobAds session].testingMode = kTestRevmobAds ? RevMobAdsTestingModeWithAds:RevMobAdsTestingModeOff;
    
    // This is deprecated
    // NSDate * date = [[NSDate alloc] initWithTimeIntervalSinceNow:kRevmobLocalNotificationScheduleTime]; // Schedule 7 days
    // [[RevMobAds session] scheduleLocalNotification:date];
}

- (void)applicationDidReceiveMemoryWarning:(UIApplication *)application
{
	[[CCDirector sharedDirector] purgeCachedData];
}

-(void) applicationDidEnterBackground:(UIApplication*)application
{
	[[CCDirector sharedDirector] stopAnimation];
}

-(void) applicationWillEnterForeground:(UIApplication*)application
{
    [self startPromoAds];
	[[CCDirector sharedDirector] startAnimation];
}

- (void)applicationWillTerminate:(UIApplication *)application
{
	CCDirector *director = [CCDirector sharedDirector];
	
	[[director openGLView] removeFromSuperview];
	
	[viewController release];
	
	[window release];
	
	[director end];
}

- (void)applicationSignificantTimeChange:(UIApplication *)application
{
	[[CCDirector sharedDirector] setNextDeltaTimeZero:YES];
}

- (void)dealloc
{
	[[CCDirector sharedDirector] end];
	[window release];
	[super dealloc];
}

#pragma mark - Apple Push Notifications
- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    PFInstallation *currentInstallation = [PFInstallation currentInstallation];
    [currentInstallation setDeviceTokenFromData:deviceToken];
    [currentInstallation saveInBackground];
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *) error
{
    DLog(@"Failed To Register For Remote Notifications With Error: %@", error);
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    DLog(@"Received remote notification: %@", userInfo);
    [PFPush handlePush:userInfo];
}

@end

