//
//  MainScene.h
//  BrainTunerX
//
//  Created by Leon Yuu on 10/5/12.
//
//

#import <UIKit/UIKit.h>
#import "cocos2d.h"
#import "GameUtils.h"
#import "PlayScene.h"
#import "AccountManager.h"
#import "GSAudio.h"
#import "AdsScene.h"
#import <Twitter/Twitter.h>

@interface MainScene : CCScene <UIAlertViewDelegate>{
    
    CCSprite * gameBackGround;
    CCSprite * brainGuy;
    CCSprite * brainBalloon;
    CCSprite * gameTitle;
    
    CCMenuItemImage * infoButton;
    CCMenuItemImage * viewHighScoreButton;
    CCMenuItemImage * moreButton;
    CCMenuItemImage * resetButton;
    CCMenuItemImage * emailButton;
    CCMenuItemImage * facebookButton;
    CCMenuItemImage * twitterButton;
    CCMenuItemImage * game20Button;
    CCMenuItemImage * game60Button;
    CCMenuItemImage * game100Button;
    CCMenuItemImage * startButton;
    
    CCLabelTTF * bestTime;
    CCLabelTTF * share;
    
    int currentNumberProblem;
    int currentDifficulty;
    int maxDifficulty;
    
    BOOL hasTwitted;
    
}

@end
