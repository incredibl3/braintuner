//
//  PlayScene.h
//  BrainTunerX
//
//  Created by Leon Yuu on 10/10/12.
//
//

#import <UIKit/UIKit.h>
#import "CCScene.h"
#import "cocos2d.h"
#import "GameUtils.h"
#import "MainScene.h"
#import "SWTableView.h"
#import "GameManager.h"
#import "MyCell.h"
#import "SubmitScoreManager.h"
#import "Reachability.h"
#import "GSAudio.h"
#import "AppDelegate.h"
#import "AccountManagementViewController.h"

@interface PlayScene : CCScene<SWTableViewDataSource,SWTableViewDelegate,UIAlertViewDelegate,SubmitScoreDelegate>{
    
    // Cocos Object
    CCSprite * headerBar;
    CCSprite * bottomBar;
    CCSprite * problemBackgroundSprite;
    CCSprite * playBarStatus;
    
    CCLabelTTF * brainTunerText;
    CCLabelTTF * rightAnswerText;
    CCLabelTTF * countDownText;
    
    CCMenuItemImage * rightButton;
    CCMenuItemImage * wrongButton;
    CCMenuItemImage * backButton;
    
    SWTableView * tableView;
    
    // Other Objects
    UIAlertView * submittingAlertView;
    SubmitScoreManager * submitScoreManager;
    BOOL isReviewing;
    
}

@end
