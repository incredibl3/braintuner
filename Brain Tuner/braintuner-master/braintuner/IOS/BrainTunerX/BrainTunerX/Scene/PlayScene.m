//
//  PlayScene.m
//  BrainTunerX
//
//  Created by Leon Yuu on 10/10/12.
//
//

#import "PlayScene.h"
#define kBackButton 0
#define kRightButton 1
#define kWrongButton 2

#define kBarHeight                  ((IS_IPAD) ? 132:66)
#define kBrainTunerTextSize         ((IS_IPAD) ? 70:35)
#define kRightAnswerTextSize        ((IS_IPAD) ? 50:25)
#define kCountDownTextSize          ((IS_IPAD) ? 170:85)

#define kSWTableViewCellHeight      ((IS_IPAD) ? 140:60)
#define kSWTableViewCellTextSize    ((IS_IPAD) ? 64:32)
#define kSWTableViewLabelOffset     ((IS_IPAD) ? 450:167)
#define kSWTableViewLabelHeight     ((IS_IPAD) ? 150:((IS_IPHONE5) ? 72:70))
#define kSWTableViewImageOffset     ((IS_IPAD) ? 600:250)
#define kPlayBarStatusSprite        ((IS_IPAD) ? @"PlayBarStatus@2x.png":@"PlayBarStatus.png")

@implementation PlayScene

- (id) init{
    self = [super init];
    if(self){
        // Get Win Size
        CGSize winsize = [[CCDirector sharedDirector] winSize];;
        
        // Init Background Color
        CCLayer * backgroundLayer = [[CCLayerColor alloc] initWithColor:ccc4(255,255,255,255)
                                                                  width:winsize.width
                                                                 height:winsize.height];
        backgroundLayer.position = ccp(0,0);
		[self addChild:backgroundLayer];
        
        // Init Bar Height Value
        int barheight = kBarHeight;
        
        // Init Play Bar Status
        playBarStatus = [CCSprite spriteWithFile:kPlayBarStatusSprite];
        playBarStatus.scale = 0.0f;
        playBarStatus.position = ccp(winsize.width + playBarStatus.contentSize.width/4,winsize.height);
        [self addChild:playBarStatus];
        
        // Init Problem Background
        float bottomoffset = 0;
        if(IS_IPAD){
            bottomoffset = 470;
        }else if(IS_IPHONE5){
            bottomoffset = 225;
        }else{
            bottomoffset = 215;
        }
        problemBackgroundSprite = [CCSprite spriteWithSpriteFrameName:@"PlaySelectionBar.png"];
        problemBackgroundSprite.scale = winsize.width/problemBackgroundSprite.contentSize.width;
		problemBackgroundSprite.position =  ccp(winsize.width/2, bottomoffset);
        problemBackgroundSprite.visible = NO;
        [self addChild:problemBackgroundSprite];
        
        // Init Table View
        tableView = [SWTableView viewWithDataSource:self
                                               size:CGSizeMake(winsize.width, winsize.height - kBarHeight)];
        [self addChild:tableView];
        tableView.visible = NO;
        
        // Init Header Bar
        headerBar = [CCSprite spriteWithSpriteFrameName:@"PlayBarBottom.png"];
        headerBar.scaleX = winsize.width/headerBar.contentSize.width;
        headerBar.scaleY = (barheight + 2)/headerBar.contentSize.height;
        headerBar.position = ccp(winsize.width/2,winsize.height - headerBar.scaleY*headerBar.contentSize.height/2);
        [self addChild:headerBar];
        
        // Init Bottom Bar
        bottomBar = [CCSprite spriteWithSpriteFrameName:@"PlayBarBottom.png"];
        bottomBar.scaleX = winsize.width/bottomBar.contentSize.width;
        bottomBar.scaleY = (barheight - 4)/bottomBar.contentSize.height;
        bottomBar.position = ccp(winsize.width/2, bottomBar.scaleY*bottomBar.contentSize.height/2);
        [self addChild:bottomBar];
        
        // Re-positionize Play Bar Status
        playBarStatus.scaleX = 1.0f;
        playBarStatus.scaleY = (winsize.height/playBarStatus.contentSize.height);
        playBarStatus.position = ccp(winsize.width + playBarStatus.contentSize.width/4,
                                     winsize.height - barheight + 2 + playBarStatus.scaleY*playBarStatus.contentSize.height/2);
        
        // Init Brain Tuner Title
        brainTunerText = [CCLabelTTF labelWithString:@"Brain Tuner"
                                            fontName:kDenneKittenHeelsFont
                                            fontSize:kBrainTunerTextSize];
        brainTunerText.color = ccWHITE;
        brainTunerText.position = ccp(winsize.width/2 - winsize.width/50,
                                      winsize.height - barheight/2);
        [self addChild:brainTunerText];
        
        // Init Count Down Text
        countDownText = [CCLabelTTF labelWithString:@"3"
                                           fontName:kHelveticaBoldFont
                                           fontSize:kCountDownTextSize];
        countDownText.color = ccBLACK;
        countDownText.position = ccp(winsize.width/2,winsize.height/2);
        countDownText.visible = NO;
        [self addChild:countDownText];
        
        // Init Buttons
        // Init Back Button
        CCMenu * menu = [CCMenu menuWithItems:nil];
        menu.position = ccp(winsize.width/2,winsize.height/2);
        menu.anchorPoint = CGPointZero;
        [self addChild:menu];
        
        backButton = [CCMenuItemImage itemFromNormalSprite:[CCSprite spriteWithSpriteFrameName:@"PlayBackUnpress.png"]
                                            selectedSprite:[CCSprite spriteWithSpriteFrameName:@"PlayBackPress.png"]
                                                    target:self
                                                  selector:@selector(onClickButton:)];
        backButton.tag = kBackButton;
        backButton.position = ccp(-winsize.width/2 + winsize.width/40 + backButton.contentSize.width/2,
                                  winsize.height/2 - barheight/2);
        [menu addChild:backButton];
        
        // Init Wrong Button
        wrongButton = [CCMenuItemImage itemFromNormalSprite:[CCSprite spriteWithSpriteFrameName:@"PlayWrongUnpress.png"]
                                             selectedSprite:[CCSprite spriteWithSpriteFrameName:@"PlayWrongPress.png"]
                                                     target:self
                                                   selector:@selector(onClickButton:)];
        wrongButton.tag = kWrongButton;
        wrongButton.position = ccp(-winsize.width/4,-winsize.height/2 + (barheight - 4)/2);
        [menu addChild:wrongButton];
        
        // Init Right Button
        rightButton = [CCMenuItemImage itemFromNormalSprite:[CCSprite spriteWithSpriteFrameName:@"PlayRightUnpress.png"]
                                             selectedSprite:[CCSprite spriteWithSpriteFrameName:@"PlayRightPress.png"]
                                                     target:self
                                                   selector:@selector(onClickButton:)];
        rightButton.tag = kRightButton;
        rightButton.position = ccp(winsize.width/4,-winsize.height/2 + (barheight - 4)/2);
        [menu addChild:rightButton];
        
        // Init Sounds
        if([NSDEF boolForKey:OPT_SOUND]){
            [GSAudio load:kCountDownSound];
            [GSAudio load:kWrongSound];
            [GSAudio load:kRightSound];
            [GSAudio load:kCompleteGame];
            [GSAudio load:kButtonClick];
        }
        
        // Prepare Game
        [[GameManager sharedGameManager] prepareGame];
        
        // Init Right Answer Text
        rightAnswerText = [CCLabelTTF labelWithString:@""
                                             fontName:kDenneKittenHeelsFont
                                             fontSize:kRightAnswerTextSize];
        [rightAnswerText setString:[NSString stringWithFormat:@"%i / %i",
                                    [GameManager sharedGameManager].correctAnswer,
                                    [GameManager sharedGameManager].TotalNumberProblems]];
        rightAnswerText.color = ccGRAY;
        rightAnswerText.position = ccp(winsize.width - winsize.width/50 - winsize.width/8,
                                       winsize.height - barheight/2);
        [self addChild:rightAnswerText];
        
        // Start Game
        if([NSDEF boolForKey:OPT_COUNTDOWN]){
            if([NSDEF boolForKey:OPT_SOUND]) [self performSelector:@selector(startBlip)
                                                        withObject:nil
                                                        afterDelay:0.0f];
            countDownText.visible = YES;
            [self schedule:@selector(countDown:) interval:1];
        }else{
            [self onStart];
        }
        
    }
    
    return self;
}

#pragma mark - Handle Event Functions
- (void)onClickButton:(id)sender
{
    int tag = (int)[sender tag];
    switch (tag)
    {
        case kBackButton:
            
            if ([NSDEF boolForKey:OPT_SOUND]) [GSAudio play:kButtonClick];
            if (isReviewing)
            {
                isReviewing = NO;
                tableView.scrollable = NO;
                [self showPlayResultDialog:YES]; // Show Submit Score Dialog Here
            }
            else
            {
                [GSAudio stop:kCountDownSound];
                [self unschedule:@selector(countDown:)];
                
                // Back to Main Scene
                [self backtoMainScene];
                
            }
            break;
            
        case kRightButton:
            
            [self onRightButton];
            break;
            
        case kWrongButton:
            
            [self onWrongButton];
            break;
            
        default:
            break;
    }
}

- (void) onRightButton {
    if([GameManager sharedGameManager].isGameRunning){
        GameManager *gameManager = [GameManager sharedGameManager];
        if([NSDEF boolForKey:OPT_SOUND]){
            Problem * problemobj = [gameManager.problemArray objectAtIndex:gameManager.currentIndex];
            if(problemobj.isRight){
                [GSAudio play:kRightSound];
            }else{
                [GSAudio play:kWrongSound];
            }
        }
        ProblemResult problemResult = [gameManager updateProblemResult:YES];
        [self invalidateTableView:problemResult];
    }
}

- (void) onWrongButton {
    if([GameManager sharedGameManager].isGameRunning){
        GameManager *gameManager = [GameManager sharedGameManager];
        if([NSDEF boolForKey:OPT_SOUND]){
            Problem * problemobj = [gameManager.problemArray objectAtIndex:gameManager.currentIndex];
            if(problemobj.isRight){
                [GSAudio play:kWrongSound];
            }else{
                [GSAudio play:kRightSound];
            }
        }
        ProblemResult problemResult = [gameManager updateProblemResult:NO];
        [self invalidateTableView:problemResult];
    }
}

- (void) invalidateTableView:(ProblemResult)problemResult {
    DLog();
    GameManager *gameManager = [GameManager sharedGameManager];
    
    // manually update corresponding cell by code, don't call reloadData
    [tableView updateCellAtIndex:gameManager.currentIndex + kPlaySceneTotalEmptyRows/2];
    [tableView insertCellAtIndex:[gameManager.problemArray count] + kPlaySceneTotalEmptyRows];
    
    // Increase status bar
    CGSize winsize = [[CCDirector sharedDirector] winSize];
    float maxBarHeight = winsize.height - 2*kBarHeight + 6;
    float increase = maxBarHeight/gameManager.TotalNumberProblems;
    playBarStatus.position = ccp(winsize.width + playBarStatus.contentSize.width/4,
                                 playBarStatus.position.y - increase);
    
    // Update Correct Answer Text
    [rightAnswerText setString:[NSString stringWithFormat:@"%i / %i",
                                gameManager.correctAnswer,
                                gameManager.TotalNumberProblems]];
    
    if (gameManager.currentIndex >= gameManager.TotalNumberProblems) {
        [self onEnd];
		return;
	}
    
    // Set Content Offset
    [tableView setContentOffset:ccp(0,
                                    [tableView minContainerOffset].y +
                                    kSWTableViewLabelHeight * (gameManager.currentIndex))
             animatedInDuration:0.15];
}


#pragma mark - Game Flows
- (void) startBlip{
    [GSAudio play:kCountDownSound];
}

- (void) countDown:(float)dt{
    DLog(@"%f",dt);
    int value = (int) ([[countDownText string] intValue] - 1);
    if(value > 0){
        [countDownText setString:[NSString stringWithFormat:@"%i",value]];
    }else{
        [GSAudio stop:kCountDownSound];
        [self unschedule:@selector(countDown:)];
        countDownText.visible = NO;
        [self performSelector:@selector(onStart) withObject:nil afterDelay:0.05f];
    }
}

- (void) onStart {
    
    problemBackgroundSprite.visible = YES;
    
    // Init Table View Settings
    tableView.direction = SWScrollViewDirectionVertical;
    tableView.position = ccp(0, 0);
    tableView.delegate = self;
    tableView.verticalFillOrder = SWTableViewFillTopDown;
    [tableView setContentOffset:ccp(0,[tableView minContainerOffset].y)];
    tableView.visible = YES;
    [tableView reloadData];
    
    [[GameManager sharedGameManager] startGame];
}

- (void) onEnd {
    DLog();
    if([NSDEF boolForKey:OPT_SOUND]) [GSAudio play:kCompleteGame];
    [rightButton setSelectedImage:[CCSprite spriteWithSpriteFrameName:@"PlayRightUnpress.png"]];
    [wrongButton setSelectedImage:[CCSprite spriteWithSpriteFrameName:@"PlayWrongUnpress.png"]];
    [[GameManager sharedGameManager] endGame];
    [self performSelector:@selector(showPlayResultDialog:) withObject:nil afterDelay:0.6f];
    
}

#pragma mark - Submitting Global High Score
- (void)showPlayResultDialog:(BOOL) withUserID
{
    // Init Play Result
    GameManager * gameManager = [GameManager sharedGameManager];
	CFTimeInterval time = gameManager.playTime;
	int correct = gameManager.correctAnswer;
	int incorrect = gameManager.incorrectAnswer;
	int accuracy = (100 * correct) / gameManager.TotalNumberProblems; //self getProblemCount
	float penalty = 5 * incorrect;
    CFTimeInterval finalTime = time + penalty;
    gameManager.finalTime = finalTime;
    gameManager.Accuracy = accuracy;
	
    // Init Record
    NSString * newRecord = @"";
    newRecord = [self storeGameRecord:finalTime Accuracy:accuracy NumberofProblems:gameManager.TotalNumberProblems];
	
    // Init Title And Message for Result Dialog
	NSString *title;
	NSString *message;
	if (finalTime >= 2.5 * gameManager.TotalNumberProblems)
    {
		if (accuracy == 100)
			title = @"Perfect But Slow";
		else
			title = @"Not Very Good";
		message = @"Get your brain in-tune\nby trying again!";
	}
    else if (finalTime >= 2 * gameManager.TotalNumberProblems)
    {
		if (accuracy == 100)
			title = @"Try For More Speed";
		else
			title = @"Try Harder";
		message = @"You'll get better with practice.\nTry again!";
	}
    else if (finalTime >= 1.5 * gameManager.TotalNumberProblems)
    {
		title = @"Good Job";
		message = @"That's a good time! Keep improving.";
	}
    else if (finalTime >= gameManager.TotalNumberProblems)
    {
		title = @"Great Speed";
		message = @"Nice job.";
	}
    else if (finalTime >= 0.5 * gameManager.TotalNumberProblems)
    {
		title = @"Congratulations!";
		message = @"You're amazing!";
	}
    else
    {
		title = @"Expert";
		message = @"WOW!";
	}
    
    if (withUserID)
    {
        title = @"Global High Score";
        message = [NSString stringWithFormat:@"Your score is %.2f sec\n Submit global highscore with ID",finalTime];
    }
    else
    {
        message = [NSString stringWithFormat:@"%.2f sec. + %.f penalty =\n%.2f seconds.%@\n%d%% Accuracy.\n%@", time, penalty, finalTime, newRecord, accuracy, message];
    }
    
    
    // Init Alert View
    if (!withUserID)
    {
        int number = (int)[NSDEF integerForKey:kGameIndexNumber];
        number ++;
        [NSDEF setInteger:number forKey:kGameIndexNumber];
        DLog(@"Game Index %d",number);
        
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:title
                                                         message:message
                                                        delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        alert.tag = kResultDialogTag;
        if ([NSDEF boolForKey:OPT_REV] && [GameManager sharedGameManager].incorrectAnswer > 0)
        {
            [alert addButtonWithTitle:@"Review"];
        }
        else
        {
            [alert addButtonWithTitle:@"Submit Score"];
        }
        
        [alert show];
        [alert release];
    }
    else
    {
        // Present Account Manager View Controller
        DLog("Show Account Manager View Controller");
        AccountManagementViewController * accountmanagercontroller = nil;
        if (IS_IPAD)
        {
            accountmanagercontroller = [[[AccountManagementViewController alloc]
                                        initWithNibName:@"AccountManagementViewControlleriPad" bundle:nil] autorelease];
        }
        else if (IS_IPHONE5)
        {
            accountmanagercontroller = [[[AccountManagementViewController alloc]
                                        initWithNibName:@"AccountManagementViewControlleriPhone5" bundle:nil] autorelease];
        }
        else
        {
            accountmanagercontroller = [[[AccountManagementViewController alloc]
                                        initWithNibName:@"AccountManagementViewControlleriPhone" bundle:nil] autorelease];
        }
        
        [[UIAppDelegate getRootViewController] presentViewController:accountmanagercontroller animated:YES completion:nil];
        
        [self playSoundButtonClick];
        [GameManager sharedGameManager].isGameRunning = NO;
        // [self performSelector:@selector(backtoMainScene) withObject:self afterDelay:0.5f];
    }
}

- (void)showSubmittingAlert {
	if (!submittingAlertView) { // just being defensive
		submittingAlertView = [[UIAlertView alloc] initWithTitle:@"Global High Score"
                                                       message:@"Submitting..."
                                                      delegate:self
                                             cancelButtonTitle:nil
                                             otherButtonTitles:@"Cancel", nil];
		[submittingAlertView show];
	}
}

- (void)dismissLoadingAlert {
	if (submittingAlertView) { // sending messages to nil is fine, but so is this
		[submittingAlertView dismissWithClickedButtonIndex:[submittingAlertView cancelButtonIndex] animated:NO];
		[submittingAlertView release];
        submittingAlertView = nil;
	}
}

- (void) showInternetConnectionError {
    UIAlertView * alertView = [[UIAlertView alloc] initWithTitle:@"No Internet Connection"
                                                         message:@"Can not submit your score to Global High Score"
                                                        delegate:self
                                               cancelButtonTitle:@"Ok"
                                               otherButtonTitles:nil];
    alertView.tag = kInternetErrorDialogTag;
    [alertView show];
    [alertView release];
}

- (void) showReviewDialog {
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Review Answers:\nScrolling Enabled"
                                                    message:[NSString stringWithFormat:@"Since you missed %d problems,\nyou may now review your answers\nby scrolling the list of problems.\nTap 'X' at the top when finished.",[GameManager sharedGameManager].incorrectAnswer]
                                                   delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    alertView.tag = kReviewDialogTag;
    [alertView show];
    [alertView release];
}

- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    DLog();
    if(alertView.tag == kResultDialogTag){
        switch (buttonIndex) {
            case 0: // Cancel Button
                
                [self backtoMainScene];
                
                break;
            case 1: // Ok Button
                
                if([NSDEF boolForKey:OPT_REV] && [GameManager sharedGameManager].incorrectAnswer > 0){
                    // Show Review Dialog Here
                    [backButton setNormalImage:[CCSprite spriteWithSpriteFrameName:@"PlayCloseUnpress.png"]];
                    [backButton setSelectedImage:[CCSprite spriteWithSpriteFrameName:@"PlayClosePress.png"]];
                    [self showReviewDialog];
                }else{
                    [self showPlayResultDialog:YES];
                }
                break;
            default:
                break;
        }

    }else if(alertView.tag == kSubmittingDialogTag){ // Submitting Dialog
        
        // Back to Main Scene
        [self backtoMainScene];
        
    }else if (alertView.tag == kInternetErrorDialogTag){ // Internet Error Dialog
        
        // Back to Main Scene
        [self backtoMainScene];
        
    }else if(alertView.tag == kSubmitScoreDoneDialogTag){ // Submit Done Dialog
        
        // Back to Main Scene
        [self backtoMainScene];
        
    }else if(alertView.tag == kSubmitScoreFailedDialogTag){ // Submit Failed Dialog
        
        // Back to Main Scene
        [self backtoMainScene];
        
    }else if(alertView.tag == kReviewDialogTag){
        // Call Review Section Here
        DLog();
        [self playSoundButtonClick];
        isReviewing = YES;
        tableView.scrollable = YES;
    }
}

- (NSString *)storeGameRecord:(CFTimeInterval) playTime Accuracy:(int) accuracy NumberofProblems:(int) numofproblems{
    DLog();
    return [[GameManager sharedGameManager] storeRecord:playTime Accuracy:accuracy withTotalNumberProblems:numofproblems];
}

- (void) backtoMainScene {
    [self playSoundButtonClick];
    [GameManager sharedGameManager].isGameRunning = NO;
    [[CCDirector sharedDirector] replaceScene:[CCTransitionPageTurn transitionWithDuration:0.5f
                                                                                     scene:[MainScene node]
                                                                                 backwards:YES]];
    [self performSelector:@selector(showAdViewAgain) withObject:self afterDelay:0.5f];
}

- (void) showAdViewAgain {
    [[NSNotificationCenter defaultCenter] postNotificationName:kShowAdViewNotification object:nil];
}

- (void) playSoundButtonClick {
    if([NSDEF boolForKey:OPT_SOUND]) [GSAudio play:kButtonClick];
}

#pragma mark - Submit Score Manager Delegate
- (BOOL) onSubmitGlobalHighScoreFinished{
    [self dismissLoadingAlert];
    UIAlertView * alertView = [[UIAlertView alloc] initWithTitle:@"Global High Score"
                                                         message:@"Submit your score successfully!"
                                                        delegate:self
                                               cancelButtonTitle:@"Ok"
                                               otherButtonTitles:nil];
    alertView.tag = kSubmitScoreDoneDialogTag;
    [alertView show];
    [alertView release];

    return YES;
}

- (BOOL) onSubmitGlobalHighScoreFailed {
    [self dismissLoadingAlert];
    UIAlertView * alertView = [[UIAlertView alloc] initWithTitle:@"Global High Score"
                                                         message:@"Submit your score failed!"
                                                        delegate:self
                                               cancelButtonTitle:@"Ok"
                                               otherButtonTitles:nil];
    alertView.tag = kSubmitScoreFailedDialogTag;
    [alertView show];
    [alertView release];
    
    return YES;
}

#pragma mark - SWTable View Data Source
-(CGSize)cellSizeForTable:(SWTableView *)table {
    CGSize winsize = [CCDirector sharedDirector].winSize;
    return CGSizeMake(winsize.width, kSWTableViewLabelHeight);
}

-(NSUInteger)numberOfCellsInTableView:(SWTableView *)table {
    return [GameManager sharedGameManager].TotalNumberProblems + kPlaySceneTotalEmptyRows;
}

-(SWTableViewCell *)table:(SWTableView *)table cellAtIndex:(NSUInteger)idx {
    DLog();
    
    SWTableViewCell *cell = [table dequeueCell];
    CGSize winsize = [[CCDirector sharedDirector] winSize];
	GameManager *gameManager = [GameManager sharedGameManager];
	
	if (idx > kPlaySceneTotalEmptyRows/2 && idx <= gameManager.TotalNumberProblems + kPlaySceneTotalEmptyRows/2) {
        
        int index = (int)idx - 1 - kPlaySceneTotalEmptyRows/2;
        
        if (!cell) {
            cell =  [[MyCell new] autorelease];
            
            CCLabelTTF * label = [CCLabelTTF labelWithString:[gameManager getProblemTextatIndex:index]
                                                  dimensions:CGSizeMake(winsize.width - 20, kSWTableViewCellHeight)
                                                   alignment:NSTextAlignmentLeft
                                                    fontName:kHelveticaBoldFont
                                                    fontSize:kSWTableViewCellTextSize];
            label.position = ccp(kSWTableViewLabelOffset,
                                 kSWTableViewLabelHeight);
            label.color = ccBLACK;
            label.tag = kTagForLabel;
            [cell addChild:label];
            
        } else {
            CCLabelTTF *label = (CCLabelTTF *)[cell getChildByTag:kTagForLabel];
            [label setString:[gameManager getProblemTextatIndex:index]];
            [cell removeChildByTag:kTagForSymbol cleanup:YES];
        }
		
		if ([gameManager getProblemResultatIndex:index] == kResultRight) {
			
			CCSprite *sprite = [CCSprite spriteWithSpriteFrameName:@"PlayRightSymbol.png"];
			sprite.anchorPoint = CGPointZero;
			sprite.position = ccp(kSWTableViewImageOffset, kSWTableViewLabelHeight);
            sprite.tag = kTagForSymbol;
			[cell addChild:sprite];
            
            float durationScale = 1.0f;
            if (gameManager.currentIndex >= gameManager.TotalNumberProblems) {
                durationScale = 4.0;
            }
            
            if (gameManager.currentIndex == idx - kPlaySceneTotalEmptyRows / 2) {
                id action1 = [CCScaleBy actionWithDuration:0.15*durationScale scale:1.2];
                id action2 = [CCScaleBy actionWithDuration:0.15*durationScale scale:0.8];
                id action3 = [CCRotateBy actionWithDuration:0.1*durationScale angle:5];
                id action4 = [CCRotateBy actionWithDuration:0.1*durationScale angle:-5];
                if(gameManager.isGameRunning) [sprite runAction: [CCSequence actions:action1, action2, action3, action4, nil]];
            }
			
		} else if ([gameManager getProblemResultatIndex:index] == kResultWrong) {
            
			CCSprite *sprite = [CCSprite spriteWithSpriteFrameName:@"PlayWrongSymbol.png"];
			sprite.anchorPoint = CGPointZero;
			sprite.position = ccp(kSWTableViewImageOffset, kSWTableViewLabelHeight);
            sprite.tag = kTagForSymbol;
			[cell addChild:sprite];
            
            if (gameManager.currentIndex == idx - kPlaySceneTotalEmptyRows / 2) {
                
                // Add slow motion here
                
                float durationScale = 1.0f;
                if (gameManager.currentIndex >= gameManager.TotalNumberProblems) {
                    durationScale = 4.0;
                }
                
                id action1 = [CCScaleBy actionWithDuration:0.15*durationScale scale:2.0];
                id action2 = [CCScaleBy actionWithDuration:0.15*durationScale scale:0.5];
                id action3 = [CCRotateBy actionWithDuration:0.1*durationScale angle:5];
                id action4 = [CCRotateBy actionWithDuration:0.1*durationScale angle:-5];
                if(gameManager.isGameRunning) [sprite runAction: [CCSequence actions:action1, action2, action3, action4, nil]];
            }
            
		}
        
    } else {
        if (!cell) {
            cell =  [[MyCell new] autorelease];
            CCLabelTTF * label = [CCLabelTTF labelWithString:@""
                                                  dimensions:CGSizeMake(winsize.width - 20, kSWTableViewCellHeight)
                                                   alignment:NSTextAlignmentLeft
                                                    fontName:kHelveticaBoldFont
                                                    fontSize:kSWTableViewCellTextSize];
            label.position = ccp(kSWTableViewLabelOffset,
                                 kSWTableViewLabelHeight);
            label.color = ccBLACK;
            label.tag = kTagForLabel;
            [cell addChild:label];
            
        } else {
            CCLabelTTF *label = (CCLabelTTF *)[cell getChildByTag:kTagForLabel];
            [label setString:@""];
            [cell removeChildByTag:kTagForSymbol cleanup:YES];
        }
    }
    
    return cell;
}

#pragma mark - SWTable View Delegate
-(void)table:(SWTableView *)table cellTouched:(SWTableViewCell *)cell {
    DLog(@"cell touched at index: %d", (int)cell.idx);
}

- (void) dealloc{
    [submitScoreManager release];
    submitScoreManager = nil;
    [super dealloc];
}

@end
