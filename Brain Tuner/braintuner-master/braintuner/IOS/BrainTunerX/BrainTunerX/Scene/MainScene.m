//
//  MainScene.m
//  BrainTunerX
//
//  Created by Leon Yuu on 10/5/12.
//
//

#import "MainScene.h"

#define kGameBackGround     ((IS_IPAD) ? @"HomeBackground@2x.png" : @"HomeBackground.png")
#define kBrainGuy           ((IS_IPAD) ? @"BrainGuy@2x.png" : @"BrainGuy.png")
#define kButton20Unpress    ((IS_IPAD) ? @"Button20Unpress@2x.png":@"Button20Unpress.png")
#define kButton20Press      ((IS_IPAD) ? @"Button20Press@2x.png":@"Button20Press.png")
#define kButton60Unpress    ((IS_IPAD) ? @"Button60Unpress@2x.png":@"Button60Unpress.png")
#define kButton60Press      ((IS_IPAD) ? @"Button60Press@2x.png":@"Button60Press.png")
#define kButton100Unpress   ((IS_IPAD) ? @"Button100Unpress@2x.png":@"Button100Unpress.png")
#define kButton100Press     ((IS_IPAD) ? @"Button100Press@2x.png":@"Button100Press.png")
#define kButtonResetUnpress ((IS_IPAD) ? @"ResetUnpress@2x.png":@"ResetUnpress.png")
#define kButtonResetPress   ((IS_IPAD) ? @"ResetPress@2x.png":@"ResetPress.png")
#define kButtonEmailUnpress ((IS_IPAD) ? @"SendEmailUnpress@2x.png":@"SendEmailUnpress.png")
#define kButtonEmailPress   ((IS_IPAD) ? @"SendEmailPress@2x.png":@"SendEmailPress.png")
#define kButtonStartUnpress ((IS_IPAD) ? @"HomeStartButtonUnpress@2x.png":@"HomeStartButtonUnpress.png")
#define kButtonStartPress   ((IS_IPAD) ? @"HomeStartButtonPress@2x.png":@"HomeStartButtonPress.png")
#define kMoreButtonUnpress  ((IS_IPAD) ? @"MoreButtonUnpress@2x.png":@"MoreButtonUnpress.png")
#define kMoreButtonPress    ((IS_IPAD) ? @"MoreButtonPress@2x.png":@"MoreButtonPress.png")


#define kGame20Button               0
#define kGame60Button               1
#define kGame100Button              2
#define kStartButton                3
#define kInfoButton                 4
#define kResetButton                5
#define kEmailButton                6
#define kFaceBookButton             7
#define kTwitterButton              8
#define kPreviousDifficultButton    9
#define kNextDifficultButton        10
#define kViewHighScoreButton        11
#define kMoreButton                 12

#define kLabelSize                  ((IS_IPAD) ? 40:20)
#define kBottomMargin               ((IS_IPAD) ? 95:48)
#define kLeftMargin                 ((IS_IPAD) ? 96:32)

#define kViewHighScoreURL           @"http://greengarstudios.com/bt2/index.php/brain_tuner_scorer/view_global_scorer"

@implementation MainScene

- (id)init
{
    self = [super init];
    if (self != nil)
    {
        DLog();
        
#pragma mark - Pre-init Values for building layout
        // Get Default Number Problem
        if([NSDEF integerForKey:kCurrentNumberProblem] == 0) [NSDEF setInteger:20 forKey:kCurrentNumberProblem];
        currentNumberProblem = (int)[NSDEF integerForKey:kCurrentNumberProblem];
        
        // Get Default Difficulty
        if([NSDEF integerForKey:kCurrentDifficulty] == 0) [NSDEF setInteger:1 forKey:kCurrentDifficulty];
        currentDifficulty = (int)[NSDEF integerForKey:kCurrentDifficulty];
        
        // Get Max Difficulty
        if([NSDEF integerForKey:kMaxDifficulty] == 0) [NSDEF setInteger:1 forKey:kMaxDifficulty];
        maxDifficulty = (int)[NSDEF integerForKey:kMaxDifficulty];
        
        // Get Win Size
        CGSize winsize = [[CCDirector sharedDirector] winSize];;
        
        // Init Background Color
        CCLayerColor * bg = [CCLayerColor layerWithColor:ccc4(255.0, 255.0, 255.0, 255.0)
                                                   width:winsize.width
                                                  height:winsize.height];
        bg.position = ccp(0,0);
        [self addChild:bg];
        
        // Init Game Background
        gameBackGround = [CCSprite spriteWithFile:kGameBackGround];
        gameBackGround.scaleX = (winsize.width/gameBackGround.contentSize.width);
        gameBackGround.scaleY = (winsize.height/gameBackGround.contentSize.height);
        gameBackGround.position = ccp(winsize.width/2,winsize.height/2);
        [self addChild:gameBackGround];
        
        // Init CCMenu Button
        CCMenu * menu = [CCMenu menuWithItems:nil];
        menu.position = ccp(winsize.width/2,winsize.height/2);
        menu.anchorPoint = CGPointZero;
        [self addChild:menu];
        
#pragma mark - Game Title Line
        // Init Game Title
        gameTitle = [CCSprite spriteWithSpriteFrameName:@"HomeTitle.png"];
        gameTitle.position = ccp(winsize.width/2,
                                 winsize.height - gameTitle.contentSize.height/2);
        gameTitle.scale = 1.0;
        [self addChild:gameTitle];
      
#pragma mark - 20,60,100 & Start Buttons lines
        // Init Games Button
        for(int i = 0; i < 3; i++){
            
            NSString * spriteNormal = @"";
            NSString * spriteSelected = @"";
            
            if(i == 0){
                spriteNormal = kButton20Press;
                spriteSelected = kButton20Press;
            }else if(i == 1){
                spriteNormal = kButton60Press;
                spriteSelected = kButton60Press;
            }else if(i == 2){
                spriteNormal = kButton100Press;
                spriteSelected = kButton100Press;
            }
            
            CCMenuItemImage * button = [CCMenuItemImage
                                        itemFromNormalSprite:[CCSprite spriteWithFile:spriteNormal]
                                            selectedSprite:[CCSprite spriteWithFile:spriteSelected]
                                                    target:self
                                                  selector:@selector(onClickButton:)];
            if(IS_IPAD || IS_IPHONE5){
                button.position = ccp(- winsize.width/2 + kLeftMargin +
                                      (2*i + 1)*0.75f*button.contentSize.width/2,
                                      winsize.height/2 - gameTitle.scale*gameTitle.contentSize.height -
                                      + winsize.height/30 - button.contentSize.height/2);
            }else{
                button.position = ccp(- winsize.width/2 + kLeftMargin +
                                      (2*i + 1)*0.75f*button.contentSize.width/2,
                                      winsize.height/2 - gameTitle.scale*gameTitle.contentSize.height -
                                      button.contentSize.height/2);
            }
            
            switch (i) {
                case kGame20Button:
                    button.tag = kGame20Button;
                    game20Button = button;
                    [menu addChild:game20Button];
                    if(currentNumberProblem == 20){
                        [game20Button setNormalImage:[CCSprite spriteWithFile:kButton20Unpress]];
                        [game20Button setSelectedImage:[CCSprite spriteWithFile:kButton20Unpress]];
                        [self animateButton:game20Button withstatus:YES];
                    }
                    break;
                case kGame60Button:
                    button.tag = kGame60Button;
                    game60Button = button;
                    [menu addChild:game60Button];
                    if(currentNumberProblem == 60){
                        [game60Button setNormalImage:[CCSprite spriteWithFile:kButton60Unpress]];
                        [game60Button setSelectedImage:[CCSprite spriteWithFile:kButton60Unpress]];
                        [self animateButton:game60Button withstatus:YES];
                    }
                    break;
                case kGame100Button:
                    button.tag = kGame100Button;
                    game100Button = button;
                    [menu addChild:game100Button];
                    if(currentNumberProblem == 100){
                        [game100Button setNormalImage:[CCSprite spriteWithFile:kButton100Unpress]];
                        [game100Button setSelectedImage:[CCSprite spriteWithFile:kButton100Unpress]];
                        [self animateButton:game100Button withstatus:YES];
                    }
                    break;
                default:
                    break;
            }
        }

        // Init Start Button
        startButton = [CCMenuItemImage itemFromNormalSprite:[CCSprite spriteWithFile:kButtonStartUnpress]
                                             selectedSprite:[CCSprite spriteWithFile:kButtonStartPress]
                                                     target:self
                                                   selector:@selector(onClickButton:)];
        startButton.tag = kStartButton;
        if(IS_IPAD){
            startButton.scale = 1.0;
        }else{
            startButton.scale = 1.0;
        }
        startButton.position = ccp(game100Button.position.x +
                                   0.8f*game100Button.contentSize.width,
                                   game100Button.position.y);
        [menu addChild:startButton];
        
#pragma mark - Best Time label and Reset button line
        // Init Best Time Label
        bestTime = [CCLabelTTF labelWithString:[[GameManager sharedGameManager]
                                                getBestRecordwithTotalNumbers:currentNumberProblem]
                                    dimensions:CGSizeMake(winsize.width/2,
                                                          startButton.scale*startButton.contentSize.height)
                                     alignment:NSTextAlignmentLeft
                                      fontName:kDenneKittenHeelsFont
                                      fontSize:kLabelSize];
        bestTime.color = ccBLACK;
        bestTime.position = ccp(kLeftMargin + bestTime.contentSize.width/2,
                                winsize.height/2 + winsize.height/20 - bestTime.contentSize.height/4);
        [self addChild:bestTime];
        
        // Add Reset Button
        resetButton = [CCMenuItemImage itemFromNormalSprite:[CCSprite spriteWithFile:kButtonResetUnpress]
                                             selectedSprite:[CCSprite spriteWithFile:kButtonResetPress]
                                                     target:self
                                                   selector:@selector(onClickButton:)];
        resetButton.tag = kResetButton;
        if(IS_IPAD){
            resetButton.position = ccp(kLeftMargin + resetButton.contentSize.width/2,
                                       bestTime.position.y  - winsize.height/2 + resetButton.contentSize.height/2);
        }else{
            resetButton.position = ccp(kLeftMargin + resetButton.contentSize.width/2,
                                       bestTime.position.y  - winsize.height/2 + resetButton.contentSize.height/2);
        }
        [menu addChild:resetButton];
        
#pragma mark - Share FB, Twitter, Email line
        
        // Init Share Label
        share = [CCLabelTTF labelWithString:@"Share:"
                                   fontName:kDenneKittenHeelsFont
                                   fontSize:kLabelSize];
        share.color = ccBLACK;
        share.position = ccp(kLeftMargin + share.contentSize.width/2,
                                winsize.height/2 - winsize.height/15);
        [self addChild:share];
        
        // Add Facebook Button
        facebookButton = [CCMenuItemImage itemFromNormalSprite:[CCSprite spriteWithSpriteFrameName:@"Facebook.png"]
                                                selectedSprite:[CCSprite spriteWithSpriteFrameName:@"FacebookPress.png"]
                                                        target:self
                                                      selector:@selector(onClickButton:)];
        facebookButton.scale = 0.8;
        facebookButton.tag = kFaceBookButton;
        facebookButton.position = ccp(-winsize.width/2 + winsize.width*0.1 +
                                      share.contentSize.width + facebookButton.contentSize.width/2,
                                      -winsize.height/15);
        [menu addChild:facebookButton];
        
        // Add Twitter Button
        twitterButton = [CCMenuItemImage itemFromNormalSprite:[CCSprite spriteWithSpriteFrameName:@"Twitter.png"]
                                               selectedSprite:[CCSprite spriteWithSpriteFrameName:@"TwitterPress.png"]
                                                       target:self
                                                     selector:@selector(onClickButton:)];
        twitterButton.scale = 0.8f;
        twitterButton.tag = kTwitterButton;
        twitterButton.position = ccp(-winsize.width/2 + winsize.width*0.1 + share.contentSize.width +
                                     facebookButton.contentSize.width/1.3f + twitterButton.contentSize.width/2,
                                     -winsize.height/15);
        [menu addChild:twitterButton];
        
        // Add Send Email Button
        emailButton = [CCMenuItemImage itemFromNormalSprite:[CCSprite spriteWithFile:kButtonEmailUnpress]
                                             selectedSprite:[CCSprite spriteWithFile:kButtonEmailPress]
                                                     target:self
                                                   selector:@selector(onClickButton:)];
        emailButton.scale = 0.6f;
        emailButton.tag = kEmailButton;
        emailButton.position = ccp(-winsize.width/2 + kLeftMargin + share.contentSize.width +
                                   facebookButton.contentSize.width/1.3f + twitterButton.contentSize.width/1.3f +
                                   emailButton.contentSize.width/2.8f,
                                   -winsize.height/15);
        [menu addChild:emailButton];
        
        
#pragma mark - Info button and View High Score button line        
        // Add View High Score Button
        viewHighScoreButton = [CCMenuItemImage itemFromNormalSprite:[CCSprite
                                                                    spriteWithSpriteFrameName:@"HomeAccountUnpress.png"]
                                                     selectedSprite:[CCSprite
                                                                    spriteWithSpriteFrameName:@"HomeAccountPress.png"]
                                                             target:self
                                                           selector:@selector(onClickButton:)];
        viewHighScoreButton.scale = 0.8;
        viewHighScoreButton.tag = kViewHighScoreButton;
        viewHighScoreButton.position = ccp(- winsize.width/2 + kLeftMargin
                                           + viewHighScoreButton.scale*viewHighScoreButton.contentSize.width/2,
                                           - winsize.height/2 + viewHighScoreButton.contentSize.height/2 + kBottomMargin);
        [menu addChild:viewHighScoreButton];
        
        // Add Info Button
        infoButton = [CCMenuItemImage itemFromNormalSprite:[CCSprite spriteWithSpriteFrameName:@"HomeAboutUnpress.png"]
                                            selectedSprite:[CCSprite spriteWithSpriteFrameName:@"HomeAboutPress.png"]
                                                    target:self
                                                  selector:@selector(onClickButton:)];
        infoButton.scale = 0.8;
        infoButton.tag = kInfoButton;
        infoButton.position = ccp(- winsize.width/2 + kLeftMargin +
                                  infoButton.scale*infoButton.contentSize.width/2 +
                                  viewHighScoreButton.scale*viewHighScoreButton.contentSize.width,
                                  - winsize.height/2 + viewHighScoreButton.contentSize.height/2 + kBottomMargin);
        [menu addChild:infoButton];
        
        // Add More Button
        moreButton = [CCMenuItemImage itemFromNormalImage:kMoreButtonUnpress
                                            selectedImage:kMoreButtonPress
                                                   target:self
                                                 selector:@selector(onClickButton:)];
        moreButton.scale = 0.8f;
        moreButton.tag = kMoreButton;
        moreButton.position = ccp(- winsize.width/2 + kLeftMargin +
                                  moreButton.scale*moreButton.contentSize.width/2,
                                  - winsize.height/2 + viewHighScoreButton.scale*viewHighScoreButton.contentSize.height + kBottomMargin + moreButton.contentSize.height/2);
        [moreButton runAction:[CCRepeatForever actionWithAction:
                               [CCSequence actions:
                                [CCRotateTo actionWithDuration:0.3f angle:10],
                                [CCRotateTo actionWithDuration:0.3f angle:-10],
                                [CCRotateTo actionWithDuration:0.3f angle:10],
                                [CCRotateTo actionWithDuration:0.3f angle:-10],
                                [CCRotateTo actionWithDuration:0.3f angle:0],
                                [CCScaleTo actionWithDuration:0.3f scale:0.9f],
                                [CCScaleTo actionWithDuration:0.3f scale:0.8f],
                                [CCScaleTo actionWithDuration:0.3f scale:0.9f],
                                [CCScaleTo actionWithDuration:0.3f scale:0.8f],
                                [CCDelayTime actionWithDuration:5],nil]]];
        [menu addChild:moreButton];
        
        
#pragma mark - Brain Guy and Brain Guy Dialog Balloon line
        float brainguyscale = 1.0f;
        float brainballoonscale = 1.0f;
        float brainguyextra = 0.0f;
        if(IS_IPAD || IS_IPHONE5){
            brainguyscale = 0.457f;
            brainballoonscale = 0.8f;
            brainguyextra = winsize.height/30;
        }else{
            brainguyscale = 0.4f;
            brainballoonscale = 0.7f;
            brainguyextra = winsize.height/60;
        }
        
        // Init Brain Guy
        brainGuy = [CCSprite spriteWithFile:kBrainGuy];
        brainGuy.scale = brainguyscale;
        brainGuy.position = ccp(winsize.width/1.6f + 0.8f*brainGuy.contentSize.width/2,
                                kBottomMargin + brainGuy.scale*brainGuy.contentSize.height/2);
        [self addChild:brainGuy];
        
        // Init Brain Guy Balloon dialog
        brainBalloon = [CCSprite spriteWithSpriteFrameName:@"HomeLongText.png"];
        brainBalloon.scale = brainballoonscale;
        brainBalloon.position = ccp(winsize.width/1.6f,
                                    kBottomMargin + brainguyextra +
                                    brainGuy.scale*brainGuy.contentSize.height/2 + brainBalloon.contentSize.height/2);
        [self addChild:brainBalloon];
        
        // Init Twitter Function
        hasTwitted = NO;
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(cancelTwitter)
                                                     name:kTwitterControllerCanceled
                                                   object:nil];
        
        // Init Sound Effect for Button
        if([NSDEF boolForKey:OPT_SOUND]){
            [GSAudio load:kButtonClick];
            [GSAudio load:kLevelSwicth];
        }
        
        // Rating Dialog Checking
        int number = (int)[NSDEF integerForKey:kGameIndexNumber];
        DLog(@"Number %d",number);
        if(number == 3 || number == 6 || (number > 0 && (number%13) == 0)){
            DLog(@"Show Review Alert");
            [L0SolicitReviewController solicit];
        }
    }
    
    return self;
}

#pragma mark - Main Functions
- (void) cancelTwitter {
    hasTwitted = NO;
}

- (void)onClickButton:(id)sender
{
    int tag = (int)[sender tag];
    switch (tag)
    {
        case kGame20Button:
            
            DLog(@"Select 20");
            if([NSDEF boolForKey:OPT_SOUND]) [GSAudio play:kLevelSwicth];
            [NSDEF setInteger:20 forKey:kCurrentNumberProblem];
            currentNumberProblem = 20;
            [bestTime setString:[[GameManager sharedGameManager]
                                 getBestRecordwithTotalNumbers:currentNumberProblem]];
            [self resetButtonNumberStatus:currentNumberProblem];
            
            break;
            
        case kGame60Button:
            
            DLog(@"Select 60");
            if([NSDEF boolForKey:OPT_SOUND]) [GSAudio play:kLevelSwicth];
            [NSDEF setInteger:60 forKey:kCurrentNumberProblem];
            currentNumberProblem = 60;
            [bestTime setString:[[GameManager sharedGameManager]
                                 getBestRecordwithTotalNumbers:currentNumberProblem]];
            [self resetButtonNumberStatus:currentNumberProblem];
            
            break;
            
        case kGame100Button:
            
            DLog(@"Select 100");
            if([NSDEF boolForKey:OPT_SOUND]) [GSAudio play:kLevelSwicth];
            [NSDEF setInteger:100 forKey:kCurrentNumberProblem];
            currentNumberProblem = 100;
            [bestTime setString:[[GameManager sharedGameManager]
                                 getBestRecordwithTotalNumbers:currentNumberProblem]];
            [self resetButtonNumberStatus:currentNumberProblem];
            
            break;
            
        case kStartButton:
            
            // Dimiss Ad View
            [[NSNotificationCenter defaultCenter] postNotificationName:kHideAdViewNotification object:nil];
            
            DLog(@"Start Game");
            DLog(@"Current Number Problems: %i",currentNumberProblem);
            DLog(@"Current Difficulty: %i",currentDifficulty);
            
            if (currentNumberProblem == 20) LOG_EVENT(kStartGame20);
            if (currentNumberProblem == 60) LOG_EVENT(kStartGame60);
            if (currentNumberProblem == 100) LOG_EVENT(kStartGame100);
            
            [[GameManager sharedGameManager] initGameWithTotalProblems:currentNumberProblem Difficulty:currentDifficulty];
            if ([NSDEF boolForKey:OPT_SOUND]) [GSAudio play:kButtonClick];
            [[CCDirector sharedDirector] replaceScene:[CCTransitionPageTurn transitionWithDuration:0.5f scene:[PlayScene node] backwards:NO]];
            
            break;
            
        case kInfoButton:
            
            LOG_EVENT(kOpenSettingsPage);
            
            // Dimiss Ad View
            [[NSNotificationCenter defaultCenter] postNotificationName:kHideAdViewNotification object:nil];
            
            DLog(@"Info Button");
            if ([NSDEF boolForKey:OPT_SOUND]) [GSAudio play:kButtonClick];
            [[NSNotificationCenter defaultCenter] postNotificationName:kShowOptionsView object:nil];
            
            break;
            
        case kViewHighScoreButton:
            
            LOG_EVENT(kOpenHighScorePage);
            
            // Dimiss Ad View
            [[NSNotificationCenter defaultCenter] postNotificationName:kHideAdViewNotification object:nil];
            
            DLog(@"View High Score Button");
            if ([NSDEF boolForKey:OPT_SOUND]) [GSAudio play:kButtonClick];
            
            [self performSelector:@selector(openHighScoreURL) withObject:self afterDelay:0.3f];
            
            break;
            
        case kResetButton:
            
            LOG_EVENT(kResetRecords);
            
            DLog(@"Reset Button");
            if([NSDEF boolForKey:OPT_SOUND]) [GSAudio play:kButtonClick];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Reset Best Time?"
                                                            message:[NSString stringWithFormat:@"Do you want to reset the best time for %d problems?", currentNumberProblem]
                                                           delegate:self
                                                  cancelButtonTitle:@"No"
                                                  otherButtonTitles:@"Yes", nil];
            alert.tag = kResetBestTimeDialog;
            [alert show];
            [alert release];
            
            break;
            
        case kEmailButton:
            
            LOG_EVENT(kShareEmail);
            
            DLog(@"Email Button");
            if([NSDEF boolForKey:OPT_SOUND]) [GSAudio play:kButtonClick];
            [[NSNotificationCenter defaultCenter] postNotificationName:kSendEmailFriend object:nil];
            
            break;
            
        case kFaceBookButton:
            
            LOG_EVENT(kShareFB);
            
            DLog(@"FB Button");
            if ([NSDEF boolForKey:OPT_SOUND]) [GSAudio play:kButtonClick];
            
            if (![self hasRecordtoPost])
            { // Checking Has Record or Not
                return;
            }
            
            if (![self hasInternetConnection:YES])
            { // Checking Has Internet Connection
                return;
            }
            
            // Create the view controller
            if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook])
            {
                SLComposeViewController *facebook = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
                
                SLComposeViewControllerCompletionHandler myBlock = ^(SLComposeViewControllerResult result){
                    
                    NSString *title = @"";
                    NSString *msg = @"";
                    
                    if (result == SLComposeViewControllerResultCancelled)
                    {
                        title = NSLocalizedString(@"Failed to share on Facebook!", nil);
                        msg = NSLocalizedString(@"Share on Facebook was canceled", nil);
                    }
                    else if (result == SLComposeViewControllerResultDone)
                    {
                        title = NSLocalizedString(@"Shared on Facebook!", nil);
                        msg = NSLocalizedString(@"Please check your Facebook", nil);
                    }
                    
                    // Show alert to see how things went...
                    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title
                                                                        message:msg
                                                                       delegate:self
                                                              cancelButtonTitle:@"OK"
                                                              otherButtonTitles:nil];
                    [alertView show];
                    [alertView release];
                    
                    [facebook dismissViewControllerAnimated:YES completion:nil];
                    //[twitter release];
                };
                
                facebook.completionHandler = myBlock;
                
                // Optional: set an image, url and initial text
                [facebook addImage:[UIImage imageNamed:@"Icon.png"]];
                [facebook addURL:[NSURL URLWithString:kAppiTunesLink]];
                [facebook setInitialText:[NSString stringWithFormat:@"%@ in #BrainTuner @Greengar", [[GameManager sharedGameManager] getSocialNetWorkUploadMessageWithNumofProblems:currentNumberProblem]]];
                
                // Show the controller
                [[UIAppDelegate getRootViewController] presentViewController:facebook animated:YES completion:nil];
            }
            else
            {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Failed to share on Facebook!", nil)
                                                                    message:NSLocalizedString(@"Facebook is not available", nil)
                                                                   delegate:nil
                                                          cancelButtonTitle:@"OK"
                                                          otherButtonTitles:nil];
                [alertView show];
                [alertView release];
            }
            
            // Should not do this, access token handler is quite complicated and buggy
            // [[AccountManager sharedManager] broadcastOnFacebook:[[GameManager sharedGameManager] getSocialNetWorkUploadMessageWithNumofProblems:currentNumberProblem]];
            
            break;
            
        case kTwitterButton:
            
            LOG_EVENT(kShareTwitter);
            
            DLog(@"Twitter Button");
            if ([NSDEF boolForKey:OPT_SOUND]) [GSAudio play:kButtonClick];
            if (![self hasRecordtoPost])
            { // Checking Has Record or Not
                return;
            }
            
            if (![self hasInternetConnection:NO])
            { // Checking Has Intenert Connection
                return;
            }
            
            // Create the view controller
            if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter])
            {
                SLComposeViewController *twitter = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
                
                SLComposeViewControllerCompletionHandler myBlock = ^(SLComposeViewControllerResult result){
                    
                    NSString *title = @"";
                    NSString *msg = @"";
                    
                    if (result == SLComposeViewControllerResultCancelled)
                    {
                        title = NSLocalizedString(@"Failed to share on Twitter!", nil);
                        msg = NSLocalizedString(@"Share on Twitter was canceled", nil);
                    }
                    else if (result == SLComposeViewControllerResultDone)
                    {
                        title = NSLocalizedString(@"Shared on Twitter!", nil);
                        msg = NSLocalizedString(@"Please check your Twitter", nil);
                    }
            
                    // Show alert to see how things went...
                    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title
                                                                        message:msg
                                                                       delegate:self
                                                              cancelButtonTitle:@"OK"
                                                              otherButtonTitles:nil];
                    [alertView show];
                    [alertView release];
            
                    [twitter dismissViewControllerAnimated:YES completion:nil];
                    //[twitter release];
                };
                
                twitter.completionHandler = myBlock;
                
                // Optional: set an image, url and initial text
                [twitter addImage:[UIImage imageNamed:@"Icon.png"]];
                [twitter addURL:[NSURL URLWithString:kAppiTunesLink]];
                [twitter setInitialText:[NSString stringWithFormat:@"%@ in #BrainTuner @Greengar", [[GameManager sharedGameManager] getSocialNetWorkUploadMessageWithNumofProblems:currentNumberProblem]]];
                
                // Show the controller
                [[UIAppDelegate getRootViewController] presentViewController:twitter animated:YES completion:nil];
            }
            else
            {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Failed to share on Twitter!", nil)
                                                                    message:NSLocalizedString(@"Twitter is not available", nil)
                                                                   delegate:nil
                                                          cancelButtonTitle:@"OK"
                                                          otherButtonTitles:nil];
                [alertView show];
                [alertView release];
            }
            
            break;
            
        case kMoreButton:
            
            LOG_EVENT(kMoreButtonEvent);
            
            DLog(@"More Button");
            if([NSDEF boolForKey:OPT_SOUND]) [GSAudio play:kButtonClick];
            [[NSNotificationCenter defaultCenter] postNotificationName:kShowRevmobAdsLink object:nil];
            
            break;
            
        default:
            break;
    }
    
}

- (void)resetButtonNumberStatus:(int)currentproblem
{
    if (currentproblem == 20)
    {
        // Set Status
        [game20Button setNormalImage:[CCSprite spriteWithFile:kButton20Unpress]];
        [game20Button setSelectedImage:[CCSprite spriteWithFile:kButton20Unpress]];
        [game60Button setNormalImage:[CCSprite spriteWithFile:kButton60Press]];
        [game60Button setSelectedImage:[CCSprite spriteWithFile:kButton60Press]];
        [game100Button setNormalImage:[CCSprite spriteWithFile:kButton100Press]];
        [game100Button setSelectedImage:[CCSprite spriteWithFile:kButton100Press]];
        
        // Start Animate
        [self animateButton:game20Button withstatus:YES];
        [self animateButton:game60Button withstatus:NO];
        [self animateButton:game100Button withstatus:NO];
    }
    else if (currentproblem == 60)
    {
        // Set Status
        [game20Button setNormalImage:[CCSprite spriteWithFile:kButton20Press]];
        [game20Button setSelectedImage:[CCSprite spriteWithFile:kButton20Press]];
        [game60Button setNormalImage:[CCSprite spriteWithFile:kButton60Unpress]];
        [game60Button setSelectedImage:[CCSprite spriteWithFile:kButton60Unpress]];
        [game100Button setNormalImage:[CCSprite spriteWithFile:kButton100Press]];
        [game100Button setSelectedImage:[CCSprite spriteWithFile:kButton100Press]];
        
        // Start Animate
        [self animateButton:game20Button withstatus:NO];
        [self animateButton:game60Button withstatus:YES];
        [self animateButton:game100Button withstatus:NO];
    }
    else if(currentproblem == 100)
    {
        // Set Status
        [game20Button setNormalImage:[CCSprite spriteWithFile:kButton20Press]];
        [game20Button setSelectedImage:[CCSprite spriteWithFile:kButton20Press]];
        [game60Button setNormalImage:[CCSprite spriteWithFile:kButton60Press]];
        [game60Button setSelectedImage:[CCSprite spriteWithFile:kButton60Press]];
        [game100Button setNormalImage:[CCSprite spriteWithFile:kButton100Unpress]];
        [game100Button setSelectedImage:[CCSprite spriteWithFile:kButton100Unpress]];
        
        // Start Animate
        [self animateButton:game20Button withstatus:NO];
        [self animateButton:game60Button withstatus:NO];
        [self animateButton:game100Button withstatus:YES];
    }
}

- (void)animateButton:(id)sender withstatus:(BOOL)enableanimate
{
    if (enableanimate)
    {
        if ([sender respondsToSelector:@selector(runAction:)])
        {
            [sender runAction:[CCRepeatForever actionWithAction:
                               [CCSequence actions:[CCScaleTo actionWithDuration:0.3f
                                                                           scale: 1.1],
                                                    [CCScaleTo actionWithDuration:0.3f
                                                                            scale:0.95f],
                                                    nil]]];
        }
    }
    else
    {
        if ([sender respondsToSelector:@selector(stopAllActions)])
        {
            [sender stopAllActions];
        }
    }
}

- (void)openHighScoreURL
{
    if ([[Reachability reachabilityForInternetConnection] currentReachabilityStatus] == NotReachable)
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:kShowHighScoreWebView object:nil];
    }
    else
    {
        [[CCDirector sharedDirector] replaceScene:[CCTransitionPageTurn transitionWithDuration:0.5f scene:[AdsScene node] backwards:NO]];
    }
}

#pragma mark - UIAlert View Delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == kResetBestTimeDialog)
    {
        switch (buttonIndex)
        {
            case 0:// No Button
                // Do Nothing
                break;
                
            case 1:// Yes Button
                [[GameManager sharedGameManager] resetBestRecordwithTotalNumbers:currentNumberProblem];
                [bestTime setString:[[GameManager sharedGameManager]
                                     getBestRecordwithTotalNumbers:currentNumberProblem]];
                break;
                
            default:
                break;
        }
    }
}

- (BOOL)hasRecordtoPost
{
    double besttime = 0.0f;
    
    besttime = [[SecurityManager sharedManager] getBestTime:currentNumberProblem];
    
    if (besttime <= 2.0f)
    {
        DLog(@"No Record Yet");
        
        NSString *message = @"Play Brain Tuner, and then share your best time!";
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No Record Yet"
                                                        message:message
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        alert.tag = kNoRecordYetDialog;
        [alert show];
        [alert release];
        return NO;
    }
    
    return YES;
}

- (BOOL)hasInternetConnection:(BOOL)isFB
{
    if ([[Reachability reachabilityForInternetConnection] currentReachabilityStatus] == NotReachable)
    {
        NSString * message = isFB ? @"Can not share to Facebook" : @"Can not share to Twitter";
        UIAlertView * alertView = [[UIAlertView alloc] initWithTitle:@"No Internet Connection"
                                                             message:message
                                                            delegate:self
                                                   cancelButtonTitle:@"Ok"
                                                   otherButtonTitles:nil];
        alertView.tag = kInternetErrorDialogTag;
        [alertView show];
        [alertView release];
        
        return NO;
    }
    
    return YES;
}

@end
