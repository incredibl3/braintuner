//
//  AdsScene.m
//  BrainTuner2
//
//  Created by Leon Yuu on 10/4/12.
//
//

#import "AdsScene.h"
#define kOffLineBannerAds ((IS_IPAD) ? @"OfflineAdiPad.png" : @"OfflineAdiPhone.png")
#define kOffLineAdsCloseButton ((IS_IPAD) ? @"OfflineAdsCloseButton@2x.png" : @"OfflineAdsCloseButton.png")
#define kOffLineAdsCloseButtonPressed ((IS_IPAD) ? @"OfflineAdsCloseButtonPressed@2x.png" : @"OfflineAdsCloseButtonPressed.png")
#define kLoadingTextSize ((IS_IPAD) ? 70:33)

#define kAdFlow1 0 // Revmob ->  Chartboost -> iAd (iPad Only) -> GG Offline Ads
#define kAdFlow2 1 // Chartboost -> Revmob -> iAd (iPad Only) -> GG Offline Ads
#define kAdFlow3 2 // iAd (iPad Only) -> Chartboost -> Revmob -> GG Offline Ads

@implementation AdsScene

static BOOL isLoaded;
static BOOL isBacktoMainScene;

+ (BOOL) isLoaded{
    return isLoaded;
}

+ (void) setBacktoMainScene {
    isBacktoMainScene = YES;
}

- (id) init{
    
    self = [super init];
    
    isLoaded = YES;
    
    if (self != nil) {

        // Win Size
        CGSize winsize = [[CCDirector sharedDirector] winSize];
        
        // Init Back Ground
        CCLayerColor * backgroundLayer = [[CCLayerColor alloc] initWithColor:ccc4(255,0,0,0)];
		[self addChild:backgroundLayer];
        
        // Init Loading Text
        NSString * text = @"This ad helps us pay for the Global Highscore hosting :)";
        NSString * text1 = [NSString stringWithFormat:@"%@.",text];
        NSString * text2 = [NSString stringWithFormat:@"%@..",text];
        NSString * text3 = [NSString stringWithFormat:@"%@...",text];
        
        count = 0; // Use for Loading Text Schedule
        textarray = [[NSArray alloc] initWithObjects:text,text1,text2,text3, nil];
        loadingText = [CCLabelTTF labelWithString:text
                                       dimensions:CGSizeMake(winsize.width*5/6, winsize.height/4)
                                        alignment:NSTextAlignmentLeft
                                         fontName:kDenneKittenHeelsFont
                                         fontSize:kLoadingTextSize];
        loadingText.position = ccp(winsize.width/2,winsize.height/2);
        [self addChild:loadingText];
        [self schedule:@selector(loadingTextSchedule:) interval:0.4f];
        
        // Init Close OfflineAds Button
        CCMenu * menu = [CCMenu menuWithItems:nil];
        menu.position = ccp(winsize.width/2,winsize.height/2);
        menu.anchorPoint = CGPointZero;
        [self addChild:menu];
        
        // Init Offline Ads
        offLineAdsBanner = [CCMenuItemImage itemFromNormalImage:kOffLineBannerAds
                                                  selectedImage:kOffLineBannerAds
                                                         target:self
                                                       selector:@selector(onBannerOfflineAdsClick)];
        offLineAdsBanner.position = ccp(0,0);
        offLineAdsBanner.scale = 1.1;
        [offLineAdsBanner setVisible:NO];
        [menu addChild:offLineAdsBanner];
        
        offlLineAdsCloseButton = [CCMenuItemImage itemFromNormalImage:kOffLineAdsCloseButton
                                                        selectedImage:kOffLineAdsCloseButtonPressed
                                                               target:self
                                                             selector:@selector(switchToViewHighScorePage)];
        offlLineAdsCloseButton.scale = 1.5f;
        offlLineAdsCloseButton.position = ccp(1.1*offLineAdsBanner.contentSize.width/2 - 10,
                                              1.1*offLineAdsBanner.contentSize.height/2- 10);
        [offlLineAdsCloseButton setVisible:NO];
        [menu addChild:offlLineAdsCloseButton];
        
        // Interstitials Ads
        if([[Reachability reachabilityForInternetConnection] currentReachabilityStatus] == NotReachable )
        {
            DLog(@"No Internet Connection, Switch to Ranking Scene");
            [self showOffLineAds];
        }
        else
        {
            DLog(@"Has Internet Connection, Show Interstitals");
            
            int random = 0; //(int) (arc4random()%5);
            switch (random)
            {
                case 0:
                case 1:
                case 2:
                case 3:
                    currentAdsFlowCase = kAdFlow1;
                    break;
                case 4:
                    currentAdsFlowCase = kAdFlow2;
                    break;
                default:
                    break;
            }
            DLog(@"Ad Flow Case %i",currentAdsFlowCase);
            
            /*
             #define kAdFlow1 0 // Revmob ->  Chartboost -> iAd (iPad Only) -> GG Offline Ads
             #define kAdFlow2 1 // Chartboost -> Revmob -> iAd (iPad Only) -> GG Offline Ads
             #define kAdFlow3 2 // iAd (iPad Only) -> Chartboost -> Revmob -> GG Offline Ads
             */
            switch (currentAdsFlowCase)
            {
                case kAdFlow1:
                    
                    [self showFullScreenRevmob];
                    break;
                    
                case kAdFlow2:
                    
                    [self showChartboost];
                    break;
                    
                case kAdFlow3:
                    
                    [self showiAdInterstitial];
                    break;
                    
                default:
                    break;
            }
        }
    }
    
    return self;
}

- (void)loadingTextSchedule:(float) dt
{
    count ++;
    int state = count % 4;
    [loadingText setString:[textarray objectAtIndex:state]];
}

- (void)showFullScreenRevmob
{
    [RevMobAds session].testingMode = kTestRevmobAds ? RevMobAdsTestingModeWithAds:RevMobAdsTestingModeOff;
    
    RevMobFullscreen * fullscreen = [[RevMobAds session] fullscreen];
    fullscreen.delegate = self;
    [fullscreen showAd];
}

- (void)showChartboost
{
    [Chartboost showInterstitial:CBLocationHomeScreen];
}

- (void)showiAdInterstitial
{
    [self showOffLineAds];
}

- (void)showOffLineAds
{
    [offLineAdsBanner setVisible:YES];
    [offlLineAdsCloseButton setVisible:YES];
}

- (void)dismissOffLineAds
{
    [offLineAdsBanner setVisible:NO];
    [offlLineAdsCloseButton setVisible:NO];
}

- (void)dismissLoadingText
{

}

- (void)switchToViewHighScorePage
{
    isLoaded = NO;
    [self unschedule:@selector(loadingTextSchedule:)];
    
    if (!isBacktoMainScene)
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:kShowHighScoreWebView object:nil];
        [self performSelector:@selector(switchToMainScene) withObject:nil afterDelay:0.5f];
    }
    else
    {
        isBacktoMainScene = NO;
        [self performSelector:@selector(switchToMainScene) withObject:nil afterDelay:0.5f];
        [[NSNotificationCenter defaultCenter] postNotificationName:kShowAdViewNotification object:nil];
    }
}

- (void)switchToMainScene
{
    [[CCDirector sharedDirector] replaceScene:[CCTransitionPageTurn transitionWithDuration:0.4f scene:[MainScene node] backwards:YES]];
}

- (void)onBannerOfflineAdsClick
{
    NSString *iTunesLink = @"http://bit.ly/GgComAllGreengarApps";
	
	loadingAlert = [[UIAlertView alloc] initWithTitle:@"Loading..." message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:@"Cancel", nil];
	[loadingAlert show];
	
	[self openReferralURL:[NSURL URLWithString:iTunesLink]];
}

#pragma mark - Revmob Delegate
/**
 Fired by Fullscreen, banner and popup. Called when the communication with the server is finished with success.
 */
- (void)revmobAdDidReceive
{
    [self dismissLoadingText];
    [self dismissOffLineAds];
}

/**
 Fired by Fullscreen, banner and popup. Called when the communication with the server is finished with error.
 
 @param error: contains error information.
 */
- (void)revmobAdDidFailWithError:(NSError *)error
{
    /*
     #define kAdFlow1 0 // Revmob ->  Chartboost -> iAd (iPad Only) -> GG Offline Ads
     #define kAdFlow2 1 // Chartboost -> Revmob -> iAd (iPad Only) -> GG Offline Ads
     #define kAdFlow3 2 // iAd (iPad Only) -> Chartboost -> Revmob -> GG Offline Ads
     */
    switch (currentAdsFlowCase)
    {
        case kAdFlow1:
            
            [self showChartboost];
            break;
            
        case kAdFlow2:
            
            [self showiAdInterstitial];
            break;
            
        case kAdFlow3:
            
            [self showOffLineAds];
            break;
            
        default:
            break;
    }
}

/**
 Fired by Fullscreen, banner and popup. Called when the Ad is displayed in the screen.
 */
- (void)revmobAdDisplayed
{

}

/**
 Fired by Fullscreen, banner, button and popup.
 */
- (void)revmobUserClickedInTheAd
{
    [self performSelector:@selector(switchToViewHighScorePage) withObject:nil afterDelay:2.5f];
}

/**
 Fired by Fullscreen and popup.
 */
- (void)revmobUserClosedTheAd
{
    // Dismiss Revmob Ad Back to Ranking Scene
    [self switchToViewHighScorePage];
}

#pragma mark - Chartboost Delegate
// Called when an interstitial has been received, before it is presented on screen
// Return NO if showing an interstitial is currently innapropriate, for example if the user has entered the main game mode.
- (BOOL)shouldDisplayInterstitial:(NSString *)location
{
    [self dismissLoadingText];
    [self dismissOffLineAds];
    return YES;
}

// Called when an interstitial has been received and cached.
- (void)didCacheInterstitial:(NSString *)location
{

}

// Called when an interstitial has failed to come back from the server
- (void)didFailToLoadInterstitial:(NSString *)location
{
    /*
     #define kAdFlow1 0 // Revmob ->  Chartboost -> iAd (iPad Only) -> GG Offline Ads
     #define kAdFlow2 1 // Chartboost -> Revmob -> iAd (iPad Only) -> GG Offline Ads
     #define kAdFlow3 2 // iAd (iPad Only) -> Chartboost -> Revmob -> GG Offline Ads
     */
    
    switch (currentAdsFlowCase) {
            
        case kAdFlow1:
            
            [self showOffLineAds];
            break;
            
        case kAdFlow2:
            
            [self showFullScreenRevmob];
            break;
            
        case kAdFlow3:
            
            [self showFullScreenRevmob];
            break;
            
        default:
            break;
    }
    
}

// Called when the user dismisses the interstitial
// If you are displaying the add yourself, dismiss it now.
- (void)didDismissInterstitial:(NSString *)location
{

}

// Same as above, but only called when dismissed for a close
- (void)didCloseInterstitial:(NSString *)location
{
    [self switchToViewHighScorePage];
}

// Same as above, but only called when dismissed for a click
- (void)didClickInterstitial:(NSString *)location
{
    [self performSelector:@selector(switchToViewHighScorePage) withObject:nil afterDelay:2.5f];
}

#pragma mark - iAd Interstitials Delegate
// When this method is invoked, if the application is using presentInView, the he content will be unloaded from the
// container shortly after this method is called and no new content will be loaded. This may occur either as a result
// of user actions or if the ad content has expired.
- (void)interstitialAdDidUnload:(ADInterstitialAd *)interstitialAd
{
    DLog();
    adInterstitialAd.delegate = nil;
    [adInterstitialAd release];
    [self performSelector:@selector(switchToViewHighScorePage) withObject:nil afterDelay:1];
}

// This method will be invoked when an error has occurred attempting to get ad content.
// The ADError enum lists the possible error codes.
- (void)interstitialAd:(ADInterstitialAd *)interstitialAd didFailWithError:(NSError *)error
{
    adInterstitialAd.delegate = nil;
    [adInterstitialAd release];
    
    /*
     #define kAdFlow1 0 // Revmob ->  Chartboost -> iAd (iPad Only) -> GG Offline Ads
     #define kAdFlow2 1 // Chartboost -> Revmob -> iAd (iPad Only) -> GG Offline Ads
     #define kAdFlow3 2 // iAd (iPad Only) -> Chartboost -> Revmob -> GG Offline Ads
     */
    
    switch (currentAdsFlowCase) {
            
        case kAdFlow1:
            
            [self showOffLineAds];
            break;
            
        case kAdFlow2:
            
            [self showOffLineAds];
            break;
            
        case kAdFlow3:
            
            [self showChartboost];
            break;
            
        default:
            break;
    }
}

// This method is invoked when the interstitial ad load ad content. The delegate should implement this method so that
// it knows when the interstitial ad is ready to be presented.
- (void)interstitialAdDidLoad:(ADInterstitialAd *)interstitialAd
{
    if(adInterstitialAd.loaded)
    {
        [adInterstitialAd presentFromViewController:[UIAppDelegate getRootViewController]];
    }
}

// This message is sent when the action has completed and control is returned to the application.
// Games, media playback, and other activities that were paused in response to the beginning
// of the action should resume at this point.
- (void)interstitialAdActionDidFinish:(ADInterstitialAd *)interstitialAd
{
    [self switchToViewHighScorePage];
}

#pragma mark - Affiliate Link
// Process a LinkShare/TradeDoubler/DGM URL to something iPhone can handle
- (void)openReferralURL:(NSURL *)referralURL
{
	self.iTunesURL = referralURL;
	DLog(@"self.iTunesURL = %@", self.iTunesURL);
    NSURLConnection *conn = [[NSURLConnection alloc] initWithRequest:[NSURLRequest requestWithURL:referralURL] delegate:self startImmediately:YES];
    [conn release];
}

#define kLoadingTitle @"Loading..."
- (void)showLoadingAlert
{
	if (!loadingAlert)
    { // just being defensive
		// show Loading... alert
		loadingAlert = [[UIAlertView alloc] initWithTitle:kLoadingTitle message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:@"Hide", nil];
		[loadingAlert show];
	}
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
	self.iTunesURL = nil;
	[loadingAlert dismissWithClickedButtonIndex:[loadingAlert cancelButtonIndex] animated:NO];
	[loadingAlert release], loadingAlert = nil;
    
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Unable to Reach App Store"
                                                    message:@"Please try again later."
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
	[alert show];
	[alert release];
}

// Save the most recent URL in case multiple redirects occur
// "iTunesURL" is an NSURL property in your class declaration
- (NSURLRequest *)connection:(NSURLConnection *)connection willSendRequest:(NSURLRequest *)request redirectResponse:(NSURLResponse *)response
{
	DLog(@"self.iTunesURL = %@", self.iTunesURL);
    
	if (self.iTunesURL)
    {
		self.iTunesURL = [response URL];
		if (!self.iTunesURL)
        {
			self.iTunesURL = [request URL];
		}
		DLog(@"self.iTunesURL = %@", self.iTunesURL);
		return request;
	}
	return nil;
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
	DLog(@"self.iTunesURL = %@", self.iTunesURL);
	if (self.iTunesURL)
    {
		[[UIApplication sharedApplication] openURL:self.iTunesURL];
    }
    
	[loadingAlert dismissWithClickedButtonIndex:[loadingAlert cancelButtonIndex] animated:NO];
	[loadingAlert release], loadingAlert = nil;
}


@end
