//
//  AdsScene.h
//  BrainTuner2
//
//  Created by Leon Yuu on 10/4/12.
//
//

#import "cocos2d.h"
#import <UIKit/UIKit.h>
#import <RevMobAds/RevMobAds.h>
#import <iAd/iAd.h>
#import <Chartboost/Chartboost.h>
#import "GameUtils.h"
#import "AppDelegate.h"
#import "Reachability.h"

@interface AdsScene : CCScene<RevMobAdsDelegate,ChartboostDelegate, ADInterstitialAdDelegate, UIAlertViewDelegate>{
    CCMenuItemImage * offLineAdsBanner;
    CCMenuItemImage * offlLineAdsCloseButton;
    CCLabelTTF * loadingText;
    NSArray * textarray;
    int count;
    int currentAdsFlowCase;
    UIAlertView * loadingAlert;
    
    ADInterstitialAd *adInterstitialAd; // Use incase Full Screen iAd - iPad Only
}

@property (nonatomic,retain) NSURL * iTunesURL;
+ (BOOL) isLoaded;
+ (void) setBacktoMainScene;

@end
