//
//  AppDelegate.h
//  BrainTunerX
//
//  Created by Leon Yuu on 10/5/12.
//  Copyright __MyCompanyName__ 2012. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "cocos2d.h"
#import "MainScene.h"
#import "GameConfig.h"
#import "RootViewController.h"
#import "OptionsViewController.h"
#import "HighScoreViewController.h"
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>
#import <Chartboost/Chartboost.h>
#import "SecurityManager.h"
#import "L0SolicitReviewController.h"
#import "Flurry.h"
#import "PromoAds.h"

#define UIAppDelegate ((AppDelegate *)[UIApplication sharedApplication].delegate)

@class RootViewController;

@interface AppDelegate : NSObject <UIApplicationDelegate, MFMailComposeViewControllerDelegate, RevMobAdsDelegate, ChartboostDelegate, UIAlertViewDelegate>
{
	UIWindow *window;
    RootViewController * viewController;
    PromoAds * promoAds;
    
    UIAlertView * loadingAlert;
}

@property (nonatomic, retain) UIWindow *window;
- (UIViewController *) getRootViewController;

@end
