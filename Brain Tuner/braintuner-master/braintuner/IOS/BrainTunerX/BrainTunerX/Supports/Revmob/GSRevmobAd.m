//
//  GSRevMobAd.m
//  Whiteboard
//
//  Created by Hector Zhao on 9/27/12.
//
//

#import "GSRevMobAd.h"

@implementation GSRevmobAd
@synthesize delegate = _delegate;

- (void)requestBannerAd:(GADAdSize)adSize
              parameter:(NSString *)serverParameter
                  label:(NSString *)serverLabel
                request:(GADCustomEventRequest *)request  {
    DLog(@"AdMob Mediation Start Custom Revmob Ad");
//    [RevMobAds session].testingMode = kTestRevmobAds ? RevMobAdsTestingModeWithAds:RevMobAdsTestingModeOff;
//    if([DeviceHelper isIOS6OrHigher]){
//        [RevMobAds showBannerAdWithDelegate:self];
//    }else{
//        [RevMobAds showBannerAdWithDelegate:self withSpecificOrientations:UIInterfaceOrientationPortrait, nil];
//    }
}

- (void) revmobAdDidReceive {
    [self.delegate customEventBanner:self didReceiveAd:nil];
}

- (void) revmobAdDidFailWithError:(NSError *)error {
    [self.delegate customEventBanner:self didFailAd:error];
}

- (void) revmobAdDisplayed {
    [self.delegate customEventBannerWillPresentModal:self];
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationShowRevMobAd object:nil];
}

- (void) revmobUserClickedInTheAd {
    // Nothing to do actually
}

- (void) revmobUserClosedTheAd {
    // Nothing to do actually
}

@end
