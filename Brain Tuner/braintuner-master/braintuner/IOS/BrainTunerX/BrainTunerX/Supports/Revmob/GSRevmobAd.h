//
//  GSRevMobAd.h
//  Whiteboard
//
//  Created by Hector Zhao on 9/27/12.
//
//

#import <Foundation/Foundation.h>
#import <GoogleMobileAds/GoogleMobileAds.h>
#import <RevMobAds/RevMobAds.h>
#import "DeviceHelper.h"

#define kNotificationShowRevMobAd @"kNotificationShowRevMobAd"
#define kNotificationHideRevMobAd @"kNotificationHideRevMobAd"
#define kNotificationFullRevMobAd @"kNotificationFullRevMobAd"

@interface GSRevmobAd : NSObject <GADCustomEventBanner, RevMobAdsDelegate>

@end
