/**
 * MyAdView.m
 * Selects between AdMob and Mobclix, and provides failover when one network can't fill the request.
 *
 * Elliot Lee <elliot@greengar.com>
 * GreenGar Studios
 */

#import "GSAdView.h"
#import "SynthesizeSingleton.h"

#define alertPromoTag        888

@implementation GSAdView
@synthesize containerViewController;
@synthesize alertPromoURL = alertPromoURL_;
@synthesize promoTitle = promoTitle_;
@synthesize promoMessage = promoMessage_;
@synthesize promoCancelButtonTitle = promoCancelButtonTitle_;
@synthesize promoOtherButtonTitle = promoOtherButtonTitle_;
@synthesize iTunesURL;
@synthesize isHidden;
@synthesize isPortrait;

BOOL TTIsStringWithAnyText(id object) {
	return [object isKindOfClass:[NSString class]] && [(NSString*)object length] > 0;
}

SYNTHESIZE_SINGLETON_FOR_CLASS(GSAdView);

// Used by singleton synthesizer
- (id)init {
	if ((self = [super initWithFrame:CGRectZero])) {
		// be sure to call -start after this (after setting delegate)
        // Init GGLocal Ads
        localAds = [[GGLocalAds alloc] init];
        localAds.delegate = self;
		
        //KONG: check for multitasking
        UIDevice *device = [UIDevice currentDevice];
        if ([device respondsToSelector:@selector(isMultitaskingSupported)] && [device isMultitaskingSupported]) {
            [[NSNotificationCenter defaultCenter] addObserver:self 
                                                     selector:@selector(applicationWillEnterForeground:) 
                                                         name:UIApplicationWillEnterForegroundNotification 
                                                       object:nil];
        }		
	}
	return self;
}

- (void)applicationWillEnterForeground:(NSNotification *)notification {

}

- (void)start {
	[self setBackgroundColor:[UIColor whiteColor]];

	if (IS_IPAD) {
        adButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, self.containerViewController.view.bounds.size.width, 90)];
        [adButton setImage:[UIImage imageNamed:@"HouseAd@2x.png"] forState:UIControlStateNormal];
	} else {
        adButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, self.containerViewController.view.bounds.size.width, 50)];
        [adButton setImage:[UIImage imageNamed:@"HouseAd.png"] forState:UIControlStateNormal];
	}
	
	
	[adButton addTarget:self action:@selector(adButtonTouchUpInside) forControlEvents:UIControlEventTouchUpInside];
	
	[self addSubview:adButton];
    [self bringSubviewToFront:adButton];
	
	self.clipsToBounds = NO;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didShowRevMobAd)
                                                 name:kNotificationShowRevMobAd
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didHideRevMobAd)
                                                 name:kNotificationHideRevMobAd
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(showFullScreenRevmob)
                                                 name:kNotificationFullRevMobAd
                                               object:nil];
    
    // Start GG Local Ads Request
    [localAds startRequest];
}

- (UIViewController *)viewControllerForPresentingModalView {
	return self.containerViewController;
}

#pragma mark - GG Local Ads Delegate
- (void) showLocalAds:(BOOL)shouldShowLocalAds{
    if(shouldShowLocalAds){
        // Show Local Ads Only
        DLog(@"Local Ads Only");
        [localAds showAdsinView:self];
    }else{
        // Start Admob Mediation Request
        DLog(@"Start Admob Mediation");
        // Init Admob Mediation
        if(IS_IPAD){
            adMobMediationView = [[GADBannerView alloc] initWithAdSize:kGADAdSizeLeaderboard];
            adMobMediationView.adUnitID = @"d2a65581e3e54823";
        }else{
            adMobMediationView = [[GADBannerView alloc] initWithAdSize:kGADAdSizeBanner];
            adMobMediationView.adUnitID = @"ef4d6130f5e94cfa";            
        }
        adMobMediationView.delegate = self;
        gotAds = NO;
        
        [self startAdmobRequest];
        
    }
}

- (void) onGGLocalAdsTapInside:(NSString *)url{
    DLog();
    
    loadingAlert = [[UIAlertView alloc] initWithTitle:@"Loading..." message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:@"Cancel", nil];
    [loadingAlert show];
    [self openReferralURL:[NSURL URLWithString:url]];
}

#pragma mark -- Admob Mediation Delegate
- (void)startAdmobRequest {
    adMobMediationView.rootViewController = containerViewController;
    [adMobMediationView loadRequest:[GADRequest request]];
}

- (void)adViewDidReceiveAd:(GADBannerView *)view {
    DLog(@"AdMob Mediation Received Ad: %@", view);
    if (isShowingRevMob) {
        return;
    }
    
    if (!gotAds) {
        gotAds = YES;
        
        if (![adMobMediationView superview]) {
            [self addSubview:adMobMediationView];
        } else {
            [self bringSubviewToFront:adMobMediationView];
        }
    }
    
    [self recalculateAdPostion];
    
}

- (void)adView:(GADBannerView *)view didFailToReceiveAdWithError:(GADRequestError *)error {
    DLog(@"AdMob Mediation Failed to load Ad: %@", error);
}

- (void)didShowRevMobAd {
    isShowingRevMob = YES;
    
    if (isHidden) { // If GSAdView is hidden already, hide the RevMob also
        [self dismissRevMob];
    }
    
    // Will hide it after 59s
    [self performSelector:@selector(hideRevMob) withObject:nil afterDelay:AD_REFRESH_PERIOD - 1];
}

- (void)didHideRevMobAd {
    isHidden = YES;
    [self dismissRevMob];
}

- (void)hideRevMob {
    [[RevMobAds session] hideBanner];
    isShowingRevMob = NO;
}

- (void)dismissRevMob {
    [[RevMobAds session] hideBanner];
}

- (void)showFullScreenRevmob {
    DLog();
    [RevMobAds session].testingMode = kTestRevmobAds ? RevMobAdsTestingModeWithAds:RevMobAdsTestingModeOff;
    [[RevMobAds session] showFullscreen];
}

- (void)recalculateAdPostion {
    
	GADAdSize adSize = [adMobMediationView adSize];
	CGRect newFrame = adMobMediationView.frame;
	
    DLog(@"%f-%f",adSize.size.width,adSize.size.height);
    
	newFrame.size = adSize.size;
	newFrame.origin.x = (self.containerViewController.view.frame.size.width - adSize.size.width) / 2;
	newFrame.origin.y = (self.frame.size.height - adSize.size.height); // / 2;
    
    if (newFrame.origin.y < 0) {
        newFrame.origin.y = 0;
    }
	//DLog(@"************* %f", newFrame.origin.y);
	adMobMediationView.frame = newFrame;
}

- (void)showPromoUsingRowURL:(BOOL)usingRowURL {
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:self.promoTitle
													message:self.promoMessage
												   delegate:self
										  cancelButtonTitle:(TTIsStringWithAnyText(self.promoCancelButtonTitle) ? self.promoCancelButtonTitle : nil)
										  otherButtonTitles:(TTIsStringWithAnyText(self.promoOtherButtonTitle) ? self.promoOtherButtonTitle : nil), nil];
	[alert show];
	[alert release];
}

#pragma mark -
#pragma mark Affiliate Link

// Process a LinkShare/TradeDoubler/DGM URL to something iPhone can handle
- (void)openReferralURL:(NSURL *)referralURL {
	self.iTunesURL = referralURL;
	DLog(@"self.iTunesURL = %@", self.iTunesURL);
    NSURLConnection *conn = [[NSURLConnection alloc] initWithRequest:[NSURLRequest requestWithURL:referralURL] delegate:self startImmediately:YES];
    [conn release];
}

// Save the most recent URL in case multiple redirects occur
// "iTunesURL" is an NSURL property in your class declaration
- (NSURLRequest *)connection:(NSURLConnection *)connection willSendRequest:(NSURLRequest *)request redirectResponse:(NSURLResponse *)response {
	DLog(@"self.iTunesURL = %@", self.iTunesURL);
	if (self.iTunesURL) {
		self.iTunesURL = [response URL];
		if (!self.iTunesURL) {
			self.iTunesURL = [request URL];
		}
		DLog(@"self.iTunesURL = %@", self.iTunesURL);
		return request;
	}
	return nil;
}

// No more redirects; use the last URL saved
- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
	DLog(@"self.iTunesURL = %@", self.iTunesURL);
	if (self.iTunesURL)
		[[UIApplication sharedApplication] openURL:self.iTunesURL];
	[loadingAlert dismissWithClickedButtonIndex:[loadingAlert cancelButtonIndex] animated:NO];
	DLog(@"releasing loadingAlert");
	[loadingAlert release], loadingAlert = nil;
}

#define kCouldntReach @"Couldn't reach Greengar.com. Open in Safari?"
- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
	[loadingAlert dismissWithClickedButtonIndex:[loadingAlert cancelButtonIndex] animated:NO];
	DLog(@"releasing loadingAlert");
	[loadingAlert release], loadingAlert = nil;
	
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Unable to Connect" message:kCouldntReach delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
	[alert show];
	[alert release];
}

#define kLoadingTitle @"Loading..."
- (void)showLoadingAlert {
	if (!loadingAlert) { // just being defensive
		// show Loading... alert
		loadingAlert = [[UIAlertView alloc] initWithTitle:kLoadingTitle message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:@"Hide", nil];
		[loadingAlert show];
	}
}

- (void)adButtonTouchUpInside {
    DLog();

	NSString *iTunesLink = @"http://itunes.apple.com/app/brain-tuner-2-pro/id288322298?mt=8"; // GOLD
	
	loadingAlert = [[UIAlertView alloc] initWithTitle:kLoadingTitle message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:@"Cancel", nil];
	[loadingAlert show];
	
	[self openReferralURL:[NSURL URLWithString:iTunesLink]];
}

- (void) alertView:(UIAlertView*)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
#if GOLD
	if ([alertView.message isEqualToString:kCouldntReach]) {
		if (buttonIndex == alertView.firstOtherButtonIndex) {
			NSString *urlString = @"http://www.greengar.com/?whiteboardlite=1";
			DLog(@"opening %@", urlString);
			[[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlString]];
		}
	} else if ([alertView.title isEqualToString:kLoadingTitle]) {
		if (buttonIndex == alertView.firstOtherButtonIndex) {
			self.iTunesURL = nil;
			[loadingAlert release], loadingAlert = nil;
		}
	} else
		
#endif          
		if (alertView.firstOtherButtonIndex == buttonIndex) {
			[[UIApplication sharedApplication] openURL:[NSURL URLWithString:self.alertPromoURL]];
		}
}

- (void)dealloc {
    [containerViewController release];
	[[NSNotificationCenter defaultCenter] removeObserver:self];
	[super dealloc];
}

@end