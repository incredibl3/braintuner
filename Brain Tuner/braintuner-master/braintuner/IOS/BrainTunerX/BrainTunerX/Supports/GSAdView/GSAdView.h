/**
 * AdMobSampleProgrammaticAdAppDelegate.h
 * AdMob iPhone SDK publisher code.
 * 
 */

#define AD_REFRESH_PERIOD 30.0 // display fresh ads once per minute

#import <UIKit/UIKit.h>

#import "GGLocalAds.h"
#import <GoogleMobileAds/GoogleMobileAds.h>
#import <RevMobAds/RevMobAds.h>
#import "GSRevmobAd.h"

#define kiPadAdsMaxHeight 66
#define kiPhoneAdsMaxHeight 50

@interface GSAdView : UIView <UIAlertViewDelegate,GGLocalAdsDelegate, GADBannerViewDelegate> {
	UIButton *adButton;
	UIAlertView *loadingAlert;
	NSURL *iTunesURL;
	
    GGLocalAds *localAds;
    GADBannerView * adMobMediationView;

	BOOL gotAds;
    BOOL isPortrait;
    BOOL isShowingRevMob;
    BOOL isHidden;
    UIViewController *containerViewController;
}

@property (nonatomic, retain) NSURL *iTunesURL;
@property (copy, nonatomic) NSString *alertPromoURL;
@property (copy, nonatomic) NSString *promoTitle;
@property (copy, nonatomic) NSString *promoMessage;
@property (copy, nonatomic) NSString *promoCancelButtonTitle;
@property (copy, nonatomic) NSString *promoOtherButtonTitle;
@property (nonatomic, retain) UIViewController *containerViewController;
@property (nonatomic) BOOL isPortrait;
@property (nonatomic) BOOL isHidden;

+ (GSAdView *)sharedGSAdView;

- (void)start;
- (void)recalculateAdPostion;
- (void)adButtonTouchUpInside;

@end