//
//  DeviceHelper.h
//  Whiteboard
//
//  Created by Hector Zhao on 9/25/12.
//
//

#import <Foundation/Foundation.h>

@interface DeviceHelper : NSObject

+ (BOOL) isIOS5OrHigher;
+ (BOOL) isIOS6OrHigher;
+ (BOOL) isIOS7OrHigher;
+ (BOOL) isIOS8OrHigher;

@end
