//
//  DeviceHelper.m
//  Whiteboard
//
//  Created by Hector Zhao on 9/25/12.
//
//

#import "DeviceHelper.h"

@implementation DeviceHelper

+ (BOOL) isIOS5OrHigher
{
    NSString *reqSysVer = @"5.0";
    NSString *currSysVer = [[UIDevice currentDevice] systemVersion];
    if ([currSysVer compare:reqSysVer options:NSNumericSearch] != NSOrderedAscending)
    {
        return YES;
    }
    return NO;
}

+ (BOOL) isIOS6OrHigher
{
    NSString *reqSysVer = @"6.0";
    NSString *currSysVer = [[UIDevice currentDevice] systemVersion];
    if ([currSysVer compare:reqSysVer options:NSNumericSearch] != NSOrderedAscending)
    {
        return YES;
    }
    return NO;
}

+ (BOOL) isIOS7OrHigher
{
    NSString *reqSysVer = @"7.0";
    NSString *currSysVer = [[UIDevice currentDevice] systemVersion];
    if ([currSysVer compare:reqSysVer options:NSNumericSearch] != NSOrderedAscending)
    {
        return YES;
    }
    return NO;
}

+ (BOOL) isIOS8OrHigher
{
    NSString *reqSysVer = @"8.0";
    NSString *currSysVer = [[UIDevice currentDevice] systemVersion];
    if ([currSysVer compare:reqSysVer options:NSNumericSearch] != NSOrderedAscending)
    {
        return YES;
    }
    return NO;
}

@end
