/*
 *  FlurryAPI+Extensions.h
 *  Flashlight
 *
 *  Created by Elliot on 6/30/10.
 *  Copyright 2010 GreenGar Studios. All rights reserved.
 *
 */

#import "Flurry.h"

#define START_TIMED_EVENT(x) ([Flurry logEvent:x timed:YES])

#define START_TIMED_EVENT_PARAMS(event, parameters) ([Flurry logEvent:(event) withParameters:(parameters) timed:YES])

#define END_TIMED_EVENT(x) ([Flurry endTimedEvent:x withParameters:nil]) // updated for v2.7 (withParameters:)

#define LOG_EVENT(x) ([Flurry logEvent:x])

#define LOG_EVENT_PARAMS(x, y) ([FlurryAPI logEvent:x withParameters:y])
