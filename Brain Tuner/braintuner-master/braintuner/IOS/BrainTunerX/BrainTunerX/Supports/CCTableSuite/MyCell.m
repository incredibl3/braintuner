//
//  MyCell.m
//  CCTableViewTest
//
//  Created by Ray Wenderlich on 3/1/11.
//  Copyright 2011 Ray Wenderlich. All rights reserved.
//

#import "MyCell.h"
#import "CCNode.h"

@implementation MyCell

+(CGSize)cellSize {
    
    CGSize screenSize = [CCDirector sharedDirector].winSize;
    
    if (IS_IPAD) {
        return CGSizeMake(screenSize.width, 145);
    }
    else {
        return CGSizeMake(screenSize.width, 60);
    }
    
}

@end
