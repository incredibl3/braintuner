//
//  GameUtils.h
//  BrainTuner2
//
//  Created by Hector Zhao on 3/2/11.
//  Copyright 2011 Greengar Studios. All rights reserved.
//

#import <Foundation/Foundation.h>

// Fonts
#define kDenneKittenHeelsFont               @"Denne Kitten Heels"
#define kHelveticaBoldFont                  @"Helvetica-Bold"

// Other Constant Values
#define kCurrentNumberProblem               @"CurrentNumberProblem"
#define kCurrentDifficulty                  @"CurrentDifficulty"
#define kMaxDifficulty                      @"MaxDifficulty"
#define kBestTime20                         @"BestTime20"
#define kBestTime60                         @"BestTime60"
#define kBestTime100                        @"BestTime100"
#define kBestAccuracy20                     @"BestAccuracy20"
#define kBestAccuracy60                     @"BestAccuracy60"
#define kBestAccuracy100                    @"BestAccuracy100"
#define kUserID                             @"UserID"
#define kBrainTunerURLString                @"http://www.greengarstudios.com/redirect/bt1braintuner.php"
#define kViewHighScoreURL                   @"http://greengarstudios.com/bt2/index.php/brain_tuner_scorer/view_global_scorer"
#define kSendEmailDev                       @"SendEmailDev"
#define kSendEmailFriend                    @"SendEmailFriend"
#define kShowOptionsView                    @"ShowOptionsView"
#define kShowHighScoreWebView               @"ShowHighScoreWebView"
#define kShowRevmobAdsLink                  @"ShowRevmobAdsLink"

// Other Tag
#define kPlaySceneTotalEmptyRows            ((IS_IPHONE5) ? 6:5)
#define kTagForLabel                        990
#define kTagForSymbol                       991

// Dialog Tag
#define kResultDialogTag                    0
#define kResultDialogTagWithUseID           1
#define kSubmittingDialogTag                2
#define kInternetErrorDialogTag             3
#define kSubmitScoreEmptyUserID             4
#define kSubmitScoreDoneDialogTag           5
#define kSubmitScoreFailedDialogTag         6
#define kResetBestTimeDialog                7
#define kNoRecordYetDialog                  8
#define kReviewDialogTag                    9
#define kLoadingHighScoreErrorDialogTag     10
#define kSignOutDialogTag                   11
#define kSignInErrorDialogTag               12

#ifdef DEBUG
#define kBuildType @"DEBUG"
#else
#define kBuildType @""
#endif

#define kVersionIndex                       @"VersionIndex"
#define kGameIndexNumber                    @"GameIndexNumber"

#define kFirstLaunch                        @"First Launch"
#define OPT_SOUND                           @"Sound"
#define OPT_COUNTDOWN                       @"Countdown Timer"
#define OPT_ADD                             @"Addition"
#define OPT_SUB                             @"Subtraction"
#define OPT_MUL                             @"Multiplication"
#define OPT_DIV                             @"Division"
#define OPT_REV                             @"Review Answers"
#define OPT_POS                             @"Positive Numbers Only"
#define OPT_NEG                             @"Negative Numbers"
#define OPT_NSL                             @"Use ÷ for Division"
#define OPT_SWP                             @"Switch Right/Wrong"
#define OPT_BIG                             @"Use Big Number"

// Options View
#define ROW_COUNT                           8 // Number of Options Row Actions
#define EMAIL_ROW                           ROW_COUNT - 1 // Index of Email Row
#define OPT_SOUND_ROW                       1
#define OPT_COUNTDOWN_ROW                   2
#define OPT_ADD_ROW                         3
#define OPT_SUB_ROW                         4
#define OPT_MUL_ROW                         5
#define OPT_DIV_ROW                         6
#define OPT_REV_ROW                         7

// Sounds
#define kWrongSound                         @"wrong.wav"
#define kRightSound                         @"right.wav"
#define kCountDownSound                     @"countdown.wav"
#define kCompleteGame                       @"completegame.wav"
#define kButtonClick                        @"buttonclick.wav"
#define kLevelSwicth                        @"button.wav"

// Show/Hide Ads View
#define kShowAdViewNotification             @"ShowAdViewNotification"
#define kHideAdViewNotification             @"HideAdViewNotification"

// Security
#define kLocalStorageKey                    @"LocalStorageKey"
#define kSubmitName                         @"SubmitScoreName"
#define kSubmitEmail                        @"SubmitScoreEmail"

// Link
#define kAppiTunesLink                      @"http://bit.ly/GgComBrainTuner1Lite"
#define kGGAppiTunesLink                    @"http://bit.ly/GgComAllGreengarApps"

// Flurry
#define kFlurryAppID                        @"edfd1da6097d14ef513f0839e6c0eef4"
#define kStartGame20                        @"StartGame20"
#define kStartGame60                        @"StartGame60"
#define kStartGame100                       @"StartGame100"
#define kResetRecords                       @"ResetRecords"
#define kShareFB                            @"ShareFB"
#define kShareTwitter                       @"ShareTwitter"
#define kShareEmail                         @"ShareEmail"
#define kOpenSettingsPage                   @"OpenSettingsPage"
#define kOpenHighScorePage                  @"OpenHighScorePage"
#define kMoreButtonEvent                    @"OpenRevMobAdLink"

@interface GameUtils : NSObject {

}

@end
