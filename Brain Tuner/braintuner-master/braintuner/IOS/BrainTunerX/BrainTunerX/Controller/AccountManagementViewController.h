//
//  AccountManagementViewController.h
//  BrainTunerX
//
//  Created by Leon Yuu on 11/2/12.
//
//

#import <UIKit/UIKit.h>
#import "cocos2d.h"
#import "MainScene.h"
#import "Reachability.h"
#import "SubmitScoreManager.h"
#import "AccountManagerDelegate.h"
#import "AdsScene.h"

typedef enum {
  StateOptions = 0,
  StateSignUp,
  StateSignIn,
  StateSubmit
} AccountPageState;

@interface AccountManagementViewController : UIViewController<UITextFieldDelegate, SubmitScoreDelegate, UIAlertViewDelegate> {
    AccountPageState state;
    SubmitScoreManager * submitScoreManager;
    BOOL isStillInAccountManagementPage;
}

- (IBAction)onCancelTouchUpInside:(id)sender;
@property (nonatomic,retain) IBOutlet UINavigationBar * navigationBar;

#pragma mark - Login View
@property (nonatomic,retain) IBOutlet UIView * loginView;

- (IBAction)onLoginFacbookTouchUpInside:(id)sender;
- (IBAction)onLoginGreengarTouchUpInside:(id)sender;

#pragma mark - Sign-up View
@property (nonatomic,retain) IBOutlet UIView      * signUpView;
@property (nonatomic,retain) IBOutlet UITextField * firstNameSignUp;
@property (nonatomic,retain) IBOutlet UITextField * lastNameSignUp;
@property (nonatomic,retain) IBOutlet UITextField * emailAddressSignUp;
@property (nonatomic,retain) IBOutlet UITextField * passWordSignUp;
@property (nonatomic,retain) IBOutlet UIButton    * signUpButton;
@property (nonatomic,retain) IBOutlet UIButton    * alreadyHasAccountButton;

- (IBAction)onSignUpButtonTouchUpInside:(id)sender;
- (IBAction)onAlreadyHasAccountTouchUpInside:(id)sender;

#pragma mark - Sign-in View
@property (nonatomic,retain) IBOutlet UIView      * signInView;
@property (nonatomic,retain) IBOutlet UITextField * emailAddressSignIn;
@property (nonatomic,retain) IBOutlet UITextField * passwordSignIn;
@property (nonatomic,retain) IBOutlet UIButton    * signInButton;
@property (nonatomic,retain) IBOutlet UIButton    * createAccountButton;
@property (nonatomic,retain) IBOutlet UIButton    * forgotPasswordButton;

- (IBAction)onSignInButtonTouchUpInside:(id)sender;
- (IBAction)oncreateAccountButtonTouchUpInside:(id)sender;
- (IBAction)onForgotPasswordTouchUpInside:(id)sender;

#pragma mark - Submit View
@property (nonatomic,retain) IBOutlet UIView      * submitView;
@property (nonatomic,retain) IBOutlet UILabel     * yourScore;
@property (nonatomic,retain) IBOutlet UILabel     * yourSubmitID;
@property (nonatomic,retain) IBOutlet UILabel     * yourEmail;

- (IBAction)onSubmitScoreTouchUpInside:(id)sender;
- (IBAction)onSignOutTouchUpInside:(id)sender;

@end
