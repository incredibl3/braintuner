//
//  RootViewController.h
//  cocostemplate
//
//  Created by Leon Yuu on 10/15/12.
//  Copyright __MyCompanyName__ 2012. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GSAdView.h"
#import "GameManager.h"
#import "RootViewController.h"

@class RootViewController;
@interface RootViewController : UIViewController {
    GSAdView * adView;
    RootViewController * rootViewController;
}

- (void) initAdView;
- (void) onSetFrame;

@end
