//
//  OptionsViewController.h
//  BrainTuner3
//
//  Created by Elliot Lee on 8/23/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GameUtils.h"
#import "GSAudio.h"
#import "GSAdView.h"

#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>

@class MainViewController;

@interface OptionsViewController : UIViewController <MFMailComposeViewControllerDelegate,UIAlertViewDelegate>
{
	//IBOutlet UITableView tableView;
	IBOutlet UILabel *versionLabel;
	UITableView *_optionsTable;
}

@property (nonatomic, retain) IBOutlet UITableView *optionsTable;
- (IBAction)clickDone;

@end
