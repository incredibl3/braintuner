//
//  HighScoreTableViewCell.h
//  BrainTunerX
//
//  Created by Hector Zhao on 3/24/15.
//
//

#import <UIKit/UIKit.h>

@interface HighScoreTableViewCell : UITableViewCell

- (void)setRank:(NSString *)rank name:(NSString *)name score:(NSString *)score;

@end
