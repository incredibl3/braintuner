//
//  RootViewController.m
//  cocostemplate
//
//  Created by Leon Yuu on 10/15/12.
//  Copyright __MyCompanyName__ 2012. All rights reserved.
//

//
// RootViewController + iAd
// If you want to support iAd, use this class as the controller of your iAd
//

#import "cocos2d.h"

#import "RootViewController.h"
#import "GameConfig.h"

@implementation RootViewController


 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
	DLog();
    if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) {
        // Custom initialization
	}
	return self;
}

 // Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView {
    DLog();
}


 // Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    DLog();
	[super viewDidLoad];
}

- (void) initAdView {
    DLog();
    // Init GSAdView
    adView = [[GSAdView sharedGSAdView] retain];
    adView.containerViewController = self;
    [self onSetFrame];
    [adView start];
    
    // Add Observer for Add
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onShowAdView:) name:kShowAdViewNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onHideAdView:) name:kHideAdViewNotification object:nil];
}

- (void) onSetFrame {
    CGSize adSize;
    if (IS_IPAD) {
        adSize = CGSizeMake(728, 90);
    } else {
        adSize = CGSizeMake(320, 50);//[adView actualAdSize];
    }
    
    CGSize screenSize = [CCDirector sharedDirector].winSize;
    adView.clipsToBounds = YES;
    adView.frame = CGRectMake(0, screenSize.height - adSize.height, screenSize.width, adSize.height);
    [self.view bringSubviewToFront:adView];
}

- (void) onShowAdView:(NSNotification *) sender {
    DLog();
    // Never show Ad on game during running time
    if ([GameManager sharedGameManager].isGameRunning) {
        DLog();
        return;
    }
    
    // currently, we only show banners in BT2 Lite
    [self.view addSubview:adView];
    [self onSetFrame];
//    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationHideRevMobAd object:nil];
}

- (void) onHideAdView:(NSNotification *) sender {
    DLog();
//    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationHideRevMobAd object:nil];
    [adView removeFromSuperview];
}

//
// This callback only will be called when GAME_AUTOROTATION == kGameAutorotationUIViewController
//
#if GAME_AUTOROTATION == kGameAutorotationUIViewController
-(void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
	//
	// Assuming that the main window has the size of the screen
	// BUG: This won't work if the EAGLView is not fullscreen
	///
	CGRect screenRect = [[UIScreen mainScreen] bounds];
	CGRect rect = CGRectZero;

	
	if(toInterfaceOrientation == UIInterfaceOrientationPortrait || toInterfaceOrientation == UIInterfaceOrientationPortraitUpsideDown)		
		rect = screenRect;
	
	else if(toInterfaceOrientation == UIInterfaceOrientationLandscapeLeft || toInterfaceOrientation == UIInterfaceOrientationLandscapeRight)
		rect.size = CGSizeMake( screenRect.size.height, screenRect.size.width );
	
	CCDirector *director = [CCDirector sharedDirector];
	EAGLView *glView = [director openGLView];
	float contentScaleFactor = [director contentScaleFactor];
	
	if( contentScaleFactor != 1 ) {
		rect.size.width *= contentScaleFactor;
		rect.size.height *= contentScaleFactor;
	}
	glView.frame = rect;
}
#endif // GAME_AUTOROTATION == kGameAutorotationUIViewController

// Supported orientations: Landscape. Customize it for your own needs
- (BOOL) shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

// Supported orientations: Landscape. Customize it for your own needs - Only iOS 6
- (BOOL) shouldAutorotate{
    return NO;
}

- (NSUInteger) supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskPortrait;
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
    [adView release];
    [super dealloc];
}


@end

