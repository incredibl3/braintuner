//
//  HighScoreTableViewCell.m
//  BrainTunerX
//
//  Created by Hector Zhao on 3/24/15.
//
//

#import "HighScoreTableViewCell.h"
#import "DeviceHelper.h"

@interface HighScoreTableViewCell()

@property (nonatomic, retain) UILabel *rankLabel;
@property (nonatomic, retain) UILabel *nameLabel;
@property (nonatomic, retain) UILabel *scoreLabel;

@end

@implementation HighScoreTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier])
    {
        [self.contentView addSubview:self.rankLabel];
        [self.contentView addSubview:self.nameLabel];
        [self.contentView addSubview:self.scoreLabel];
        
        [self.contentView setClipsToBounds:YES];
    }
    return self;
}

- (void)setRank:(NSString *)rank name:(NSString *)name score:(NSString *)score
{
    self.rankLabel.text = rank;
    self.nameLabel.text = name;
    self.scoreLabel.text = score;
    
    if ([rank isEqualToString:NSLocalizedString(@"Rank", nil)])
    {
        self.rankLabel.textColor = [UIColor whiteColor];
        self.nameLabel.textColor = [UIColor whiteColor];
        self.scoreLabel.textColor = [UIColor whiteColor];
        self.nameLabel.textAlignment = NSTextAlignmentCenter;
        
        if ([DeviceHelper isIOS7OrHigher])
        {
            self.contentView.backgroundColor = GREEN_COLOR;
        }
        else
        {
            self.backgroundColor = GREEN_COLOR;
        }
    }
    else
    {
        self.rankLabel.textColor = [UIColor blackColor];
        self.nameLabel.textColor = [UIColor blackColor];
        self.scoreLabel.textColor = [UIColor blackColor];
        self.nameLabel.textAlignment = NSTextAlignmentLeft;
        
        if ([DeviceHelper isIOS7OrHigher])
        {
            self.contentView.backgroundColor = [UIColor clearColor];
        }
        else
        {
            self.backgroundColor = [UIColor whiteColor];
        }
    }
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    if ([DeviceHelper isIOS7OrHigher])
    {
        self.rankLabel.frame = CGRectMake(0, 0, self.frame.size.width/5, self.frame.size.height);
        self.nameLabel.frame = CGRectMake(self.frame.size.width/5, 0, self.frame.size.width*3/5, self.frame.size.height);
        self.scoreLabel.frame = CGRectMake(self.frame.size.width*4/5, 0, self.frame.size.width/5, self.frame.size.height);
    }
    else
    {
        self.rankLabel.frame = CGRectMake(0, 0, self.frame.size.width*0.9/5, self.frame.size.height);
        self.nameLabel.frame = CGRectMake(self.frame.size.width*0.9/5, 0, self.frame.size.width*2.7/5, self.frame.size.height);
        self.scoreLabel.frame = CGRectMake(self.frame.size.width*3.7/5, 0, self.frame.size.width/5, self.frame.size.height);
    }
}

- (UILabel *)rankLabel
{
    if (!_rankLabel) {
        _rankLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 320, 55)];
        _rankLabel.backgroundColor = [UIColor clearColor];
        _rankLabel.textAlignment = NSTextAlignmentCenter;
        _rankLabel.font = [UIFont boldSystemFontOfSize:16.0f];
    }
    return _rankLabel;
}

- (UILabel *)nameLabel
{
    if (!_nameLabel) {
        _nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 320, 55)];
        _nameLabel.backgroundColor = [UIColor clearColor];
        _nameLabel.font = [UIFont boldSystemFontOfSize:16.0f];
    }
    return _nameLabel;
}

- (UILabel *)scoreLabel
{
    if (!_scoreLabel) {
        _scoreLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 320, 55)];
        _scoreLabel.backgroundColor = [UIColor clearColor];
        _scoreLabel.textAlignment = NSTextAlignmentCenter;
        _scoreLabel.font = [UIFont boldSystemFontOfSize:16.0f];
    }
    return _scoreLabel;
}

- (void)dealloc
{
    [_rankLabel release];
    [_nameLabel release];
    [_scoreLabel release];
    
    [super dealloc];
}

@end
