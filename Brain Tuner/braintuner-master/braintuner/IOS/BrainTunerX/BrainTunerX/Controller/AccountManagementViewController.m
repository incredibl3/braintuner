//
//  AccountManagementViewController.m
//  BrainTunerX
//
//  Created by Leon Yuu on 11/2/12.
//
//

#import "AccountManagementViewController.h"

#define kYourScoreTitle     @"Your Score is"
#define kYourIDTitle        @"Your ID:"
#define kYourEmailTitle     @"Your Email:"


@interface AccountManagementViewController ()

@end

@implementation AccountManagementViewController
@synthesize loginView,navigationBar;
@synthesize signUpView,firstNameSignUp,lastNameSignUp,emailAddressSignUp,passWordSignUp,signUpButton,alreadyHasAccountButton;
@synthesize signInView,emailAddressSignIn,passwordSignIn,signInButton,createAccountButton,forgotPasswordButton;
@synthesize submitView,yourScore,yourSubmitID,yourEmail;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Do any additional setup after loading the view from its nib.
    
    isStillInAccountManagementPage = YES;
    
    // Init Values for UI Objects
    firstNameSignUp.borderStyle = UITextBorderStyleRoundedRect;
    lastNameSignUp.borderStyle = UITextBorderStyleRoundedRect;
    emailAddressSignUp.borderStyle = UITextBorderStyleRoundedRect;
    passWordSignUp.borderStyle = UITextBorderStyleRoundedRect;
    
    emailAddressSignIn.borderStyle = UITextBorderStyleRoundedRect;
    passwordSignIn.borderStyle = UITextBorderStyleRoundedRect;
    
    // Set Up Account Page State
    NSString *submitName = [NSDEF stringForKey:kSubmitName];
    NSString *submitEmail = [NSDEF stringForKey:kSubmitEmail];
    
    if (submitName != nil && ![submitName isEqualToString:@""] &&
       submitEmail != nil && ![submitEmail isEqualToString:@""])
    {
        state = StateSubmit;
    }
    else
    {
        state = StateOptions;
    }
    
    [self setUpPageWithState:state];
    
    // Init Ads view
    [self initAdView];
}

- (void) initAdView
{
    if ([GSAdView sharedGSAdView] != nil)
    {
        // Already Init in Root View Controller, just get and init here
        // Add AdView to Superview
        [self.view addSubview:[GSAdView sharedGSAdView]];
        [self.view bringSubviewToFront:[GSAdView sharedGSAdView]];
    }
    
}

- (void)setUpPageWithState:(AccountPageState) currentstate
{
    // Set Up Page status
    [self.view endEditing:TRUE];
    switch (currentstate)
    {
        case StateOptions:
            DLog(@"Options Page - FB/GG login");
            loginView.hidden = NO;
            signUpView.hidden = YES;
            signInView.hidden = YES;
            submitView.hidden = YES;
            [self.view bringSubviewToFront:loginView];
            
            [navigationBar.topItem setTitle:@"Submit Score"];
            break;
        case StateSignUp:
            DLog(@"Sign Up Page - GG Sign Up");
            loginView.hidden = YES;
            signUpView.hidden = NO;
            signInView.hidden = YES;
            submitView.hidden = YES;
            [self.view bringSubviewToFront:signUpView];
            
            [navigationBar.topItem setTitle:@"Sign Up"];
            break;
        case StateSignIn:
            DLog(@"Sign In Page - GG Sign In");
            loginView.hidden = YES;
            signUpView.hidden = YES;
            signInView.hidden = NO;
            submitView.hidden = YES;
            [self.view bringSubviewToFront:signInView];
            
            [navigationBar.topItem setTitle:@"Sign In"];
            break;
        case StateSubmit:
            DLog(@"Submit Page - Submit with Score");
            loginView.hidden = YES;
            signUpView.hidden = YES;
            signInView.hidden = YES;
            submitView.hidden = NO;
            [self.view bringSubviewToFront:submitView];
            
            [yourScore setText:[NSString stringWithFormat:@"%@ %.2f seconds",kYourScoreTitle,[GameManager sharedGameManager].finalTime]];
            [yourSubmitID setText:[NSString stringWithFormat:@"%@ %@", kYourIDTitle, [NSDEF stringForKey:kSubmitName]]];
            [yourEmail setText:[NSString stringWithFormat:@"%@ %@", kYourEmailTitle, [NSDEF stringForKey:kSubmitEmail]]];
            
            [navigationBar.topItem setTitle:@"Submit Score"];
            break;
        default:
            break;
    }
    
    [self.view bringSubviewToFront:[GSAdView sharedGSAdView]];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) dealloc {
    [loginView release];
    [navigationBar release];
    
    [signUpView release];
    [firstNameSignUp release];
    [lastNameSignUp release];
    [emailAddressSignUp release];
    [passWordSignUp release];
    [signUpButton release];
    [alreadyHasAccountButton release];
    
    [signInView release];
    [emailAddressSignIn release];
    [passwordSignIn release];
    [signInButton release];
    [createAccountButton release];
    [forgotPasswordButton release];
    
    [submitView release];
    [yourScore release];
    [yourSubmitID release];
    
    [super dealloc];
}

- (IBAction)onCancelTouchUpInside:(id)sender
{
    DLog();
    [self.view endEditing:TRUE];
    [self resetAllTextFieldContent];
    
    switch (state)
    {
        case StateOptions:
            isStillInAccountManagementPage = NO;
            [self dismissViewControllerAnimated:YES completion:nil];
            [self backtoMainScene];
            break;
        case StateSignUp:
            state = StateOptions;
            [self setUpPageWithState:state];
            break;
        case StateSignIn:
            state = StateOptions;
            [self setUpPageWithState:state];
            break;
        case StateSubmit:
            isStillInAccountManagementPage = NO;
            [self dismissViewControllerAnimated:YES completion:nil];
            [self backtoMainScene];
            break;
        default:
            break;
    }
}

- (void)backtoMainScene
{
    [self playSoundButtonClick];
    [GameManager sharedGameManager].isGameRunning = NO;
    [[CCDirector sharedDirector] replaceScene:[MainScene node]];
    [self performSelector:@selector(showAdViewAgain) withObject:self afterDelay:0.5f];
}

- (void)gotoAdsScene
{
    [self.view endEditing:TRUE];
    [self resetAllTextFieldContent];
    
    isStillInAccountManagementPage = NO;
    [self dismissViewControllerAnimated:YES completion:nil];
    
    [self playSoundButtonClick];
    [GameManager sharedGameManager].isGameRunning = NO;
    [[CCDirector sharedDirector] replaceScene:[AdsScene node]];
}

- (void)showAdViewAgain
{
    [[NSNotificationCenter defaultCenter] postNotificationName:kShowAdViewNotification object:nil];
}

- (void)playSoundButtonClick
{
    if ([NSDEF boolForKey:OPT_SOUND]) [GSAudio play:kButtonClick];
}

#pragma mark - Login View Functions
- (IBAction)onLoginFacbookTouchUpInside:(id)sender
{
    [[AccountManager sharedManager] connectWithFacebookFinished:^(BOOL succeed, NSError *error) {
        if (succeed && !error)
        {
            state = StateSubmit;
            [self setUpPageWithState:state];
        }
    }];
}

- (IBAction)onLoginGreengarTouchUpInside:(id)sender
{
    state = StateSignUp;
    [self setUpPageWithState:state];
}

#pragma mark - Sign Up Functions
- (IBAction)onSignUpButtonTouchUpInside:(id)sender
{
    NSString * firstname = firstNameSignUp.text;
    NSString * lastname = lastNameSignUp.text;
    NSString * email = emailAddressSignUp.text;
    NSString * password = passWordSignUp.text;
    
    [[AccountManager sharedManager] registerWithFirstname:firstname
                                                 lastname:lastname
                                                    email:email
                                                 password:password
                                                 finished:^(BOOL succeed, NSError *error) {
         if (succeed && !error)
         {
             state = StateSubmit;
             [self setUpPageWithState:state];
         }
    }];
}

- (IBAction)onAlreadyHasAccountTouchUpInside:(id)sender
{
    state = StateSignIn;
    [self setUpPageWithState:state];
    [self resetAllTextFieldContent];
}

#pragma mark - Sign In Functions
- (IBAction)onSignInButtonTouchUpInside:(id)sender
{
    // Sign In Process
    NSString *email = emailAddressSignIn.text;
    NSString *password = passwordSignIn.text;
    
    [[AccountManager sharedManager] signInWithEmail:email password:password finished:^(BOOL succeed, NSError *error) {
        
        if (succeed && !error)
        {
            state = StateSubmit;
            [self setUpPageWithState:state];
        }
    }];
}

- (IBAction)oncreateAccountButtonTouchUpInside:(id)sender
{
    state = StateSignUp;
    [self setUpPageWithState:state];
    [self resetAllTextFieldContent];
}

- (IBAction)onForgotPasswordTouchUpInside:(id)sender
{
    [[AccountManager sharedManager] showForgotPasswordAlert];
}

#pragma mark - Submit Functions
- (IBAction)onSubmitScoreTouchUpInside:(id)sender
{
    // Check Internet Connection
    if(![[AccountManager sharedManager] checkInternetConnection])
    {
        DLog(@"No Internet Connection");
        return;
    }
    
    // Submit Score
    [self submitGlobalHighScore];
}

- (IBAction)onSignOutTouchUpInside:(id)sender
{
    UIAlertView * alertView = [[UIAlertView alloc] initWithTitle:nil
                                                         message:@"Do you want to Sign Out?"
                                                        delegate:self
                                               cancelButtonTitle:@"No"
                                               otherButtonTitles:@"Yes",nil];
    alertView.tag = kSignOutDialogTag;
    [alertView show];
    [alertView release];
}

- (void)onSignOut
{
    // Sign Out
    [NSDEF setObject:@"" forKey:kSubmitEmail];
    [NSDEF setObject:@"" forKey:kSubmitName];
    
    [[AccountManager sharedManager] signOut];
    
    state  = StateOptions;
    [self setUpPageWithState:state];
}

#pragma mark - Supports Functions
- (void)resetAllTextFieldContent
{
    [firstNameSignUp setText:@""];
    [lastNameSignUp setText:@""];
    [emailAddressSignUp setText:@""];
    [passWordSignUp setText:@""];
    
    [emailAddressSignIn setText:@""];
    [passwordSignIn setText:@""];
}

- (void)submitGlobalHighScore
{
    [[AccountManager sharedManager] showLoadingAlertwithTitle:@"Global High Score"
                                                   andMessage:@"Submitting..."];
    
    if (submitScoreManager)
    {
        [submitScoreManager release];
        submitScoreManager = nil;
    }
    
    submitScoreManager = [[SubmitScoreManager alloc] init];
    submitScoreManager.delegate = self;
    GameManager * gameManager = [GameManager sharedGameManager];
    
    DLog(@"Play Time %f", gameManager.finalTime);
    DLog(@"Accuracy %d", gameManager.Accuracy);
    DLog(@"Number of Problems %d", gameManager.TotalNumberProblems);
    
    [submitScoreManager submitGlobalHighScorewithScore:gameManager.finalTime
                                              accuracy:gameManager.Accuracy
                                         totalProblems:gameManager.TotalNumberProblems
                                                userID:[NSDEF stringForKey:kSubmitName]
                                                userEmail:[NSDEF stringForKey:kSubmitEmail]];
}

#pragma mark - Submit Score Manager Delegate
- (BOOL) onSubmitGlobalHighScoreFinished
{
    [[AccountManager sharedManager] dismissLoadingAlert];
    
    UIAlertView * alertView = [[UIAlertView alloc] initWithTitle:@"Global High Score"
                                                         message:@"Submit your score successfully!"
                                                        delegate:self
                                               cancelButtonTitle:@"OK"
                                               otherButtonTitles:nil];
    alertView.tag = kSubmitScoreDoneDialogTag;
    [alertView show];
    [alertView release];
    
    return YES;
}

- (BOOL) onSubmitGlobalHighScoreFailed
{
    [[AccountManager sharedManager] dismissLoadingAlert];
    
    UIAlertView * alertView = [[UIAlertView alloc] initWithTitle:@"Global High Score"
                                                         message:@"Submit your score failed!\nPlease try again"
                                                        delegate:nil
                                               cancelButtonTitle:@"OK"
                                               otherButtonTitles:nil];
    [alertView show];
    [alertView release];
    
    return YES;
}

#pragma mark - UI Alert View Delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if (isStillInAccountManagementPage)
    {
        if (alertView.tag == kSubmitScoreDoneDialogTag)
        {
            [AdsScene setBacktoMainScene];
            [self performSelector:@selector(gotoAdsScene) withObject:nil afterDelay:0.3f];
        }
        else if(alertView.tag == kSignOutDialogTag)
        {
            if (buttonIndex == 1)
            {
                [self onSignOut];
            }
        }
        else if(alertView.tag == kSignInErrorDialogTag)
        {
            switch (buttonIndex)
            {
                case 0: // New Account
                    
                    state = StateSignUp;
                    [self setUpPageWithState:state];
                    break;
                    
                case 1: // Try again
                    
                    // Do Nothing - just dismiss dialog
                    break;
                    
                case 2:// Forgot password
                    
                    [self onForgotPasswordTouchUpInside:nil];
                    break;
                    
                default:
                    break;
            }
        }
    }
}

/* Deprecated
#pragma mark - GS Authentication Delegate
- (void)authentication:(GSAuthentication *)auth succeeded:(NXOAuth2Client *)oauthClient {
    DLog(@"Sign-in Succeed");
    
    // Get User Data Info
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
							@"me", @"user_id",
							nil];
	
    [GSHelperRequest requestWithEndPoint:kGreengarEndpointGetUserInfo
                                  params:params
                                delegate:self
                                callback:@selector(request:gotUserInfo:)];
    
}

- (void)request:(GSHelperRequest *)request gotUserInfo:(id)result {
    DLog();
    
    [self dismissLoadingAlert];
    
    if ([result isKindOfClass:[NSDictionary class]]) {
        // MSHEEHAN do GC stuff here
        // got the user profile, check association, and maybe request to add it
        
        result = [[GSAppInfo contactClass] contactFromDictionary:result];
        if ([result isKindOfClass:[GSContact class]]) {
            myProfile = result;
            
            NSString * name = myProfile.name;
            NSString * email = myProfile.email;
            
            DLog(@"User Name %@", name);
            DLog(@"User Email %@", email);
            
            [NSDEF setObject:name forKey:kSubmitName];
            [NSDEF setObject:email forKey:kSubmitEmail];
            
            [self showAlertViewwithTitle:@"Sign In Success"
                              andMessage:@"You can submit your score now"
                           andCancelText:@"Ok"
                                  andTag:-1];
            
            state = StateSubmit;
            [self setUpPageWithState:state];
            return;
        }
        
	}
    
    [self showAlertViewwithTitle:@"Sign In Error"
                      andMessage:@"Cannot get user info from server\nPlease try again"
                   andCancelText:@"Ok"
                          andTag:-1];
    
}


- (void)authentication:(GSAuthentication *)auth failed:(NSError *)error {
    DLog(@"Sign-in Failed %@",error);
    [self dismissLoadingAlert];
    
    UIAlertView * alertView = [[UIAlertView alloc] initWithTitle:@"Sign In Error"
                                                         message:@"Please check your username or password"
                                                        delegate:self
                                               cancelButtonTitle:@"New Account"
                                               otherButtonTitles:@"Try again",@"Forgot password?",nil];
    alertView.tag = kSignInErrorDialogTag;
    [alertView show];
    [alertView release];
    
}

- (void)registration:(GSAuthentication *)auth successed:(NXOAuth2Client *)oauthClient {
    DLog(@"Sign-up Success");
    [self dismissLoadingAlert];
    
    NSString * userName = [NSString stringWithFormat:@"%@ %@",firstNameSignUp.text,lastNameSignUp.text];
    NSString * emailAddress = emailAddressSignUp.text;
    
    [NSDEF setObject:userName forKey:kSubmitName];
    [NSDEF setObject:emailAddress forKey:kSubmitEmail];
    
    [self showAlertViewwithTitle:@"Sign Up Successs" andMessage:@"Check your email for verification\nYou can submit your score now" andCancelText:@"Ok" andTag:-1];
    state = StateSubmit;
    [self setUpPageWithState:state];
    
}

- (void)registration:(GSAuthentication *)auth failed:(NSError *)error {
    DLog(@"Sign-up Failed %@",error);
    [self dismissLoadingAlert];
    
    [self showAlertViewwithTitle:@"Sign Up Error" andMessage:@"This email address has aldready been registered" andCancelText:@"Try again" andTag:-1];
    
} */

@end
