//
//  HighScoreViewController.m
//  BrainTunerX
//
//  Created by Leon Yuu on 10/17/12.
//
//

#import "HighScoreViewController.h"
#import "HighScoreTableViewCell.h"
#import <Parse/Parse.h>

@implementation HighScoreViewController
@synthesize result, tableView, doneButton, indicatorView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        self.result = [NSMutableArray new];
        self.result20 = [NSMutableArray new];
        self.result60 = [NSMutableArray new];
        self.result100 = [NSMutableArray new];
    }
    return self;
}

- (IBAction)clickDone:(id)sender
{
    if([NSDEF boolForKey:OPT_SOUND])[GSAudio play:kButtonClick];
    indicatorView.hidden = YES;
    [indicatorView stopAnimating];
    isInHighScorePage = NO;
    [self dismissViewControllerAnimated:YES completion:nil];
    [self performSelector:@selector(showAdViewAgain) withObject:self afterDelay:0.5f];
}

- (IBAction)clickRefresh:(id)sender
{
    [self reloadData];
}

- (void)reloadData
{
    PFQuery *query1 = [PFQuery queryWithClassName:@"HighScore"];
    [query1 whereKey:@"finishTime" greaterThan:@(0)];
    [query1 orderByAscending:@"finishTime"];
    [query1 setLimit:10];
    
    indicatorView.hidden = NO;
    [indicatorView startAnimating];
    
    [query1 findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        
        if (!error)
        {
            [self.result removeAllObjects];
            [self.result addObjectsFromArray:objects];
            [self.tableView reloadData];
        }
        
        indicatorView.hidden = YES;
        [indicatorView stopAnimating];
    }];
    
    PFQuery *query2 = [PFQuery queryWithClassName:@"HighScore"];
    [query2 whereKey:@"finishTime" greaterThan:@(0)];
    [query2 orderByAscending:@"finishTime"];
    [query2 whereKey:@"problems" equalTo:@(20)];
    [query2 setLimit:10];
    
    indicatorView.hidden = NO;
    [indicatorView startAnimating];
    
    [query2 findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        
        if (!error)
        {
            [self.result20 removeAllObjects];
            [self.result20 addObjectsFromArray:objects];
            [self.tableView reloadData];
        }
        
        indicatorView.hidden = YES;
        [indicatorView stopAnimating];
    }];
    
    PFQuery *query3 = [PFQuery queryWithClassName:@"HighScore"];
    [query3 whereKey:@"finishTime" greaterThan:@(0)];
    [query3 orderByAscending:@"finishTime"];
    [query3 whereKey:@"problems" equalTo:@(60)];
    [query3 setLimit:10];
    
    indicatorView.hidden = NO;
    [indicatorView startAnimating];
    
    [query3 findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        
        if (!error)
        {
            [self.result60 removeAllObjects];
            [self.result60 addObjectsFromArray:objects];
            [self.tableView reloadData];
        }
        
        indicatorView.hidden = YES;
        [indicatorView stopAnimating];
    }];
    
    PFQuery *query4 = [PFQuery queryWithClassName:@"HighScore"];
    [query4 whereKey:@"finishTime" greaterThan:@(0)];
    [query4 orderByAscending:@"finishTime"];
    [query4 whereKey:@"problems" equalTo:@(100)];
    [query4 setLimit:10];
    
    indicatorView.hidden = NO;
    [indicatorView startAnimating];
    
    [query4 findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        
        if (!error)
        {
            [self.result100 removeAllObjects];
            [self.result100 addObjectsFromArray:objects];
            [self.tableView reloadData];
        }
        
        indicatorView.hidden = YES;
        [indicatorView stopAnimating];
    }];
}

- (void)showAdViewAgain
{
    [[NSNotificationCenter defaultCenter] postNotificationName:kShowAdViewNotification object:nil];
}

#pragma mark - GS Ad View Methods
- (void) initAdView
{
    if ([GSAdView sharedGSAdView] != nil)
    {
        // Already Init in Root View Controller, just get and init here
        // Add AdView to Superview
        [self.view addSubview:[GSAdView sharedGSAdView]];
    }
    
}

#pragma mark - UI Web View Delegate
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (section == 0)
    {
        return @"All-time High Score";
    }
    else if (section == 1)
    {
        return @"20 Problems";
    }
    else if (section == 2)
    {
        return @"60 Problems";
    }
    else
    {
        return @"100 Problems";
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 4;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0)
    {
        return [self.result count]+1;
    }
    else if (section == 1)
    {
        return [self.result20 count]+1;
    }
    else if (section == 2)
    {
        return [self.result60 count]+1;
    }
    else
    {
        return [self.result100 count]+1;
    }
}

- (UITableViewCell *)tableView:(UITableView *)_tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    HighScoreTableViewCell *cell = (HighScoreTableViewCell *)[_tableView dequeueReusableCellWithIdentifier:@"HighScoreTableViewCell"];
    
    if (cell == nil)
    {
        cell = [[HighScoreTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"HighScoreTableViewCell"];
    }
    
    if ([indexPath row] == 0)
    {
        [cell setRank:@"Rank" name:@"Name" score:@"Time"];
    }
    else
    {
        PFObject *score;
        if ([indexPath section] == 0)
        {
            score = [self.result objectAtIndex:([indexPath row]-1)];
        }
        else if ([indexPath section] == 1)
        {
            score = [self.result20 objectAtIndex:([indexPath row]-1)];
        }
        else if ([indexPath section] == 2)
        {
            score = [self.result60 objectAtIndex:([indexPath row]-1)];
        }
        else
        {
            score = [self.result100 objectAtIndex:([indexPath row]-1)];
        }
        
        [cell setRank:[NSString stringWithFormat:@"%d", (int)[indexPath row]]
                 name:[score objectForKey:@"name"]
                score:[NSString stringWithFormat:@"%.3f", [[score objectForKey:@"finishTime"] floatValue]]];
    }

    return cell;
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == kLoadingHighScoreErrorDialogTag)
    {
        [self clickDone:nil];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    isInHighScorePage = YES;
    
    [self reloadData];
    
    // Init Ad View
    [self initAdView];
    
}

- (void) dealloc {
    [result release];
    [tableView release];
    [indicatorView release];
    [doneButton release];
    [adView release];
    [super dealloc];
}

@end
