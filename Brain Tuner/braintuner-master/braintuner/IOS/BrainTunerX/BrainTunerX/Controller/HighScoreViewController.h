//
//  HighScoreViewController.h
//  BrainTunerX
//
//  Created by Leon Yuu on 10/17/12.
//
//

#import <UIKit/UIKit.h>
#import "GameUtils.h"
#import "GSAudio.h"
#import "GSAdView.h"

@interface HighScoreViewController : UIViewController<UITableViewDelegate, UITableViewDataSource, UIAlertViewDelegate> {
    GSAdView * adView;
    BOOL isInHighScorePage;
}

@property (nonatomic, retain) NSMutableArray *result;
@property (nonatomic, retain) NSMutableArray *result20;
@property (nonatomic, retain) NSMutableArray *result60;
@property (nonatomic, retain) NSMutableArray *result100;
@property (nonatomic, retain) IBOutlet UITableView * tableView;
@property (nonatomic, retain) IBOutlet UIBarButtonItem * doneButton;
@property (nonatomic, retain) IBOutlet UIBarButtonItem * refreshButton;
@property (nonatomic, retain) IBOutlet UIActivityIndicatorView * indicatorView;

- (IBAction)clickDone:(id)sender;
- (IBAction)clickRefresh:(id)sender;

// Ad View Methods
- (void)initAdView;

@end
