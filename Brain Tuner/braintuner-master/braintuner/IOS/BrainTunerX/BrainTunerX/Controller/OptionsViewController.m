//
//  OptionsViewController.m
//  BrainTuner3
//
//  Created by Elliot Lee on 8/23/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import "OptionsViewController.h"

#define LABEL_TAG 3
#define SWITCH_TAG 4
#define LEFT_COLUMN_OFFSET 10
#define RIGHT_COLUMN_OFFSET ((IS_IPAD) ? 640:270)
#define ROW_HEIGHT ((IS_IPAD) ? (90 - 1):(44 -1))// for bottom border
#define SWITCH_WIDTH 94
#define UPPER_ROW_TOP 8
#define MAIN_FONT_SIZE ((IS_IPAD) ? 24:16)

@implementation OptionsViewController

@synthesize optionsTable = _optionsTable;

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return ROW_COUNT;
}


#define EMAIL_CELL_IDENTIFIER @"EmailCell"
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	
    UILabel *label;
	UISwitch *uiswitch = nil;
    
	if (indexPath.row == EMAIL_ROW) {
		UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:EMAIL_CELL_IDENTIFIER];
		if (cell == nil) {
			cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:EMAIL_CELL_IDENTIFIER] autorelease];
            
            CGFloat boundsX = cell.contentView.bounds.origin.x;
            CGFloat boundsY = cell.contentView.bounds.origin.y;
            CGRect frame = CGRectMake(boundsX + LEFT_COLUMN_OFFSET,
                                      boundsY,
                                      boundsX + RIGHT_COLUMN_OFFSET,
                                      ROW_HEIGHT); //label.font.pointSize + 8
            
            label = [[[UILabel alloc] initWithFrame:frame] autorelease]; //CGRectMake(12.0, 0.0, 252.0, 68.0)
            label.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0];
            label.font = [UIFont boldSystemFontOfSize:MAIN_FONT_SIZE]; //[label.font fontWithSize:32.0];
            label.text = @"Send Feedback by Email";
            [cell.contentView addSubview:label];
			cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
		}
		return cell;
	}
	
	static NSString *identity = @"OptionCell";
	
	NSString *option;
	
	// Offset the row by 1 for backward-compatibility with Brain Tuner 1.4
	NSUInteger row = indexPath.row + 1;
	if (row == OPT_SOUND_ROW) {
		option = @"Sound";
	} else if (row == OPT_COUNTDOWN_ROW) {
		option = @"Countdown Timer";
	} else if (row == OPT_ADD_ROW) {
		option = @"Addition";
	} else if (row == OPT_SUB_ROW) {
		option = @"Subtraction";
	} else if (row == OPT_MUL_ROW) {
		option = @"Multiplication";
	} else if (row == OPT_DIV_ROW) {
		option = @"Division";
	} else if (row == OPT_REV_ROW) {
		option = @"Review Answers";
	}
	
    // Analyze: Function call argument is an uninitialized value
	//DLog(@"row:%d option:%@", row, option);
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identity];//CELL_IDENTIFIER
	
	if (cell == nil) {
		
		cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identity] autorelease];
		cell.selectionStyle = UITableViewCellSelectionStyleNone;
		
		CGFloat boundsX = cell.contentView.bounds.origin.x;	
		CGFloat boundsY = cell.contentView.bounds.origin.y;
		CGRect frame = CGRectMake(boundsX + LEFT_COLUMN_OFFSET,
								  boundsY,
								  boundsX + RIGHT_COLUMN_OFFSET,
								  ROW_HEIGHT); //label.font.pointSize + 8
		
		label = [[[UILabel alloc] initWithFrame:frame] autorelease]; //CGRectMake(12.0, 0.0, 252.0, 68.0)
        label.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0];
		label.tag = LABEL_TAG;
		label.font = [UIFont boldSystemFontOfSize:MAIN_FONT_SIZE]; //[label.font fontWithSize:32.0];
		
		[cell.contentView addSubview:label];
		
		
		UISwitch *switchView = [[UISwitch alloc] init];
		switchView.tag = SWITCH_TAG + row;
		[switchView addTarget:self action:@selector(tapSwitch:) forControlEvents:UIControlEventValueChanged];
		cell.accessoryView = switchView;
		[switchView release];
		
	}
	
	// Normal cell with UISwitch
	label = (UILabel *)[cell.contentView viewWithTag:LABEL_TAG];
    label.text = option;
    
	// Use "row" for backward-compatibility with 1.4
    NSMutableArray *options = [NSMutableArray arrayWithObjects:@"",OPT_SOUND,OPT_COUNTDOWN,OPT_ADD,OPT_SUB,OPT_MUL,OPT_DIV,OPT_REV,nil];
	uiswitch = (UISwitch *)cell.accessoryView;
	uiswitch.tag = SWITCH_TAG + row;
	[uiswitch setOn:[NSDEF boolForKey:[options objectAtIndex:row]]];
	
	return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	
	// Apple's documentation on Managing Selections does not animate this deselection
	[tableView deselectRowAtIndexPath:indexPath animated:NO];
	
	if (indexPath.row != EMAIL_ROW) {
		return;
	}
	
	// Send an email to braintunersupport@greengar.com
	
	// This sample can run on devices running iPhone OS 2.0 or later  
	// The MFMailComposeViewController class is only available in iPhone OS 3.0 or later. 
	// So, we must verify the existence of the above class and provide a workaround for devices running 
	// earlier versions of the iPhone OS. 
	// We display an email composition interface if MFMailComposeViewController exists and the device can send emails.
	// We launch the Mail application on the device, otherwise.
	
	// Send Email Here
    [self showConfirmDialog];
    
}

- (void) tapSwitch:(id) sender{
    int tag = (int)[sender tag];
    DLog(@"Switch %i",tag);
    switch (tag - SWITCH_TAG) {
        case 1: // Option Sound
            [NSDEF setBool:![NSDEF boolForKey:OPT_SOUND] forKey:OPT_SOUND];
            DLog(@"Sound %i", [NSDEF boolForKey:OPT_SOUND]);
            break;
        case 2: // Option Count Down
            [NSDEF setBool:![NSDEF boolForKey:OPT_COUNTDOWN] forKey:OPT_COUNTDOWN];
            DLog(@"Countdown %i", [NSDEF boolForKey:OPT_COUNTDOWN]);
            break;
        case 3: // Option Addition
            [NSDEF setBool:![NSDEF boolForKey:OPT_ADD] forKey:OPT_ADD];
            DLog(@"Addition %i", [NSDEF boolForKey:OPT_ADD]);
            break;
        case 4: // Option Subtraction
            [NSDEF setBool:![NSDEF boolForKey:OPT_SUB] forKey:OPT_SUB];
            DLog(@"Subtraction %i", [NSDEF boolForKey:OPT_SUB]);
            break;
        case 5: // Option Multiplication
            [NSDEF setBool:![NSDEF boolForKey:OPT_MUL] forKey:OPT_MUL];
            DLog(@"Multiplication %i", [NSDEF boolForKey:OPT_MUL]);
            break;
        case 6: // Option Division
            [NSDEF setBool:![NSDEF boolForKey:OPT_DIV] forKey:OPT_DIV];
            DLog(@"Division %i", [NSDEF boolForKey:OPT_DIV]);
            break;
        case 7: // Option Review Answers
            [NSDEF setBool:![NSDEF boolForKey:OPT_REV] forKey:OPT_REV];
            DLog(@"Review Answers %i", [NSDEF boolForKey:OPT_REV]);
            break;
        default:
            break;
    }
}

#pragma mark - Compose Email
- (void) showConfirmDialog {
    if ([MFMailComposeViewController canSendMail]) {
        UIAlertView *confirmalert = [[UIAlertView alloc] initWithTitle:@"Send Feedback" message:@"Do you want to send feedback to Greengar?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes",nil];
		[confirmalert show];
		[confirmalert release];
    }else{
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Your device is unable to send email" message:@"Please check your Mail configuration" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[alert show];
		[alert release];
    }
}

- (void) sendEmailDev {
    MFMailComposeViewController *composeViewController = [[MFMailComposeViewController alloc] init];
    NSString *subject = @"[BrainTunerX.iOS] Feedback";
    NSString *emailAddress = @"braintuner.lite.ios@greengar.com";
    NSString *applicationName = @"BrainTunerX.iOS";
    NSString *messageBody = [NSString stringWithFormat:@"\n\n\n=== Application information ===\n---    (please do not remove)   ---\n%@.v%@ %@\n%@\niOS %@\n=========================", applicationName, [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"], kBuildType, [[UIDevice currentDevice] model],[[UIDevice currentDevice] systemVersion]];
    
    [composeViewController setToRecipients:[NSArray arrayWithObject:emailAddress]];
    
    [composeViewController setSubject:subject];
    
    [composeViewController setMessageBody:messageBody isHTML:NO];
    
    composeViewController.mailComposeDelegate = self;
    
    [self presentViewController:composeViewController animated:YES completion:nil];
    
    [composeViewController release];
    
}

#pragma mark - UIAlert View Delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex == 1)
    {
        [self sendEmailDev];
    }
}


// Dismisses the email composition interface when users tap Cancel or Send. Proceeds to update the message field with the result of the operation.
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {	
	[self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Main Functions
- (IBAction)clickDone
{
    if([NSDEF boolForKey:OPT_SOUND])[GSAudio play:kButtonClick];
    [self dismissViewControllerAnimated:YES completion:nil];
    [self performSelector:@selector(showAdViewAgain) withObject:self afterDelay:0.5f];
}

- (void)showAdViewAgain{
    DLog();
    [[NSNotificationCenter defaultCenter] postNotificationName:kShowAdViewNotification object:nil];
}

- (void)viewWillDisappear:(BOOL)animated {
	[super viewWillDisappear:animated];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
	if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) {
		// Initialization code
	}
	return self;
}

- (void)viewDidLoad {
	[super viewDidLoad];
	
	versionLabel.text = [NSString stringWithFormat:@"v %@", [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"]];
	
	self.view.backgroundColor = [UIColor groupTableViewBackgroundColor];
    if([NSDEF boolForKey:OPT_SOUND]){
        [GSAudio load:kButtonClick];
    }
    
    [self initAdView];
}

#pragma mark - GS Ad View Methods
- (void) initAdView {
    DLog();
    
    if([GSAdView sharedGSAdView] != nil){
        DLog();
        // Already Init in Root View Controller, just get and init here
        // Add AdView to Superview
        [self.view addSubview:[GSAdView sharedGSAdView]];
    }
    
}

- (void)viewWillAppear:(BOOL)animated {
	DLog();
}
	 
- (void)viewDidAppear:(BOOL)animated {
	[super viewDidAppear:animated];
	[NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(flashScrollIndicators:) userInfo:nil repeats:NO];
}


- (void)flashScrollIndicators:(NSTimer*)theTimer {
	[self.optionsTable flashScrollIndicators];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
	// Return YES for supported orientations
	return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning]; // Releases the view if it doesn't have a superview
	// Release anything that's not essential, such as cached data
}

- (void)dealloc {
	[super dealloc];
}


@end
