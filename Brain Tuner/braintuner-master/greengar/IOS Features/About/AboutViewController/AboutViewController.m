//
//  AboutViewController.m
//  Greengar
//
//  Created by Hector Zhao on 3/21/11.
//  Copyright 2011 Greengar Studios. All rights reserved.
//

#import "AboutViewController.h"

#define kLoadingTitle @"Loading..."

@implementation AboutViewController

@synthesize iTunesURL;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)dealloc
{
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (IBAction) getMoreApplications {
    AboutManager *aboutManager = [AboutManager sharedAboutManager];
    
	NSString *iTunesLink = [aboutManager getGreengarAppsLink];
	
	NSString *message = nil;
    
	if ([[Reachability reachabilityForInternetConnection] currentReachabilityStatus] == NotReachable) {
		message = @"You do not seem to be connected\nto the Internet.\nThis probably won't work.";
	}
	loadingAlert = [[UIAlertView alloc] initWithTitle:kLoadingTitle message:message delegate:self cancelButtonTitle:nil otherButtonTitles:@"Cancel", nil];
    
	[loadingAlert show];
    
    self.iTunesURL = [NSURL URLWithString:iTunesLink];
    
    NSURLConnection *conn = [[NSURLConnection alloc] initWithRequest:[NSURLRequest requestWithURL:self.iTunesURL] delegate:self startImmediately:YES];
    [conn release];
}

- (IBAction) sendEmailToDeveloper {
    
    if ([MFMailComposeViewController canSendMail]) {
        
        AboutManager *aboutManager = [AboutManager sharedAboutManager];
        
		MFMailComposeViewController *composeViewController = [[MFMailComposeViewController alloc] init];
		
        NSString *subject = [aboutManager getEmailSubject];;
        
		NSString *emailAddress = [aboutManager getEmail];
        
		NSString *messageBody = [NSString stringWithFormat:@"Hi,\n I have feedback for the developers of %@ %@. My device runs iOS %@.", [aboutManager getApplcationName], [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"], [[UIDevice currentDevice] systemVersion]];
		
		[composeViewController setToRecipients:[NSArray arrayWithObject:emailAddress]];
        
		[composeViewController setSubject:subject];
        
		[composeViewController setMessageBody:messageBody isHTML:NO];
        
		composeViewController.mailComposeDelegate = self;
        
		[self presentModalViewController:composeViewController animated:YES];
        
		[composeViewController release];
        
	} else {
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Your device is unable to send email" message:@"Please check your Mail configuration" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[alert show];
		[alert release];
	}
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
	self.iTunesURL = nil;
	[loadingAlert dismissWithClickedButtonIndex:[loadingAlert cancelButtonIndex] animated:NO];
	[loadingAlert release], loadingAlert = nil;
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Unable to Reach App Store" message:@"Please try again later." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
	[alert show];
	[alert release];
}

// Save the most recent URL in case multiple redirects occur
// "iTunesURL" is an NSURL property in your class declaration
- (NSURLRequest *)connection:(NSURLConnection *)connection willSendRequest:(NSURLRequest *)request redirectResponse:(NSURLResponse *)response {
	DLog(@"self.iTunesURL = %@", self.iTunesURL);
	if (self.iTunesURL) {
		self.iTunesURL = [response URL];
		if (!self.iTunesURL) {
			self.iTunesURL = [request URL];
		}
		DLog(@"self.iTunesURL = %@", self.iTunesURL);
		return request;
	}
	return nil;
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
	DLog(@"self.iTunesURL = %@", self.iTunesURL);
	if (self.iTunesURL)
		[[UIApplication sharedApplication] openURL:self.iTunesURL];
	[loadingAlert dismissWithClickedButtonIndex:[loadingAlert cancelButtonIndex] animated:NO];
	[loadingAlert release], loadingAlert = nil;
}

- (void)mailComposeController:(MFMailComposeViewController*)_controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
	[self dismissModalViewControllerAnimated:YES];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
	if ([alertView.title isEqualToString:kLoadingTitle]) {
		if (buttonIndex == alertView.firstOtherButtonIndex) {
			self.iTunesURL = nil;
			[loadingAlert release], loadingAlert = nil;
		}
	}
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    AboutManager *aboutManager = [AboutManager sharedAboutManager];
    
    [self setBackgroundImage:[aboutManager getApplicationBackgroundName]];
    
    [self setDescriptions:[aboutManager getApplicationDescriptions] withFont:[aboutManager getApplicationDescriptionsFont] withSize:[aboutManager getApplicationDescriptionsSize] withColor:[aboutManager getApplicationDescriptionsColor]];
    
    [self setVersion:[aboutManager getApplicationVersion] withFont:[aboutManager getApplicationVersionFont] withSize:[aboutManager getApplicationVersionSize] withColor:[aboutManager getApplicationVersionColor]];

    [self setEmailButton:[aboutManager getEmailFont] withSize:[aboutManager getEmailSize]];
    
    [self setMoreAppsButton:[aboutManager getMoreAppsFont] withSize:[aboutManager getMoreAppsSize]];
    
}

- (void) setBackgroundImage:(NSString *)backgroundImage {
    
    if (backgroundImage == nil || [backgroundImage isEqualToString:@""]) {
        background.image = [UIImage imageNamed:@"AboutBackground.png"];
    }
    else {
        background.image = [UIImage imageNamed:backgroundImage]; 
    }
    
}

- (void) setDescriptions:(NSString *)descriptions withFont:(NSString *)font withSize:(int)size withColor:(UIColor *)color {
    
    if (descriptions == nil) {
        [applicationVersion setText:@""];
    }
    else {
        [applicationDescriptions setText:descriptions];
    }
    
    if (font == nil || [font isEqualToString:@""]) {
        if (size <= 0) {
            [applicationDescriptions setFont:[UIFont fontWithName:@"Helvetica-Bold" size:17]]; 
        }
        else {
            [applicationDescriptions setFont:[UIFont fontWithName:@"Helvetica-Bold" size:size]]; 
        }
        
    }
    else  {
        if (size <= 0) {
            [applicationDescriptions setFont:[UIFont fontWithName:font size:17]]; 
        }
        else {
            [applicationDescriptions setFont:[UIFont fontWithName:font size:size]]; 
        }
        
    }
    
    if (color != nil) {
        applicationDescriptions.textColor = color;
    }
    else {
        UIColor *color = [UIColor colorWithRed:0.12f green:0.51f blue:0.16f alpha:1.0f];
        applicationDescriptions.textColor = color;
    }
}

- (void) setVersion:(NSString *)version withFont:(NSString *)font withSize:(int)size withColor:(UIColor *)color {
    
    if (applicationVersion == nil) {
        [applicationVersion setText:@""];
    }
    else {
        [applicationVersion setText:version];
    }
    
    if (font == nil || [font isEqualToString:@""]) {
        if (size <= 0) {
            [applicationVersion setFont:[UIFont fontWithName:@"Helvetica-Bold" size:14]]; 
        }
        else {
            [applicationVersion setFont:[UIFont fontWithName:@"Helvetica-Bold" size:size]]; 
        }
        
    }
    else  {
        if (size <= 0) {
            [applicationVersion setFont:[UIFont fontWithName:font size:14]]; 
        }
        else {
            [applicationVersion setFont:[UIFont fontWithName:font size:size]]; 
        }
        
    }
    
    if (color != nil) {
        applicationVersion.textColor = color;
    }
    else {
        applicationVersion.textColor = [UIColor whiteColor];
    }
    
}

- (void) setEmailButton:(NSString *)font withSize:(int)size {
    
    if (font == nil || [font isEqualToString:@""]) {
        if (size <= 0) {
            emailDeveloperButton.titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:15];
        }
        else {
            emailDeveloperButton.titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:size];
        }
    }
    else {
        if (size <= 0) {
            emailDeveloperButton.titleLabel.font = [UIFont fontWithName:font size:15];
        }
        else {
            emailDeveloperButton.titleLabel.font = [UIFont fontWithName:font size:size];
        }
    }
    
}

- (void) setMoreAppsButton:(NSString *)font withSize:(int)size {
    
    if (font == nil || [font isEqualToString:@""]) {
        if (size <= 0) {
            moreApplicationsButton.titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:15];
        }
        else {
            moreApplicationsButton.titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:size];
        }
    }
    else {
        if (size <= 0) {
            moreApplicationsButton.titleLabel.font = [UIFont fontWithName:font size:15];
        }
        else {
            moreApplicationsButton.titleLabel.font = [UIFont fontWithName:font size:size];
        }
    }
    
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
