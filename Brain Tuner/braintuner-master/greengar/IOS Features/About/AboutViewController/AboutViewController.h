//
//  AboutViewController.h
//  Greengar
//
//  Created by Hector Zhao on 3/21/11.
//  Copyright 2011 Greengar Studios. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import "AboutManager.h"
#import "Reachability.h"


@interface AboutViewController : UIViewController <MFMailComposeViewControllerDelegate>{
    IBOutlet UIImageView    *applicationIcon;
    IBOutlet UIImageView    *background;
    IBOutlet UILabel        *applicationVersion;
    IBOutlet UILabel        *applicationDescriptions;
    IBOutlet UIButton       *emailDeveloperButton;
    IBOutlet UIButton       *moreApplicationsButton;
    UIAlertView *loadingAlert;
	NSURL *iTunesURL;
}

-(IBAction) sendEmailToDeveloper;
-(IBAction) getMoreApplications;

-(void) setBackgroundImage:(NSString *)backgroundImage;
-(void) setDescriptions:(NSString *)descriptions withFont:(NSString *)font withSize:(int)size withColor:(UIColor *)color;
-(void) setVersion:(NSString *)version withFont:(NSString *)font withSize:(int)size withColor:(UIColor *)color;
-(void) setEmailButton:(NSString *)font withSize:(int)size;
-(void) setMoreAppsButton:(NSString *)font withSize:(int)size;

@property (nonatomic, retain) NSURL *iTunesURL;

@end
