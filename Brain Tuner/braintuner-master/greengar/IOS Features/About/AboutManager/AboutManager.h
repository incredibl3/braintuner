//
//  AboutManager.h
//  Greengar
//
//  Created by Hector Zhao on 3/21/11.
//  Copyright 2011 Greengar Studios. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface AboutManager : NSObject {
    
    // Name
    NSString            * applicationName;
    
    // Icon and Background
    NSString            * applicationIconImageName;
    NSString            * applicationBackgroundName;
    
    // Version
    NSString            * applicationVersion;
    UIColor             * applicationVersionColor;
    NSString            * applicationVersionFont;
    int                   applicationVersionSize;
    
    // Descriptions
    NSString            * applicationDescriptions;
    UIColor             * applicationDescriptionsColor;
    NSString            * applicationDescriptionsFont;
    int                   applicationDescriptionsSize;
    
    // Developer List
    NSMutableArray      * developerList;
    
    // Feedback email and subject
    NSString            * emailSubject;
    NSString            * email;
    
    // More applications by Greengar
    NSString            * greengarAppsLink;
    
    // Email button
    NSString            * emailFont;
    int                   emailSize;
    
    // More applications button
    NSString            * moreAppsFont;
    int                   moreAppsSize;
}

+ (AboutManager *)		sharedAboutManager;

- (id) init;

- (void) setApplicationName:(NSString *)name;

- (void) setApplicationIconImageName:(NSString *)imageName;
- (void) setApplicationBackgroundName:(NSString *)backgroundName;

- (void) setApplicationVersion:(NSString *)version;
- (void) setApplicationVersionColor:(UIColor *)versionColor;
- (void) setApplicationVersionFont:(NSString *)versionFont;
- (void) setApplicationVersionSize:(int)versionSize;

- (void) setApplicationDescriptions:(NSString *)descriptions;
- (void) setApplicationDescriptionsColor:(UIColor *)descriptionsColor;
- (void) setApplicationDescriptionsFont:(NSString *)descriptionsFont;
- (void) setApplicationDescriptionsSize:(int)descriptionsSize;

- (void) setDeveloperList:(NSMutableArray *)list;
- (void) addDeveloper:(NSString *)devName;

- (void) setEmailSubject:(NSString *)subject;
- (void) setEmail:(NSString *)mail;

- (void) setGreengarAppsLink:(NSString *)link;

- (void) setEmailFont:(NSString *)font;
- (void) setEmailSize:(int)size;

- (void) setMoreAppsFont:(NSString *)font;
- (void) setMoreAppsSize:(int)size;

- (NSString *)          getApplcationName;

- (NSString *)          getApplicationIconImageName;
- (NSString *)          getApplicationBackgroundName;

- (NSString *)          getApplicationVersion;
- (UIColor *)           getApplicationVersionColor;
- (NSString *)          getApplicationVersionFont;
- (int)                 getApplicationVersionSize;

- (NSString *)          getApplicationDescriptions;
- (UIColor *)           getApplicationDescriptionsColor;
- (NSString *)          getApplicationDescriptionsFont;
- (int)                 getApplicationDescriptionsSize;

- (NSMutableArray *)    getDeveloperList;
- (NSString *)          getDeveloperLeftList;
- (NSString *)          getDeveloperRightList;

- (NSString *)          getEmailSubject;
- (NSString *)          getEmail;

- (NSString *)          getGreengarAppsLink;

- (NSString *)          getEmailFont;
- (int)                 getEmailSize;

- (NSString *)          getMoreAppsFont;
- (int)                 getMoreAppsSize;

@property (nonatomic, retain)   NSString            *applicationName;
@property (nonatomic, retain)   NSString            *applicationIconImageName;
@property (nonatomic, retain)   NSString            *applicationBackgroundName;
@property (nonatomic, retain)   NSString            *applicationVersion;
@property (nonatomic, retain)   UIColor             *applicationVersionColor;
@property (nonatomic, retain)   NSString            *applicationVersionFont;
@property (nonatomic)           int                 applicationVersionSize;
@property (nonatomic, retain)   NSString            *applicationDescriptions;
@property (nonatomic, retain)   UIColor             *applicationDescriptionsColor;
@property (nonatomic, retain)   NSString            *applicationDescriptionsFont;
@property (nonatomic)           int                 applicationDescriptionsSize;
@property (nonatomic, retain)   NSMutableArray      *developerList;
@property (nonatomic, retain)   NSString            *emailSubject;
@property (nonatomic, retain)   NSString            *email;
@property (nonatomic, retain)   NSString            *greengarAppsLink;
@property (nonatomic, retain)   NSString            *emailFont;
@property (nonatomic)           int                 emailSize;
@property (nonatomic, retain)   NSString            *moreAppsFont;
@property (nonatomic)           int                 moreAppsSize;

@end
