//
//  AboutManager.m
//  Greengar
//
//  Created by Hector Zhao on 3/21/11.
//  Copyright 2011 Greengar Studios. All rights reserved.
//

#import "AboutManager.h"

static AboutManager *sharedAboutManager = nil;

@implementation AboutManager

@synthesize applicationName, applicationIconImageName, applicationBackgroundName, applicationVersion, applicationVersionColor, applicationVersionFont, applicationVersionSize, applicationDescriptions, applicationDescriptionsColor, applicationDescriptionsFont, applicationDescriptionsSize, developerList, emailSubject, email, greengarAppsLink, emailFont, emailSize, moreAppsFont, moreAppsSize;

+ (AboutManager *)sharedAboutManager {
	
	if (sharedAboutManager == nil) {
		sharedAboutManager = [[super allocWithZone:NULL] init];
	}
	
	return sharedAboutManager;
}

- (id) init {
    if (developerList != nil) {
		[developerList removeAllObjects];
		[developerList release];
		developerList = nil;
	}
	
    applicationIconImageName = @"";
    applicationBackgroundName = @"";
    applicationVersion = @"";
    applicationVersionFont = @"";
    applicationVersionSize = 0;
    applicationVersionColor = nil;
    applicationDescriptions = @"";
    applicationDescriptionsFont = @"";
    applicationDescriptionsSize = 0;
    applicationDescriptionsColor = nil;
    emailSubject = @"";
    email = @"";
    greengarAppsLink = @"";
    emailFont = @"";
    emailSize = 0;
    moreAppsFont = @"";
    moreAppsSize = 0;
    
    // Dont need to show Developer list
	//developerList = [[NSMutableArray alloc] init];
	return self;
}

- (void) setApplicationName:(NSString *)name {
    applicationName = name;
}

- (void) setApplicationIconImageName:(NSString *)imageName {
    applicationIconImageName = imageName;
}

- (void) setApplicationBackgroundName:(NSString *)backgroundName {
    applicationBackgroundName = backgroundName;
}

- (void) setApplicationVersion:(NSString *)version {
    applicationVersion = version;
}

- (void) setApplicationVersionColor:(UIColor *)versionColor {
    applicationVersionColor = versionColor;
}

- (void) setApplicationVersionFont:(NSString *)versionFont {
    applicationVersionFont = versionFont;
}

- (void) setApplicationVersionSize:(int)versionSize {
    applicationVersionSize = versionSize;
}

- (void) setApplicationDescriptions:(NSString *)descriptions {
    applicationDescriptions = descriptions;
}

- (void) setApplicationDescriptionsColor:(UIColor *)descriptionsColor {
    applicationDescriptionsColor = descriptionsColor;
}

- (void) setApplicationDescriptionsFont:(NSString *)descriptionsFont {
    applicationDescriptionsFont = descriptionsFont;
}

- (void) setApplicationDescriptionsSize:(int)descriptionsSize {
    applicationDescriptionsSize = descriptionsSize;
}

- (void) setDeveloperList:(NSMutableArray *)list {
    developerList = list;
}

- (void) addDeveloper:(NSString *)devName {
    NSMutableString *dev = [NSMutableString string];
    [dev appendString:devName];
    [dev appendString:@"\n"];
    [developerList addObject:dev];
}

- (void) setEmailSubject:(NSString *)subject {
    emailSubject = subject;
}

- (void) setEmail:(NSString *)mail {
    email = mail;
}

- (void) setGreengarAppsLink:(NSString *)link {
    greengarAppsLink = link;
}

- (void) setEmailFont:(NSString *)font {
    emailFont = font;
}

- (void) setEmailSize:(int)size {
    emailSize = size;
}

- (void) setMoreAppsFont:(NSString *)font {
    moreAppsFont = font;
}

- (void) setMoreAppsSize:(int)size {
    moreAppsSize = size;
}

- (NSString *) getApplcationName {
    return applicationName;
}

- (NSString *) getApplicationIconImageName {
    return applicationIconImageName;
}

- (NSString *) getApplicationBackgroundName {
    return applicationBackgroundName;
}

- (NSString *) getApplicationVersion {
    return  applicationVersion;
}

- (UIColor *) getApplicationVersionColor {
    return applicationVersionColor;
}

- (NSString *) getApplicationVersionFont {
    return applicationVersionFont;
}

- (int) getApplicationVersionSize {
    return applicationVersionSize;
}

- (NSString *) getApplicationDescriptions {
    return applicationDescriptions;
}

- (UIColor *) getApplicationDescriptionsColor {
    return applicationDescriptionsColor;
}

- (NSString *) getApplicationDescriptionsFont {
    return applicationDescriptionsFont;
}

- (int) getApplicationDescriptionsSize {
    return applicationDescriptionsSize;
}

- (NSMutableArray *) getDeveloperList {
    return developerList;
}

- (NSString *) getDeveloperLeftList {
    if (([developerList count] % 2) == 1) {
        [developerList addObject:@""];
    }
    NSMutableString *leftList = [NSMutableString string];
    for (int i = 0; i < [developerList count]; i = i + 2) {
        [leftList appendString:[developerList objectAtIndex:i]];
    }
    return leftList;
}

- (NSString *) getDeveloperRightList {
    if (([developerList count] % 2) == 1) {
        [developerList addObject:@""];
    }
    NSMutableString *rightList = [NSMutableString string];
    for (int i = 1; i < [developerList count]; i = i + 2) {
        [rightList appendString:[developerList objectAtIndex:i]];
    }
    return rightList;
}

- (NSString *) getEmailSubject {
    return emailSubject;
}

- (NSString *) getEmail {
    return email;
}

- (NSString *) getGreengarAppsLink {
    return greengarAppsLink;
}

- (NSString *) getEmailFont {
    return emailFont;
}

- (int) getEmailSize {
    return emailSize;
}

- (NSString *) getMoreAppsFont {
    return moreAppsFont;
}

- (int) getMoreAppsSize {
    return moreAppsSize;
}

@end
