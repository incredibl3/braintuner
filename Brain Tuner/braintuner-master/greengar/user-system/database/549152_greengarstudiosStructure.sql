-- phpMyAdmin SQL Dump
-- version 2.11.11.1
-- http://www.phpmyadmin.net
--
-- Host: mysql50-74.wc2:3306
-- Generation Time: Jan 24, 2011 at 05:48 AM
-- Server version: 5.0.77
-- PHP Version: 5.2.13

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `549152_greengarstudios`
--

--
-- Table structure for table `ggs_bt_android_score`
--
-- Creation: Dec 29, 2010 at 02:28 AM
--

DROP TABLE IF EXISTS `ggs_bt_android_score`;
CREATE TABLE IF NOT EXISTS `ggs_bt_android_score` (
  `score_id` bigint(20) unsigned NOT NULL auto_increment,
  `user_id` bigint(20) unsigned NOT NULL,
  `user_login` varchar(60) character set utf8 NOT NULL,
  `display_name` varchar(250) character set utf8 default NULL,
  `mode` varchar(20) character set utf8 NOT NULL,
  `problems` tinyint(3) unsigned NOT NULL,
  `level` tinyint(3) unsigned NOT NULL,
  `score` float unsigned NOT NULL,
  `date` timestamp NULL default CURRENT_TIMESTAMP,
  `email` varchar(250) default NULL,
  PRIMARY KEY  (`score_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Table structure for table `ggs_wb_friend`
--
-- Creation: Dec 28, 2010 at 01:50 AM
--

DROP TABLE IF EXISTS `ggs_wb_friend`;
CREATE TABLE IF NOT EXISTS `ggs_wb_friend` (
  `user_id` bigint(20) unsigned NOT NULL,
  `friend_id` bigint(20) unsigned NOT NULL,
  `user_login` varchar(60) character set utf8 NOT NULL,
  `friend_login` varchar(60) character set utf8 NOT NULL,
  `user_fb_id` longtext character set utf8 NOT NULL,
  `friend_fb_id` longtext character set utf8 NOT NULL,
  PRIMARY KEY  (`user_id`,`friend_id`),
  KEY `friend_id` (`friend_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ggs_wb_xmpp`
--
-- Creation: Jan 20, 2011 at 09:16 PM
--

DROP TABLE IF EXISTS `ggs_wb_xmpp`;
CREATE TABLE IF NOT EXISTS `ggs_wb_xmpp` (
  `user_id` bigint(20) unsigned NOT NULL,
  `user_login` varchar(60) character set utf8 NOT NULL,
  `xmpp_account` varchar(255) character set utf8 NOT NULL,
  `xmpp_domain` varchar(255) character set utf8 NOT NULL,
  PRIMARY KEY  (`user_id`,`xmpp_account`,`xmpp_domain`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
