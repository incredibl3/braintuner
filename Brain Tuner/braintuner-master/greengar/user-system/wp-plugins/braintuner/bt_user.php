<?php
/**
 * @package braintuner
 * @version 1.0
 */
/*
 Plugin Name: Brain Tuner User
 Plugin URI: http://greengarstudio.com
 Description: Manage Brain Tuner User
 Author: _Nam
 Version: 1.0
 Author URI: mailto:nam@greengar.com
 */
include_once(ABSPATH . 'wp-content/plugins/greengar/user.php');

add_filter( 'xmlrpc_methods', 'add_bt_user_xmlrpc_methods' );
function add_bt_user_xmlrpc_methods( $methods ) {
	$methods['ggs.bt.getUserInfo'] = 'ggs_bt_get_user_info';
	return $methods;
}

/**
 * Log in and get user info
 *
 * @since 1.0
 *
 * @param array $args (username, password)
 * @return nicename, display_name, email, facebook_id
 */
function ggs_bt_get_user_info($args) {
	global $auth_error;

	escape($args);

	$user_name = $args[0];
	$user_password = $args[1];

	if ( !$user = login($user_name, $user_password) )
	return $auth_error;

	// Check the user class

	$facebook_id = get_user_meta($user->ID, 'facebook_id', true);

	return array(
		  	'nicename' => $user->user_nicename,
			'display_name' => $user->display_name,
			'email' => $user->user_email,
			'facebook_id' => $facebook_id
	);
}
?>