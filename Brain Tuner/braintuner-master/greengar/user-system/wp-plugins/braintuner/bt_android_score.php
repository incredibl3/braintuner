<?php

/**
 * @package braintuner
 * @version 1.0
 */
/*
 Plugin Name: Brain Tuner Android Score
 Plugin URI: http://greengarstudio.com
 Description: Manage Brain Tuner Android Online Score
 Author: _Nam
 Version: 1.0
 Author URI: mailto:nam@greengar.com
 */
include_once(ABSPATH . 'wp-content/plugins/greengar/user.php');

add_filter( 'xmlrpc_methods', 'add_bt_score_xmlrpc_methods' );
function add_bt_score_xmlrpc_methods( $methods ) {
	$methods['ggs.bt.postScore'] = 'ggs_bt_post_score';
	$methods['ggs.bt.getScore'] = 'ggs_bt_get_score';
	$methods['ggs.bt.rateScore'] = 'ggs_bt_rate_score';
	return $methods;
}

/**
 * Submit score
 *
 * @since 1.0
 *
 * @param array $args (username, password, mode, problems, level, score)
 * @return array
 */

function ggs_bt_post_score($args) {
	
	global $wpdb;
	global $auth_error;

	escape($args);

	$user_name = $args[0];
	$user_password = $args[1];

	if ( !$user = login($user_name, $user_password) )
	return $auth_error;

	$facebook_id = get_user_meta($user->ID, 'facebook_id', true);
	$nicename = $user->user_nicename;
	$display_name = $user->display_name;
	$email = $user->user_email;
	$mode = $args[2];
	$problems = $args[3];
	$level = $args[4];
	$score = $args[5];
	$hash = $args[6];

	$seed = $display_name . $mode . $problems . $level . '6uphEGev' . $score . '.0' . $display_name . $mode . $problems . $level . '6uphEGev' . $score . '.0';
	$myhash = md5($seed);

	if($hash != $myhash)
		return new IXR_Error(1000, __($seed . "--------" .$myhash));

	$query_columns = " (user_id, user_login, display_name, email, mode, problems, level, score) ";
	$query_values = "(" . $user->ID . ", '" . $user_name . "', '" . $display_name . "', '" . $email . "', '" . $mode . "', " . $problems . ", " . $level . ", " . $score . ")";
	$query = "INSERT INTO ggs_bt_android_score" . $query_columns . " VALUES " . $query_values;
	$add = $wpdb->query($query);
	
	$query = "SELECT * FROM ggs_bt_android_score WHERE mode = '" . $mode . "' AND problems = " . $problems . " AND level = " . $level . " ORDER BY score DESC LIMIT 0, 5";
	$results = $wpdb->get_results($query);
	
	$scores = array();
	foreach($results as $result)
		array_push($scores, $result);

	return $scores;
	
}


/**
 * Get score
 *
 * @since 1.0
 *
 * @param array $args (username, password, mode, problems, level)
 * @return array
 */
function ggs_bt_get_score($args) {
	
	global $wpdb;
	
	escape($args);

	$mode = $args[2];
	$problems = $args[3];
	$level = $args[4];
	
	if($mode == 'speed') {
		$query = "SELECT display_name, score FROM ggs_bt_android_score WHERE mode = '" . $mode . "' AND problems = " . $problems . " AND level = " . $level . " ORDER BY score ASC LIMIT 0, 5";
	} else {
		$query = "SELECT display_name, score FROM ggs_bt_android_score WHERE mode = '" . $mode . "' AND problems = " . $problems . " AND level = " . $level . " ORDER BY score DESC LIMIT 0, 5";
	}
	$results = $wpdb->get_results($query);
	
	$scores = array();
	foreach($results as $result)
		array_push($scores, $result);

	return $scores;
	
}

/**
 * Rate score
 *
 * @since 1.0
 *
 * @param array $args ()
 * @return array
 */
function ggs_bt_rate_score($args) {
	global $wpdb;

	escape($args);

	$mode_speed = 'speed';
	$problems_speed = array(20, 60, 100);
	$mode_time = 'speed';
	$problems_time = array(150, 200, 300);
	$levels = array(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11);
	$standards = array();

	foreach($problems_speed as $problems) {
		foreach($levels as $level) {
			$max_rank_query = "select max(rank) from (select @rownum:=@rownum+1 rank, gbass.score from (SELECT @rownum:=0) r, (select distinct score from ggs_bt_android_score where mode = 'speed' and problems = " . $problems . " and level = " . $level . " order by score desc) gbass) rank_table";

			$max_rank = $wpdb->get_var($max_rank_query);
			//return $max_rank;

			if(!$max_rank)
			continue;

			$rank_20 = (int) ($max_rank * 0.2);
			if($rank_20 <= 0) {
				$rank_20 = 1;
			}
			$rank_40 = (int) ($max_rank * 0.4);
			if($rank_40 <= 1) {
				$rank_40 = 2;
			}
			$rank_60 = (int) ($max_rank * 0.6);
			if($rank_60 <= 2) {
				$rank_60 = 3;
			}

			$score_rank_query = "select score from (select @rownum:=@rownum+1 rank, gbass.score from (SELECT @rownum:=0) r, (select distinct score from ggs_bt_android_score where mode = 'speed' and problems = " . $problems . " and level = " . $level . " order by score asc) gbass) rank_table where rank = " . $rank_20 . " or rank = " . $rank_40 . " or rank = " . $rank_60;

			$results = $wpdb->get_results($score_rank_query);

			if(!$results)
			continue;

			$standard = array();
			$counter = 20;
			foreach($results as $result) {
				$standard['' . $counter] = $result;
				$counter += 20;
			}

			$tag = 'speed_' . $problems . "_" . $level;
			$standards[$tag] = $standard;
		}
	}


	foreach($problems_time as $problems) {
		foreach($levels as $level) {
			$max_rank_query = "select max(rank) from (select @rownum:=@rownum+1 rank, gbass.score from (SELECT @rownum:=0) r, (select distinct score from ggs_bt_android_score where mode = 'time' and problems = " . $problems . " and level = " . $level . " order by score desc) gbass) rank_table";
			$max_rank = $wpdb->get_var($max_rank_query);

			if(!$max_rank)
			continue;

			$rank_20 = (int) ($max_rank * 0.2);
			if($rank_20 <= 0) {
				$rank_20 = 1;
			}
			$rank_40 = (int) ($max_rank * 0.4);
			if($rank_40 <= 1) {
				$rank_40 = 2;
			}
			$rank_60 = (int) ($max_rank * 0.6);
			if($rank_60 <= 2) {
				$rank_60 = 3;
			}

			$score_rank_query = "select score from (select @rownum:=@rownum+1 rank, gbass.score from (SELECT @rownum:=0) r, (select distinct score from ggs_bt_android_score where mode = 'time' and problems = " . $problems . " and level = " . $level . " order by score desc) gbass) rank_table where rank = " . $rank_20 . " or rank = " . $rank_40 . " or rank = " . $rank_60;

			$results = $wpdb->get_results($score_rank_query);

			if(!$results)
			continue;

			$standard = array();
			$counter = 20;
			foreach($results as $result) {
				$standard['' . $counter] = $result;
				$counter += 20;
			}
			$tag = 'time_' . $problems  . "_" . $level;
			$standards[$tag] = $standard;
		}
	}

	return $standards;
}
?>