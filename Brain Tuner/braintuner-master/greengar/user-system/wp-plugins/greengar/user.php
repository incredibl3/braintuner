<?php
/**
 * @package greengar
 * @version 1.0
 */
/*
 Plugin Name: Greengar All
 Plugin URI: http://greengarstudio.com
 Description: Manage Greengar User
 Author: _Nam
 Version: 1.0
 Author URI: mailto:nam@greengar.com
 */

include_once(ABSPATH . 'wp-admin/includes/admin.php');
include_once(ABSPATH . 'wp-includes/user.php');
include_once(ABSPATH . WPINC . '/class-IXR.php');

$auth_error = new IXR_Error(0, 'NULL');

add_filter( 'xmlrpc_methods', 'add_user_xmlrpc_methods' );
function add_user_xmlrpc_methods( $methods ) {
	$methods['ggs.register'] = 'ggs_register';
	$methods['ggs.assosiate'] = 'ggs_assosiate';
	$methods['ggs.getUserInfo'] = 'ggs_get_user_info';
	$methods['ggs.findFriends'] = 'ggs_find_friends';
	return $methods;
}

function login($username, $password) {
	global $auth_error;

	$user = wp_authenticate($username, $password);

	if (is_wp_error($user)) {
		$auth_error = new IXR_Error(403, __('Bad login/pass combination.'));
		return false;
	}

	return $user;
}

function escape(&$array) {
	global $wpdb;

	if (!is_array($array)) {
		return($wpdb->escape($array));
	} else {
		foreach ( (array) $array as $k => $v ) {
			if ( is_array($v) ) {
				escape($array[$k]);
			} else if ( is_object($v) ) {
				//skip
			} else {
				$array[$k] = $wpdb->escape($v);
			}
		}
	}
}

/**
 * Register a greengar user
 *
 * @since 1.0
 *
 * @param array $args (username, password, email)
 * @return userid
 */
function ggs_register($args) {
	global $auth_error;
	escape($args);

	$user_name = $args[0];
	$user_password = $args[1];
	$user_email = $args[2];

	$user_id = wp_create_user($user_name, $user_password, $user_email);
	
	if ( is_wp_error( $user_id ) ) {
		return (new IXR_Error(500, __($user_id->get_error_message())));
	}

	$i = 3;
	for(; $i < count($args); $i += 2) {
		$key = $args[$i];
		$value = $args[$i + 1];
		$add = add_user_meta($user_id, $key, $value, true);
	}

	return array(
		   'user_login' => $user_name, 
		   'user_id' => $user_id, 
		   'email' => $user_email
	);
}

/**
 * Add or update user meta
 *
 * @since 1.0
 *
 * @param array $args (username, password, key, value)
 * @return string
 */
function ggs_assosiate($args) {
	global $auth_error;
	escape($args);

	$user_name = $args[0];
	$user_password = $args[1];
	$key = $args[2];
	$value   = $args[3];

	if ( !$user = login($user_name, $user_password) )
		return $auth_error;

	// We also need to check the key if it is valid, we dont want garbage meta in our db
	$old = get_user_meta($user->ID, $key, true);
	$add = update_user_meta($user->ID, $key, $value, $old);

	if (!$add) {
		// What should we do ???
		return (new IXR_Error(1000, 'Cannot add data ???.')); 
	}

	return array(
		   	'username' => $user_name, 
		  	'userid' => $user->ID, 
		    'key' => $key,
		 	'value' => $value
	);
}

/**
 * Get User ID
 *
 * @since 3.0.3
 *
 * @param array $args (username, password)
 * @return user_id
 */
function ggs_get_user_info($args) {
	global $auth_error;
	escape($args);

	$user_name = $args[0];
	$user_password = $args[1];

	if ( !$user = login($user_name, $user_password) )
		return $auth_error;

	return array(
		  	'user_id' => $user->ID
	);
}

/**
 * Suggest Friend
 *
 * @since 1.0
 *
 * @param array $args (username, password, name)
 * @return string
 */
function ggs_find_friends($args) {
	global $wpdb;
	global $auth_error;

	escape($args);

	$user_name =  $args[0];
	$user_password    = $args[1];
	$display_name =  $args[2];
	if ( !$user = login($user_name, $user_password) )
		return $auth_error;

	$names = array();

	$query = "SELECT user_login, display_name FROM " . $wpdb->prefix . "users WHERE display_name LIKE '%" . $display_name . "%' OR user_login LIKE '%" . $display_name . "%'";
	$results = $wpdb->get_results($query);

	foreach($results as $result)
		array_push($names, $result);

	return $names;
}

?>