<?php
/**
 * @package whiteboard
 * @version 1.0
 */
/*
 Plugin Name: Whiteboard User
 Plugin URI: http://greengarstudio.com
 Description: Manage Whiteboard User
 Author: _Nam
 Version: 1.0
 Author URI: mailto:nam@greengar.com
 */
include_once(ABSPATH . 'wp-content/plugins/greengar/user.php');

add_filter( 'xmlrpc_methods', 'add_wb_user_xmlrpc_methods' );
function add_wb_user_xmlrpc_methods( $methods ) {
	$methods['ggs.wb.getUserInfo'] = 'ggs_wb_get_user_info';
	$methods['ggs.wb.addXMPP'] = 'ggs_wb_addXMPP';
	$methods['ggs.wb.getXMPPList'] = 'ggs_wb_getXMPPList';
	$methods['ggs.wb.submitXMPPError'] = 'ggs_wb_submit_XMPP_Error';
	$methods['ggs.wb.addFriend'] = 'ggs_wb_addFriend';
	$methods['ggs.wb.getFriends'] = 'ggs_wb_getFriends';
	$methods['ggs.wb.findFriends'] = 'ggs_wb_find_friends';
	return $methods;
}

/**
 * Get all WB User Info
 *
 * @since 1.0
 *
 * @param array $args (username, password)
 * @return userid, nicename, display_name, email, facebook_id, xmpp_user, xmpp_domain
 */
function ggs_wb_get_user_info($args) {
	
	//$tStart = time();
	
	global $auth_error;

	escape($args);

	$user_name = $args[0];
	$user_password = $args[1];

	if ( !$user = login($user_name, $user_password) )
		return $auth_error;

	$user_id = $user->ID;
	$displayname = $user->display_name;
	$facebook_id = get_user_meta($user_id, 'facebook_id', true);
	$xmpp_user = get_user_meta($user_id, 'wb_xmpp_user', true);
	$xmpp_domain = get_user_meta($user_id, 'wb_xmpp_domain', true);
	
	//$tEnd = time();
	
	//$timeResponse = $tEnd - $tStart;
	
	//return $timeResponse;

	return array(
			
			'user_id' => (string)$user->ID,
		   	'nicename' => (string)$user->user_nicename,
			'display_name' => (string)$user->display_name,
			'email' => (string)$user->user_email,
			'facebook_id' => (string)$facebook_id,
			'xmpp_user' => (string)$xmpp_user,
			'xmpp_domain' => (string)$xmpp_domain
	);
}

/**
 * Assosiate an XMPP Account for WB
 *
 * @since 1.0
 *
 * @param array $args (username, password, XMPP_acc, XMPP_domain)
 * @return userid, xmpp_user@xmpp_domain
 */
function ggs_wb_addXMPP($args) {
	
	global $wpdb;
	global $auth_error;
	escape($args);

	$user_name =  $args[0];
	$user_password    = $args[1];
	$XMPP_acc   = $args[2];
	$XMPP_domain = $args[3];
	
	
	if(!$XMPP_acc || !$XMPP_domain)
		return new IXR_Error(999, __('Kong should not submit null'));
		
	if ( !$user = login($user_name, $user_password) )
		return $auth_error;
		
		
	// Check the user class
	// return "INSERT INTO ggs_wb_xmpp VALUES(" . $user->ID . ", " . $user_name . ", '" . $XMPP_acc . "', '" . $XMPP_domain . "')";
	$wpdb->query("INSERT INTO ggs_wb_xmpp VALUES(" . $user->ID . ", '" . $user->user_login . "', '" . $XMPP_acc . "', '" . $XMPP_domain . "')");
	
	$old = get_user_meta($user->ID, 'wb_xmpp_user', true);
	$add = update_user_meta($user->ID, 'wb_xmpp_user', $XMPP_acc, $old);
	$old = get_user_meta($user->ID, 'wb_xmpp_domain', true);
	$add = update_user_meta($user->ID, 'wb_xmpp_domain', $XMPP_domain, $old);

	return array(
		   	'user_id' => (string)$user->ID,
			'XMPP_acc' => (string)($XMPP_acc . "@" . $XMPP_domain)
	);
}

/**
 * Get current/ latest XMPP accounts
 *
 * @since 1.0
 *
 * @param array $args (username, password)
 * @return string
 */
function ggs_wb_getXMPPList($args) {
	global $wpdb;
	global $auth_error;
	escape($args);

	$user_name = $args[0];
	$user_password = $args[1];

	if ( !$user = login($user_name, $user_password) )
		return $auth_error;

	$XMPP_accs = array();

	$results = $wpdb->get_results("SELECT xmpp_account, xmpp_domain FROM ggs_wb_xmpp WHERE user_login = '" . $user->user_login . "'");

	// Check the user class

	foreach($results as $result)
		array_push($XMPP_accs, $result);

	return $XMPP_accs;
}

/**
 * Report XMPP server error
 *
 * @since 1.0
 *
 * @param array $args (username, password)
 * @return string
 */
function ggs_wb_submit_XMPP_Error($args) {
	global $wpdb;
	global $auth_error;
	escape($args);

	$user_name = $args[0];
	$user_password = $args[1];
	$emai_body = $args[2];

	if ( !$user = login($user_name, $user_password) )
		return $auth_error;

	$XMPP_user = get_user_meta($user->ID, 'wb_xmpp_user', true);
	$XMPP_domain = get_user_meta($user->ID, 'wb_xmpp_domain', true);

	return array (
			'XMPP_user' => (string)$XMPP_user,
			'XMPP_domain' => (string)$XMPP_domain
	);
}

/**
 * Add a Whiteboard friend
 *
 * @since 1.0
 *
 * @param array $args (username, password, friendname)
 * @return string
 */
function ggs_wb_addFriend($args) {
	
	
	global $wpdb;
	global $auth_error;
	escape($args);

	$user_name =  $args[0];
	$user_password    = $args[1];
	$friend_name = $args[2];

	if ( !$user = login($user_name, $user_password) )
		return $auth_error;
	
	$user_id = $user->ID;
	$frienddata = get_user_by('login', $friend_name);
	$friend_id = $frienddata->ID;
	$user_fb_id = get_user_meta($user_id, 'facebook_id', true);
	$friend_fb_id = get_user_meta($friend_id, 'facebook_id', true);
	$query = "INSERT INTO ggs_wb_friend VALUES(" . $user_id . ", " . $friend_id . ", '" . $user_name . "', '" . $friend_name . "', '" . $user_fb_id . "', '" . $friend_fb_id . "')";
//	return $query;
	$wpdb->query($query);
//	return 123;
	return array(
		  	'user_id' => (string)$user_id, 
			'friend_id' => (string)$friend_id
	);
}

/**
 * Get a Whiteboard friendlist
 *
 * @since 3.0.3
 *
 * @param array $args (username, password)
 * @return string
 */
function ggs_wb_getFriends($args) {
	global $wpdb;
	global $auth_error;
	escape($args);

	$user_name = $args[0];
	$user_password = $args[1];

	if ( !$user = login($user_name, $user_password) )
		return $error;

	$user_id = $user->ID;
	$friends = array();
	$query = "select ID as user_id, user_login, display_name, wb_xmpp_user, wb_xmpp_domain from " . $wpdb->prefix . "users, (select user.user_id, wb_xmpp_user, wb_xmpp_domain, ".$wpdb->prefix."capabilities from (select user_id, meta_value as wb_xmpp_user from  " . $wpdb->prefix ."usermeta Where meta_key = 'wb_xmpp_user') as user, (select user_id, meta_value as wb_xmpp_domain from  " . $wpdb->prefix ."usermeta where meta_key = 'wb_xmpp_domain') as dom, (select user_id, meta_value as ".$wpdb->prefix."capabilities from " . $wpdb->prefix ."usermeta where meta_key = '".$wpdb->prefix."capabilities') as cap where user.user_id = dom.user_id and user.user_id = cap.user_id and cap.user_id = dom.user_id and ".$wpdb->prefix."capabilities like '%subscriber%') as xmpp where ".$wpdb->prefix."users.ID = xmpp.user_id and xmpp.user_id in (SELECT friend_id FROM ggs_wb_friend ggs_f WHERE ggs_f.user_id = " . $user_id . ") ORDER BY user_login ASC";
	$results = $wpdb->get_results($query);

	foreach($results as $result)
		array_push($friends, $result);

	return $friends;
}

/**
 * Get a Whiteboard Friend name suggestion
 *
 * @since 3.0.3
 *
 * @param array $args (username, password, target)
 * @return string
 */
function ggs_wb_find_friends($args) {
	
//	return 'Kong';

	global $wpdb;
	global $auth_error;

	escape($args);

	$user_name =  $args[0];
	$user_password    = $args[1];
	$display_name =  $args[2];
	
	if ( !$user = login($user_name, $user_password) )
		return $auth_error;

	$names = array();
	$query = "select ID as user_id, user_login, display_name, wb_xmpp_user, wb_xmpp_domain from " . $wpdb->prefix . "users, (select user.user_id, wb_xmpp_user, wb_xmpp_domain, ".$wpdb->prefix."capabilities from (select user_id, meta_value as wb_xmpp_user from " . $wpdb->prefix ."usermeta Where meta_key = 'wb_xmpp_user') as user, (select user_id, meta_value as wb_xmpp_domain from " . $wpdb->prefix ."usermeta where meta_key = 'wb_xmpp_domain') as dom, (select user_id, meta_value as ".$wpdb->prefix."capabilities from " . $wpdb->prefix ."usermeta where meta_key = '".$wpdb->prefix."capabilities') as cap where user.user_id = dom.user_id and user.user_id = cap.user_id and cap.user_id = dom.user_id and ".$wpdb->prefix."capabilities like '%subscriber%') as xmpp where (display_name LIKE '%" . $display_name ."%' OR user_login LIKE '%" . $display_name ."%') and " . $wpdb->prefix . "users.ID = xmpp.user_id ORDER BY user_login ASC" ;
	//return $query;
	$results = $wpdb->get_results($query);

	foreach($results as $result)
		array_push($names, $result);

	return $names;

}
?>