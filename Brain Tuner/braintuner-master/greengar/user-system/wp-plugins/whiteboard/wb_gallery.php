<?php
/**
 * @package whiteboard
 * @version 1.0
 */
/*
Plugin Name: Whiteboad Gallery
Plugin URI: http://greengarstudio.com
Description: Manage Whiteboad Gallery
Author: _Nam
Version: 1.0
Author URI: mailto:nam@greengar.com
*/
include_once(ABSPATH . 'wp-content/plugins/greengar/user.php');

add_filter( 'xmlrpc_methods', 'add_wb_gallery_xmlrpc_methods' );
function add_wb_gallery_xmlrpc_methods( $methods ) {
	$methods['ggs.wbg.getDrawings'] = 'ggs_wbg_get_drawings';
//	$methods['ggs.wbg.searchDrawingsFuzzy'] = 'ggs_wbg_search_drawings_fuzzy';
//	$methods['ggs.wbg.searchDrawingsExact'] = 'ggs_wbg_search_drawings_exact';
//	$methods['ggs.wbg.submitDrawings'] = 'ggs_wbg_submit_drawings';
//	$methods['ggs.wbg.claimDrawings'] = 'ggs_wbg_claim_drawings';
//	$methods['ggs.wbg.commentDrawings'] = 'ggs_wbg_comment_drawings';
//	$methods['ggs.wbg.rateDrawings'] = 'ggs_wbg_rate_drawings';
//	$methods['ggs.wbg.deleteDrawings'] = 'ggs_wbg_delete_drawings';
	return $methods;
}

define('PATH', '');

/**
 * Get drawings
 *
 * @since 1.0
 *
 * @param array $args (orderby, order, keywords)
 * @return array(user_login, display_name, model, date, comment, ratingavg, md5)
 */
function ggs_wbg_get_drawings($args) {
	
	global $wpdb;
	global $auth_error;

	escape($args);

	$orderby = $args[0];
	$order = $args[1];
	
	$keyword = $args[2];
	
	// Temporarily search base on comment
	$WHERE = " WHERE comment LIKE '%" . $keyword . "%' ";
	
	$count = 3;
	for($count = 3; $count < count($args); $count++) {
		$keyword = $args[$count];
		$WHERE = $WHERE . " OR comment LIKE '%" . $keyword . "%' ";
	}
	
	$query = "SELECT id, user_login, display_name, model, date, comment, ratingavg, md5, tags FROM drawings " . $WHERE;
	
	$qRets = $wpdb->get_results($query);
	
	$result = array();
	
	foreach($qRets as $qRet) {
		array_push($result, $qRet);
	}
	
	return array(
		'path' => PATH,
		'result' => $result
	);
}


/**
 * Get drawings by user (fuzzy search by user_login)
 *
 * @since 1.0
 *
 * @param array $args (orderby, order, user)
 * @return array(user_login, display_name, model, date, comment, ratingavg, md5)
 */
function ggs_wbg_search_drawings_fuzzy($args) {
	
	global $wpdb;
	global $auth_error;

	escape($args);

	$orderby = $args[0];
	$order = $args[1];
	
	$user = $args[2];
	
	$WHERE = " WHERE user_login LIKE '%" . $user . "%' ";
	
	$query = "SELECT id, user_login, display_name, model, date, comment, ratingavg, md5, tags FROM drawings " . $WHERE;
	$qRets = $wpdb->get_results($query);
	
	$result = array();
	
	foreach($qRets as $qRet) {
		array_push($result, $qRet);
	}
	
	return array(
		'path' => PATH,
		'result' => $result
	);
}

/**
 * Get drawings by user (exact search by user_login)
 *
 * @since 1.0
 *
 * @param array $args (orderby, order, user)
 * @return array(user_login, display_name, model, date, comment, ratingavg, md5)
 */
function ggs_wbg_search_drawings_exact($args) {
	
	global $wpdb;
	global $auth_error;

	escape($args);

	$orderby = $args[0];
	$order = $args[1];
	
	$user = $args[2];
	
	$WHERE = " WHERE user_login = '" . $user . "' ";
	
	$query = "SELECT id, user_login, display_name, model, date, comment, ratingavg, md5, tags FROM drawings " . $WHERE;
	$qRets = $wpdb->get_results($query);
	
	$result = array();
	
	foreach($qRets as $qRet) {
		array_push($result, $qRet);
	}
	
	return array(
		'path' => PATH,
		'result' => $result
	);
}

/**
 * Claim drawings to user
 *
 * @since 1.0
 *
 * @param array $args (username, password, deiviceid, $orderby, $order);
 * @return array(user_login, display_name, model, date, comment, ratingavg, md5)
 */
function ggs_wbg_claim_drawings($args) {
	
	global $wpdb;
	global $auth_error;

	escape($args);

	
	$username = $args[0];
	$password = $args[1];
	$deviceid = $args[2];
	$orderby = $args[3];
	$order = $args[4];
	
	if ( !$user = $this->login($user_name, $user_password) )
		return $auth_error;
	
	$claim = "UPDATE drawings set user_id = " . $user->ID 
							 . ", user_login = '" . $user->user_login 
							. "', display_name = '" . $user->display_name
							 . "' WHERE user_id is null AND deviceid = " . $deviceid;
	
	$update = $wpdb->query($claim);
	
	$WHERE = " WHERE user_login = '" . $user->user_login . "' ";
	
	$query = "SELECT id, user_login, display_name, model, date, comment, ratingavg, md5, tags FROM drawings " . $WHERE;
	$qRets = $wpdb->get_results();
	
	$result = array();
	
	foreach($qRets as $qRet) {
		array_push($result, $qRet);
	}
	
	return array(
		'claim' => $update,
		'path' => PATH,
		'result' => $result
	);
}
 ?>