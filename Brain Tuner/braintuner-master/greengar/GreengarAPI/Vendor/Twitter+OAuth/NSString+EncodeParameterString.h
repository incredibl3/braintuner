//
//  NSString+EncodeParameterString.h
//  Whiteboard
//
//  Created by Hector Zhao on 10/17/11.
//  Copyright 2011 Greengar Studios. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (EncodeParameterString)

- (NSString *)encodedURLString;
- (NSString *)encodedURLParameterString;

@end
