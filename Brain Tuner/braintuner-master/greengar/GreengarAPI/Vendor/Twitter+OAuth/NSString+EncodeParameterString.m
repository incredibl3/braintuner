//
//  NSString+EncodeParameterString.m
//  Whiteboard
//
//  Created by Hector Zhao on 10/17/11.
//  Copyright 2011 Greengar Studios. All rights reserved.
//

#import "NSString+EncodeParameterString.h"

@implementation NSString (EncodeParameterString)

- (NSString *)encodedURLString {
    NSString *result = (NSString *)CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault,
                                                                           (CFStringRef)self,
                                                                           NULL,                   // characters to leave unescaped (NULL = all escaped sequences are replaced)
                                                                           CFSTR("?=&+"),          // legal URL characters to be escaped (NULL = all legal characters are replaced)
                                                                           kCFStringEncodingUTF8); // encoding
    [result autorelease];
    return result;
}

- (NSString *)encodedURLParameterString {
    NSString *result = (NSString *)CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault,
                                                                           (CFStringRef)self,
                                                                           NULL,
                                                                           CFSTR(":/=,!$&'()*+;[]@#?"),
                                                                           kCFStringEncodingUTF8);
    [result autorelease];
    return result;
}

@end
