//
//  GreengarAPISampleAppDelegate.h
//  GreengarAPISample
//
//  Created by Cong Vo on 4/12/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Greengar.h"
@interface GreengarAPISampleAppDelegate : NSObject <UIApplicationDelegate, GSAuthenticationDelegate> {
//    GSAuthentication *_authentication;
    Greengar *_greengar;
}

@property (nonatomic, retain) IBOutlet UIWindow *window;
@property (nonatomic, retain) Greengar *greengar;

@property (nonatomic, retain) IBOutlet UINavigationController *navigationController;

- (void)startTest;
- (void)authentication;
- (void)registration;
@end
