//
//  GreengarAPISampleAppDelegate.m
//  GreengarAPISample
//
//  Created by Cong Vo on 4/12/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "GreengarAPISampleAppDelegate.h"
#import "GSRequest.h"

@implementation GreengarAPISampleAppDelegate


@synthesize window=_window;

@synthesize navigationController=_navigationController;
@synthesize greengar = _greengar;

static BOOL usingUser1 = YES;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    
    //KONG: set appropriate Endpoint and application for your apps
    [GSRequest setApp:kAppWhiteboard];
//    [GSRequest setApp:kAppOpinions];    

    
    //KONG: API Test
    // Set methods that need to be called in startTest method
    // Set authentication with User1 or User2 by using variable `usingUser1`
    // TODO: add response-parser and more assertions
    
//    _authentication = [GSAuthentication authenticationWithDelegate:self];
    
    self.greengar = [[[Greengar alloc] init] autorelease];
    

    
    usingUser1 = 
//    YES; // koncer
    NO; //tester1
    
    [GSAuthentication logout];
//    if ([GSAuthentication isAuthenticated] == NO) {
//        
//    }

    [self authentication];
//    [self registration];
    
    
    
    
    
    // View controller stuff
    self.window.rootViewController = self.navigationController;
    [self.window makeKeyAndVisible];    
    return YES;
}

#pragma mark Authentication



- (void)authentication {
    NSString *email;
    NSString *password;


    if (usingUser1) {
        //KONG: user1        
        email = @"koncer3@g.co";
        password = @"thcongvnn";  
        //21812
    } else {
        //KONG: user2        
//        email = @"tester1@a.a";
//        password = @"tester1";
        email = @"koncerz@g.co";
        password = @"thcongvnn";
    }

//    @"kong@greengar.com"
//    @"athanhcong@gmail.com"    

//    @"thcongvnn"
//    @"yykpnfkl"
    
    [_greengar.authentication authenticateWithUsername:email
                                     password:password
                                     delegate:self];
}

- (void)authentication:(GSAuthentication *)auth succeeded:(NXOAuth2Client *)oauthClient {
    DLog();
    [self startTest];
}

- (void)authentication:(GSAuthentication *)auth failed:(NSError *)error {
    DLog(@"%@", error);
}

#pragma mark Registration


- (void)registration {
    [_greengar.authentication registerWithEmail:@"athanhcong@gmail.com"
                             firstName:@"Cong"
                              lastName:@"Vo"
                              delegate:self];
}

- (void)registration:(GSAuthentication *)auth successed:(NXOAuth2Client *)oauthClient {
    DLog();
    /*
    {
        "avatar_url" = "";
        email = "kong@greengar.com";
        "facebook_id" = "";
        "first_name" = Cong;
        "last_name" = Vo;
        signature = "BHxA7jG/M7hE2edcSy9zsFrLpcA=";
        timestamp = "256598.806509";
        username = "";
    }
    */
}

- (void)registration:(GSAuthentication *)auth failed:(NSError *)error {
    DLog(@"%@", error);
}

#pragma mark UserInfo

- (void)getAuthenticatedUserInfo {
	NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
							@"me", @"user_id",
							nil];
	
	[GSRequest requestWithEndPoint:kGreengarEndpointGetUserInfo
                                      params:params
                                    delegate:self
                                    callback:@selector(request:gotUserInfo:)];
}

- (void)getInfo:(NSString *)userID {
	NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
							userID, @"user_id",
							nil];
	
	[GSRequest requestWithEndPoint:kGreengarEndpointGetUserInfo
                                    params:params
                                  delegate:self
                                  callback:@selector(request:gotUserInfo:)];
}

- (void)request:(GSRequest *)request gotUserInfo:(id)result {
    DLog(@"%@", result);
    /*
    {
        email = "koncer3@g.co";
        guid = 6945;
        icon = "http://184.106.64.162/elgg_dev/mod/profile/graphics/defaultmedium.gif";
        name = koncer3;
        username = koncer3;
    }
     
     {
     email = "tester1@a.a";
     guid = 64;
     icon = "http://184.106.64.162/elgg_dev/mod/profile/graphics/defaultmedium.gif";
     name = tester1;
     username = tester1;
     }
     
    */
    NSAssert([(NSObject *)result isKindOfClass:[NSError class]] == NO , @"Should not have error");    
}

#pragma mark Association

- (void)associationXMPPAccount {
    /*
     user: koncer3.g.g.s - pass: d462106170 - domain xmpp.ws
     user: koncer2.g.g.s - pass: 78ef8e2041 - domain xmpp.ws
     user: koncer4.g.g.s - pass: 36b071a3f4 - domain xmpp.us
    */

    //KONG: user1: koncer3@g.co
//    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:@"koncer3.g.g.s", @"login",
//                            @"xmpp.ws" , @"domain", nil];
    //KONG: user2: tester1@a.a
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:@"koncer4.g.g.s", @"login",
                            @"xmpp.us" , @"domain", nil];
    

    [GSRequest requestWithEndPoint:kWhiteboardEndpointAssociateAccount
                              params:params
                            delegate:self 
                            callback:@selector(whiteboard:associatedAccount:)];
}

- (void)whiteboard:(GSRequest *)request associatedAccount:(id)result {
	DLog(@"response: %@", result);	
    NSAssert([(NSObject *)result isKindOfClass:[NSError class]] == NO , @"Should not have error");
}


- (void)getAllAssociatedXMPPAccounts {

    [GSRequest requestWithEndPoint:kWhiteboardEndpointGetAccount
                              params:nil
                            delegate:self
                          callback:@selector(whiteboard:gotUserUsedServices:)];
}

- (void)whiteboard:(GSRequest *)request gotUserUsedServices:(id)result {
    DLog(@"%@", result);
    NSAssert([(NSObject *)result isKindOfClass:[NSError class]] == NO, @"Should not have error");
}


#pragma mark findFriend

- (void)searchForFriends {
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            @"tester", @"keywords", 
//                                        @"[\"whiteboard\"]", @"apps",
                            nil];
    
    [GSRequest requestWithEndPoint:kGreengarEndpointFindFriends     
                                      params:params
                                    delegate:self
                                  callback:@selector(greengar:foundFriends:)];            
}

- (void)greengar:(GSRequest *)request foundFriends:(id)result {
    DLog(@"%@", result);
    NSAssert([(NSObject *)result isKindOfClass:[NSError class]] == NO, @"Should not have error");    
}

#pragma mark add/accept/remove friends


- (void)getFriends {
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            @"[\"both\",\"from\",\"to\"]", @"relationships", 
                            @"[\"whiteboard\"]", @"apps",
                            nil];
    
    
    [GSRequest requestWithEndPoint:kGreengarEndpointGetFriends
                                      params:params
                                    delegate:self
                                  callback:@selector(request:gotFriends:)];

}

- (void)request:(GSRequest *)request gotFriends:(id)result {
    DLog(@"%@", result);
    NSAssert([(NSObject *)result isKindOfClass:[NSError class]] == NO, @"Should not have error");    

}

/*
    email = "koncer3@g.co";
    guid = 6945;
    username = koncer3;

    email = "tester1@a.a";
    guid = 64;
    username = tester1;
*/

//KONG: logged into User1
- (void)addFriend:(NSString *)friendID accepted:(BOOL)isYES {
    NSString *response;
    if (isYES) {
        response = @"yes";
    } else {
        response = @"no";
    }
    
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            friendID, @"friend_id", 
                            response, @"response",
                            nil];
    
    
    [GSRequest requestWithEndPoint:kGreengarEndpointAddFriend
                                      params:params
                                    delegate:self
                                  callback:@selector(greengar:changedFriendship:)];      
}

- (void)greengar:(GSRequest *)request changedFriendship:(id)result {
    DLog(@"%@", result);
    NSAssert([(NSObject *)result isKindOfClass:[NSError class]] == NO, @"Should not have error");        
}




//KONG: test friend:
// Step1: using user1, authenticate, call addFriend
// Step2: using user2, authenticate, call getNotification, and respondFriend
// Step3: using user3, authenticate, call getNotification, and checkResponse

- (void)addFriend {
    //    usingUser1 = NO;    
//    [self getFriends];
//    [self addFriend:@"15872" accepted:YES];
    [self addFriend:@"15880" accepted:YES];
//    [self getFriends];    
}

- (void)respondFriend {
//    usingUser1 = NO;
    [self addFriend:@"6945" accepted:YES];
    [self getFriends];

}

- (void)checkResponse {
//    usingUser1 = YES;
    [self getFriends];    
}


//- (void)testAddFriend {
//    [self performSelector:@selector(addFriend) 
//               withObject:nil
//               afterDelay:2];
//    [self performSelector:@selector(respondFriend) 
//               withObject:nil
//               afterDelay:8];
//
//    [self performSelector:@selector(checkResponse) 
//               withObject:nil
//               afterDelay:14];
//
//    
//}


//KONG: logged into User1
- (void)removeFriend:(NSString *)friendID {

    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            friendID, @"friend_id", 
                            nil];
    
    
    [GSRequest requestWithEndPoint:kGreengarEndpointRemoveFriend
                                    params:params
                                  delegate:self
                                  callback:@selector(greengar:changedFriendship:)];      
}


#pragma mark Notification

- (void)getNotification {
    [GSRequest requestWithEndPoint:kGreengarEndpointGetNotifications
                                      params:nil  //params
                                    delegate:self
                                  callback:@selector(greengar:gotNotifications:)];
}

- (void)greengar:(GSRequest *)request gotNotifications:(id)result {
    DLog(@"%@", result);
    NSAssert([(NSObject *)result isKindOfClass:[NSError class]] == NO, @"Should not have error");            
}



#pragma opinions associate account

- (void)associateAccount {
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
							@"Gowalla", @"type",
							@"123231", @"id",  
							nil];
	
	[GSRequest requestWithEndPoint:kOpinionsEndpointAssociateAccount
                                      params:params
                                    delegate:self
                                  callback:@selector(opinions:associatedAccount:)];
}


- (void)opinions:(GSRequest *)request associatedAccount:(id)result {
    DLog(@"%@", result);
    NSAssert([(NSObject *)result isKindOfClass:[NSError class]] == NO, @"Should not have error");
}

#pragma mark Change password
- (void)changePassword {
//    change_password
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
							@"password", @"password",
							nil];
	
	[GSRequest requestWithEndPoint:kGreengarEndpointChangePassword
                            params:params
                          delegate:self
                          callback:@selector(greengar:changedPassword:)];    
    
}

- (void)greengar:(GSRequest *)request changedPassword:(id)result {
    DLog(@"%@", result);
    NSAssert([(NSObject *)result isKindOfClass:[NSError class]] == NO, @"Should not have error");
}

#pragma mark TestSuite

- (void)startTest {
    [self getAuthenticatedUserInfo];
    
    
//    [self associationXMPPAccount];
//    [self getAuthenticatedUserInfo];
    
//    [self getAllAssociatedXMPPAccounts];
//    [self searchForFriends];

    // log into user1
//    [self addFriend];
    
    // log into user2
//    [self respondFriend];
    
    // log into user1
//    [self checkResponse];
//    [self removeFriend:@"15880"];
//    [self getInfo:@"21812"];

    [self getFriends];
    
    [self changePassword];
//   [self getNotification];
//    [self associateAccount];
}


- (void)dealloc
{
    [_window release];
    [_navigationController release];
    [super dealloc];
}

@end
