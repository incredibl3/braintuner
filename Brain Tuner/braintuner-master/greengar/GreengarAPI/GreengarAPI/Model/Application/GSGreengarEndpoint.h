//
//  GSGreengarEndpoint.h
//  Whiteboard
//
//  Created by Cong Vo on 4/6/11.
//  Copyright 2011 Greengar Studios LLC. All rights reserved.
//

#pragma mark Domain

#ifndef USE_DEVELOPMENT_SERVER
    // For Production (Distribution, AdHocTestFlight, QA, etc.)
    #define USE_DEVELOPMENT_SERVER 0
    // To use the dev server for testing, change the above value to 1
    // Don't commit this change; just use it locally.
#endif

#if USE_DEVELOPMENT_SERVER

//static NSString *const kGreengarDomain = @"184.106.64.162";
//static NSString *const kGreengarServerURL = 
//                        @"http://184.106.64.162/elgg_dev/pg/greengar/";
//
//static NSString *const kGreengarEndpointLoginAuthorize = 
//                        @"http://184.106.64.162/elgg_dev/pg/greengar/oauth2/authorize";
//
//static NSString *const kGreengarEndpointLoginToken = 
//                        @"http://184.106.64.162/elgg_dev/pg/greengar/oauth2/token";

static NSString *const kGreengarDomain = @"www.greengarstudios.com";

static NSString *const kGreengarServerURL = 
@"https://www.greengarstudios.com/usersystem_staging/pg/greengar/";

static NSString *const kGreengarEndpointLoginAuthorize = 
@"https://www.greengarstudios.com/usersystem_staging/pg/greengar/oauth2/authorize";

static NSString *const kGreengarEndpointLoginToken = 
@"https://www.greengarstudios.com/usersystem_staging/pg/greengar/oauth2/token";

#else
static NSString *const kGreengarDomain = @"www.greengarstudios.com";

static NSString *const kGreengarServerURL = 
                        @"https://www.greengarstudios.com/usersystem/pg/greengar/";

static NSString *const kGreengarEndpointLoginAuthorize = 
                        @"https://www.greengarstudios.com/usersystem/pg/greengar/oauth2/authorize";

static NSString *const kGreengarEndpointLoginToken = 
                        @"https://www.greengarstudios.com/usersystem/pg/greengar/oauth2/token";

#endif




#pragma mark User System Endpoint

static NSString *const kGreengarEndpointRegister                = @"user/register";
static NSString *const kGreengarEndpointRegisterWithGameCenter  = @"user/gamecenter_register";
static NSString *const kGreengarEndpointRegisterWithTwitter     = @"user/twitter_register";
static NSString *const kGreengarEndpointLoginWithFacebook       = @"user/facebook_login";
static NSString *const kGreengarEndpointLoginWithGameCenter     = @"user/gamecenter_login";
static NSString *const kGreengarEndpointLoginWithTwitter     = @"user/twitter_login";

static NSString *const kGreengarEndpointGetUserInfo         = @"user/get_user";
static NSString *const kGreengarEndpointChangePassword      = @"user/change_password";
static NSString *const kGreengarEndpointResetPassword       = @"user/reset_password";

static NSString *const kGreengarEndpointGetFriends          = @"user/get_friends";
static NSString *const kGreengarEndpointFindFriends         = @"user/find_friends";      
static NSString *const kGreengarEndpointAddFriend           = @"user/add_friend";
static NSString *const kGreengarEndpointCheckFriend         = @"user/check_friend";      
static NSString *const kGreengarEndpointRemoveFriend        = @"user/remove_friend";

//static NSString *const kGreengarEndpointGetFriends          = @"user/get_followings";
//static NSString *const kGreengarEndpointFindFriends         = @"user/find_friends";      
//static NSString *const kGreengarEndpointAddFriend           = @"user/add_following";
//static NSString *const kGreengarEndpointCheckFriend         = @"user/check_following";      
//static NSString *const kGreengarEndpointRemoveFriend        = @"user/remove_following";


static NSString *const kGreengarEndpointGetFollowing        = @"user/get_followings";
static NSString *const kGreengarEndpointGetFollower         = @"user/get_followings";      
static NSString *const kGreengarEndpointAddFollowing        = @"user/add_following";
static NSString *const kGreengarEndpointCheckFollowing      = @"user/check_following";      
static NSString *const kGreengarEndpointRemoveFollowing     = @"user/remove_following";



static NSString *const kGreengarEndpointGetNotifications	= @"user/get_notifications";	
static NSString *const kGreengarEndpointSetUserName         = @"user/set_user_name";
static NSString *const kGreengarEndpointSetUserGender         = @"user/set_user_gender";
static NSString *const kGreengarEndpointSetUserDOB         = @"user/set_user_dob";
