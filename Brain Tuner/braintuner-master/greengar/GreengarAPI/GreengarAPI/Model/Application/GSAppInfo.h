//
//  GreengarApp.h
//  Whiteboard
//
//  Created by Cong Vo on 4/28/11.
//  Copyright 2011 Greengar Studios LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
@protocol GSContact;

@interface GSAppInfo : NSObject {
    
}

// Required settings
// Please provide these configuration info, before making any API call
// Tips: Usually put it into in appDidFinishLaunching method.
+ (void)setClientID:(NSString *)appID;
+ (void)setClientSecret:(NSString *)appSecret;

+ (NSString *)clientID;
+ (NSString *)clientSecret;




// Optional settings
// Contact Class: if your app's contact class is inherited GSContact
// This will help Greengar Lib parse your reponse contact better
+ (Class)contactClass;
+ (void)setContactClass:(Class <GSContact> )classname;


@end
