//
//  GSWhiteboardEndpoint.h
//  Opinions
//
//  Created by Cong Vo on 4/5/11.
//  Copyright 2011 Greengar Studios LLC. All rights reserved.
//

#import "GSGreengarEndpoint.h"



#pragma mark Whiteboard Application Key & Secret

static NSString *const kWhiteboardClientId = @"whiteboard";

#if USE_DEVELOPMENT_SERVER
static NSString *const kWhiteboardClientSecret = @"whiteboardsecret";
#else
static NSString *const kWhiteboardClientSecret =  @"tH8WihoPoBd8hExInwBr8uRGheOri6mi";
#endif

#pragma mark Whiteboard Endpoint

// NOTICE: "param" with a * mark means Required

static NSString *const kWhiteboardEndpointAssociateAccount          = @"whiteboard/associate_account";
/*
 Purpose: Associate XMPP account
 Param: "login"*, "domain"*
 
 */

static NSString *const kWhiteboardEndpointGetAccount                = @"whiteboard/get_account";
/*
 Purpose: Get my XMPP used accounts
 Param: 
 
 */

static NSString *const kWhiteboardEndpointUploadURL                 = @"whiteboard/uploadurl";
static NSString *const kWhiteboardEndpointGetDrawings                = @"whiteboard/get_drawings";
static NSString *const kWhiteboardEndpointGetDrawing                = @"whiteboard/get_drawing";

static NSString *const kWhiteboardEndpointAddComment          = @"whiteboard/add_comment";
static NSString *const kWhiteboardEndpointGetComments          = @"whiteboard/get_comments";

static NSString *const kWhiteboardEndpointGetFavorites          = @"user/get_favorites";
//static NSString *const kGreengarEndpointFindFriends         = @"user/find_friends";      
//static NSString *const kGreengarEndpointAddFriend           = @"user/add_favorite";
static NSString *const kWhiteboardEndpointAddFavorite           = @"user/add_favorite";
static NSString *const kWhiteboardEndpointCheckFavorite         = @"user/check_favorite";      
static NSString *const kWhiteboardEndpointRemoveFavorite        = @"user/remove_favorite";

static NSString *const kWhiteboardEndpointMatchFBFriend         = @"whiteboard/suggest_friends_wb3";

#pragma mark Template

//static NSString *const kWhiteboardEndpoint = @"";
/*
 Purpose: 
 Param: 
 
 */
