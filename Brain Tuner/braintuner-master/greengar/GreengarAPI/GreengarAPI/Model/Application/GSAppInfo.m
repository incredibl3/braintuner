//
//  GreengarApp.m
//  Whiteboard
//
//  Created by Cong Vo on 5/13/11.
//  Copyright 2011 Greengar Studios LLC. All rights reserved.
//

#import "GSAppInfo.h"
#import "GSContact.h"

static NSString * clientID = nil;
static NSString * clientSecret = nil;
static Class contactClass = nil;

@implementation GSAppInfo

+ (void)initialize {
    contactClass = [GSContact class];
}

+ (NSString *)clientID {
    return clientID;
}

+ (void)setClientID:(NSString *)appID {
    clientID = [appID retain];
}


+ (NSString *)clientSecret {
    return clientSecret;
}

+ (void)setClientSecret:(NSString *)appSecret {
    clientSecret = [appSecret retain];
}


//Contact class
+ (Class)contactClass {
    return contactClass;
}

+ (void)setContactClass:(Class <GSContact> )classname {
    contactClass = classname;
}

@end