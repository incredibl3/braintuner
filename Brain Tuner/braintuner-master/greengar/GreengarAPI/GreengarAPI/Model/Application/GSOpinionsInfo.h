//
//  GSOpinionsEndpoint.h
//  Opinions
//
//  Created by Cong Vo on 4/5/11.
//  Copyright 2011 Greengar Studios LLC. All rights reserved.
//

#import "GSGreengarEndpoint.h"



#pragma mark Opinions Application Key & Secret

static NSString *const kOpinionsEndpointClientId = @"opinions";
#if USE_DEVELOPMENT_SERVER
static NSString *const kOpinionsEndpointClientSecret = @"opinionssecret";
#else
static NSString *const kOpinionsEndpointClientSecret = @"tH8WihoPoBd8hExInwBr8uRGheOri6mi";
#endif

// Opinions 
static NSString *const kOpinionsFacebookAppId = @"158554237501746";
static NSString *const kOpinionsSecretSalt = @"yQ%)^fi<FnceBz1$>>D!wwhX&?2OhJK|C;R;?X6>)/ruX2)}^p|f+6rkn14A=mbx";


#pragma mark Opinions Endpoint

static NSString *const kOpinionsEndpointAssociateAccount            = @"opinions/associate_account";	
static NSString *const kOpinionsEndpointSuggestFriends              = @"opinions/suggest_friends";

static NSString *const kOpinionsEndpointAddInterest                 = @"opinions/add_interest";	
static NSString *const kOpinionsEndpointInterestDetail              = @"opinions/get_interest_detail";
static NSString *const kOpinionsEndpointRateInterest                = @"opinions/rate_interest";	
static NSString *const kOpinionsEndpointGetCategories               = @"opinions/get_categories";	
static NSString *const kOpinionsEndpointGetInterests                = @"opinions/get_interests";	
static NSString *const kOpinionsEndpointSuggestInterests            = @"opinions/suggest_interests";
static NSString *const kOpinionsEndpointSuggestInterestsHITS        = @"opinions/suggest_interests_hits";
static NSString *const kOpinionsEndpointRecentInterests             = @"opinions/get_recent_interests";
static NSString *const kOpinionsEndpointPopularInterests            = @"opinions/get_popular_interests";
static NSString *const kOpinionsEndpointNearbyInterests             = @"opinions/get_nearby_interests";
static NSString *const kGreengarEndpointGetRecentActivities         = @"opinions/get_recent_activities";
static NSString *const kOpinionsEndpointFindMatchingFriends         = @"opinions/get_matching_friends";
static NSString *const kOpinionsEndpointGetInterestFriendRatings    = @"opinions/get_friend_ratings";
static NSString *const kOpinionsEndpointAddRatingComment            = @"opinions/add_rating_comment";
static NSString *const kOpinionsEndpointGetFriendCommentsOnRating   = @"opinions/get_rating_comments";

static NSString *const kOpinionsEndpointInterestUploadImage         = @"opinions/upload";
static NSString *const kOpinionsEndpointChangePassword              = @"opinions/change_password";
static NSString *const kGreengarEndpointSearchInterest              = @"opinions/search_interest";
static NSString *const kGreengarEndpointGetInterestRatingDetail     = @"opinions/get_interest_rating";
static NSString *const kOpinionsEndpointUnRatedInterest             = @"opinions/unrated";
static NSString *const kOpinionsEndpointUploadAvatarImage           = @"opinions/set_user_avatar";
static NSString *const kOpinionsEndpointGetUserRatings              = @"opinions/get_user_ratings";
static NSString *const kOpinionsEndpointGetFriendsOfFriend          = @"opinions/get_friends_of_friend";

// reset password endponint of Opinions
static NSString *const kOpinionsEndpointResetPassword               = @"opinions/reset_password";

// report abuse
static NSString *const kOpinionsEndpointReportAbuse                 = @"opinions/report_abused";
