//
//  PaintingManager.h
//  Whiteboard
//
//  Created by _Nam on 7/13/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ASICloudFilesRequest.h"
#import "ASICloudFilesContainerRequest.h"
#import "ASICloudFilesCDNRequest.h"
#import "ASICloudFilesObjectRequest.h"
#import <CommonCrypto/CommonDigest.h>

typedef enum {
    GSCloudFilesUploadGreengarUserAvatar,
    GSCloudFilesUploadOpinionsInterestImage,
    GSCloudFilesUploadWhiteboardGalleryImage
} GSCloudFilesUploader;

static NSString *const kGreengarCloudFilesUserName  = @"greengarinc";
static NSString *const kGreengarCloudFilesAPIKey    = @"e7d31711dbfe01b9085117422201e58b";

// Greengar User Avatars
static NSString *const kGreengarCloudFilesGreengarUserAvatarContainerName    = @"avatar.user.greengar.com";
static NSString *const kGreengarCloudFilesGreengarUserAvatarContainerURL    = @"http://c819747.r47.cf2.rackcdn.com/";

// Opinions Interest Images
static NSString *const kGreengarCloudFilesOpinionsInterestImageContainerName    = @"interest.image.opinions.greengar.com";
static NSString *const kGreengarCloudFilesOpinionsInterestImageContainerURL    = @"http://c819746.r46.cf2.rackcdn.com/";

// Whiteboard Drawings
static NSString *const kGreengarCloudFilesWhiteboardDrawingsContainerName    = @"drawings.whiteboard.greengar.com";
static NSString *const kGreengarCloudFilesWhiteboardDrawingsContainerURL    = @"http://c819749.r49.cf2.rackcdn.com/";
static NSString *const kGreengarCloudFilesWhiteboardDrawingsContainerNameFormat    = @"drawings.%@.whiteboard.greengar.com";

@class CloudFilesManager;

@protocol CloudFilesManagerDelegate <NSObject>

- (void)didUploadImageData:(NSString*)url;

@end

@interface CloudFilesManager : NSObject {
    id<CloudFilesManagerDelegate> _delegate;
    NSData   * _data;
    NSString * _containerName;
    NSString * fileName;
    NSString * fileMD5;
    NSString * url;
}

+ (CloudFilesManager *)sharedManager;
@property (nonatomic, retain) NSString * fileName;
@property (nonatomic, retain) NSString * fileMD5;
@property (nonatomic, retain) NSString * url;

- (NSString *)uploadImageData:(NSData *)imageData delegate:(id)delegate uploader:(GSCloudFilesUploader)uploader;

@end
