//
//  GSRequest.h
//  Opinions
//
//  Created by Cong Vo on 3/7/11.
//  Copyright 2011 Greengar Studios LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NXOAuth2ConnectionDelegate.h"

#import "GSAppInfo.h"
#import "GSWhiteboardInfo.h"
#import "GSOpinionsInfo.h"

@interface GSRequest : NSObject <NXOAuth2ConnectionDelegate> {
    id _delegate;
    SEL _callback;
    
    // Request info. 
    // Use this delegate can get more info about which request is calling back.
    NSString *_endPoint;
    NSDictionary *_params;

    // Connection.
    NSURLConnection *_conn;
    NSMutableData *_receivedData;
}

@property (nonatomic, retain) NSDictionary *params;


// Main method for Greengar social network APIs
+ (GSRequest *)requestWithEndPoint:(NSString *)endPoint
                            params:(NSDictionary *)params
                          delegate:(id)delegate
                          callback:(SEL)callback;

+ (GSRequest *)requestWithEndPoint:(NSString *)endPoint
                         HTTPMethod:(NSString *)HTTPMethod
                             params:(NSDictionary *)params
                           delegate:(id)delegate
                           callback:(SEL)callback;

+ (GSRequest *)requestWithURL:(NSString *)URLString 
            HTTPMethod:(NSString *)HTTPMethod
                params:(NSDictionary *)params
              delegate:(id)delegate
              callback:(SEL)callback;

+ (void)uploadImageData:(NSData *)data
               delegate:(id)delegate
               callback:(SEL)callback;

- (id)initWithDelegate:(id)delegate callback:(SEL)callback;
- (void)requestWithURL:(NSString *)URLString 
                   HTTPMethod:(NSString *)HTTPMethod
                       params:(NSDictionary *)params;
@end
