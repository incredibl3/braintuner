//
//  GameCenterManager.m
//  Whiteboard
//
//  Created by Sheehan on 6/30/11.
//  Copyright 2011 GreenGar studios. All rights reserved.
//

#import "GameCenterManager.h"
#import <QuartzCore/QuartzCore.h>
#import <CommonCrypto/CommonHMAC.h>
#import "GSRequest.h"

@implementation GameCenterManager

@synthesize authenticationHandler, userAuthenticatedGC, userAuthenticatedGG;

static GameCenterManager *sharedGameCenterManager = nil;

#pragma mark Singleton methods

+ (GameCenterManager *) sharedGameCenterManager {    
    if (!sharedGameCenterManager) {
        sharedGameCenterManager = [[GameCenterManager alloc] init];
    }
    return sharedGameCenterManager;
}

// registers for notification
- (id) init {
	self = [super init];
	if(self != NULL) {
        userAuthenticatedGC = FALSE;
        if ([GameCenterManager isGameCenterAvailable]) { 
            userAuthenticatedGC = NO;
            userAuthenticatedGG = NO;
            userAuthenticatedGCGG = NO;
            haveCheckedForAssociation = NO;
            isThereAssociation = NO;
            loggedIntoGGViaGC = NO;
            //[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(authenticationChanged)                                                          name:GKPlayerAuthenticationDidChangeNotificationName object:nil];
        }
        
	}
	return self;
}

+ (BOOL) isGameCenterAvailable {
	// check for presence of GKLocalPlayer API
	Class gcClass = (NSClassFromString(@"GKLocalPlayer"));
	
	// check if the device is running iOS 4.1 or later
	NSString *reqSysVer = @"4.1";
	NSString *currSysVer = [[UIDevice currentDevice] systemVersion];
	BOOL osVersionSupported = ([currSysVer compare:reqSysVer options:NSNumericSearch] != NSOrderedAscending);
	
	return (gcClass && osVersionSupported);
}

- (void) deallocPrivate {
    userAuthenticatedGC = NO;
    userAuthenticatedGG = NO;
    userAuthenticatedGCGG = NO;
    haveCheckedForAssociation = NO;
    isThereAssociation = NO;
    loggedIntoGGViaGC = NO;
    authenticationHandler = nil;
    [sharedGameCenterManager release];
    sharedGameCenterManager = nil;
	[super dealloc];
}

+ (void) dealloc {
    if (sharedGameCenterManager) {
        [sharedGameCenterManager deallocPrivate];
    }
}

#pragma mark Registration

/* This is called when Game Center is logged in but the user cannot be authenticated, which
 means they need to have a Greengar account silently created for them. */
- (void) registerUsingGameCenter {
    GKLocalPlayer *localPlayer = [GKLocalPlayer localPlayer];
    
    [authenticationHandler registeredUsingGameCenterID:localPlayer.playerID andAlias:localPlayer.alias];
/*    
    //[[self class] logout];
    
    double currentTimeStamp = CACurrentMediaTime();
    
    NSString *email = [NSString stringWithFormat:@"%@@greengarstudios.com", localPlayer.playerID];
    
    NSString * sourceString = [NSString stringWithFormat:@"%@,%@,%@,%@,%@,%@,%@,%f",
                               email, // email
                               localPlayer.alias, // username
                               @"", // first_name
                               @"", //last_name
                               @"", // avatar_url
                               localPlayer.playerID, // gamecenter_id
                               localPlayer.alias, // gamecenter_alias
                               currentTimeStamp];
    
    NSData *clearTextData = [sourceString dataUsingEncoding:NSUTF8StringEncoding];
    NSData *keyData = [kOpinionsSecretSalt dataUsingEncoding:NSUTF8StringEncoding];
    
    uint8_t digest[CC_SHA1_DIGEST_LENGTH] = {0};
    
    CCHmacContext hmacContext;
    CCHmacInit(&hmacContext, kCCHmacAlgSHA1, keyData.bytes, keyData.length);
    CCHmacUpdate(&hmacContext, clearTextData.bytes, clearTextData.length);
    CCHmacFinal(&hmacContext, digest);
    
    NSString * signature = [self base64forData:[NSData dataWithBytes:digest length:CC_SHA1_DIGEST_LENGTH]];
    
    // send POST request here
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            email, @"email",
                            localPlayer.alias, @"username",
                            @"", @"first_name",
                            @"", @"last_name",
                            @"", @"avatar_url",
                            localPlayer.playerID, @"gamecenter_id", 
                            localPlayer.alias, @"gamecenter_alias",
                            [NSString stringWithFormat:@"%f", currentTimeStamp], @"timestamp",
                            signature, @"signature",
                            nil];
    
    [GSRequest requestWithEndPoint:kGreengarEndpointRegisterWithGameCenter
                            params:params
                          delegate:self
                          callback:@selector(greengar:finishedRegistration:)];
 
 */
}

- (void)registration:(GSAuthentication *)auth successed:(NXOAuth2Client *)oauthClient {
    //[self getXMPPServices];
}

#pragma mark Authentication


// NOTE:  GameCenter does not guarantee that callback blocks will be execute on the main thread. 
// As such, your application needs to be very careful in how it handles references to view
// controllers.  If a view controller is referenced in a block that executes on a secondary queue,
// that view controller may be released (and dealloc'd) outside the main queue.  This is true
// even if the actual block is scheduled on the main thread.  In concrete terms, this code
// snippet is not safe, even though viewController is dispatching to the main queue:
//
//	[object doSomethingWithCallback:  ^()
//	{
//		dispatch_async(dispatch_get_main_queue(), ^(void)
//		{
//			[viewController doSomething];
//		});
//	}];
//
// UIKit view controllers should only be accessed on the main thread, so the snippet above may
// lead to subtle and hard to trace bugs.  Many solutions to this problem exist.  In this sample,
// I'm bottlenecking everything through  "callDelegateOnMainThread" which calls "callDelegate". 
// Because "callDelegate" is the only method to access the delegate, I can ensure that delegate
// is not visible in any of my block callbacks.

- (void) callDelegate: (SEL) selector withArg: (id) arg error: (NSError*) err
{
	assert([NSThread isMainThread]);
	if([self respondsToSelector: selector])
	{
		if(arg != NULL)
		{
			[self performSelector: selector withObject: arg withObject: err];
		}
		else
		{
			[self performSelector: selector withObject: err];
		}
	}
	else
	{
		DLog(@"Missed Method");
	}
}


- (void) callDelegateOnMainThread: (SEL)selector withArg: (id)arg error: (NSError*)err
{
	dispatch_async(dispatch_get_main_queue(), ^(void)
                   {
                       [self callDelegate: selector withArg: arg error: err];
                   });
}


- (void) authenticateLocalUser {
    
    if (![GameCenterManager isGameCenterAvailable]) 
    { return; }
    
//	if([GKLocalPlayer localPlayer].authenticated == NO)
	{
        // block programming is sure to work on any GC enabled phone
		[[GKLocalPlayer localPlayer] authenticateWithCompletionHandler:^(NSError *error) 
         {
             [self callDelegateOnMainThread: @selector(processGameCenterAuth:) withArg: NULL error: error];
         }];
	}
}



/*
- (void) authenticationChanged {
    if ([GKLocalPlayer localPlayer].isAuthenticated) {
        NSLog(@"# Authentication changed: player authenticated: %@ = %@", [GKLocalPlayer localPlayer].alias, [GKLocalPlayer localPlayer].playerID);
        userAuthenticatedGC = TRUE;
        
        
        // processGameCenterAuth in RootViewController handles this
        
        //if([[[GKLocalPlayer localPlayer] alias] isEqualToString:curPlayer]){
        // NSLog(@"And is same player");
        //}
        //else {
        //curPlayer = [[GKLocalPlayer localPlayer] alias];
        //[[NSNotificationCenter defaultCenter] postNotificationName:kMultiplayerCancelled object:nil];
        //}
    } else {
        // kick them out
        NSLog(@"# Authentication changed: player not authenticated: %@",[GKLocalPlayer localPlayer].alias);
        if(userAuthenticatedGC){
            userAuthenticatedGC = FALSE;
            @try {
                //[[NSNotificationCenter defaultCenter] postNotificationName:kMultiplayerCancelled object:nil];
            }
            @catch (NSException *exception) {
                NSLog(@"# %@", [exception reason]);
            }
        }
    }
}
 */



// called whenever GC tries to authenticate a player (such as coming to forground) and gets a response from the server
- (void) processGameCenterAuth: (NSError*) error {
    GKLocalPlayer *localPlayer = [GKLocalPlayer localPlayer];
    
    if(error == NULL) {
        if(localPlayer.authenticated){
            DLog(@"# processGameCenterAuth: player authenticated: %@ = %@", localPlayer.alias, localPlayer.playerID);
            userAuthenticatedGC = TRUE;
            // if checkAssociationX has been called, then GG is logged in and was waiting for us, so we should act
//            if(haveCheckedForAssociation){
//                [authenticationHandler authenticatedWithGameCenterID:localPlayer.playerID andAlias:localPlayer.alias];
//            }
            [authenticationHandler authenticatedWithGameCenterID:localPlayer.playerID andAlias:localPlayer.alias];            
        }
        else {
            DLog(@"# processGameCenterAuth: player not authenticated: %@ = %@", localPlayer.alias, localPlayer.playerID);
            userAuthenticatedGC = FALSE;
            // don't do anything
        }
    }
    else {
        DLog(@"# %@", [error description]);
        if([error code] == kErrorCodeNotRecognizedByGameCenter){
            // if we're authenticated, do it anyway
            if([GameCenterManager sharedGameCenterManager].userAuthenticatedGC){
                DLog(@"# processGameCenterAuth: player authenticated: %@ = %@", localPlayer.alias, localPlayer.playerID);
                // send a login command to greengar server
                
            }
        }
        // this will be gotten rid of when ready to release (so user doesn't know about Game Center login)
        else {
            UIAlertView* alert= [[UIAlertView alloc] initWithTitle: @"Error" message:@"Game Center server not responding"
                                                          delegate:self cancelButtonTitle: @"OK" otherButtonTitles: NULL];
            [alert show];
            [alert release];
        }
    }
    
}

#pragma mark Association

// updates the users logged in to GG and/or GC status and if appropriate, checks for association
- (void) checkAssociationAndUserLoggedIntoGG:(BOOL)authenticatedByGG {
    GKLocalPlayer *localPlayer = [GKLocalPlayer localPlayer];
    userAuthenticatedGG = authenticatedByGG;
    haveCheckedForAssociation = YES;
    if(userAuthenticatedGC){
        [authenticationHandler authenticatedWithGameCenterID:localPlayer.playerID andAlias:localPlayer.alias];
    }
}

- (void) authentication:(GSAuthentication *)auth succeeded:(NXOAuth2Client *)oauthClient {
    
}


- (void) authentication:(GSAuthentication *)auth failed:(NSError *)error {
    
}

@end
