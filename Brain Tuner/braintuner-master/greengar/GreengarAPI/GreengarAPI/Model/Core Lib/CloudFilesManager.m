//
//  PaintingManager.m
//  Whiteboard
//
//  Created by _Nam on 7/13/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "CloudFilesManager.h"

@implementation CloudFilesManager

@synthesize fileMD5, fileName, url;

+ (CloudFilesManager *)sharedManager { 
    static CloudFilesManager *sharedManager; 
    static dispatch_once_t done; 
    dispatch_once(&done, ^{ sharedManager = [CloudFilesManager new]; }); 
    return sharedManager;
}

- (id) init {
    self = [super init];
    if(self) {
        // NAM: init cloud file
//        [ASICloudFilesRequest setUsername:kGreengarCloudFilesUserName];
//        [ASICloudFilesRequest setApiKey:kGreengarCloudFilesAPIKey];
//        [ASICloudFilesRequest authenticate];
        
//        _containerName = [[NSString alloc] init];
//        _data = [[NSData alloc] init];
//        fileMD5 = [[NSString alloc] init];
//        fileName = [[NSString alloc] init];
//        url = [[NSString alloc] init];
    }
    return self;
}

- (NSString *)getContainerName:(GSCloudFilesUploader)uploader {
    switch (uploader) {
        case GSCloudFilesUploadGreengarUserAvatar:
            return kGreengarCloudFilesGreengarUserAvatarContainerName;
            break;
        case GSCloudFilesUploadOpinionsInterestImage:
            return kGreengarCloudFilesOpinionsInterestImageContainerName;
            break; 
        case GSCloudFilesUploadWhiteboardGalleryImage:
            return nil;
            break;
        default:
            return nil;
            break;
    }
}

- (NSString *)getContainerURL:(GSCloudFilesUploader)uploader {
    switch (uploader) {
        case GSCloudFilesUploadGreengarUserAvatar:
            return kGreengarCloudFilesGreengarUserAvatarContainerURL;
            break;
        case GSCloudFilesUploadOpinionsInterestImage:
            return kGreengarCloudFilesOpinionsInterestImageContainerURL;
            break;   
        case GSCloudFilesUploadWhiteboardGalleryImage:
            return nil;
            break;
        default:
            return nil;
            break;
    }
}

- (NSString *)getContainerName:(GSCloudFilesUploader)uploader for:(NSString*)file {
    //NSString * containerId = [file substringToIndex:4];
    //containerName = [NSString stringWithFormat:kGreengarCloudFilesWhiteboardDrawingsContainerNameFormat, containerId];
    switch (uploader) {
        case GSCloudFilesUploadGreengarUserAvatar:
            return kGreengarCloudFilesGreengarUserAvatarContainerName;
            break;
        case GSCloudFilesUploadOpinionsInterestImage:
            return kGreengarCloudFilesOpinionsInterestImageContainerName;
            break; 
        case GSCloudFilesUploadWhiteboardGalleryImage:
            return nil;
            break;
        default:
            return nil;
            break;
    }
}

- (NSString *)getContainerURL:(GSCloudFilesUploader)uploader for:(NSString*)file {
    //NSString * containerId = [file substringToIndex:4];
    //containerName = [NSString stringWithFormat:kGreengarCloudFilesWhiteboardDrawingsContainerNameFormat, containerId];
    switch (uploader) {
        case GSCloudFilesUploadGreengarUserAvatar:
            return kGreengarCloudFilesGreengarUserAvatarContainerURL;
            break;
        case GSCloudFilesUploadOpinionsInterestImage:
            return kGreengarCloudFilesOpinionsInterestImageContainerURL;
            break;   
        case GSCloudFilesUploadWhiteboardGalleryImage:
            return nil;
            break;
        default:
            return nil;
            break;
    }
}

- (NSString *)md5StringFromData:(NSData *)data
{
    void *cData = malloc([data length]);
    unsigned char resultCString[16];
    [data getBytes:cData length:[data length]];
    
    CC_MD5(cData, (int)[data length], resultCString);
    free(cData);
    
    NSString *result = [NSString stringWithFormat:
                        @"%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X",
                        resultCString[0], resultCString[1], resultCString[2], resultCString[3], 
                        resultCString[4], resultCString[5], resultCString[6], resultCString[7],
                        resultCString[8], resultCString[9], resultCString[10], resultCString[11],
                        resultCString[12], resultCString[13], resultCString[14], resultCString[15]
                        ];
    return result;
}

BOOL needCreateContainer = NO;
BOOL needEnableCDN = NO;

- (void)requestFinished:(ASIHTTPRequest *)request {
    
    if(needCreateContainer) {
        //ASICloudFilesCDNRequest *req = 
        
        
        //    self.delegate = delegate;
        //    self.callback = callback;
        //    [cloudRequest setDelegate:self];
        //    [cloudRequest startAsynchronous];
        //    _callback = callback;
        needCreateContainer = NO;
        
        if(needEnableCDN) {
            
            needEnableCDN = NO;
            
            ASICloudFilesCDNRequest *cdnRequest = [ASICloudFilesCDNRequest postRequestWithContainer:_containerName cdnEnabled:YES ttl:4320];
            [cdnRequest setDelegate:self];
            [cdnRequest startAsynchronous];
            return;
        }
        
        ASICloudFilesObjectRequest *cloudRequest = 
        [ASICloudFilesObjectRequest putObjectRequestWithContainer:_containerName 
                                                       objectPath:fileName contentType:@"image/png" objectData:_data metadata:nil etag:nil];
        [cloudRequest setDelegate:self];
        [cloudRequest startAsynchronous];
        return;
    }
    
    // Use when fetching text data
    NSString *responseString = [request responseString];
    DLog(@"responseString: %@", responseString);
    if ([responseString isKindOfClass:[NSString class]]) {
        // [self objectFromJSON:responseString];  
        [_delegate didUploadImageData:url];
    }
}

- (void)requestFailed:(ASIHTTPRequest *)request {
    
    //ELLIOT: prevent compile warning when building for Release/Distribution
    //NSError *error = [request error];
    
    DLog(@"error: %@", ([request error]));
    [_delegate didUploadImageData:nil];
}

- (void) reset {
    [url release];
    [fileName release];
    [fileMD5 release];
    [_delegate release];
    [_data release];
    [_containerName release];
}

- (NSString *)uploadImageData:(NSData *)imageData delegate:(id)delegate uploader:(GSCloudFilesUploader)uploader {
    // [self reset];
    // NAM: init cloud file
    [ASICloudFilesRequest setUsername:kGreengarCloudFilesUserName];
    [ASICloudFilesRequest setApiKey:kGreengarCloudFilesAPIKey];
    [ASICloudFilesRequest authenticate];
    
    fileMD5 = [[NSString alloc] initWithFormat:@"%@", [self md5StringFromData:imageData]];
    fileName = [[NSString alloc] initWithFormat:@"%@.png", fileMD5];
    
    DLog(@"%@ -> %@", fileMD5, fileName);
    
    NSString * containerId = [fileMD5 substringToIndex:4];
    
    if(uploader == GSCloudFilesUploadWhiteboardGalleryImage) {
        _containerName = [[NSString alloc] initWithFormat:kGreengarCloudFilesWhiteboardDrawingsContainerNameFormat, containerId];
        DLog(@"%@ -> %@", fileMD5, _containerName);
        ASICloudFilesContainerRequest *req = [ASICloudFilesContainerRequest createContainerRequest:_containerName];
        [req setDelegate:self];
        _delegate = delegate;
        needCreateContainer = YES;
        needEnableCDN = NO;
        _data = [[NSData alloc] initWithData:imageData];
//        _data = [imageData retain];
        [req startAsynchronous];
        return nil;
    }
    
    // NAM: check existence of object before upload ???
    //    ASICloudFilesObjectRequest *request = 
    //    [ASICloudFilesObjectRequest getObjectRequestWithContainer:kGreengarCloudFilesOpinionsContainerName
    //                                                   objectPath:@"/path/to/the/object"];
    //    [request startSynchronous];
    //    ASICloudFilesObject *object = [request object];
    
    //NSData *testdata = [@"this is a test" dataUsingEncoding:NSUTF8StringEncoding];
    
    ASICloudFilesObjectRequest *cloudRequest = 
    [ASICloudFilesObjectRequest putObjectRequestWithContainer:[self getContainerName:uploader for:fileName] 
                                                   objectPath:fileName contentType:@"image/png" objectData:imageData metadata:nil etag:nil];
    
    //    self.delegate = delegate;
    //    self.callback = callback;
    //    [cloudRequest setDelegate:self];
    //    [cloudRequest startAsynchronous];
    url = [[NSString alloc] initWithFormat:@"%@%@", [self getContainerURL:uploader], fileName];
    
    _delegate = delegate;
//    _callback = callback;
    [cloudRequest setDelegate:self];
    [cloudRequest startAsynchronous];
    return url;
}

- (void) dealloc {
    [url release];
    [fileName release];
    [fileMD5 release];
    [_delegate release];
    [_data release];
    [_containerName release];
    [super dealloc];
}

@end
