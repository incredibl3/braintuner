//
//  GSRequest.m
//  Opinions
//
//  Created by Cong Vo on 3/7/11.
//  Copyright 2011 Greengar Studios LLC. All rights reserved.
//

#import "GSRequest.h"
#import "GSAuthentication.h"

#import "SBJson.h"
#import "NXOAuth2Connection.h"
#import "GameCenterManager.h"

@interface GSRequest ()

@property (nonatomic, retain) NSURLConnection *conn;
//Not assign value to endPoint yet
@property (nonatomic, retain) NSString *endPoint;

- (void)requestWithURL:(NSString *)URLString 
            HTTPMethod:(NSString *)HTTPMethod
                params:(NSDictionary *)params;    
- (void)uploadImageData:(NSData *)data
               delegate:(id)delegate
               callback:(SEL)callback;
- (void)requestEndWithError:(NSError *)error response:(id)response;


- (void)startConnection:(NSURLRequest *)request;
@end


@implementation GSRequest
@synthesize params      = _params,
            endPoint    = _endPoint,
            conn        = _conn;

- (id)initWithDelegate:(id)delegate callback:(SEL)callback {
    if ((self = [super init])) {
        _delegate = delegate;
        _callback = callback;
        _receivedData = [[NSMutableData alloc] init];
    }
    return self;
}


- (void)dealloc {
    [_receivedData release];
    [_params release];
    [_endPoint release];
    [_conn release];
    
    [super dealloc];
}


+ (GSRequest *)requestWithURL:(NSString *)URLString 
                  HTTPMethod:(NSString *)HTTPMethod
                      params:(NSDictionary *)params
                    delegate:(id)delegate
                    callback:(SEL)callback {
    
    //KONG: memory: self-retained, will be released when connection finished
    GSRequest *request = [[GSRequest alloc] initWithDelegate:delegate callback:callback];
    request.params = params;
    [request requestWithURL:URLString HTTPMethod:HTTPMethod params:params];
    return request;
}

+ (GSRequest *)requestWithEndPoint:(NSString *)endPoint
                 HTTPMethod:(NSString *)HTTPMethod
                     params:(NSDictionary *)params
                   delegate:(id)delegate
                   callback:(SEL)callback {
    DLog(@"endpoint: %@ & param = %@", endPoint, params);
    NSString *URLString = [NSString stringWithFormat:@"%@%@", kGreengarServerURL, endPoint];
    return [self requestWithURL:URLString
              HTTPMethod:HTTPMethod 
                  params:params
                delegate:delegate
                callback:callback];
}

+ (GSRequest *)requestWithEndPoint:(NSString *)endPoint
                     params:(NSDictionary *)params
                   delegate:(id)delegate
                   callback:(SEL)callback {
    return [self requestWithEndPoint:endPoint
                   HTTPMethod:@"POST"
                       params:params
                     delegate:delegate
                     callback:callback];
}

+ (void)uploadImageData:(NSData *)data
                   delegate:(id)delegate
                   callback:(SEL)callback {
    GSRequest *helper = [[GSRequest alloc] initWithDelegate:delegate callback:callback];
    [helper uploadImageData:data
                delegate:delegate
                callback:callback];
}

- (void)uploadImageData:(NSData *)data
               delegate:(id)delegate
               callback:(SEL)callback {
	
    DLog(@"endpoint: %@ & param = %@", @"upload", nil);
    NSString *URLString = [NSString stringWithFormat:@"%@%@", kGreengarServerURL, kOpinionsEndpointInterestUploadImage];    
	NSURL *url = [NSURL URLWithString:URLString];
	NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
//    NSMutableURLRequest* request = [NSMutableURLRequest requestWithURL:url
//                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
//                                                       timeoutInterval:60.0];

	[request setHTTPMethod:@"POST"];
	
    // File upload code adapted from http://www.cocoadev.com/index.pl?HTTPFileUpload
    // and http://www.cocoadev.com/index.pl?HTTPFileUploadSample
    
    NSString* stringBoundary = @"0xKhTmLbOuNdArY";
    
    NSMutableDictionary* headers = [[[NSMutableDictionary alloc] init] autorelease];
    [headers setValue:@"no-cache" forKey:@"Cache-Control"];
    [headers setValue:@"no-cache" forKey:@"Pragma"];
    [headers setValue:[NSString stringWithFormat:@"multipart/form-data; boundary=%@", stringBoundary] forKey:@"Content-Type"];
    

    [request setHTTPMethod:@"POST"];
    [request setAllHTTPHeaderFields:headers];
    
    NSMutableData* postData = [NSMutableData dataWithCapacity:[data length] + 512];
    [postData appendData:[[NSString stringWithFormat:@"--%@\r\n", stringBoundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    // title
    [postData appendData:[@"Content-Disposition: form-data; name=\"title\"\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
	[postData appendData:[@"Opinions.image" dataUsingEncoding:NSUTF8StringEncoding]];
	[postData appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",stringBoundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // description
	[postData appendData:[@"Content-Disposition: form-data; name=\"description\"\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
	[postData appendData:[@"some description" dataUsingEncoding:NSUTF8StringEncoding]];
	[postData appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",stringBoundary] dataUsingEncoding:NSUTF8StringEncoding]];
	
    // tags
    [postData appendData:[@"Content-Disposition: form-data; name=\"tags\"\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
	[postData appendData:[@"" dataUsingEncoding:NSUTF8StringEncoding]];
	[postData appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",stringBoundary] dataUsingEncoding:NSUTF8StringEncoding]];
	
	[postData appendData:[@"Content-Disposition: form-data; name=\"upload\"; filename=\"ipodfile.png\"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
	[postData appendData:[@"Content-Type: image/png\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
	
    [postData appendData:data];
    [postData appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n", stringBoundary] dataUsingEncoding:NSUTF8StringEncoding]];
	
	
    [request setHTTPBody:postData];
    
    [self startConnection:request];
}


- (void)requestWithURL:(NSString *)URLString 
            HTTPMethod:(NSString *)HTTPMethod
                params:(NSDictionary *)params {
	
	NSURL *url = [NSURL URLWithString:URLString];
	NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
	
	[request setHTTPMethod:HTTPMethod];
	
	NSString* charset = (NSString*)CFStringConvertEncodingToIANACharSetName(CFStringConvertNSStringEncodingToEncoding(NSUTF8StringEncoding));
    
	[request setValue:[NSString stringWithFormat:@"application/x-www-form-urlencoded; charset=%@", charset] forHTTPHeaderField:@"Content-Type"];
	
	//application/x-www-form-urlencoded
    
    NSMutableString *httpPOSTBody = [NSMutableString string];
    
    for (NSString* key in params) {
        id value = [params objectForKey:key];
        [httpPOSTBody appendFormat:@"%@=%@", key, value];
        
        if (key != [[params allKeys] lastObject]) {
            [httpPOSTBody appendString:@"&"];
        }
    }
    
    [httpPOSTBody appendString:[NSString stringWithFormat:@"&client_id=%@", [GSAppInfo clientID]]];
    
    //DLog(@"HTTP BODY : %@", httpPOSTBody);
	[request setHTTPBody:[httpPOSTBody dataUsingEncoding:NSUTF8StringEncoding]];

    [self startConnection:request];
}

- (void)requestEndWithError:(NSError *)error response:(id)response {
//    [(UIAlertView *)[[[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"Error when parsing response"]
//                                 message:[NSString stringWithFormat:@"Error %d: %@ \n Response: `%@`",
//                                          error.code, [error localizedDescription], response]
//                                delegate:nil
//                       cancelButtonTitle:@"OK"
//                       otherButtonTitles:nil] autorelease] show];
    
    [_delegate performSelector:_callback withObject:self withObject:error];
}

#pragma mark Asynchronous request
// static BOOL const asynchronous = YES;

- (void)startConnection:(NSMutableURLRequest *)request {

    if ([GSAuthentication isAuthenticated] == NO) {
        
        self.conn = [[[NSURLConnection alloc] initWithRequest:request
                                               delegate:self
                                       startImmediately:YES] autorelease];
        
    } else {
        self.conn = (NSURLConnection *)[[[NXOAuth2Connection alloc] initWithRequest:request
                                                                  requestParameters:nil
                                                                        oauthClient:[GSAuthentication oauthClient]
                                                                           delegate:self] autorelease];
    }
    
    if (_conn == nil) {
        NSMutableDictionary* info = [NSMutableDictionary dictionaryWithObject:[request URL] forKey:NSURLErrorFailingURLStringErrorKey];
        [info setObject:@"Could not open connection" forKey:NSLocalizedDescriptionKey];
        NSError* error = [NSError errorWithDomain:@"GSRequest" code:1 userInfo:info];
        [self requestEndWithError:error response:info];
    }

}

- (void)didFinishLoadingData:(NSData *)data {
    
    NSError *error = nil;
    // API fetch succeeded
    NSString *str = [[[NSString alloc] initWithData:data
                                           encoding:NSUTF8StringEncoding] autorelease];
    
    DLog(@"|||||%@||||", str);
    // Parse
    SBJsonParser *jsonParser = [[[SBJsonParser alloc] init] autorelease];
    id respondingObject = [jsonParser objectWithString:str
                                               error:&error];
    
	if (error != nil) {
        [self requestEndWithError:error response:str];
        return;        
	}

    
    if ([respondingObject isKindOfClass:[NSError class]] == NO) {
        id error_code_obj = [respondingObject valueForKey:@"status"];
        if ([error_code_obj isKindOfClass:[NSNumber class]]) {
            int error_code = [error_code_obj intValue];
            id result = [respondingObject objectForKey:@"result"];
            if (error_code == 200) {            
                if (_delegate) {
                    [_delegate performSelector:_callback withObject:self withObject:result];
//                    // the user has been authenticated so the sGCM must take care of association
//                    if([GameCenterManager isGameCenterAvailable]){
//                        [[GameCenterManager sharedGameCenterManager] checkAssociationAndUserLoggedIntoGG:YES];
//                    }
                }

            } else {
                //KONG: check for unauthorized error_code
                error = [NSError errorWithDomain:@"Error" code:error_code userInfo:
                         [NSDictionary dictionaryWithObject:result                                                          
                                                     forKey:NSLocalizedDescriptionKey]];
                [self requestEndWithError:error response:respondingObject];
            }
            
        } else {
            if (_delegate) {
                [_delegate performSelector:_callback withObject:self withObject:respondingObject];
            }
        }
	}
    
}

#pragma mark -
#pragma mark NSURLConnection delegate methods

- (void)connection:(NSURLConnection *)connection didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge {
    DLog();
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    DLog("%@", response);
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    [_receivedData appendData:data];
    DLog();
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error{
    DLog();
    [self requestEndWithError:error response:nil];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    DLog();
    [self didFinishLoadingData:_receivedData];
    [self release];
}

#pragma mark -
#pragma mark NXOAuth2Connection delegate methods

- (void)oauthConnection:(NXOAuth2Connection *)connection didFailWithError:(NSError *)error {
    [self requestEndWithError:error response:nil];
}

- (void)oauthConnection:(NXOAuth2Connection *)connection didReceiveData:(NSData *)data {
    [_receivedData appendData:data];
}

- (void)oauthConnection:(NXOAuth2Connection *)connection didFinishWithData:(NSData *)data {
    [self didFinishLoadingData:_receivedData];
//    [self release];
}

@end
