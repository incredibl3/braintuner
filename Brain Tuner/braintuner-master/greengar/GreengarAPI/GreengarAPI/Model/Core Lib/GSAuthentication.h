//
//  GSAuthentication.h
//  Opinions
//
//  Created by Cong Vo on 4/5/11.
//  Copyright 2011 Greengar Studios LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NXOAuth2.h"
#import <Facebook-iOS-SDK/FacebookSDK/FacebookSDK.h>
#import "SA_OAuthTwitterEngine.h"
#import "SA_OAuthTwitterController.h"
#import "Constants.h"

static NSString * const GSAuthErrorSessionInvalid = @"SessionInvalid";
 
@class GSAuthentication;

@protocol GSAuthenticationDelegate <NSObject> 
@optional

- (void)authentication:(GSAuthentication *)auth succeeded:(NXOAuth2Client *)oauthClient;
- (void)authentication:(GSAuthentication *)auth failed:(NSError *)error;

- (void)registration:(GSAuthentication *)auth successed:(NXOAuth2Client *)oauthClient;
- (void)registration:(GSAuthentication *)auth failed:(NSError *)error;

@end


typedef enum {
    ActionNone,
    
    ActionAuthenticationUsername,
    ActionAuthenticationFacebook,
    
    ActionRegistrationUsername,
    ActionRegistrationFacebook,
} GSAuthenticationAction;

@interface GSAuthentication : NSObject <NXOAuth2ClientDelegate, FBSessionDelegate, FBRequestDelegate, SA_OAuthTwitterControllerDelegate> {
    id <GSAuthenticationDelegate>   _delegate; // weak retain
    
    NSString                    * _username;
    NSString                    * _password;
    
    Facebook                    * facebook;
    NSArray                     * facebookPermissions;
    FacebookAPIAction             currentFBAction;
    
    
    // memory
    NSString                    * fbEmail;    
    NSString                    * fbUsername;
    NSString                    * fbFirstName;
    NSString                    * fbLastName;
    NSString                    * fbAvatarUrl;    
    NSString                    * fbUid;
    
    NSString                    * twID;
    NSString                    * twTimestamp;
    NSString                    * twSignature;
    
    SA_OAuthTwitterEngine       * twitterEngine;
    
    GSAuthenticationAction _action;
}

@property (nonatomic, assign) id <GSAuthenticationDelegate> delegate;

@property (nonatomic, retain) Facebook                    * facebook;
@property (nonatomic, retain) NSArray                     * facebookPermissions;
@property (nonatomic, retain) SA_OAuthTwitterEngine       * twitterEngine;

+ (BOOL)isAuthenticated;
- (void)authenticateWithUsername:(NSString *)username
                        password:(NSString *)password
                        delegate:(id <GSAuthenticationDelegate>)delegate;
- (void)authenticateWithGameCenter:(id<GSAuthenticationDelegate>)delegate;
- (void)authenticateByFacebook:(id <GSAuthenticationDelegate>)delegate;
- (void)authenticateWithTwitter:(id <GSAuthenticationDelegate>)delegate viewController:(UIViewController *)viewController;
- (void)authenticateWithTwitterIOS5:(id <GSAuthenticationDelegate>)delegate withUsername:(NSString *)username;

- (BOOL)isIOS5OrHigher;

- (void)logoutFromFacebook:(id<GSAuthenticationDelegate>)delegate;
- (void)logoutFromTwitter:(id<GSAuthenticationDelegate>)delegate;
- (void)logoutFromGameCenter:(id<GSAuthenticationDelegate>)delegate;

+ (NXOAuth2Client *)oauthClient;


#pragma mark Registration

- (void)registerWithEmail:(NSString *)email 
                firstName:(NSString *)firstname 
                 lastName:(NSString *)lastname
                 delegate:(id <GSAuthenticationDelegate>)delegate;
- (void)registerWithEmail:(NSString *)email 
                firstName:(NSString *)firstname 
                 lastName:(NSString *)lastname
                 password:(NSString *)password
                 delegate:(id <GSAuthenticationDelegate>)delegate;
- (void)registerUsingGameCenterID:(id <GSAuthenticationDelegate>)delegate;
- (void)registerByFacebook:(id <GSAuthenticationDelegate>)delegate;


+ (void)logout;

#pragma mark GameCenterManager delegate
- (void)registeredUsingGameCenterID:(NSString *)playerID 
                           andAlias:(NSString *)alias; // Game Center

- (void)authenticatedWithGameCenterID:(NSString *)playerID 
                             andAlias:(NSString *)alias; // Game Center

@end
