//
//  GSAuthentication.m
//  Opinions
//
//  Created by Cong Vo on 4/5/11.
//  Copyright 2011 Greengar Studios LLC. All rights reserved.
//

#import "GSAuthentication.h"
#import <QuartzCore/QuartzCore.h>
#import <CommonCrypto/CommonHMAC.h>
#import "GSRequest.h"
#import "FBConnect.h"
#import "GameCenterManager.h"
#import "ASIHTTPRequest.h"

@interface GSAuthentication ()

@property (nonatomic, retain) NSString *username, *password;
@property (nonatomic, retain) NSString *fbEmail, *fbUsername, *fbFirstName, *fbLastName, *fbAvatarUrl, *fbUid;


- (void)failedWithErrorDescription:(NSString *)errorString;
- (void)failedWithError:(NSError *)error;

@end

static NXOAuth2Client *oauth2Client;
//static GSAuthentication *authentication;

@implementation GSAuthentication
@synthesize delegate = _delegate,
            username = _username, 
            password = _password;
            
@synthesize facebook, facebookPermissions, fbEmail, fbUsername, fbFirstName, fbLastName, fbAvatarUrl, fbUid;
@synthesize twitterEngine;

+ (NXOAuth2Client *)oauthClient {
    if (oauth2Client == nil) {
#if DEBUG        
        NSAssert([GSAppInfo clientID] != nil, @"ERROR: please using [GSAppInfo setClientID:name] to set App ID");
#endif        
        
        oauth2Client = [[NXOAuth2Client alloc] initWithClientID:[GSAppInfo clientID]
                                                   clientSecret:[GSAppInfo clientSecret]
                                                   authorizeURL:[NSURL URLWithString:kGreengarEndpointLoginAuthorize]
                                                       tokenURL:[NSURL URLWithString:kGreengarEndpointLoginToken]
                                                       delegate:nil];
    }    
    return oauth2Client;
}

- (id)init {
    self = [super init];
    if (self) {
        [[[self class] oauthClient] setDelegate:self];

        // GameCenterManager is in Greengar.m
    }
    return self;
}

//+ (GSAuthentication *)authenticationWithDelegate:(id <GSAuthenticationDelegate>)delegate {
//    if (authentication == nil) {
//        authentication = [[GSAuthentication alloc] initWithDelegate:delegate];
//    }
//    [authentication setDelegate:delegate];
//    return authentication;
//}

- (void)dealloc {
    oauth2Client.delegate = nil;
    [GameCenterManager sharedGameCenterManager].authenticationHandler = nil;
    //[GameCenterManager dealloc];    
    [_username release];
    [_password release];
    [facebook release];
    [super dealloc];
}

- (void)startAction:(GSAuthenticationAction)action {
    _action = action;
}

- (void)endAction {
    _action = ActionNone;
}

#pragma mark Authentication

+ (BOOL)isAuthenticated {
    [[self oauthClient] setPersistent:YES];
    return ([[self oauthClient] accessToken] != nil);
}

- (void)authenticateWithUsername:(NSString *)username
                        password:(NSString *)password
                        delegate:(id <GSAuthenticationDelegate>)delegate {
    DLog();
    self.delegate = delegate;
    [[self class] logout];
    [self startAction:ActionAuthenticationUsername];
    self.username = username;
    self.password = password;
    [oauth2Client requestAccess];
}


#pragma mark Callback

- (void)failedWithErrorDescription:(NSString *)errorString {
    NSError *error = nil;
    if (errorString) {
        error = [NSError errorWithDomain:nil
                                    code:0
                                userInfo:[NSDictionary dictionaryWithObject:errorString
                                                                     forKey:NSLocalizedDescriptionKey]];
    }
    [self failedWithError:error];
}

- (void)failedWithError:(NSError *)error {
    
    if ([GSAuthentication isAuthenticated]) {
        [[NSNotificationCenter defaultCenter] postNotificationName:GSAuthErrorSessionInvalid object:nil];
    } else {
        if ([_delegate respondsToSelector:@selector(authentication:failed:)]) {
            [_delegate authentication:self failed:error];
        }
    }
}


#pragma mark OAuth2 Callbacks

- (void)oauthClientNeedsAuthentication:(NXOAuth2Client *)aClient {
    DLog();
    
//    if (aClient != oauth2Client) {
//
//    }
    
    NSAssert(aClient == oauth2Client, @"Not equal", @"equal");
    
//    [aClient.accessToken removeFromDefaultKeychainWithServiceProviderName:kGreengarDomain];
	// user credentials flow
	[oauth2Client authenticateWithUsername:_username password:_password];
	// you probably don't yet have username & password.
	// if so, open a view to query them from the user & call this method with the results asynchronously.
    
    // this is your redirect url. register it with your app
	//NSURL *authorizationURL = [oauth2Client authorizationURLWithRedirectURL:[NSURL URLWithString:@"opinions://success"]];
	//[[UIApplication sharedApplication] openURL:authorizationURL];	// this line quits the application or puts it to the background, be prepared
}

- (void)oauthClientDidGetAccessToken:(NXOAuth2Client *)client {
    NSAssert(client == oauth2Client, @"Not equal", @"equal");
    
    DLog(@"oauthClientDidGetAccessToken: %@", client.accessToken);
    if ([client.accessToken hasExpired]) {
        DLog(@"AccessToken expired: %@", client.accessToken);
                
        // TODO: specify for app
        [client.accessToken removeFromDefaultKeychainWithServiceProviderName:
//         [NSString stringWithFormat:@"%@-%@", kGreengarDomain, [GSRequest appID]]];
         kGreengarDomain]; 
        
        [client refreshAccessToken];
        
        return;
    }
    
    if (_action == ActionAuthenticationUsername) {
        //KONG: callback
        if ([_delegate respondsToSelector:@selector(authentication:succeeded:)]) {
            [_delegate authentication:self succeeded:client];
        }        
    }
}

- (void)oauthClientDidLoseAccessToken:(NXOAuth2Client *)client {
    DLog(@"oauthClientDidLoseAccessToken");
}

- (void)oauthClient:(NXOAuth2Client *)client didFailToGetAccessTokenWithError:(NSError *)error {
    NSAssert(client == oauth2Client, @"Not equal", @"equal");
    [self failedWithError:error];
}

#pragma mark Utilities

// From: http://www.cocoadev.com/index.pl?BaseSixtyFour

- (NSString*)base64forData:(NSData*)theData {
	
	const uint8_t* input = (const uint8_t*)[theData bytes];
	NSInteger length = [theData length];
	
    static char table[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
	
    NSMutableData* data = [NSMutableData dataWithLength:((length + 2) / 3) * 4];
    uint8_t* output = (uint8_t*)data.mutableBytes;
	
	NSInteger i;
    for (i=0; i < length; i += 3) {
        NSInteger value = 0;
		NSInteger j;
        for (j = i; j < (i + 3); j++) {
            value <<= 8;
			
            if (j < length) {
                value |= (0xFF & input[j]);
            }
        }
		
        NSInteger theIndex = (i / 3) * 4;
        output[theIndex + 0] =                    table[(value >> 18) & 0x3F];
        output[theIndex + 1] =                    table[(value >> 12) & 0x3F];
        output[theIndex + 2] = (i + 1) < length ? table[(value >> 6)  & 0x3F] : '=';
        output[theIndex + 3] = (i + 2) < length ? table[(value >> 0)  & 0x3F] : '=';
    }
	
    return [[[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding] autorelease];
}

- (BOOL) isIOS5OrHigher {
    NSString *reqSysVer = @"5.0";
    NSString *currSysVer = [[UIDevice currentDevice] systemVersion];
    if ([currSysVer compare:reqSysVer options:NSNumericSearch] != NSOrderedAscending) {
        return YES;
    }
    return NO;
}

#pragma mark Facebook Authentication

// the process of signing in with Facebook account is as following:
// 1. login with Facebook
// 2. get Facebook token
// 3. get current timestamp
// 4. use Facebook token to query Facebook id, email
// 5. compose h = hmac((Facebook id, email, timestamp), secret salt)
// 6. send to Opinions server in a POST request: Facebook id, email, timestamp, h
// 7. Opinions server checks provided info and return token if succeed

- (void)authenticateByFacebook:(id <GSAuthenticationDelegate>)delegate {
    DLog();
    self.delegate = delegate;
    [self startAction:ActionAuthenticationFacebook];
    
    facebook.sessionDelegate = self;
    [facebook authorize:facebookPermissions];
    
    currentFBAction = FacebookAPI_Unset;
}

- (void)logoutFromFacebook:(id<GSAuthenticationDelegate>)delegate {
    self.delegate = delegate;
    [facebook logout:self];
}


- (NSString *)urlEncodeValue:(NSString *)str {
	NSString *result = (NSString *) CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault, (CFStringRef)str, NULL, CFSTR("!*'();:@&=+$,/?%#[]"), kCFStringEncodingUTF8);
	return [result autorelease];
}

- (void)fbAuthenticationRequest:(FBRequest*)request didLoad:(id)result {
	if ([result isKindOfClass:[NSArray class]]) {
        result = [result objectAtIndex:0];
    }
    
	if (currentFBAction == FacebookAPI_Me) {
		
        self.fbUid = [result valueForKey:@"id"];
        self.fbUsername = [result valueForKey:@"username"];
        self.fbFirstName = [result valueForKey:@"first_name"];
        self.fbLastName = [result valueForKey:@"last_name"];
        self.fbEmail = [result valueForKey:@"email"];
        
        currentFBAction = FacebookAPI_Avatar;
        [facebook requestWithGraphPath:@"me/picture" andDelegate:self];
        
    } else if (currentFBAction == FacebookAPI_Avatar) {
        
        //NSLog(@"%@", result);
        // TODO: facebook returns binary array instead of url
        fbAvatarUrl = @"";  // must not be nil
        if (!self.fbUsername) {
            self.fbUsername = @"";
        }
        if (!self.fbFirstName) {
            self.fbFirstName = @"";
        }
        if (!self.fbLastName) {
            self.fbLastName = @"";
        }
        double currentTimeStamp = CACurrentMediaTime();
        
        NSString * sourceString = [NSString stringWithFormat:@"%@,%@,%@,%@,%@,%@,%f",
                                   fbEmail,
                                   fbUsername,
                                   fbFirstName,
                                   fbLastName,
                                   fbAvatarUrl,
                                   fbUid,
                                   currentTimeStamp];
        
        DLog(@"%@", sourceString);
        
        NSData *clearTextData = [sourceString dataUsingEncoding:NSUTF8StringEncoding];
        NSData *keyData = [kOpinionsSecretSalt dataUsingEncoding:NSUTF8StringEncoding];
        
        uint8_t digest[CC_SHA1_DIGEST_LENGTH] = {0};
        
        CCHmacContext hmacContext;
        CCHmacInit(&hmacContext, kCCHmacAlgSHA1, keyData.bytes, keyData.length);
        CCHmacUpdate(&hmacContext, clearTextData.bytes, clearTextData.length);
        CCHmacFinal(&hmacContext, digest);
        
        NSString * signature = [self base64forData:[NSData dataWithBytes:digest length:CC_SHA1_DIGEST_LENGTH]];
        
        // send POST request here
        NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                                fbEmail, @"email",
                                fbUsername, @"username",
                                fbFirstName, @"first_name",
                                fbLastName, @"last_name",
                                fbAvatarUrl, @"avatar_url",
                                fbUid, @"facebook_id",                                
                                [NSString stringWithFormat:@"%f", currentTimeStamp], @"timestamp",
                                [self urlEncodeValue:signature], @"signature",
                                nil];
        
        currentFBAction = FacebookAPI_Unset;
        [GSRequest requestWithEndPoint:kGreengarEndpointLoginWithFacebook
                                          params:params
                                        delegate:self
                                      callback:@selector(greengar:authenticatedByFacebook:)];
    }
}

- (void)greengar:(GSRequest *)request authenticatedByFacebook:(id)result {
    if ([result isKindOfClass:[NSDictionary class]]) {
        NSDictionary * tokenDict = result;
        NSDate *expiryDate = nil;
        expiryDate = [NSDate dateWithTimeIntervalSinceNow:[[tokenDict valueForKey:@"expires_in"] integerValue]];
        
        NXOAuth2AccessToken * accessToken = [[[NXOAuth2AccessToken alloc] 
                                             initWithAccessToken:[tokenDict valueForKey:@"access_token"]
                                             refreshToken:[tokenDict valueForKey:@"refresh_token"] expiresAt:expiryDate] autorelease];        
        
        [[[self class] oauthClient] setAccessToken:accessToken];
        
        //KONG: callback
        if ([_delegate respondsToSelector:@selector(authentication:succeeded:)]) {
            [_delegate authentication:self succeeded:[[self class] oauthClient]];
        }
	} else {
        if ([_delegate respondsToSelector:@selector(registration:failed:)]) {
            [_delegate registration:self failed:result];
        }   
        if ([_delegate respondsToSelector:@selector(authentication:failed:)]) {
            [_delegate authentication:self failed:result];
        }
    }
    [self endAction];
}

// the process of signing in with Facebook account is as following:
// 1. login with Twitter
// 2. get Rwitter token
// 3. get current timestamp
// 4. use Twitter token to query Twitter id, email
// 5. compose h = hmac((Facebook id, email, timestamp), secret salt)
// 6. send to Opinions server in a POST request: Twitter id, email, timestamp, h
// 7. Opinions server checks provided info and return token if succeed

- (void)authenticateWithTwitter:(id <GSAuthenticationDelegate>)delegate viewController:(UIViewController *)viewController {
    self.delegate = delegate;
    
    UIViewController * controller = [SA_OAuthTwitterController controllerToEnterCredentialsWithTwitterEngine:twitterEngine delegate: self];
	
	if (controller) {
        
		[viewController presentModalViewController:controller animated:YES];
		// we wil send status update in delegate method authenticatedWithUsername , after user authorization
	} else {
        [self.delegate authentication:self succeeded:[[self class] oauthClient]];
        //[self performSelector:@selector(postInAppTwitter) withObject:nil afterDelay:1.0];
	}
}

- (void) authenticateWithTwitterIOS5:(id <GSAuthenticationDelegate>)delegate withUsername:(NSString *)username {
    self.delegate = delegate;
    // TODO: get Twitter Id from username and register with Greengar
    /*
    NSLog(@"======---------------- Twitter test username: %@", username);
    
    NSURL *url = [NSURL URLWithString:[[NSString alloc] initWithFormat:@"http://api.twitter.com/1/users/show.json?screen_name=%@", username] ]; 
    ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url]; 
    [request setDelegate:self]; 
    [request setDidFinishSelector:@selector(requestTwitterIOS5Done:)];
    [request setDidFailSelector:@selector(requestTwitterIOS5WentWrong:)];
    [request startAsynchronous];
     */
    
    
    DLog(@"Twitter authenicated with account name %@", username);
    
    double currentTimeStamp = CACurrentMediaTime();
    
    NSString * sourceString = [NSString stringWithFormat:@"%@,%f",
                               username,
                               currentTimeStamp];
    
    DLog(@"%@", sourceString);
    
    NSData *clearTextData = [sourceString dataUsingEncoding:NSUTF8StringEncoding];
    NSData *keyData = [kOpinionsSecretSalt dataUsingEncoding:NSUTF8StringEncoding];
    
    uint8_t digest[CC_SHA1_DIGEST_LENGTH] = {0};
    
    CCHmacContext hmacContext;
    CCHmacInit(&hmacContext, kCCHmacAlgSHA1, keyData.bytes, keyData.length);
    CCHmacUpdate(&hmacContext, clearTextData.bytes, clearTextData.length);
    CCHmacFinal(&hmacContext, digest);
    
    NSString * signature = [self base64forData:[NSData dataWithBytes:digest length:CC_SHA1_DIGEST_LENGTH]];
    
    //NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
      //                      username, @"screen_name",
        //                    nil];
    
    //    [GSRequest requestWithEndPoint:kGreengarEndpointLoginWithTwitter    
    /*
     [GSRequest requestWithEndPoint:@"http://api.twitter.com/1/users/show.json"
     params:params
     delegate:self
     callback:@selector(greengar:getTwitterIDByScreenName:)];
     */
    
    twID = [[NSString alloc] initWithFormat:@"%@", username];
    twTimestamp = [[NSString alloc] initWithFormat:@"%f", currentTimeStamp];
    twSignature = [[NSString alloc] initWithFormat:@"%@", signature];
    
    NSURL *url = [NSURL URLWithString:[[NSString alloc] initWithFormat:@"http://api.twitter.com/1/users/show.json?screen_name=%@", username] ]; 
    ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url]; 
    [request setDelegate:self]; 
    [request setDidFinishSelector:@selector(requestTwitterIOS5Done:)];
    [request setDidFailSelector:@selector(requestTwitterIOS5WentWrong:)];
    [request startAsynchronous];
}

- (void)requestTwitterIOS5Done:(ASIHTTPRequest *)request
{
    NSLog(@"twitter ok: %@", [request responseString]);
    NSString *responseString = [request responseString];
    NSDictionary *responseDict = [responseString JSONValue];
    NSString *id_str = [responseDict objectForKey:@"id_str"];
    
    NSString * _email = [NSString stringWithFormat:@"twitter.%@@greengarstudios.com",id_str];
    
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            _email, @"email",
                            twID, @"twitter_id",
                            [NSString stringWithFormat:@"%@", twTimestamp], @"timestamp",
                            [self urlEncodeValue:twSignature], @"signature",
                            nil];
    
    
    [GSRequest requestWithEndPoint:kGreengarEndpointRegisterWithTwitter
                            params:params
                          delegate:self
                          callback:@selector(greengar:authenticatedByTwitter:)];
}

- (void)requestTwitterIOS5WentWrong:(ASIHTTPRequest *)request
{
    NSError *error = [request error];
    NSLog(@"error error error: %@", error);
}


- (void) logoutFromTwitter:(id<GSAuthenticationDelegate>)delegate {
    
}

//=============================================================================================================================
#pragma mark SA_OAuthTwitterEngineDelegate
- (void) storeCachedTwitterOAuthData: (NSString *) data forUsername: (NSString *) username {
	NSUserDefaults			*defaults = [NSUserDefaults standardUserDefaults];
	[defaults setObject: data forKey: @"authData"];
    [defaults setObject: username forKey: @"twitterUsername"];
	[defaults synchronize];
}

- (NSString *) cachedTwitterOAuthDataForUsername: (NSString *) username {
	return [[NSUserDefaults standardUserDefaults] objectForKey: @"authData"];
}

//=============================================================================================================================
#pragma mark SA_OAuthTwitterControllerDelegate
- (void) OAuthTwitterController: (SA_OAuthTwitterController *) controller authenticatedWithUsername: (NSString *) username {
	DLog(@"Twitter authenicated with account name %@", username);
    
    double currentTimeStamp = CACurrentMediaTime();
    
    NSString * sourceString = [NSString stringWithFormat:@"%@,%f",
                               username,
                               currentTimeStamp];
    
    DLog(@"%@", sourceString);
    
    NSData *clearTextData = [sourceString dataUsingEncoding:NSUTF8StringEncoding];
    NSData *keyData = [kOpinionsSecretSalt dataUsingEncoding:NSUTF8StringEncoding];
    
    uint8_t digest[CC_SHA1_DIGEST_LENGTH] = {0};
    
    CCHmacContext hmacContext;
    CCHmacInit(&hmacContext, kCCHmacAlgSHA1, keyData.bytes, keyData.length);
    CCHmacUpdate(&hmacContext, clearTextData.bytes, clearTextData.length);
    CCHmacFinal(&hmacContext, digest);
    
    NSString * signature = [self base64forData:[NSData dataWithBytes:digest length:CC_SHA1_DIGEST_LENGTH]];
    
//    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
//                            username, @"screen_name",
//                            nil];
    
    //    [GSRequest requestWithEndPoint:kGreengarEndpointLoginWithTwitter    
    /*
    [GSRequest requestWithEndPoint:@"http://api.twitter.com/1/users/show.json"
                            params:params
                          delegate:self
                          callback:@selector(greengar:getTwitterIDByScreenName:)];
    */
    
    twID = [[NSString alloc] initWithFormat:@"%@", username];
    twTimestamp = [[NSString alloc] initWithFormat:@"%f", currentTimeStamp];
    twSignature = [[NSString alloc] initWithFormat:@"%@", signature];
    
    NSURL *url = [NSURL URLWithString:[[NSString alloc] initWithFormat:@"http://api.twitter.com/1/users/show.json?screen_name=%@", username] ]; 
    ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url]; 
    [request setDelegate:self]; 
    [request startAsynchronous];
    
    /*
    NSString * _email = [[NSString alloc] initWithFormat:@"%@.greengar@greengarstudios.com",username];
    
    // send POST request here
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            _email, @"email",
                            username, @"twitter_id",
                            [NSString stringWithFormat:@"%f", currentTimeStamp], @"timestamp",
                            [self urlEncodeValue:signature], @"signature",
                            nil];
//    [GSRequest requestWithEndPoint:kGreengarEndpointLoginWithTwitter    
    [GSRequest requestWithEndPoint:kGreengarEndpointRegisterWithTwitter
                            params:params
                          delegate:self
                          callback:@selector(greengar:authenticatedByTwitter:)];
     */
}

- (void)requestFinished:(ASIHTTPRequest *)request { // do something with [request responseString] 
    NSLog(@"twitter ok: %@", [request responseString]);
    NSString *responseString = [request responseString];
    NSDictionary *responseDict = [responseString JSONValue];
    NSString *id_str = [responseDict objectForKey:@"id_str"];
    
    NSString * _email = [[NSString alloc] initWithFormat:@"twitter.%@@greengarstudios.com",id_str];
    
    
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            _email, @"email",
                            twID, @"twitter_id",
                            [NSString stringWithFormat:@"%@", twTimestamp], @"timestamp",
                            [self urlEncodeValue:twSignature], @"signature",
                            nil];

    
    [GSRequest requestWithEndPoint:kGreengarEndpointRegisterWithTwitter
                            params:params
                          delegate:self
                          callback:@selector(greengar:authenticatedByTwitter:)];
    
} 

- (void)requestFailed:(ASIHTTPRequest *)request { // do something with [request error]
    NSLog(@"twitter failed: %@", [request error]);
}

/* Called when the server responds. If it doesn't work, silently create a Greengar account for the user.
 This silent account will only be accessible through game center, so they won't need the password, 
 but when authenticated by Game Center, they will be logged in */ 
- (void)greengar:(GSRequest *)request authenticatedByTwitter:(id)result {
    if ([result isKindOfClass:[NSDictionary class]]) {
        NSDictionary * tokenDict = result;
        NSDate *expiryDate = nil;
        expiryDate = [NSDate dateWithTimeIntervalSinceNow:[[tokenDict valueForKey:@"expires_in"] integerValue]];
        
        NXOAuth2AccessToken * accessToken = [[[NXOAuth2AccessToken alloc] 
                                              initWithAccessToken:[tokenDict valueForKey:@"access_token"]
                                              refreshToken:[tokenDict valueForKey:@"refresh_token"] expiresAt:expiryDate] autorelease];        
        
        [[[self class] oauthClient] setAccessToken:accessToken];
        
        //KONG: callback
        if ([_delegate respondsToSelector:@selector(authentication:succeeded:)]) {
            NSLog(@"hector hector hector");
            [_delegate authentication:self succeeded:[[self class] oauthClient]];
        }
    } else {
        // login using Twitter credentials failed, so silently create a Greengar account
        //[self registerUsingGameCenter];
        if ([_delegate respondsToSelector:@selector(registration:failed:)]) {
            [_delegate registration:self failed:result];
        }   
        if ([_delegate respondsToSelector:@selector(authentication:failed:)]) {
            [_delegate authentication:self failed:result];
        }
    }
    [self endAction];
}

- (void) OAuthTwitterControllerFailed: (SA_OAuthTwitterController *) controller {
    DLog(@"Twitter authentication Failed.");
    [self.delegate authentication:self failed:nil];
}

- (void) OAuthTwitterControllerCanceled: (SA_OAuthTwitterController *) controller {
	DLog(@"Twitter authentication Canceled.");
    [self.delegate authentication:self failed:nil];
}


#pragma mark Registration

- (void)registerWithEmail:(NSString *)email 
                firstName:(NSString *)firstname 
                 lastName:(NSString *)lastname 
                 delegate:(id <GSAuthenticationDelegate>)delegate {
    self.delegate = delegate;
    [self startAction:ActionRegistrationUsername];
    [[self class] logout];
    
    double currentTimeStamp = CACurrentMediaTime();
    
    NSString * sourceString = [NSString stringWithFormat:@"%@,%@,%@,%@,%@,%@,%f",
                               email,
                               @"",
                               firstname,
                               lastname,
                               @"",
                               @"",
                               currentTimeStamp];
    
    NSData *clearTextData = [sourceString dataUsingEncoding:NSUTF8StringEncoding];
    NSData *keyData = [kOpinionsSecretSalt dataUsingEncoding:NSUTF8StringEncoding];
    
    uint8_t digest[CC_SHA1_DIGEST_LENGTH] = {0};
    
    CCHmacContext hmacContext;
    CCHmacInit(&hmacContext, kCCHmacAlgSHA1, keyData.bytes, keyData.length);
    CCHmacUpdate(&hmacContext, clearTextData.bytes, clearTextData.length);
    CCHmacFinal(&hmacContext, digest);
    
    NSString * signature = [self base64forData:[NSData dataWithBytes:digest length:CC_SHA1_DIGEST_LENGTH]];
    
    // send POST request here
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            email, @"email",
                            @"", @"username",
                            firstname, @"first_name",
                            lastname, @"last_name",
                            @"", @"avatar_url",
                            @"", @"facebook_id",                                
                            [NSString stringWithFormat:@"%f", currentTimeStamp], @"timestamp",
                            signature, @"signature",
                            nil];
    
    [GSRequest requestWithEndPoint:kGreengarEndpointRegister
                                      params:params
                                    delegate:self
                                  callback:@selector(greengar:finishedRegistration:)];
}

- (void)registerWithEmail:(NSString *)email 
                firstName:(NSString *)firstname 
                 lastName:(NSString *)lastname
                 password:(NSString *)password
                 delegate:(id <GSAuthenticationDelegate>)delegate {
    self.delegate = delegate;
    [self startAction:ActionRegistrationUsername];
    [[self class] logout];
    
    double currentTimeStamp = CACurrentMediaTime();
    
    NSString * sourceString = [NSString stringWithFormat:@"%@,%@,%@,%@,%@,%@,%f",
                               email,
                               @"",
                               firstname,
                               lastname,
                               @"",
                               @"",
                               currentTimeStamp];
    
    NSData *clearTextData = [sourceString dataUsingEncoding:NSUTF8StringEncoding];
    NSData *keyData = [kOpinionsSecretSalt dataUsingEncoding:NSUTF8StringEncoding];
    
    uint8_t digest[CC_SHA1_DIGEST_LENGTH] = {0};
    
    CCHmacContext hmacContext;
    CCHmacInit(&hmacContext, kCCHmacAlgSHA1, keyData.bytes, keyData.length);
    CCHmacUpdate(&hmacContext, clearTextData.bytes, clearTextData.length);
    CCHmacFinal(&hmacContext, digest);
    
    NSString * signature = [self base64forData:[NSData dataWithBytes:digest length:CC_SHA1_DIGEST_LENGTH]];
    
    // send POST request here
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            email, @"email",
                            password, @"password",
                            @"", @"username",
                            firstname, @"first_name",
                            lastname, @"last_name",
                            @"", @"avatar_url",
                            @"", @"facebook_id",
                            [NSString stringWithFormat:@"%f", currentTimeStamp], @"timestamp",
                            signature, @"signature",
                            nil];
    
    [GSRequest requestWithEndPoint:kGreengarEndpointRegister
                            params:params
                          delegate:self
                          callback:@selector(greengar:finishedRegistration:)];
}

- (void)greengar:(GSRequest *)request finishedRegistration:(id)result {
    
	if ([result isKindOfClass:[NSDictionary class]]) {
        NSDictionary * tokenDict = result;
        NSDate *expiryDate = nil;
        expiryDate = [NSDate dateWithTimeIntervalSinceNow:[[tokenDict valueForKey:@"expires_in"] integerValue]];
        
        NXOAuth2AccessToken * accessToken = [[[NXOAuth2AccessToken alloc] 
                                            initWithAccessToken:[tokenDict valueForKey:@"access_token"]
                                            refreshToken:[tokenDict valueForKey:@"refresh_token"] expiresAt:expiryDate] autorelease];        
        
        [[[self class] oauthClient] setAccessToken:accessToken];
        
        if ([_delegate respondsToSelector:@selector(registration:successed:)]) {
            [_delegate registration:self successed:[[self class] oauthClient]];
        }
	} else {
        if ([_delegate respondsToSelector:@selector(registration:failed:)]) {
            [_delegate registration:self failed:result];
        }        
    }
	[self endAction];
}

#pragma mark Facebook Registration

// the process of signing up with Facebook account is as following:
// 1. login with Facebook
// 2. get Facebook token
// 3. get current timestamp
// 4. use Facebook token to query Facebook id, first name, last name, email and avatar
// 5. compute h = hmac( (Facebook id, first name, last name, email and avatar, timestamp), secret salt)
// 6. send to Opinions server in a POST request: Facebook id, first name, last name, email and avatar, timestamp, h
// 7. Opinions server verifies timestamp and hash
// 8. Opinions server creates a Opinions account based on provided info
// 9. Opinions server returns result to client

- (void)registerByFacebook:(id <GSAuthenticationDelegate>)delegate {
    self.delegate = delegate;
    [self startAction:ActionRegistrationFacebook];

    facebook.sessionDelegate = self;
    [facebook authorize:facebookPermissions];
}

- (void)storeAuthData:(NSString *)accessToken expiresAt:(NSDate *)expiresAt {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:accessToken forKey:@"FBAccessTokenKey"];
    [defaults setObject:expiresAt forKey:@"FBExpirationDateKey"];
    [defaults synchronize];
}

-(void)fbDidExtendToken:(NSString *)accessToken expiresAt:(NSDate *)expiresAt {
    DLog(@"token extended");
    [self storeAuthData:accessToken expiresAt:expiresAt];
}

-(void) fbDidLogin {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:[facebook accessToken] forKey:@"FBAccessTokenKey"];
    [defaults setObject:[facebook expirationDate] forKey:@"FBExpirationDateKey"];
    [defaults synchronize];

    // use both for registration and authentication
	DLog(@"facebook did login: requesting uid");
    currentFBAction = FacebookAPI_Me;
	[facebook requestWithGraphPath:@"me" andDelegate:self];
    
    // notify delegate
    if ([_delegate respondsToSelector:@selector(fbDidLogin)]) {
        [(id <FBSessionDelegate>) _delegate fbDidLogin];
    }
    
//    [UIAppDelegate fbDidLogin];
}

- (void)fbDidNotLogin:(BOOL)cancelled {
	DLog(@"facebook did not login: %d", cancelled);
    if ([_delegate respondsToSelector:@selector(authentication:failed:)]) {
        [_delegate authentication:self failed:nil];
    }
//    [UIAppDelegate fbDidNotLogin:cancelled];
    [self endAction];
}

- (void) fbDidLogout {
    // Remove saved authorization information if it exists
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if ([defaults objectForKey:@"FBAccessTokenKey"]) {
        [defaults removeObjectForKey:@"FBAccessTokenKey"];
        [defaults removeObjectForKey:@"FBExpirationDateKey"];
        [defaults synchronize];
    }
    //[UIAppDelegate fbDidLogout];
}


- (void)fbRegistrationRequest:(FBRequest*)request didLoad:(id)result {
	
	if (currentFBAction == FacebookAPI_Me) {
		
		fbUid = [[result valueForKey:@"id"] retain];
        fbUsername = [[result valueForKey:@"username"] retain];
        fbFirstName = [[result valueForKey:@"first_name"] retain];
        fbLastName = [[result valueForKey:@"last_name"] retain];
        fbEmail = [[result valueForKey:@"email"] retain];
        
        currentFBAction = FacebookAPI_Avatar;
        [facebook requestWithGraphPath:@"me/picture" andDelegate:self];
        
    } else if (currentFBAction == FacebookAPI_Avatar) {
        
        //NSLog(@"%@", result);
        // TODO: facebook returns binary array instead of url
        fbAvatarUrl = @"";  // must not be nil
        
        double currentTimeStamp = CACurrentMediaTime();
        
        NSString * sourceString = [NSString stringWithFormat:@"%@,%@,%@,%@,%@,%@,%f",
                                   fbEmail,
                                   fbUsername,
                                   fbFirstName,
                                   fbLastName,
                                   fbAvatarUrl,
                                   fbUid,
                                   currentTimeStamp];
        
        DLog(@"%@", sourceString);
        
        NSData *clearTextData = [sourceString dataUsingEncoding:NSUTF8StringEncoding];
        NSData *keyData = [kOpinionsSecretSalt dataUsingEncoding:NSUTF8StringEncoding];
        
        uint8_t digest[CC_SHA1_DIGEST_LENGTH] = {0};
        
        CCHmacContext hmacContext;
        CCHmacInit(&hmacContext, kCCHmacAlgSHA1, keyData.bytes, keyData.length);
        CCHmacUpdate(&hmacContext, clearTextData.bytes, clearTextData.length);
        CCHmacFinal(&hmacContext, digest);
        
        NSString * signature = [self base64forData:[NSData dataWithBytes:digest length:CC_SHA1_DIGEST_LENGTH]];
        
        // send POST request here
        NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                                fbEmail, @"email",
                                fbUsername, @"username",
                                fbFirstName, @"first_name",
                                fbLastName, @"last_name",
                                fbAvatarUrl, @"avatar_url",
                                fbUid, @"facebook_id",                                
                                [NSString stringWithFormat:@"%f", currentTimeStamp], @"timestamp",
                                [self urlEncodeValue:signature], @"signature",
                                nil];
        
        currentFBAction = FacebookAPI_Unset;
        [GSRequest requestWithEndPoint:kGreengarEndpointRegister
                                          params:params
                                        delegate:self
                                      callback:@selector(greengar:finishedRegistration:)];
    }
}

#pragma mark Facebook callback

- (void)request:(FBRequest*)request didReceiveResponse:(NSURLResponse*)response {
//    if ([request.graphPath isEqualToString:@"me/picture"]) {
//        
//        NSHTTPURLResponse * r = (NSHTTPURLResponse *)response;
//        int statusCode = [r statusCode];
//        
//        /*! 
//         @method allHeaderFields
//         @abstract Returns a dictionary containing all the HTTP header fields
//         of the receiver.
//         @discussion By examining this header dictionary, clients can see
//         the "raw" header information which was reported to the protocol
//         implementation by the HTTP server. This may be of use to
//         sophisticated or special-purpose HTTP clients.
//         @result A dictionary containing all the HTTP header fields of the
//         receiver.
//         */
//        NSDictionary * d = [r allHeaderFields];
//        DLog(@"%d %@", statusCode, d);
//    }
}

- (void)request:(FBRequest*)request didLoad:(id)result {
    switch (_action) {
        case ActionAuthenticationFacebook:
            [self fbAuthenticationRequest:request didLoad:result];
            break;
        case ActionRegistrationFacebook:
            [self fbRegistrationRequest:request didLoad:result];
            break;
            
        default:
            break;
    }
}

+ (void)logout {
    [[[[self class] oauthClient] accessToken] removeFromDefaultKeychainWithServiceProviderName:kGreengarDomain];
    [[self oauthClient] setAccessToken:nil];
}

#pragma mark Game Center Authentication 

- (void)registerUsingGameCenterID:(id <GSAuthenticationDelegate>)delegate {
    self.delegate = delegate;
    if([GameCenterManager isGameCenterAvailable]){
        [GameCenterManager sharedGameCenterManager];
        [GameCenterManager sharedGameCenterManager].authenticationHandler = self;
        [[GameCenterManager sharedGameCenterManager] authenticateLocalUser];
    }    
}

- (void)registeredUsingGameCenterID:(NSString *)playerID 
                          andAlias:(NSString *)alias {
    return;
}

- (void)authenticateWithGameCenter:(id<GSAuthenticationDelegate>)delegate {
    self.delegate = delegate;
    if([GameCenterManager isGameCenterAvailable]){
        [GameCenterManager sharedGameCenterManager];
        [GameCenterManager sharedGameCenterManager].authenticationHandler = self;
        [[GameCenterManager sharedGameCenterManager] authenticateLocalUser];
    }
}

- (void)logoutFromGameCenter:(id<GSAuthenticationDelegate>)delegate {
    self.delegate = delegate;
}

// request to login to Greengar server with Game Center info
- (void)authenticatedWithGameCenterID:(NSString *)playerID andAlias:(NSString *)alias {

    /* IDK why or what this does
     if ([request.graphPath isEqualToString:@"me"]) {
     
     self.fbUid = [result valueForKey:@"id"];
     self.fbUsername = [result valueForKey:@"username"];
     self.fbFirstName = [result valueForKey:@"first_name"];
     self.fbLastName = [result valueForKey:@"last_name"];
     self.fbEmail = [result valueForKey:@"email"];
     [_facebook requestWithGraphPath:@"me/picture" andDelegate:self];
     } else if ([request.graphPath isEqualToString:@"me/picture"]) {
     */
    
    //NSLog(@"%@", result);
    
    double currentTimeStamp = CACurrentMediaTime();
    
    NSString * sourceString = [NSString stringWithFormat:@"%@,%@,%f",
                               playerID,
                               alias,
                               currentTimeStamp];
    
    DLog(@"%@", sourceString);
    
    NSData *clearTextData = [sourceString dataUsingEncoding:NSUTF8StringEncoding];
    NSData *keyData = [kOpinionsSecretSalt dataUsingEncoding:NSUTF8StringEncoding];
    
    uint8_t digest[CC_SHA1_DIGEST_LENGTH] = {0};
    
    CCHmacContext hmacContext;
    CCHmacInit(&hmacContext, kCCHmacAlgSHA1, keyData.bytes, keyData.length);
    CCHmacUpdate(&hmacContext, clearTextData.bytes, clearTextData.length);
    CCHmacFinal(&hmacContext, digest);
    
    NSString * signature = [self base64forData:[NSData dataWithBytes:digest length:CC_SHA1_DIGEST_LENGTH]];
    NSString * _emailGC = [[NSString alloc] initWithFormat:@"%@@greengarstudios.com", alias];
    // send POST request here
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            _emailGC, @"email",
                            playerID, @"gamecenter_id",
                            alias, @"gamecenter_alias",
                            [NSString stringWithFormat:@"%f", currentTimeStamp], @"timestamp",
                            [self urlEncodeValue:signature], @"signature",
                            nil];
    
//    [GSRequest requestWithEndPoint:kGreengarEndpointLoginWithGameCenter
    [GSRequest requestWithEndPoint:kGreengarEndpointRegisterWithGameCenter
                            params:params
                          delegate:self
                          callback:@selector(greengar:authenticatedByGameCenter:)];
}

/* Called when the server responds. If it doesn't work, silently create a Greengar account for the user.
 This silent account will only be accessible through game center, so they won't need the password, 
 but when authenticated by Game Center, they will be logged in */ 
- (void)greengar:(GSRequest *)request authenticatedByGameCenter:(id)result {
    if ([result isKindOfClass:[NSDictionary class]]) {
        NSDictionary * tokenDict = result;
        NSDate *expiryDate = nil;
        expiryDate = [NSDate dateWithTimeIntervalSinceNow:[[tokenDict valueForKey:@"expires_in"] integerValue]];
        
        NXOAuth2AccessToken * accessToken = [[[NXOAuth2AccessToken alloc] 
                                              initWithAccessToken:[tokenDict valueForKey:@"access_token"]
                                              refreshToken:[tokenDict valueForKey:@"refresh_token"] expiresAt:expiryDate] autorelease];        
        
        [[[self class] oauthClient] setAccessToken:accessToken];
        
        //KONG: callback
        if ([_delegate respondsToSelector:@selector(authentication:succeeded:)]) {
            [_delegate authentication:self succeeded:[[self class] oauthClient]];
        }
    } else {
        // login using Game Center credentials failed, so silently create a Greengar account
        //[self registerUsingGameCenter];
        if ([_delegate respondsToSelector:@selector(registration:failed:)]) {
            [_delegate registration:self failed:result];
        }   
        if ([_delegate respondsToSelector:@selector(authentication:failed:)]) {
            [_delegate authentication:self failed:result];
        }
    }
    [self endAction];
}

@end
