//
//  LBSContact.h
//  Opinions
//
//  Created by Silvercast on 3/13/11.
//  Copyright 2011 GreenGar studios. All rights reserved.
//

#import <Foundation/Foundation.h>
//#import "DDLastCheckIn.h"

@interface LBSContact : NSObject {
	NSString				* lsuid;	// location-service user id
	NSString				* lstype;	// location-service type:   GW, FB,...
	NSString				* firstName;
	NSString				* lastName;
	NSString				* avatarURLString;
	
//	DDLastCheckIn			* lastCheckIn;	
}

@property (nonatomic, retain) NSString				* lsuid;	// location-service user id
@property (nonatomic, retain) NSString				* lstype;
@property (nonatomic, retain) NSString				* firstName;
@property (nonatomic, retain) NSString				* lastName;
@property (nonatomic, retain) NSString				* avatarURLString;

//@property (nonatomic, retain) DDLastCheckIn			* lastCheckIn;

- (NSString *)name;
- (void)pullLatestLocation;

@end
