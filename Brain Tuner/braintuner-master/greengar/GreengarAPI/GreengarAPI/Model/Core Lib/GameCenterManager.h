//
//  GameCenterManager.h
//  Whiteboard
//
//  Created by Sheehan on 6/30/11.
//  Copyright 2011 GreenGar studios. All rights reserved.
//
//  The plan is to have this class handle all registration and authentication via Game Center, but
//  pass that off to the GSAuthentication class when dealing with the silently created Greengar
//  account. There is no need to handle invites because the MatchMaker is never shown, so it is
//  impossible to invite people.
//

#import <Foundation/Foundation.h>
#import <GameKit/GameKit.h>
#import "GSAuthentication.h"

/* If the game isn't recognized (Game Center isn't enabled) then the error code on attempted Game Center
    authentication is 15. This is a problem with iTunes Connect because Elliot enabled it 30/6/11. 
    Though an error is returned, the GKLocalPlayer is still authenticated, so we ignore the error.
    So far I've only used Sandbox accounts, so a regular account will need to be tried during testing. */
#define kErrorCodeNotRecognizedByGameCenter 15

@class GKPlayer;

@interface GameCenterManager : NSObject <GSAuthenticationDelegate> {
    GSAuthentication *authenticationHandler; // used to register/login/logout of silent Greengar account
    
    // Whiteboard disconnects on backgrounding/switching, so need to reauthenticate and reconnect each time
    BOOL userAuthenticatedGC; // Game Center
    BOOL userAuthenticatedGG; // Greengar
    BOOL userAuthenticatedGCGG; // 
    
    // NO until association checked. Stays yes until a logout or backgrounding, which will cause reauthentication
    BOOL haveCheckedForAssociation; // Used in case GC authenticates faster than GG
    
    BOOL isThereAssociation;
    
    // used to provide protection to a users GG account if they authenticate through GC instead of entering their password
    BOOL loggedIntoGGViaGC;
}

@property (nonatomic, assign)  GSAuthentication *authenticationHandler;
@property (nonatomic, readonly) BOOL userAuthenticatedGC;
@property (nonatomic, readonly) BOOL userAuthenticatedGG;

+ (GameCenterManager *) sharedGameCenterManager; // create/get reference to GameCenterManager singleton
+ (void) dealloc; // dealloc if it exists

+ (BOOL) isGameCenterAvailable;

// Currently only used when first created. All other authentications should be via callback.
- (void) authenticateLocalUser; 

// updates the users logged in to GG and/or GC status and if appropriate, checks for association
- (void) checkAssociationAndUserLoggedIntoGG:(BOOL)authenticatedByGG;

// GSAuthenticationDelegate authentication methods
// Called via GSAuthentication :: greengar:(GSRequest *)request authenticatedByGameCenter:(id)result
- (void) authentication:(GSAuthentication *)auth succeeded:(NXOAuth2Client *)oauthClient;
- (void) authentication:(GSAuthentication *)auth failed:(NSError *)error;


@end