//
//  LBSContact.m
//  Opinions
//
//  Created by Silvercast on 3/13/11.
//  Copyright 2011 GreenGar studios. All rights reserved.
//

#import "LBSContact.h"


@implementation LBSContact

@synthesize lsuid;
@synthesize lstype;
@synthesize firstName;
@synthesize lastName;
//@synthesize lastCheckIn;
@synthesize avatarURLString;

- (NSString *)name {
	return [NSString stringWithFormat:@"%@ %@", self.firstName, self.lastName];
}

- (void)pullLatestLocation {
}

@end
