//
//  FBContact.h
//  Opinions
//
//  Created by Silvercast on 11/19/10.
//  Copyright 2010 Greengar Studios LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LBSContact.h"

@interface FBContact : LBSContact {
    NSString        * userID;
    NSString        * facebookID;
    NSString        * friendName;
    NSString        * friendAvatarURLString;
    int             reputation;
}

@property(nonatomic, retain) NSString        * userID;
@property(nonatomic, retain) NSString        * facebookID;
@property(nonatomic, retain) NSString        * friendName;
@property(nonatomic, retain) NSString        * friendAvatarURLString;
@property(nonatomic, assign) int            reputation;

- (id)initWithFBDictionary:(NSDictionary *)fbInfo;
- (id)initWithMoreDataFBDictionary:(NSDictionary *)fbInfo;
- (id)initWithDictionary:(NSDictionary *)userInfo;

- (BOOL) isRandomUser;

@end
