//
//  GSFriends.h
//  Opinions
//
//  Created by Cong Vo on 4/21/11.
//  Copyright 2011 Greengar Studios LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GSContact.h"
#import "GSRequest.h"
@protocol GSParser;


static NSString *const kNotificationFriendsUpdated = @"kNotificationFriendsUpdated";

@interface GSFriends : NSObject {
    NSArray *_friends;
}

@property (nonatomic, retain) NSArray *friends;

//KONG: update friends list, using notification: kNotificationFriendsUpdated
- (GSRequest *)getFriends:(id)delegate callback:(SEL)callback;

- (GSRequest *)getAllTypeFriends:(id)delegate callback:(SEL)callback;

//KONG: check if a contact is in Friends list
- (BOOL)isFriend:(GSContact *)contact;

- (GSRequest*)sendFriendRequest:(GSContact *)contact delegate:(id)d callback:(SEL)c;
- (GSRequest*)cancelFriendRequest:(GSContact *)contact delegate:(id)d callback:(SEL)c;
- (GSRequest*)removeFriendRequest:(GSContact *)contact delegate:(id)d callback:(SEL)c;
- (GSRequest*)acceptRequest:(GSContact *)contact delegate:(id)d callback:(SEL)c;
- (GSRequest*)denyRequest:(GSContact *)contact delegate:(id)d callback:(SEL)c;

@end


@protocol GSParser <NSObject>

- (GSContact *)contactFromDictionary:(NSDictionary *)contactInfo;

@end

