//
//  Greengar.h
//  Opinions
//
//  Created by Cong Vo on 4/22/11.
//  Copyright 2011 Greengar Studios LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GSAuthentication.h"
#import "GSContact.h"
#import "GSFriends.h"
#import "GSFollowers.h"
#import "GSFollowings.h"

#import "GSRequest.h"


@interface Greengar : NSObject {
    GSAuthentication *_authentication;
    GSContact *_myProfile;    
    GSFriends *_friends;
    GSFollowers *_followers;
    GSFollowings *_followings;
}
@property (nonatomic, retain) GSAuthentication *authentication;
@property (nonatomic, retain) GSContact *myProfile;
@property (nonatomic, retain) GSFriends *friends;
@property (nonatomic, retain) GSFollowers *followers;
@property (nonatomic, retain) GSFollowings *followings;

- (void)getMyProfileWithDelegate:(id)delegate callback:(SEL)callback;
- (void)getProfileOf:(NSString*)userId withDelegate:(id)delegate callback:(SEL)callback;
+ (GSRequest *)requestWithEndPoint:(NSString *)endPoint
                            params:(NSDictionary *)params
                          delegate:(id)delegate
                          callback:(SEL)callback;

@end


@interface GSHelperRequest : GSRequest {
    id _appDelegate;
    SEL _appCallback;
    
    id _object;
}

@property (nonatomic, assign) id appDelegate;
@property (nonatomic, assign) SEL appCallback;
@property (nonatomic, retain) id object;

@end