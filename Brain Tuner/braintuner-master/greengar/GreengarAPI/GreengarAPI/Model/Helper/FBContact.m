//
//  FBContact.m
//  Opinions
//
//  Created by Silvercast on 11/19/10.
//  Copyright 2010 Greengar Studios LLC. All rights reserved.
//

#import "FBContact.h"

@implementation FBContact
@synthesize userID;
@synthesize facebookID;
@synthesize friendName;
@synthesize friendAvatarURLString;
@synthesize reputation;

- (void)dealloc {
    [userID release];
    [facebookID release];
    [friendName release];
    [friendAvatarURLString release];
    [super dealloc];
}

- (id)initWithFBDictionary:(NSDictionary *)fbInfo {
    self = [super init];
    if (self) {
        self.facebookID = [NSString stringWithString:[fbInfo valueForKey:@"id"]];
        //self.friendName = [NSString stringWithString:[fbInfo valueForKey:@"name"]];
        self.friendAvatarURLString = [NSString stringWithFormat:@"https://graph.facebook.com/%@/picture", self.facebookID];
        reputation = 0;
    }
    return self;
}

- (id)initWithMoreDataFBDictionary:(NSDictionary *)fbInfo {
    self = [super init];
    if (self) {
        self.facebookID = [NSString stringWithString:[fbInfo valueForKey:@"id"]];
        self.friendName = [NSString stringWithString:[fbInfo valueForKey:@"name"]];
        self.friendAvatarURLString = [NSString stringWithFormat:@"https://graph.facebook.com/%@/picture", self.facebookID];
        reputation = 0;
    }
    return self;
}

- (id)initWithDictionary:(NSDictionary *)userInfo {
    
    self = [super init];
    if (self) {
        self.userID = [NSString stringWithString:[userInfo objectForKey:@"user_id"]];
        self.facebookID = [NSString stringWithString:[userInfo objectForKey:@"facebook_id"]];
        self.friendName = [NSString stringWithString:[userInfo objectForKey:@"fullname"]];
        self.friendAvatarURLString = [NSString stringWithFormat:@"https://graph.facebook.com/%@/picture", self.facebookID];
        if ([userInfo objectForKey:@"reputation"]) {
            reputation = [[userInfo objectForKey:@"reputation"] intValue];
        }
    }
    return self;
}

- (BOOL) isRandomUser {
    return [self.facebookID isEqualToString:@"?????????"];
}

@end
