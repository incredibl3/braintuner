//
//  GSFriends.h
//  Opinions
//
//  Created by Cong Vo on 4/21/11.
//  Copyright 2011 Greengar Studios LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GSContact.h"
#import "GSRequest.h"
@protocol GSParser;


static NSString *const kNotificationFollowingsUpdated = @"kNotificationFollowingsUpdated";

@interface GSFollowings : NSObject {
    NSArray *_followings;
}

@property (nonatomic, retain) NSArray *followings;

//KONG: update friends list, using notification: kNotificationFriendsUpdated
- (GSRequest *)getFollowings:(id)delegate callback:(SEL)callback;

//KONG: check if a contact is in Friends list
- (BOOL)isFollowing:(GSContact *)contact;


- (GSRequest*)addFollowing:(GSContact *)contact delegate:(id)d callback:(SEL)c;
- (GSRequest*)removeFollowing:(GSContact *)contact delegate:(id)d callback:(SEL)c;

@end

