//
//  GSFriends.m
//  Opinions
//
//  Created by Cong Vo on 4/21/11.
//  Copyright 2011 Greengar Studios LLC. All rights reserved.
//

#import "GSFollowers.h"
#import "GSRequest.h"
#import "Greengar.h"

@implementation GSFollowers
@synthesize followers         = _followers;

- (id)init {
    self = [super init];
    if (self) {
        _followers = [[NSArray alloc] init];
    }
    return self;
}
- (void)dealloc {
    [_followers release];
    [super dealloc];
}
#pragma mark Friends

- (GSRequest *)getFollowers:(id)delegate callback:(SEL)callback {
    
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                    [NSString stringWithFormat:@"[\"%@\"]", [GSAppInfo clientID]], @"client_id",
                                   @"[\"to\"]", @"relationships", 
                                   nil];
    
    GSHelperRequest *request = (GSHelperRequest *) [GSHelperRequest requestWithEndPoint:kGreengarEndpointGetFollower
                                                                                      params:params
                                                                                    delegate:self
                                                                                    callback:@selector(request:gotFollowers:)];
    request.appDelegate = delegate;
    request.appCallback = callback;
    
    return request;
}

- (void)request:(GSHelperRequest *)request gotFollowers:(id)result {

    if ([result isKindOfClass:[NSArray class]]) {
        NSMutableArray *followers = [NSMutableArray array];
        for (int i = 0; i < [result count]; i++) {
            NSDictionary * dict = [result objectAtIndex:i];            
            GSContact * contact = [[GSAppInfo contactClass] contactFromDictionary:dict];
            [followers addObject:contact];
        }
        self.followers = followers;
        result = _followers;
	}
    
    if (request.appDelegate && [request.appDelegate respondsToSelector:request.appCallback]) {
        [request.appDelegate performSelector:request.appCallback withObject:request withObject:result];
    }
}

//KONG: check if a contact is in Friends list
- (BOOL)isFollower:(GSContact *)contact {
    
    return NO;
}


- (GSRequest*)addFollower:(GSContact *)contact delegate:(id)d callback:(SEL)c {
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                   [GSAppInfo clientID], @"client_id",
                                   contact.uid, @"f_id",
                                   nil];
    
    
    GSHelperRequest *request = (GSHelperRequest *) [GSHelperRequest requestWithEndPoint:kGreengarEndpointAddFollowing
                                                                                 params:params
                                                                               delegate:self
                                                                               callback:@selector(request:addedFollower:)];
    
    return request;
}
- (GSRequest*)removeFollower:(GSContact *)contact delegate:(id)d callback:(SEL)c {
    
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                   [GSAppInfo clientID], @"client_id",
                                   contact.uid, @"f_id",
                                   nil];
    
    
    GSHelperRequest *request = (GSHelperRequest *) [GSHelperRequest requestWithEndPoint:kGreengarEndpointRemoveFollowing
                                                                                 params:params
                                                                               delegate:self
                                                                               callback:@selector(request:removedFollower:)];
    
    request.appDelegate = d;
    request.appCallback = c;
    return request;
}

- (void)request:(GSHelperRequest *)request addedFollower:(id)result {
    
    //    if ([result isKindOfClass:[NSArray class]]) {
    //        NSMutableArray *followers = [NSMutableArray array];
    //        for (int i = 0; i < [result count]; i++) {
    //            NSDictionary * dict = [result objectAtIndex:i];            
    //            GSContact * contact = [[GSAppInfo contactClass] contactFromDictionary:dict];
    //            [followers addObject:contact];
    //        }
    //        self.followers = followers;
    //        result = _followers;
    //	}
    
    if (request.appDelegate && [request.appDelegate respondsToSelector:request.appCallback]) {
        [request.appDelegate performSelector:request.appCallback withObject:request withObject:result];
    }
}

- (void)request:(GSHelperRequest *)request removedFollower:(id)result {
    
    //    if ([result isKindOfClass:[NSArray class]]) {
    //        NSMutableArray *followers = [NSMutableArray array];
    //        for (int i = 0; i < [result count]; i++) {
    //            NSDictionary * dict = [result objectAtIndex:i];            
    //            GSContact * contact = [[GSAppInfo contactClass] contactFromDictionary:dict];
    //            [followers addObject:contact];
    //        }
    //        self.followers = followers;
    //        result = _followers;
    //	}
    
    if (request.appDelegate && [request.appDelegate respondsToSelector:request.appCallback]) {
        [request.appDelegate performSelector:request.appCallback withObject:request withObject:result];
    }
}

@end
