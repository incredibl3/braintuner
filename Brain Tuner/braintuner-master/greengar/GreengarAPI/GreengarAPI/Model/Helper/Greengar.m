//
//  Greengar.m
//  Opinions
//
//  Created by Cong Vo on 4/22/11.
//  Copyright 2011 Greengar Studios LLC. All rights reserved.
//

#import "Greengar.h"
#import "GSRequest.h"

@implementation Greengar
@synthesize authentication  = _authentication,
            myProfile       = _myProfile,
            friends         = _friends;

@synthesize followers = _followers, followings = _followings;


- (id)init {
    self = [super init];
    if (self) {
        _authentication = [[GSAuthentication alloc] init];
        _friends = [[GSFriends alloc] init];
        _followings = [[GSFollowings alloc] init];
        _followers = [[GSFollowers alloc] init];

    }
    return self;
}

- (void)dealloc {
    [_authentication release];
    [_friends release];
    [_myProfile release];
    [_followers release];
    [_followings release];
    [super dealloc];
}

+ (GSRequest *)requestWithEndPoint:(NSString *)endPoint
                            params:(NSDictionary *)params
                          delegate:(id)delegate
                          callback:(SEL)callback {
    return [GSRequest requestWithEndPoint:endPoint
                                   params:params
                                 delegate:delegate
                                 callback:callback];
}

#pragma mark My Profile

- (void)getMyProfileWithDelegate:(id)delegate callback:(SEL)callback {
    
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
							@"me", @"user_id",
							nil];
	
	GSHelperRequest *request = (GSHelperRequest *)[GSHelperRequest requestWithEndPoint:kGreengarEndpointGetUserInfo
                                                                                      params:params
                                                                                    delegate:self
                                                                                   callback:@selector(request:gotUserInfo:)];
    request.appDelegate = delegate;
    request.appCallback = callback;

}

- (void)getProfileOf:(NSString*)userId withDelegate:(id)delegate callback:(SEL)callback {
    
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
							userId, @"user_id",
							nil];
	
	GSHelperRequest *request = (GSHelperRequest *)[GSHelperRequest requestWithEndPoint:kGreengarEndpointGetUserInfo
                                                                                params:params
                                                                              delegate:self
                                                                              callback:@selector(request:gotUserInfo:)];
    request.appDelegate = delegate;
    request.appCallback = callback;
    
}

// called when user retrieved from GG servers
- (void)request:(GSHelperRequest *)request gotUserInfo:(id)result {
    
    if ([result isKindOfClass:[NSDictionary class]]) {
        // MSHEEHAN do GC stuff here
        // got the user profile, check association, and maybe request to add it
        
        result = [[GSAppInfo contactClass] contactFromDictionary:result];
        if ([result isKindOfClass:[GSContact class]]) {
            self.myProfile = result;            
        }
	}
	    
    if (request.appDelegate && [request.appDelegate respondsToSelector:request.appCallback]) {
        [request.appDelegate performSelector:request.appCallback withObject:request withObject:result];
    }
}

@end

@implementation GSHelperRequest
@synthesize appDelegate = _appDelegate,
            appCallback = _appCallback,
            object      = _object;

+ (GSHelperRequest *)requestWithURL:(NSString *)URLString 
                        HTTPMethod:(NSString *)HTTPMethod
                            params:(NSDictionary *)params
                          delegate:(id)delegate
                          callback:(SEL)callback {
    
    // memory: self-retained, will be released when connection finished
    GSHelperRequest *helper = [[GSHelperRequest alloc] initWithDelegate:delegate callback:callback];
    [helper requestWithURL:URLString HTTPMethod:HTTPMethod params:params];
    return helper;
}

- (void)dealloc {
    [_object release];
    [super dealloc];
}
@end
