//
//  GSContact.m
//  Opinions
//
//  Created by Cong Vo on 4/8/11.
//  Copyright 2011 Greengar Studios LLC. All rights reserved.
//

#import "GSContact.h"
#import "FBContact.h"

@implementation GSContact

@synthesize uid, 
            firstName, lastName, fullName,
            email,
            playerAlias,
            friendStatus,
            followingStatus;
@synthesize gender, dob;

@synthesize isAvatarCached;
@synthesize avatarURLString;
@synthesize avatar;
@synthesize associatedLBSAccounts;
@synthesize username = username;

- (id)initWithDictionary:(NSDictionary *)contactInfo {
    /*
     {"guid":"598","username":"kingkong","name":"Vo Thanh Cong","email":"kingkong@kongking.us","playerAlias":"Greenguygar"}
     */
    
    self = [super init];
    if (self) {
        
        DLog(@"# %@", [contactInfo description]);
        DLog(@"# %@", [contactInfo descriptionInStringsFileFormat]);
        
        uid = [[contactInfo objectForKey:@"guid"] retain];
        fullName = [[contactInfo objectForKey:@"name"] retain];
        email = [[contactInfo objectForKey:@"email"] retain];
        gender = [[contactInfo objectForKey:@"gender"] retain];
        dob = [[contactInfo objectForKey:@"dob"] retain];
        
         self.username = [NSString stringWithString:[contactInfo objectForKey:@"username"]];
        
        // Game Center Alias
        NSDictionary *gcInfo = [contactInfo objectForKey:@"gamecenter_id"];
        if (gcInfo) {
            // playerAlias might be nil or @""
            playerAlias = [gcInfo objectForKey:@"alias"];
        }
        
        //        if ([email isEqualToString:@"koncerz@g.co"]) {
        //            DLog();
        //        }
        
        // relationship
        NSDictionary *relationshipInfo = [contactInfo objectForKey:@"relationship"];
        if (relationshipInfo) {
            NSString *relationship = [relationshipInfo objectForKey:@"relationship"];
            if ([relationship isEqualToString:@"both"]) {
                friendStatus = GSContactFriendStatusBoth;
            } else if ([relationship isEqualToString:@"from"]) {
                friendStatus = GSContactFriendStatusFrom;
            } else if ([relationship isEqualToString:@"to"]) {
                friendStatus = GSContactFriendStatusTo;
            } else if ([relationship isEqualToString:@"none"]) {
                friendStatus = GSContactFriendStatusNone;
            }
        }
        
        relationshipInfo = [contactInfo objectForKey:@"following"];
        if (relationshipInfo) {
            NSString *relationship = [relationshipInfo objectForKey:@"relationship"];
            if ([relationship isEqualToString:@"both"]) {
                followingStatus = GSContactFollowingStatusBoth;
            } else if ([relationship isEqualToString:@"from"]) {
                followingStatus = GSContactFollowingStatusFrom;
            } else if ([relationship isEqualToString:@"to"]) {
                followingStatus = GSContactFollowingStatusTo;
            } else if ([relationship isEqualToString:@"none"]) {
                followingStatus = GSContactFollowingStatusNone;
            }
        }
        
        self.avatarURLString = [NSString stringWithString:[contactInfo objectForKey:@"icon"]];
        
        NSDictionary * fbDictionary = [contactInfo objectForKey:@"facebook_id"];
        if (fbDictionary) {
            self.avatarURLString = [NSString stringWithFormat:@"https://graph.facebook.com/%@/picture", [fbDictionary objectForKey:@"id"]];
        }
        
        self.associatedLBSAccounts = [NSMutableArray array];
        
        NSArray * associatedAccounts = [contactInfo objectForKey:@"social_ids"];
        for (NSDictionary * accountInfo in associatedAccounts) {
            NSString * lstype = [accountInfo objectForKey:@"type"];
            
            if ([lstype isEqualToString:@"Gowalla"]) {
//                GWContact * gwContact = [[GWContact alloc] init];
//                gwContact.lstype = @"Gowalla";
//                gwContact.lsuid = [accountInfo objectForKey:@"id"];
//                
//                [associatedLBSAccounts addObject:gwContact];
                
            } else if ([lstype isEqualToString:@"Facebook"]) {
                
                FBContact * fbContact = [[FBContact alloc] init];
                fbContact.lstype = @"Facebook";
                fbContact.lsuid = [accountInfo objectForKey:@"id"];
                
                [associatedLBSAccounts addObject:fbContact];
                self.avatarURLString = [NSString stringWithFormat:@"https://graph.facebook.com/%@/picture", fbContact.lsuid];
                
            } else if ([lstype isEqualToString:@"Foursquare"]) {
                
//                F4SContact * f4sContact = [[F4SContact alloc] init];
//                f4sContact.lstype = @"Foursquare";
//                f4sContact.lsuid = [accountInfo objectForKey:@"id"];
//                
//                [associatedLBSAccounts addObject:f4sContact];
            }
        }
    }
    return self;
}

+ (GSContact *)contactFromDictionary:(NSDictionary *)contactInfo {
    return [[[[self class] alloc] initWithDictionary:contactInfo] autorelease];
}

- (void)dealloc {
    [uid release];
    [firstName release];
	[lastName release];
    [fullName release];
	[email release];
    [playerAlias release];
    [avatarURLString release];
    [associatedLBSAccounts release];
    [super dealloc];
}


- (NSString *)name {
    if (fullName) {
        return fullName;
    }
	return [NSString stringWithFormat:@"%@ %@", self.firstName, self.lastName];
}

#pragma mark Search/Filter

- (BOOL)doesString:(NSString *)mainString containsString:(NSString *)subString {
    if (mainString == nil) {
        return NO;
    }
    NSRange range = [mainString rangeOfString:subString options:(NSCaseInsensitiveSearch|NSDiacriticInsensitiveSearch)];
    return (range.location != NSNotFound);
}

- (BOOL)containsInfo:(NSString *)searchText {
    return ([self doesString:[self name] containsString:searchText]
            || [self doesString:email containsString:searchText]
//            || [self doesString:username containsString:searchText]
//            || [self doesString:playerAlias containsString:searchText]
            );
}

- (NSString *)description {
    return [NSString stringWithFormat:@"uid: %@, name: %@, email: %@, relationship: %d, following: %d", 
            uid, self.name, email, friendStatus, followingStatus];
}


- (NSString *)friendStatusDescription {
    switch (friendStatus) {
        case GSContactFriendStatusBoth:
            return @"Friend";
            break;            
        case GSContactFriendStatusFrom:
            return @"Requesting";
            break;
        case GSContactFriendStatusTo:
            return @"Pending";
            break;
        default:
            break;
    }

    return nil;
}

- (id)copyWithZone:(NSZone *)zone
{
	GSContact *gsContactCopy = [[GSContact allocWithZone: zone] init];
    
    gsContactCopy.uid = (uid) ? [NSString stringWithString:uid] : nil;
    gsContactCopy.firstName = (firstName) ? [NSString stringWithString:firstName] : nil;
    gsContactCopy.lastName = (lastName) ? [NSString stringWithString:lastName] : nil;
    gsContactCopy.fullName = (fullName) ? [NSString stringWithString:fullName] : nil;
    gsContactCopy.username = (username) ? [NSString stringWithString:username] : nil;
    gsContactCopy.email = (email) ? [NSString stringWithString:email] : nil;
    gsContactCopy.playerAlias = (playerAlias) ? [NSString stringWithString:playerAlias] : nil;
    gsContactCopy.friendStatus = self.friendStatus;
    gsContactCopy.followingStatus = self.followingStatus;
    gsContactCopy.avatarURLString = (avatarURLString) ? [NSString stringWithString:avatarURLString] : nil;
    gsContactCopy.isAvatarCached = self.isAvatarCached;
    gsContactCopy.avatar = [self.avatar copy];
    gsContactCopy.associatedLBSAccounts = [self.associatedLBSAccounts copy];
	return gsContactCopy;
}
@end