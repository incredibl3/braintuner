//
//  GSFriends.h
//  Opinions
//
//  Created by Cong Vo on 4/21/11.
//  Copyright 2011 Greengar Studios LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GSContact.h"
#import "GSRequest.h"
@protocol GSParser;


static NSString *const kNotificationFollowersUpdated = @"kNotificationFollowersUpdated";

@interface GSFollowers : NSObject {
    NSArray *_followers;
}

@property (nonatomic, retain) NSArray *followers;

//KONG: update friends list, using notification: kNotificationFriendsUpdated
- (GSRequest *)getFollowers:(id)delegate callback:(SEL)callback;

//KONG: check if a contact is in Friends list
- (BOOL)isFollower:(GSContact *)contact;

- (GSRequest*)addFollower:(GSContact *)contact delegate:(id)d callback:(SEL)c;
- (GSRequest*)removeFollower:(GSContact *)contact delegate:(id)d callback:(SEL)c;

@end

