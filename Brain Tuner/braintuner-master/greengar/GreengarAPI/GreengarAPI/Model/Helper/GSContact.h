//
//  GSContact.h
//  Donde
//
//  Created by Cong Vo on 4/8/11.
//  Copyright 2011 Greengar Studios LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

typedef enum {
    GSContactFriendStatusUnknown = 0,
    GSContactFriendStatusNone,
    GSContactFriendStatusTo,
    GSContactFriendStatusFrom,
    GSContactFriendStatusBoth
} GSContactFriendStatus;

typedef enum {
    GSContactFollowingStatusUnknown = 0,
    GSContactFollowingStatusNone,
    GSContactFollowingStatusTo,
    GSContactFollowingStatusFrom,
    GSContactFollowingStatusBoth
} GSContactFollowingStatus;

@class GSContact;

@protocol GSContact <NSObject>
+ (GSContact *)contactFromDictionary:(NSDictionary *)contactInfo;
@end

@interface GSContact : NSObject <GSContact, NSCopying> {
    NSString				* uid;
	NSString				* firstName;
	NSString				* lastName;
    NSString                * fullName;
    NSString                * username;

    NSString				* email;
    
    NSString                *playerAlias;
    
    GSContactFriendStatus   friendStatus;
    GSContactFollowingStatus followingStatus;
    
    NSString				* avatarURLString;
	BOOL					  isAvatarCached;
	UIImage					* avatar;
    
    NSMutableArray			* associatedLBSAccounts;
    NSString *gender;
    NSString *dob;
}

@property (nonatomic, retain) NSString				* uid;
@property (nonatomic, retain) NSString				* firstName;
@property (nonatomic, retain) NSString				* lastName;
@property (nonatomic, retain) NSString				* fullName;
@property (nonatomic, retain) NSString				* username;
@property (nonatomic, retain) NSString				* email;
@property (nonatomic, retain) NSString				* gender;
@property (nonatomic, retain) NSString				* dob;
@property (nonatomic, retain) NSString				* playerAlias;
@property (nonatomic) GSContactFollowingStatus followingStatus;
@property (nonatomic) GSContactFriendStatus   friendStatus;
@property (nonatomic, readonly) NSString            *name;

@property (nonatomic, retain) NSString				* avatarURLString;
@property (nonatomic)		  BOOL					  isAvatarCached;
@property (nonatomic, retain) UIImage				* avatar;


@property (nonatomic, retain) NSMutableArray		* associatedLBSAccounts;

- (id)initWithDictionary:(NSDictionary *)contactInfo;
- (BOOL)containsInfo:(NSString *)searchText;

- (NSString *)friendStatusDescription;

@end


