//
//  GSFriends.m
//  Opinions
//
//  Created by Cong Vo on 4/21/11.
//  Copyright 2011 Greengar Studios LLC. All rights reserved.
//

#import "GSFriends.h"
#import "GSRequest.h"
#import "Greengar.h"

@implementation GSFriends
@synthesize friends         = _friends;

- (id)init {
    self = [super init];
    if (self) {
        _friends = [[NSArray alloc] init];
    }
    return self;
}
- (void)dealloc {
    [_friends release];
    [super dealloc];
}
#pragma mark Friends

- (GSRequest *)getFriends:(id)delegate callback:(SEL)callback pending:(BOOL)pendingIncluded {
    
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                    [NSString stringWithFormat:@"[\"%@\"]", [GSAppInfo clientID]], @"apps",
                                    nil];
    pendingIncluded = YES;
    if (pendingIncluded) {
        [params setObject:@"[\"both\",\"from\",\"to\"]" forKey:@"relationships"];
    }
    
    GSHelperRequest *request = (GSHelperRequest *) [GSHelperRequest requestWithEndPoint:kGreengarEndpointGetFriends
                                                                                      params:params
                                                                                    delegate:self
                                                                                    callback:@selector(request:gotFriends:)];
    request.appDelegate = delegate;
    request.appCallback = callback;
    
    return request;
}



- (GSRequest *)getFriends:(id)delegate callback:(SEL)callback {
     return [self getFriends:delegate callback:callback pending:NO];
}

- (GSRequest *)getAllTypeFriends:(id)delegate callback:(SEL)callback {
    return [self getFriends:delegate callback:callback pending:YES];    
}

- (void)request:(GSHelperRequest *)request gotFriends:(id)result {

    if ([result isKindOfClass:[NSArray class]]) {
        NSMutableArray *friends = [NSMutableArray array];
        for (int i = 0; i < [result count]; i++) {
            NSDictionary * dict = [result objectAtIndex:i];            
            GSContact * contact = [[GSAppInfo contactClass] contactFromDictionary:dict];
            [friends addObject:contact];
        }
        self.friends = friends;
        result = _friends;
	}
    
    if (request.appDelegate && [request.appDelegate respondsToSelector:request.appCallback]) {
        [request.appDelegate performSelector:request.appCallback withObject:request withObject:result];
    }
}

//KONG: check if a contact is in Friends list
- (BOOL)isFriend:(GSContact *)contact {
    
    return NO;
}

- (GSRequest*)sendFriendRequest:(GSContact *)contact delegate:(id)d callback:(SEL)c {
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            contact.uid, @"friend_id", nil];
    
    GSHelperRequest *request = (GSHelperRequest *) [GSHelperRequest requestWithEndPoint:kGreengarEndpointAddFriend
                                                                                 params:params
                                                                               delegate:self
                                                                               callback:@selector(request:addedFriend:)];
    request.appDelegate = d;
    request.appCallback = c;
    return request;
}

- (GSRequest*)cancelFriendRequest:(GSContact *)contact delegate:(id)d callback:(SEL)c {    
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            contact.uid, @"friend_id", 
                            nil];
    
    GSHelperRequest *request = (GSHelperRequest *) [GSHelperRequest requestWithEndPoint:kGreengarEndpointRemoveFriend
                                                                                 params:params
                                                                               delegate:self
                                                                               callback:@selector(request:canceledFriend:)];
    request.appDelegate = d;
    request.appCallback = c;
    return request;
}

- (GSRequest*)removeFriendRequest:(GSContact *)contact delegate:(id)d callback:(SEL)c {
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            contact.uid, @"friend_id", 
                            nil];
    
    GSHelperRequest *request = (GSHelperRequest *) [GSHelperRequest requestWithEndPoint:kGreengarEndpointRemoveFriend
                                                                                 params:params
                                                                               delegate:self
                                                                               callback:@selector(request:removedFriend:)];
    request.appDelegate = d;
    request.appCallback = c;
    return request;
}

- (GSRequest*)acceptRequest:(GSContact *)contact delegate:(id)d callback:(SEL)c {
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            contact.uid, @"friend_id", 
                            @"yes", @"response",
                            nil];
    
    GSHelperRequest *request = (GSHelperRequest *) [GSHelperRequest requestWithEndPoint:kGreengarEndpointAddFriend
                                                                                 params:params
                                                                               delegate:self
                                                                               callback:@selector(request:acceptedFriend:)];
    request.appDelegate = d;
    request.appCallback = c;
    return request;
}

- (GSRequest*)denyRequest:(GSContact *)contact delegate:(id)d callback:(SEL)c {
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            contact.uid, @"friend_id", 
                            @"no", @"response",
                            nil];
    
    GSHelperRequest *request = (GSHelperRequest *) [GSHelperRequest requestWithEndPoint:kGreengarEndpointAddFriend
                                                                                 params:params
                                                                               delegate:self
                                                                               callback:@selector(request:acceptedFriend:)];
    request.appDelegate = d;
    request.appCallback = c;
    return request;
}

- (void)request:(GSHelperRequest *)request addedFriend:(id)result {
    
    if (request.appDelegate && [request.appDelegate respondsToSelector:request.appCallback]) {
        [request.appDelegate performSelector:request.appCallback withObject:request withObject:result];
    }
}

- (void)request:(GSHelperRequest *)request canceledFriend:(id)result {
    
    if (request.appDelegate && [request.appDelegate respondsToSelector:request.appCallback]) {
        [request.appDelegate performSelector:request.appCallback withObject:request withObject:result];
    }
}

- (void)request:(GSHelperRequest *)request removedFriend:(id)result {
    
    if (request.appDelegate && [request.appDelegate respondsToSelector:request.appCallback]) {
        [request.appDelegate performSelector:request.appCallback withObject:request withObject:result];
    }
}

- (void)request:(GSHelperRequest *)request acceptedFriend:(id)result {
    
    if (request.appDelegate && [request.appDelegate respondsToSelector:request.appCallback]) {
        [request.appDelegate performSelector:request.appCallback withObject:request withObject:result];
    }
}

- (void)request:(GSHelperRequest *)request deniedFriend:(id)result {
    
    if (request.appDelegate && [request.appDelegate respondsToSelector:request.appCallback]) {
        [request.appDelegate performSelector:request.appCallback withObject:request withObject:result];
    }
}

@end
