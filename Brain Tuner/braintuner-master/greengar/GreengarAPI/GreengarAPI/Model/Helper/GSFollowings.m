//
//  GSFriends.m
//  Opinions
//
//  Created by Cong Vo on 4/21/11.
//  Copyright 2011 Greengar Studios LLC. All rights reserved.
//

#import "GSFollowings.h"
#import "GSRequest.h"
#import "Greengar.h"

@implementation GSFollowings
@synthesize followings         = _followings;

- (id)init {
    self = [super init];
    if (self) {
        _followings = [[NSArray alloc] init];
    }
    return self;
}
- (void)dealloc {
    [_followings release];
    [super dealloc];
}
#pragma mark Friends

- (GSRequest *)getFollowings:(id)delegate callback:(SEL)callback {
    
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                    [NSString stringWithFormat:@"[\"%@\"]", [GSAppInfo clientID]], @"client_id",
                                   @"[\"from\"]", @"relationships",
                                    nil];
    
    GSHelperRequest *request = (GSHelperRequest *) [GSHelperRequest requestWithEndPoint:kGreengarEndpointGetFollowing
                                                                                      params:params
                                                                                    delegate:self
                                                                                    callback:@selector(request:gotFollowings:)];
    request.appDelegate = delegate;
    request.appCallback = callback;
    
    return request;
}

- (void)request:(GSHelperRequest *)request gotFollowings:(id)result {

    if ([result isKindOfClass:[NSArray class]]) {
        NSMutableArray *followings = [NSMutableArray array];
        for (int i = 0; i < [result count]; i++) {
            NSDictionary * dict = [result objectAtIndex:i];            
            GSContact * contact = [[GSAppInfo contactClass] contactFromDictionary:dict];
            [followings addObject:contact];
        }
        self.followings = followings;
        result = _followings;
	}
    
    if (request.appDelegate && [request.appDelegate respondsToSelector:request.appCallback]) {
        [request.appDelegate performSelector:request.appCallback withObject:request withObject:result];
    }
}

//KONG: check if a contact is in Friends list
- (BOOL)isFollowing:(GSContact *)contact {
    
    return NO;
}


- (GSRequest*)addFollowing:(GSContact *)contact delegate:(id)d callback:(SEL)c {
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                   [GSAppInfo clientID], @"client_id",
                                   contact.uid, @"friend_id",
                                   nil];
    
    
    GSHelperRequest *request = (GSHelperRequest *) [GSHelperRequest requestWithEndPoint:kGreengarEndpointAddFollowing
                                                                                 params:params
                                                                               delegate:self
                                                                               callback:@selector(request:addedFollowing:)];
    request.appDelegate = d;
    request.appCallback = c;
    
    return request;
}
- (GSRequest*)removeFollowing:(GSContact *)contact delegate:(id)d callback:(SEL)c {
    
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                   [GSAppInfo clientID], @"client_id",
                                   contact.uid, @"friend_id",
                                   nil];
    
    
    GSHelperRequest *request = (GSHelperRequest *) [GSHelperRequest requestWithEndPoint:kGreengarEndpointRemoveFollowing
                                                                                 params:params
                                                                               delegate:self
                                                                               callback:@selector(request:removedFollowing:)];
    
    request.appDelegate = d;
    request.appCallback = c;
    return request;
}

- (void)request:(GSHelperRequest *)request addedFollowing:(id)result {
    
    //    if ([result isKindOfClass:[NSArray class]]) {
    //        NSMutableArray *followings = [NSMutableArray array];
    //        for (int i = 0; i < [result count]; i++) {
    //            NSDictionary * dict = [result objectAtIndex:i];            
    //            GSContact * contact = [[GSAppInfo contactClass] contactFromDictionary:dict];
    //            [followings addObject:contact];
    //        }
    //        self.followings = followings;
    //        result = _followings;
    //	}
    
    if (request.appDelegate && [request.appDelegate respondsToSelector:request.appCallback]) {
        [request.appDelegate performSelector:request.appCallback withObject:request withObject:result];
    }
}

- (void)request:(GSHelperRequest *)request removedFollowing:(id)result {
    
    //    if ([result isKindOfClass:[NSArray class]]) {
    //        NSMutableArray *followings = [NSMutableArray array];
    //        for (int i = 0; i < [result count]; i++) {
    //            NSDictionary * dict = [result objectAtIndex:i];            
    //            GSContact * contact = [[GSAppInfo contactClass] contactFromDictionary:dict];
    //            [followings addObject:contact];
    //        }
    //        self.followings = followings;
    //        result = _followings;
    //	}
    
    if (request.appDelegate && [request.appDelegate respondsToSelector:request.appCallback]) {
        [request.appDelegate performSelector:request.appCallback withObject:request withObject:result];
    }
}


@end
