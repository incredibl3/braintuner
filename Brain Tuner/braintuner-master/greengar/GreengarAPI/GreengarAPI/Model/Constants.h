//
//  Constants.h
//  Opinions
//
//  Created by silvercast on 8/10/11.
//  Copyright 2011 GreenGar studios. All rights reserved.
//

typedef enum {
    FacebookAPI_Unset = 0,
    FacebookAPI_Me = 1,
    FacebookAPI_Friends = 2,
    FacebookAPI_Checkins = 3,
    FacebookAPI_UserInterests = 4,
    FacebookAPI_PlaceSearch = 5,
    FacebookAPI_Avatar = 6
} FacebookAPIAction;
