//
//  GSResetPasswordView.h
//  Whiteboard
//
//  Created by Cong Vo on 6/6/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface GSResetPasswordView : UITableViewController <UITextFieldDelegate> {
    UITextField *_emailField;
    NSString *_email;
    
    BOOL _addDoneButton;
}


@property (nonatomic, retain) UITextField *emailField;
@property (nonatomic, retain) NSString *email;
@property (nonatomic, assign) BOOL addDoneButton;

- (id)initWithEmail:(NSString *)email;
- (void)resetPassword:(NSString *)email;
@end
