
#import "GSAddFriendsView.h"
#import "GSRequest.h"
#import "GSContact.h"
#import "Greengar.h"

@interface GSAddFriendsView()

- (void)searchFriendsFromID:(NSInteger)fromID;
- (void)setSearchScope:(SearchScope)scope;
- (NSArray *)filterContacts:(NSArray *)contacts forSearchText:(NSString *)searchText;

@end


@implementation GSAddFriendsView
@synthesize tableView;
@synthesize listContent, savedSearchTerm, savedScopeButtonIndex;
@synthesize suggestedFriends, searchedFriends;
@synthesize searchBar   = _searchBar;

#pragma mark - 
#pragma mark Lifecycle methods

- (id)init {
	if ((self = [super initWithNibName:@"GSAddFriendsView" bundle:nil])) {
	}
	return self;
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
	[listContent release];
    [suggestedFriends release];
    [searchedFriends release];
    [_searchBar release];
    
	[super dealloc];
}

- (void)viewDidLoad {
	[super viewDidLoad];
    self.title = @"Add friends";
    _searchBar.placeholder = @"Name or email";
    self.suggestedFriends = [NSArray array];
	self.searchedFriends = [NSMutableArray array];
    self.listContent = suggestedFriends;
	
    _searchBar.showsScopeBar = NO;
    [self setSearchScope:SearchScopeAll];
    CGRect searchBarFrame = _searchBar.frame;
    searchBarFrame.size.height = 44.0f;
    _searchBar.frame = searchBarFrame;

    
    tableView = [[UITableView alloc] initWithFrame:self.view.frame style:UITableViewStylePlain];
    tableView.dataSource = self;
    tableView.delegate = self;
    [self.view addSubview:tableView];
    tableView.tableHeaderView = _searchBar;
	tableView.scrollEnabled = YES;
    
    loadingIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    loadingIndicator.center = self.view.center;
    [loadingIndicator startAnimating];
    
    //self.pullToReloadHeaderView.hidden = YES;
	showLastLoadingCell = NO;
    _isSearching = NO;
}

- (void)viewDidAppear:(BOOL)animated {
	[super viewDidAppear:animated];
	
//	[self checkSuggestedFriends];
}

- (void)viewDidDisappear:(BOOL)animated
{
    // save the state of the search UI so that it can be restored if the view is re-created
}


- (void)checkSuggestedFriends {
	
//	NSMutableDictionary *params = [NSMutableDictionary dictionary];
//	
//	for (LBSDatasource * source in [ContactManager sharedManager].dataSourceArray) {
//		
//		NSArray * array = [source contactUIDs];
//		
//		if ([array count] == 0) {
//			continue;
//		}
//		
//		SBJSON * json = [[SBJSON alloc] init];
//		NSString * requestString = [json stringWithObject:array error:nil];
//		
//		NSString * encodedRequestString = [requestString urlEncodedStringWithEncoding:NSUTF8StringEncoding];
//		[params setValue:encodedRequestString forKey:[NSString stringWithFormat:@"%@_ids", source.name]];		
//	}
//	
//	[GSRequest requestWithOpinionsEndPoint:kOpinionsEndpointSuggestFriends
//							HTTPMethod:@"POST"
//								params:params
//							  delegate:self
//							  callback:@selector(dondeDidFinishSuggestFriends:)];
}

//- (void)dondeDidFinishSuggestFriends:(id)result {
//	if ([result isKindOfClass:[NSError class]]) {
//		DLog(@"failed. Error: %@", result);
//		return;
//	}
//	
//	DLog(@"completed. API response:%@", result);
//	
//	NSMutableArray * sgfa = [[NSMutableArray alloc] init];
//	
//	for (NSDictionary * dict in result) {
//		DDContact * suggestedFriend = [DDContact contactFromDictionary:dict];
//		[sgfa addObject:suggestedFriend];
//	}
//	self.suggestedFriends = sgfa;
//	
//	if (_searchBar.selectedScopeButtonIndex == 1) {
//		self.listContent = suggestedFriends;
//		[self.tableView reloadData];		
//	}
//
//}

#pragma mark -
#pragma mark UITableView data source and delegate methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSInteger numOfRow = [self.listContent count];
    numOfRow += ((showLastLoadingCell)? 1: 0);
    return numOfRow;
}

- (UITableViewCell *)tableView:(UITableView *)tableView_ cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell;
    if ([self.listContent count] == indexPath.row) {

        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil] autorelease];        
        
        cell.textLabel.text = @"Loading more...";            

//        cell.textLabel.textAlignment = UITextAlignmentCenter;
        cell.textLabel.font = [UIFont boldSystemFontOfSize:14];
        cell.textLabel.textColor = [UIColor colorWithRed:0.176 green:0.310 blue:0.522 alpha:1.0];
        if (_isSearching == NO) {
            [self searchFriendsFromID:minGUID-1];            
        }
    } else {
        
        static NSString *kCellID = @"kFriendCell";
        
        cell = [tableView_ dequeueReusableCellWithIdentifier:kCellID];
        
        if (cell == nil) {
            cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:kCellID] autorelease];
            cell.accessoryType = UITableViewCellAccessoryNone;
            cell.detailTextLabel.text = nil;
        }
        
        /*
         If the requesting table view is the search display controller's table view, configure the cell using the filtered content, otherwise use the main list.
         */
        
        GSContact *contact = [self.listContent objectAtIndex:indexPath.row];
        
        cell.textLabel.text = [contact name];
        
        cell.detailTextLabel.text = [contact friendStatusDescription];
    }
    
	return cell;
}

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    return ([indexPath row] >= [self.listContent count])? nil : indexPath;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	/*
	 If the requesting table view is the search display controller's table view, configure the next view controller using the filtered content, otherwise use the main list.
	 */
	GSContact *contact = nil;
    contact = [self.listContent objectAtIndex:indexPath.row];
    DLog(@"%@", contact);
//    GSUserViewController *detailsViewController = [[GSUserViewController alloc] initWithContact:contact];
//    [[self navigationController] pushViewController:detailsViewController animated:YES];
//    [detailsViewController release];
}

- (void)searchFriendsFromID:(NSInteger)fromID { // ignore param if fromID = -1
    
    [self.view addSubview:loadingIndicator];
    
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                            _searchBar.text, @"keywords", 
//                            [NSString stringWithFormat:@"%d", kFriendSearchResultCount], @"count",
                            [NSString stringWithFormat:@"[\"%@\"]", [GSAppInfo clientID]], @"apps",
                            nil];
    
    if (fromID > 0) {
        [params setObject:[NSString stringWithFormat:@"%d", (int)fromID] forKey:@"from"];
    }
    
    [GSRequest requestWithEndPoint:kGreengarEndpointFindFriends
                                    params:params
                                  delegate:self
                                  callback:@selector(greengar:foundFriends:)];  
    _isSearching = YES;
}


- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {	
//	NSString *urlStr = @"http://184.106.64.162/elgg/pg/greengardonde/suggest_friends"; 
        
    DLog(@"searchBar text: %@", _searchBar.text);
    [searchBar resignFirstResponder];
    if (_searchBar.selectedScopeButtonIndex == SearchScopeAll) {
        if ([_searchBar.text isEqualToString:savedSearchTerm]) {
            if (searchedFriends != nil) {
                self.listContent = searchedFriends;
            }
        } else {
            self.savedSearchTerm = _searchBar.text;
            self.listContent = nil;
            
            // Cancel previous search request
            if (_searchBar.text != nil && ![_searchBar.text isEqualToString:@""]) {
                // Create new search request
                [self.searchedFriends removeAllObjects];
                minGUID = 9999999;
                [self searchFriendsFromID:-1];
            }
        }
    } else {
        self.listContent = [self filterContacts:suggestedFriends forSearchText:_searchBar.text];
    }
    
    [tableView reloadData];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
//    [self setSearchScope:_searchBar.selectedScopeButtonIndex];
    [_searchBar resignFirstResponder];
    DLog(@"searchBar.showsScopeBar: %d", searchBar.showsScopeBar);
    DLog(@"searchBar. height: %f", searchBar.frame.size.height);
//    searchBar.showsScopeBar = YES;
//    searchBar.frame = CGRectMake(0, -44, 320, 88);
    [loadingIndicator removeFromSuperview];
}


- (id)contactFromDictionary:(NSDictionary *)contactInfo {
    return [[[GSContact alloc] initWithDictionary:contactInfo] autorelease];
}

/*
- (void)addPullToSearchMore {
    self.pullToReloadHeaderView.hidden = NO;
    if (listContent.count) {
        [self.pullToReloadHeaderView setLastUpdatedDate: [NSDate date]];
        [self.pullToReloadHeaderView finishReloading:self.tableView animated:YES];
        
        //    UITableViewCell * lastCell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:self.listContent.count-1 inSection:0]];
        self.pullToReloadHeaderView.frame = CGRectMake(0, self.tableView.contentSize.height, 320, 65);
        
        [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:self.listContent.count-1 inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:YES];
    }
}*/


- (void)greengar:(GSRequest *)request foundFriends:(id)result {

    [loadingIndicator removeFromSuperview];
    
    // KONG: prevent previous result intefere current search.
    NSString *searchedKey = [request.params objectForKey:@"keywords"];
    if ([searchedKey isEqualToString:savedSearchTerm] == NO) {
        return;
    }
    
    _isSearching = NO;
    
    if ([result isKindOfClass:[NSArray class]] == NO) {
//        DLog(@"%@", result);
        return;
    }
    
    for (NSDictionary *contactInfo in result) {
        GSContact * contact = [self contactFromDictionary:contactInfo];
        if (contact) {
            [self.searchedFriends addObject:contact];            
            if ([contact.uid intValue] < minGUID) {
                minGUID = [contact.uid intValue];
            }            
        }
    }

    if ([result count] >= kFriendSearchPaging) {
        
        showLastLoadingCell = YES;
        
    } else {
        
        showLastLoadingCell = NO;
        
    }
    
    [self searchBarSearchButtonClicked:_searchBar];
    
    if ([self.searchedFriends count] == 0) {
        [[[[UIAlertView alloc] initWithTitle:@"Add friends"
                                     message:@"No result found." 
                                    delegate:nil
                           cancelButtonTitle:@"OK"
                           otherButtonTitles:nil] autorelease] show];
    }
}

#pragma mark Search Scope

- (void)setSearchScope:(SearchScope)scope {
    // Change view
    _searchBar.selectedScopeButtonIndex = scope;
    
    // Change Table View data
    [self searchBarSearchButtonClicked:_searchBar];
}

- (NSArray *)filterContacts:(NSArray *)contacts forSearchText:(NSString *)searchText {
    if (searchText == nil || [searchText isEqualToString:@""]) {
        return contacts;
    }
    
    NSMutableArray *filterContacts = [NSMutableArray array];
	for (GSContact *contact in contacts) {
        if ([contact containsInfo:searchText]) {
            [filterContacts addObject:contact];
        }
	}
    
    return [NSArray arrayWithArray:filterContacts];
}

- (void)searchBar:(UISearchBar *)searchBar selectedScopeButtonIndexDidChange:(NSInteger)selectedScope {
    DLog();
    [self setSearchScope:(int)selectedScope];
}

#pragma mark - Pull up to update

-(void) pullDownToReloadAction {	
    [self searchFriendsFromID:minGUID-1];
}

@end