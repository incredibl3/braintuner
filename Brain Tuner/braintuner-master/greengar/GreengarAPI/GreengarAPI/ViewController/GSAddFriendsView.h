#import <UIKit/UIKit.h>
#import "GSFriends.h"

//KONG: a common search friend method for Greengar apps

typedef enum {
    SearchScopeAll,
    SearchScopeSuggestedFriends
} SearchScope;

#define kFriendSearchResultCount 10
#define kFriendSearchPaging 10

@interface GSAddFriendsView : UIViewController <UISearchBarDelegate, UITableViewDelegate, UITableViewDataSource>
{
    UITableView             * tableView;
    UIActivityIndicatorView * loadingIndicator;
    
	NSArray                 * listContent;			// The master content.
    NSMutableArray          * searchedFriends;
    
    NSArray                 * suggestedFriends;
    NSString                * savedSearchTerm;
    int                       minGUID;              // for search paging
    
    BOOL                      showLastLoadingCell;
    
    UISearchBar             * _searchBar;
    
    //KONG: avoid a searching request is accidentally repeated twice
    // is set to YES when we make a new request
    // is set to NO when we cancel last request, or that request finished
    BOOL                    _isSearching;    
}

@property (nonatomic, retain) UITableView             * tableView;
@property (nonatomic, retain) NSArray *listContent, *suggestedFriends;
@property (nonatomic, retain) NSMutableArray         *searchedFriends;
@property (nonatomic, retain) IBOutlet UISearchBar *searchBar;


@property (nonatomic, copy) NSString *savedSearchTerm;
@property (nonatomic) NSInteger savedScopeButtonIndex;
//@property (nonatomic) BOOL searchWasActive;

- (void)checkSuggestedFriends;
- (void)setSearchScope:(SearchScope)scope;
- (NSArray *)filterContacts:(NSArray *)contacts forSearchText:(NSString *)searchText;

- (void)searchFriendsFromID:(NSInteger)fromID; // ignore param if fromID = -1
@end
