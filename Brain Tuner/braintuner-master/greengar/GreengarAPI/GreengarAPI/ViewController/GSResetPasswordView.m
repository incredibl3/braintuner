//
//  GSResetPasswordView.m
//  Whiteboard
//
//  Created by Cong Vo on 6/6/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "GSResetPasswordView.h"
#import "GSRequest.h"

@implementation GSResetPasswordView
@synthesize emailField      = _emailField,
            email           = _email,
            addDoneButton   = _addDoneButton;

- (id)initWithEmail:(NSString *)email {
    self = [super initWithStyle:UITableViewStyleGrouped];
    if (self) {
        // Custom initialization
        _email = [email retain];
    }
    return self;
}



- (void)dealloc {
    [_emailField release];
    [_email release];

    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)displayDoneButton {
    if (_addDoneButton == YES) {    
        self.navigationItem.hidesBackButton = YES;
        self.navigationItem.rightBarButtonItem = [[[UIBarButtonItem alloc] initWithTitle:@"Cancel" 
                                                                                   style:UIBarButtonItemStyleBordered
                                                                                  target:self
                                                                                  action:@selector(doneButtonPressed)] autorelease];
    }    
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    self.title = @"Reset password";
    [self displayDoneButton];
    
    if ([self respondsToSelector:@selector(setContentSizeForViewInPopover:)]) {
        self.contentSizeForViewInPopover = CGSizeMake(320, 460);
    }
    
}


- (void)doneButtonPressed {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [_emailField becomeFirstResponder];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return 1;
}

- (UITextField *)textFieldForInputCell {
	UITextField *playerTextField = [[[UITextField alloc] initWithFrame:CGRectMake(85, 12, 190, 22)] autorelease];
	playerTextField.adjustsFontSizeToFitWidth = YES;
	playerTextField.textColor = [UIColor blackColor];
	playerTextField.backgroundColor = [UIColor clearColor];    
	playerTextField.autocorrectionType = UITextAutocorrectionTypeNo; // no auto correction support
	playerTextField.autocapitalizationType = UITextAutocapitalizationTypeNone; // no auto capitalization support
	playerTextField.textAlignment = UITextAlignmentLeft;
    playerTextField.text = _email;
	playerTextField.delegate = self;
    
	playerTextField.clearButtonMode = UITextFieldViewModeWhileEditing; // no clear 'x' button to the right
	[playerTextField setEnabled: YES];	
	return playerTextField;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
        
    }
    UITextField *playerTextField = [self textFieldForInputCell];
    [cell addSubview:playerTextField]; 
    
    
    playerTextField.placeholder = @"name@gmail.com";
    playerTextField.returnKeyType = UIReturnKeySend;
    playerTextField.keyboardType = UIKeyboardTypeEmailAddress;
    self.emailField = playerTextField;
    cell.textLabel.text = @"Email";
    
    return cell;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return @"Reset account's password";
}

- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section {
    return @"You will then receive a confirmation email.";
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    DLog();
    [self resetPassword:_emailField.text];
	return YES;
}

- (void)resetPassword:(NSString *)email {
    if (email.length  == 0) {
        return;
    }
    
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            email, @"email",
                            nil];
    
    [GSRequest requestWithEndPoint:kGreengarEndpointResetPassword
                            params:params
                          delegate:self
                          callback:@selector(greengar:resetedPassword:)];
    
    // Show indicator
    UIActivityIndicatorView *indicator = [[[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite] autorelease];
    [indicator startAnimating];
    self.navigationItem.rightBarButtonItem = [[[UIBarButtonItem alloc] initWithCustomView:indicator] autorelease];
    
    [self retain];
}

- (void)greengar:(GSRequest *)resquest resetedPassword:(id)result {
    DLog();
    [self displayDoneButton];    
    NSString *alertTitle;
    NSString *alertMessage;    
    if ([result isKindOfClass:[NSError class]]) {
        NSError *error = result;
        alertTitle = @"Reset password error";
        alertMessage = [error localizedDescription];
    } else {
        alertTitle = @"Reset password is successfully!";
        alertMessage = @"Please check your confirmation email";
        [self.navigationController popToRootViewControllerAnimated:YES];        
    }
    
    UIAlertView *alertView = [[[UIAlertView alloc] initWithTitle:alertTitle
                                                         message:alertMessage
                                                        delegate:nil
                                               cancelButtonTitle:@"OK"
                                               otherButtonTitles:nil] autorelease];
    [alertView show];
    [self autorelease];
}
/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     [detailViewController release];
     */
}

@end
