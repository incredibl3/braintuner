//
//  GSAlertPromoAds.h
//  BrainTuner2
//
//  Created by Hector Zhao on 8/22/12.
//
//

#import <Foundation/Foundation.h>
#import "ASIHTTPRequest.h"
#import "SBJson.h"

@interface GSAlertPromoAds : NSObject <UIAlertViewDelegate> {
    NSString *adID;
    NSString *title;
    NSString *message;
    NSString *yesButtonTitle;
    NSString *noButtonTitle;
    NSString *urlString;
    NSString *appScheme;
    int      durationInSec;
    
    ASIHTTPRequest *request;
    BOOL shouldShowPromoAds;
    NSTimeInterval lastTimeShowAd;
}

- (void) start;
- (void) show;

@property (nonatomic, retain)   NSString *adID;
@property (nonatomic, retain)   NSString *title;
@property (nonatomic, retain)   NSString *message;
@property (nonatomic, retain)   NSString *yesButtonTitle;
@property (nonatomic, retain)   NSString *noButtonTitle;
@property (nonatomic, retain)   NSString *urlString;
@property (nonatomic, retain)   NSString *appScheme;
@property (nonatomic)           int      durationInSec;

@end
