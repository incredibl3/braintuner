//
//  GSAlertPromoAds.m
//  BrainTuner2
//
//  Created by Hector Zhao on 8/22/12.
//
//

#import "GSAlertPromoAds.h"
#import "Reachability.h"

#define kLastAdIdKey @"kLastAdIdKey"
#define kLastTitleKey @"kLastTitleKey"
#define kLastMessageKey @"kLastMessageKey"
#define kLastYesButtonTitleKey @"kLastYesButtonTitleKey"
#define kLastNoButtonTitleKey @"kLastNoButtonTitleKey"
#define kLastURLStringKey @"kLastURLStringKey"
#define kLastAppSchemeKey @"kLastAppSchemeKey"
#define kLastDurationKey @"kLastDurationKey"

#define kShouldShowAdKey @"kShouldShowAdKey"
#define kLastTimeShowAdKey @"kLastTimeShowAdKey"

@implementation GSAlertPromoAds
@synthesize title;
@synthesize message;
@synthesize yesButtonTitle;
@synthesize noButtonTitle;
@synthesize urlString;
@synthesize appScheme;
@synthesize durationInSec;
@synthesize adID;

- (id) init {
    if (self = [super init]) {
        adID = nil;
        title = nil;
        message = nil;
        yesButtonTitle = nil;
        noButtonTitle = nil;
        urlString = nil;
        appScheme = nil;
        durationInSec = 0;
        shouldShowPromoAds = YES;
    }
    return self;
}

- (void) checkLocalCachedAdData {
    NSString *lastAdID = [[NSUserDefaults standardUserDefaults] objectForKey:kLastAdIdKey];
    NSString *lastTitle = [[NSUserDefaults standardUserDefaults] objectForKey:kLastTitleKey];
    NSString *lastMessage = [[NSUserDefaults standardUserDefaults] objectForKey:kLastMessageKey];
    NSString *lastYesButtonTitle = [[NSUserDefaults standardUserDefaults] objectForKey:kLastYesButtonTitleKey];
    NSString *lastNoButtonTitle = [[NSUserDefaults standardUserDefaults] objectForKey:kLastNoButtonTitleKey];
    NSString *lastURLString = [[NSUserDefaults standardUserDefaults] objectForKey:kLastURLStringKey];
    NSString *lastAppScheme = [[NSUserDefaults standardUserDefaults] objectForKey:kLastAppSchemeKey];
    NSString *lastDurationInString = [[NSUserDefaults standardUserDefaults] objectForKey:kLastDurationKey];
    
    BOOL shouldRescheduleAd = NO;
    
    if (lastAdID == nil || (lastAdID && ![lastAdID isEqualToString:adID])) {
        [[NSUserDefaults standardUserDefaults] setObject:adID forKey:kLastAdIdKey];
        shouldRescheduleAd = YES;
    }
    
    if (lastTitle == nil || (lastTitle && ![lastTitle isEqualToString:title])) {
        [[NSUserDefaults standardUserDefaults] setObject:title forKey:kLastTitleKey];
        shouldRescheduleAd = YES;
    }
    
    if (lastMessage == nil || (lastMessage && ![lastMessage isEqualToString:message])) {
        [[NSUserDefaults standardUserDefaults] setObject:message forKey:kLastMessageKey];
        shouldRescheduleAd = YES;
    }
    
    if (lastYesButtonTitle == nil || (lastYesButtonTitle && ![lastYesButtonTitle isEqualToString:yesButtonTitle])) {
        [[NSUserDefaults standardUserDefaults] setObject:yesButtonTitle forKey:kLastYesButtonTitleKey];
        shouldRescheduleAd = YES;
    }
    
    if (lastNoButtonTitle == nil || (lastNoButtonTitle && ![lastNoButtonTitle isEqualToString:noButtonTitle])) {
        [[NSUserDefaults standardUserDefaults] setObject:noButtonTitle forKey:kLastNoButtonTitleKey];
        shouldRescheduleAd = YES;
    }
    
    if (lastURLString == nil || (lastURLString && ![lastURLString isEqualToString:urlString])) {
        [[NSUserDefaults standardUserDefaults] setObject:urlString forKey:kLastURLStringKey];
        shouldRescheduleAd = YES;
    }
    
    if (lastAppScheme == nil || (lastAppScheme && ![lastAppScheme isEqualToString:appScheme])) {
        [[NSUserDefaults standardUserDefaults] setObject:appScheme forKey:kLastAppSchemeKey];
        shouldRescheduleAd = YES;
    }
    
    if (lastDurationInString == nil || (lastDurationInString && [lastDurationInString intValue] != durationInSec)) {
        [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%d", durationInSec] forKey:kLastDurationKey];
        shouldRescheduleAd = YES;
    }
    
    if (shouldRescheduleAd) { // New ad data

        [self showAndCheck];
        
    } else {
        
        shouldShowPromoAds = [[NSUserDefaults standardUserDefaults] boolForKey:kShouldShowAdKey];
        if (shouldShowPromoAds) {
            NSTimeInterval lastTime = [[NSUserDefaults standardUserDefaults] integerForKey:kLastTimeShowAdKey];
            
            if ([[NSDate date] timeIntervalSince1970] >= lastTime) {
                [self showAndCheck];
            }
        }
    }
}

- (void) showAndCheck {
    if ([self hasAdData]) {
        [self show];
    }
    
    if (durationInSec == 0) {
        shouldShowPromoAds = NO;
        [[NSUserDefaults standardUserDefaults] setBool:shouldShowPromoAds forKey:kShouldShowAdKey];
        
    } else {
        lastTimeShowAd = [[NSDate date] timeIntervalSince1970];
        lastTimeShowAd += durationInSec;
        
        shouldShowPromoAds = YES;
        [[NSUserDefaults standardUserDefaults] setBool:shouldShowPromoAds forKey:kShouldShowAdKey];
        [[NSUserDefaults standardUserDefaults] setInteger:lastTimeShowAd forKey:kLastTimeShowAdKey];
    }
    
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void) start {
    int adVersion = 6;
    
    #if (__IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_6_0)
        NSString *UDID = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
    #else
        NSString *UDID = [[UIDevice currentDevice] uniqueIdentifier];
    #endif
    
    NSString *bundleId = [[NSBundle mainBundle] bundleIdentifier];
    NSString *osVersion = [[UIDevice currentDevice] systemVersion];
    
#if DEBUG
    NSString * bundleIdDEBUG = [NSString stringWithFormat:@"%@%@",bundleId,@"DEBUG"];
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://greengarstudios.com/ads/ads.php?ads_version=%d&udid=%@&bundle_id=%@&device=x86_64&platform=IOS&software=%@&ads_type=ALERT", adVersion, UDID, bundleIdDEBUG, osVersion]];
#else
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://greengarstudios.com/ads/ads.php?ads_version=%d&udid=%@&bundle_id=%@&device=x86_64&platform=IOS&software=%@&ads_type=ALERT", adVersion, UDID, bundleId, osVersion]];
#endif
    
    DLog(@"Start Alert Promo Ads with URL: %@", url);
    
    request = [[ASIHTTPRequest alloc] initWithURL:url];
    [request setDelegate:self];
    [request startAsynchronous];
}

- (void) show {
    if (![self isAppInstalled:appScheme]) {
        // Show alert
        UIAlertView *promoAlertView = [[UIAlertView alloc] initWithTitle:title
                                                                 message:message
                                                                delegate:self
                                                       cancelButtonTitle:noButtonTitle
                                                       otherButtonTitles:yesButtonTitle, nil];
        if (promoAlertView) {
            [promoAlertView show];
            [promoAlertView release];
        }
    }
}

- (void)requestFinished:(ASIHTTPRequest *)request_ {
    NSString *responseString = [request_ responseString];
    if ([responseString isKindOfClass:[NSString class]]) {

        BOOL finishParseData = [self parseAdsData:responseString];
        
        // Data loaded, now check cached data
        // If cached data matches, use the existing mechanism of load ratio
        // Otherwise, replace cached data with new data and reload the mechanism
        if (finishParseData && [self hasAdData]) {
            [self checkLocalCachedAdData];
        }
    }
}

- (void)requestFailed:(ASIHTTPRequest *)request_ {
    DLog(@"Alert Promo Ads requests failed");
}

- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == alertView.firstOtherButtonIndex) {
        if (urlString) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlString]];
        }
    }
}

- (NSInteger)parseAdsVersion:(NSString *)adsString {
    id responseObj = [self objectFromJSON:adsString];
    if ([responseObj isKindOfClass:[NSDictionary class]]) {
        return [[responseObj objectForKey:@"version"] intValue];
    }
    return 0;
}

- (BOOL)parseAdsData:(NSString *)adsString {
    id responseObj = [self objectFromJSON:adsString];
    if ([responseObj isKindOfClass:[NSDictionary class]]) {
        NSArray *adsInfo = [responseObj objectForKey:@"ads"];
        for (NSDictionary *adInfo in adsInfo) {
            if ([adInfo isKindOfClass:[NSDictionary class]]) {
                adID = [[NSString alloc] initWithFormat:@"%@", [adInfo objectForKey:@"id"]];
                title = [[NSString alloc] initWithFormat:@"%@", [adInfo objectForKey:@"title"]];
                message = [[NSString alloc] initWithFormat:@"%@", [adInfo objectForKey:@"message"]];
                urlString = [[NSString alloc] initWithFormat:@"%@", [adInfo objectForKey:@"action_yes"]];
                yesButtonTitle = [[NSString alloc] initWithFormat:@"%@", [adInfo objectForKey:@"action_yes_button"]];
                noButtonTitle = [[NSString alloc] initWithFormat:@"%@", [adInfo objectForKey:@"action_no_button"]];
                
                NSDictionary *appInfo = [adInfo objectForKey:@"app"];
                appScheme = [[appInfo objectForKey:@"url_scheme"] retain];
                
                NSDictionary *displayInfo = [adInfo objectForKey:@"display"];
                durationInSec = [[displayInfo objectForKey:@"interval"] intValue] * 3600; // 1 hours = 3600 sec
                
                return YES;
                
            } else if ([adInfo isKindOfClass:[NSString class]]) {
                DLog(@"error: adInfo = %@", adInfo);
                
            } else {
                DLog(@"error: adInfo is not an NSDictionary nor NSString");
            }
        }
    }
    
    return NO;
}

- (id)objectFromJSON:(NSString *)JSONString {
    SBJSON *jsonParser = [[[SBJSON alloc] init] autorelease];
    id respondingObject = [jsonParser objectWithString:JSONString];
    return respondingObject;
}

- (BOOL)isAppInstalled:(NSString *)protocolScheme {
    NSString *testScheme = [NSString stringWithFormat:@"%@://doX", protocolScheme];
    return [[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:testScheme]];
}

- (BOOL)hasAdData {
    return (title != nil) && (message != nil) && (yesButtonTitle != nil) && (noButtonTitle != nil) && (urlString != nil) && (appScheme != nil);
}

- (void)dealloc {
    [title release];
    [message release];
    [yesButtonTitle release];
    [noButtonTitle release];
    [urlString release];
    [appScheme release];
    [super dealloc];
}

@end
