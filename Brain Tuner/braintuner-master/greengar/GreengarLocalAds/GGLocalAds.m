//
//  GGLocalAds.m
//  Flashlight
//
//  Created by Leon Yuu on 9/25/12.
//  Copyright (c) 2012 GreenGar Studios. All rights reserved.
//

#import "GGLocalAds.h"
#import "DeviceHelper.h"

@implementation GGLocalAds
@synthesize adsVersion;
@synthesize deviceToken;
@synthesize bundleID;
@synthesize platForm;
@synthesize softWare;
@synthesize adsType;
@synthesize locationData;
@synthesize delegate;
@synthesize locationManager;


- (id) init{
    if(self = [super init]){
        DLog();
        
        // Init Request Data Here
        adsVersion = @"";
        deviceToken = @"";
        bundleID= @"";
        platForm = @"";
        softWare = @"";
        adsType = @"";
        locationData = @"";
        
        // Init Response Data Here
        responseType = @"";
        responseTitle = @"";
        responseMesssage = @"";
        responseClickedURL = @"";
        responseImageURL = @"";
        
        // Start Update Location
        locationManager = [[CLLocationManager alloc] init];
        locationManager.delegate = self;
        [locationManager startUpdatingLocation];
    }
    return self;
}

- (void) startRequest{
    // Init Request Values
    adsVersion = @"7";
    
#if (__IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_6_0)
    deviceToken = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
#else
    deviceToken = [DeviceHelper createNewUUID];
#endif
    
    bundleID = [[NSBundle mainBundle] bundleIdentifier];
    platForm = @"IOS";
    softWare = [[UIDevice currentDevice] systemVersion];
    adsType = @"BANNER";
    NSString * size = @"";
    if(IS_IPAD){
        size = @"728,90";
    }else{
        size = @"320,50";
    }
    // locationData is update in Core Location Delegate
    
    /*http://www.greengarstudios.com/ads/ads.php
     Params:
     ads_version=7
     device_token=....
     bundle_id=com.greengar.braintunerlite
     platform=IOS
     software=5.1
     ads_type=BANNER
     location=10.1114,106.1113
     */
     
    
#if DEBUG
    NSString * bundleIdDEBUG = [NSString stringWithFormat:@"%@%@",bundleID,@"DEBUG"];
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://www.greengarstudios.com/ads/ads.php?ads_version=%@&device_token=%@&bundle_id=%@&platform=%@&software=%@&ads_type=%@&location=%@&size=%@", adsVersion, deviceToken, bundleIdDEBUG,platForm,softWare,adsType,locationData,size]];
#else
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://www.greengarstudios.com/ads/ads.php?ads_version=%@&device_token=%@&bundle_id=%@&platform=%@&software=%@&ads_type=%@&location=%@&size=%@", adsVersion, deviceToken, bundleID,platForm,softWare,adsType,locationData,size]];
#endif
    DLog(@"GG Request URL %@", [url absoluteString]);
    
    httpRequest = [[ASIHTTPRequest alloc] initWithURL:url];
    [httpRequest setDelegate:self];
    [httpRequest startAsynchronous];
}

- (void) showAdsinView:(UIView *)root{
    if(root){
        
        float adWidth = 0;
        float adHeight = 0;
        
        if(IS_IPAD){
            adWidth = 728;
            adHeight = 90;
        }else{
            adWidth = 320;
            adHeight = 50;
        }
        
        DLog(@"%f - %f",root.frame.size.width,root.frame.size.height);
        egoLocalAdsView = [[EGOImageButton alloc] initWithPlaceholderImage:[UIImage imageNamed:@""]];
        egoLocalAdsView.frame = CGRectMake((root.frame.size.width - adWidth)/2, 0, adWidth, adHeight);
        [egoLocalAdsView addTarget:self action:@selector(bannerTapped:) forControlEvents:UIControlEventTouchUpInside];
        [root addSubview:egoLocalAdsView];
        egoLocalAdsView.imageURL = [NSURL URLWithString:responseImageURL];
        [root bringSubviewToFront:egoLocalAdsView];
    }
}

- (void)bannerTapped:(UIGestureRecognizer *)gestureRecognizer {
    DLog(@"");
    if(delegate && [((id) delegate) respondsToSelector:@selector(onGGLocalAdsTapInside:)]){
        [delegate onGGLocalAdsTapInside:responseClickedURL];
    }
}

- (void) dealloc{
    [adsVersion release];
    [deviceToken release];
    [bundleID release];
    [platForm release];
    [softWare release];
    [adsType release];
    [locationData release];
    [locationManager release];
    
    // Release Ego Local Adsview
    [egoLocalAdsView cancelImageLoad];
    [egoLocalAdsView release];
    
    [super dealloc];
}

#pragma mark - ASIHTTPRequest Delegate
- (void)requestFinished:(ASIHTTPRequest *)request{
    DLog(@"GG Local Ads Request Finished");
    NSString * responseString = [request responseString];
    DLog(@"GG Local Response %@",responseString);
    
    BOOL isShowLocalAds = NO;
    
    if ([responseString isKindOfClass:[NSString class]]) {
        BOOL finishParseData = [self parseAdsData:responseString];
        // Data loaded, now check cached data
        // If cached data matches, use the existing mechanism of load ratio
        // Otherwise, replace cached data with new data and reload the mechanism
        if (finishParseData && [self hasAdData]) {
            DLog(@"Has Data, will show Local Ads");
            isShowLocalAds = YES;
        }else{
            DLog(@"Has No Data, show Admob Only");
        }
    }
    
    if(delegate && [((id) delegate) respondsToSelector:@selector(showLocalAds:)]){
        [delegate showLocalAds:isShowLocalAds];
    }
}

- (void)requestFailed:(ASIHTTPRequest *)request{
    DLog(@"GG Local Ads Request Failed");
    if(delegate && [((id) delegate) respondsToSelector:@selector(showLocalAds:)]){
        [delegate showLocalAds:NO];
    }
}

#pragma mark - Core Location Delegate
- (void) locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation{
    locationData = [NSString stringWithFormat:@"%f,%f",newLocation.coordinate.latitude,newLocation.coordinate.longitude];
}

- (void) locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error{
    DLog(@"Location Error %@",error);
}

#pragma mark - Parse Json Data Functions
- (NSInteger)parseAdsVersion:(NSString *)adsString {
    id responseObj = [self objectFromJSON:adsString];
    if ([responseObj isKindOfClass:[NSDictionary class]]) {
        return [[responseObj objectForKey:@"version"] intValue];
    }
    return 0;
}

- (BOOL)parseAdsData:(NSString *)adsString {
    id responseObj = [self objectFromJSON:adsString];
    if ([responseObj isKindOfClass:[NSDictionary class]]) {
        NSArray *adsInfo = [responseObj objectForKey:@"ads"];
        for (NSDictionary *adInfo in adsInfo) {
            if ([adInfo isKindOfClass:[NSDictionary class]]) {
                responseType = [[NSString alloc] initWithFormat:@"%@", [adInfo objectForKey:@"type"]];
                responseTitle = [[NSString alloc] initWithFormat:@"%@", [adInfo objectForKey:@"title"]];
                responseMesssage = [[NSString alloc] initWithFormat:@"%@", [adInfo objectForKey:@"message"]];
                responseImageURL = [[NSString alloc] initWithFormat:@"%@", [adInfo objectForKey:@"image_url"]];
                responseClickedURL = [[NSString alloc] initWithFormat:@"%@", [adInfo objectForKey:@"url"]];
                
                DLog(@"Image URL %@",responseImageURL);
                DLog(@"Clicked URL %@", responseClickedURL);
                return YES;
                
            } else if ([adInfo isKindOfClass:[NSString class]]) {
                DLog(@"error: adInfo = %@", adInfo);
                
            } else {
                DLog(@"error: adInfo is not an NSDictionary nor NSString");
            }
        }
    }
    
    return NO;
}

- (id)objectFromJSON:(NSString *)JSONString {
    SBJsonParser *jsonParser = [[[SBJsonParser alloc] init] autorelease];
    id respondingObject = [jsonParser objectWithString:JSONString];
    return respondingObject;
}

- (BOOL)isAppInstalled:(NSString *)protocolScheme {
    NSString *testScheme = [NSString stringWithFormat:@"%@://doX", protocolScheme];
    return [[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:testScheme]];
}

- (BOOL)hasAdData {
    return (![responseType isEqualToString:@""] &&
            ![responseTitle isEqualToString:@""] &&
            ![responseMesssage isEqualToString:@""] &&
            ![responseImageURL isEqualToString:@""] &&
            ![responseClickedURL isEqualToString:@""]);
}

@end
