//
//  GGLocalAds.h
//  Flashlight
//
//  Created by Leon Yuu on 9/25/12.
//  Copyright (c) 2012 GreenGar Studios. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import "ASIHTTPRequest.h"
#import "EGOImageView.h"
#import "EGOImageButton.h"
#import "SBJson.h"

@protocol GGLocalAdsDelegate
- (void) showLocalAds:(BOOL) shouldShowLocalAds;
- (void) onGGLocalAdsTapInside:(NSString *) url;
@end

@interface GGLocalAds : NSObject <ASIHTTPRequestDelegate,CLLocationManagerDelegate>{
    
    // Request Data
    NSString * adsVersion;
    NSString * deviceToken;
    NSString * bundleID;
    NSString * platForm;
    NSString * softWare;
    NSString * adsType;
    NSString * locationData;
    
    // Link Data
    NSString * responseType;
    NSString * responseTitle;
    NSString * responseMesssage;
    NSString * responseClickedURL;
    NSString * responseImageURL;
 
    ASIHTTPRequest * httpRequest;
    UIView * rootView;
    EGOImageButton * egoLocalAdsView;
    id<GGLocalAdsDelegate> delegate;
    
    BOOL shouldShowLocalAds;
    
    // Core Location
    CLLocationManager * locationManager;
}

- (void) startRequest;
- (void) showAdsinView:(UIView*)root;

#pragma mark - Properties
@property (nonatomic, retain) NSString * adsVersion;
@property (nonatomic, retain) NSString * deviceToken;
@property (nonatomic, retain) NSString * bundleID;
@property (nonatomic, retain) NSString * platForm;
@property (nonatomic, retain) NSString * softWare;
@property (nonatomic, retain) NSString * adsType;
@property (nonatomic, retain) NSString * locationData;
@property (nonatomic, retain) id<GGLocalAdsDelegate> delegate;
@property (nonatomic, retain) CLLocationManager * locationManager;

@end
