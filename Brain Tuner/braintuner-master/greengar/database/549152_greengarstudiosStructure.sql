-- phpMyAdmin SQL Dump
-- version 2.11.11.1
-- http://www.phpmyadmin.net
--
-- Host: mysql50-74.wc2:3306
-- Generation Time: Jan 24, 2011 at 05:48 AM
-- Server version: 5.0.77
-- PHP Version: 5.2.13

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `549152_greengarstudios`
--

-- --------------------------------------------------------

--
-- Table structure for table `drawings`
--
-- Creation: Jan 20, 2011 at 09:49 PM
-- Last update: Jan 20, 2011 at 10:31 PM
--

DROP TABLE IF EXISTS `drawings`;
CREATE TABLE IF NOT EXISTS `drawings` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `user_id` bigint(20) unsigned default NULL,
  `user_login` varchar(60) default NULL,
  `display_name` varchar(250) default NULL,
  `model` varchar(255) NOT NULL,
  `date` datetime NOT NULL,
  `ip` int(10) unsigned NOT NULL,
  `deviceuid` varchar(255) NOT NULL,
  `comment` varchar(255) NOT NULL,
  `rating` tinyint(1) NOT NULL default '-1',
  `location` varchar(255) NOT NULL,
  `ratingcount` int(10) NOT NULL,
  `ratingavg` float NOT NULL,
  `md5` varchar(255) NOT NULL,
  `tags` varchar(255) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=87098 ;

-- --------------------------------------------------------

--
-- Table structure for table `ggs_bt_android_score`
--
-- Creation: Dec 29, 2010 at 02:28 AM
--

DROP TABLE IF EXISTS `ggs_bt_android_score`;
CREATE TABLE IF NOT EXISTS `ggs_bt_android_score` (
  `score_id` bigint(20) unsigned NOT NULL auto_increment,
  `user_id` bigint(20) unsigned NOT NULL,
  `user_login` varchar(60) character set utf8 NOT NULL,
  `display_name` varchar(250) character set utf8 default NULL,
  `mode` varchar(20) character set utf8 NOT NULL,
  `problems` tinyint(3) unsigned NOT NULL,
  `level` tinyint(3) unsigned NOT NULL,
  `score` float unsigned NOT NULL,
  `date` timestamp NULL default CURRENT_TIMESTAMP,
  `email` varchar(250) default NULL,
  PRIMARY KEY  (`score_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=64 ;

-- --------------------------------------------------------

--
-- Table structure for table `ggs_bt_android_standalone_score`
--
-- Creation: Dec 30, 2010 at 01:06 AM
--

DROP TABLE IF EXISTS `ggs_bt_android_standalone_score`;
CREATE TABLE IF NOT EXISTS `ggs_bt_android_standalone_score` (
  `score_id` bigint(20) unsigned NOT NULL auto_increment,
  `display_name` varchar(250) character set utf8 NOT NULL,
  `email` varchar(250) character set utf8 NOT NULL,
  `date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `mode` varchar(20) character set utf8 NOT NULL,
  `problems` tinyint(3) unsigned NOT NULL,
  `level` tinyint(3) unsigned NOT NULL,
  `score` float unsigned NOT NULL,
  PRIMARY KEY  (`score_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ggs_wb_friend`
--
-- Creation: Dec 28, 2010 at 01:50 AM
--

DROP TABLE IF EXISTS `ggs_wb_friend`;
CREATE TABLE IF NOT EXISTS `ggs_wb_friend` (
  `user_id` bigint(20) unsigned NOT NULL,
  `friend_id` bigint(20) unsigned NOT NULL,
  `user_login` varchar(60) character set utf8 NOT NULL,
  `friend_login` varchar(60) character set utf8 NOT NULL,
  `user_fb_id` longtext character set utf8 NOT NULL,
  `friend_fb_id` longtext character set utf8 NOT NULL,
  PRIMARY KEY  (`user_id`,`friend_id`),
  KEY `friend_id` (`friend_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ggs_wb_xmpp`
--
-- Creation: Jan 20, 2011 at 09:16 PM
--

DROP TABLE IF EXISTS `ggs_wb_xmpp`;
CREATE TABLE IF NOT EXISTS `ggs_wb_xmpp` (
  `user_id` bigint(20) unsigned NOT NULL,
  `user_login` varchar(60) character set utf8 NOT NULL,
  `xmpp_account` varchar(255) character set utf8 NOT NULL,
  `xmpp_domain` varchar(255) character set utf8 NOT NULL,
  PRIMARY KEY  (`user_id`,`xmpp_account`,`xmpp_domain`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `log`
--
-- Creation: Jan 16, 2011 at 10:18 PM
-- Last update: Jan 16, 2011 at 10:18 PM
--

DROP TABLE IF EXISTS `log`;
CREATE TABLE IF NOT EXISTS `log` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `date` datetime NOT NULL,
  `ip` int(10) unsigned NOT NULL,
  `deviceuid` varchar(255) NOT NULL,
  `request` varchar(255) NOT NULL,
  `response` text NOT NULL,
  `runcount` int(10) unsigned NOT NULL,
  `version` varchar(255) NOT NULL,
  `systemversion` varchar(255) NOT NULL,
  `platform` varchar(255) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

-- --------------------------------------------------------

--
-- Table structure for table `moderated`
--
-- Creation: Jan 16, 2011 at 10:18 PM
-- Last update: Jan 16, 2011 at 10:18 PM
--

DROP TABLE IF EXISTS `moderated`;
CREATE TABLE IF NOT EXISTS `moderated` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `ip` int(10) unsigned NOT NULL,
  `deviceuid` varchar(255) NOT NULL,
  `approved` int(10) NOT NULL,
  `deleted` int(10) NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `ip` (`ip`,`deviceuid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

-- --------------------------------------------------------

--
-- Table structure for table `promo`
--
-- Creation: Jan 16, 2011 at 10:18 PM
-- Last update: Jan 16, 2011 at 10:18 PM
--

DROP TABLE IF EXISTS `promo`;
CREATE TABLE IF NOT EXISTS `promo` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `date` datetime NOT NULL,
  `ip` int(10) unsigned NOT NULL,
  `deviceuid` varchar(255) NOT NULL,
  `runcount` int(10) unsigned NOT NULL,
  `version` varchar(255) NOT NULL,
  `systemversion` varchar(255) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3469892 ;

-- --------------------------------------------------------

--
-- Table structure for table `ratings`
--
-- Creation: Jan 23, 2011 at 08:08 PM
-- Last update: Jan 23, 2011 at 08:08 PM
--

DROP TABLE IF EXISTS `ratings`;
CREATE TABLE IF NOT EXISTS `ratings` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `user_id` bigint(20) default NULL,
  `user_login` varchar(60) default NULL,
  `drawingid` int(10) unsigned NOT NULL,
  `useragent` varchar(255) NOT NULL,
  `date` datetime NOT NULL,
  `ip` int(10) NOT NULL,
  `rating` int(10) NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `drawingid` (`drawingid`,`ip`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5929 ;

-- --------------------------------------------------------

--
-- Table structure for table `scores`
--
-- Creation: Dec 22, 2010 at 01:29 AM
-- Last update: Dec 22, 2010 at 01:33 AM
-- Last check: Dec 22, 2010 at 01:29 AM
--

DROP TABLE IF EXISTS `scores`;
CREATE TABLE IF NOT EXISTS `scores` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `name` varchar(255) collate latin1_general_ci NOT NULL default '',
  `email` varchar(255) collate latin1_general_ci NOT NULL default '',
  `score` double NOT NULL default '0',
  `problems` varchar(255) collate latin1_general_ci NOT NULL default '',
  `deviceuid` varchar(255) collate latin1_general_ci NOT NULL default '',
  `ip` int(10) unsigned NOT NULL default '0',
  `date` datetime NOT NULL default '0000-00-00 00:00:00',
  `hash` varchar(255) collate latin1_general_ci NOT NULL default '',
  `version` varchar(255) collate latin1_general_ci NOT NULL default '',
  `problemarray` text collate latin1_general_ci NOT NULL,
  `d` varchar(255) collate latin1_general_ci NOT NULL default '',
  `cc` char(2) collate latin1_general_ci NOT NULL,
  `level` int(11) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `score` (`score`),
  KEY `ipaddress` (`ip`),
  KEY `level` (`level`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=349002 ;

-- --------------------------------------------------------

--
-- Table structure for table `wcdpropromo`
--
-- Creation: Jan 16, 2011 at 10:18 PM
-- Last update: Jan 16, 2011 at 10:18 PM
--

DROP TABLE IF EXISTS `wcdpropromo`;
CREATE TABLE IF NOT EXISTS `wcdpropromo` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `date` datetime NOT NULL,
  `ip` int(10) unsigned NOT NULL,
  `deviceuid` varchar(255) NOT NULL,
  `runcount` int(10) unsigned NOT NULL,
  `promoid` int(10) unsigned NOT NULL,
  `promosubid` int(10) unsigned NOT NULL,
  `version` varchar(255) NOT NULL,
  `systemversion` varchar(255) NOT NULL,
  `platform` varchar(255) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=19289 ;

-- --------------------------------------------------------
