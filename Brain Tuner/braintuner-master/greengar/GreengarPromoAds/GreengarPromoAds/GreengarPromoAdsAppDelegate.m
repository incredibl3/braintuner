//
//  GreengarPromoAdsAppDelegate.m
//  GreengarPromoAds
//
//  Created by Cong Vo on 5/27/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "GreengarPromoAdsAppDelegate.h"

#import "GreengarPromoAdsViewController.h"
#import "PromoAds.h"
#import "EnviromentDetector.h"

@implementation GreengarPromoAdsAppDelegate


@synthesize window=_window;

@synthesize viewController=_viewController;


- (void)startPromoAds {
    [_promoAds startPromoAds:self.viewController];    
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    DLog();
    // Override point for customization after application launch.
     
    [self.window addSubview:self.viewController.view];
    [self.window makeKeyAndVisible];
    
    [self testEnvironment];
    
    _promoAds = [[PromoAds alloc] init];
    [self startPromoAds];

    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    /*
     Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
     Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
     */
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    /*
     Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
     If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
     */
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    DLog();
    /*
     Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
     */

    [self startPromoAds];
}

//- (void)testAppInstalled {
//    BOOL isInstalled = [EnviromentDetector isAppInstalled:@"ggs-WhiteboardiPad"];
//    DLog(@"ggs-WhiteboardiPad: %@", (isInstalled) ? @"installed" : @"not installed");
//    //KONG: ok
//}
//
//
- (void)testEnvironment {
//    [self testAppInstalled];
    [self testVersionCompare];
}



- (void)applicationDidBecomeActive:(UIApplication *)application {
    /*
     Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
     */
//    [self performSelector:@selector(showPromoAds) withObject:nil afterDelay:2.0];
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    /*
     Called when the application is about to terminate.
     Save data if appropriate.
     See also applicationDidEnterBackground:.
     */
}

- (void)dealloc
{
    [_window release];
    [_viewController release];
    [super dealloc];
}

- (BOOL)isVersion:(NSString *)receiver notLowerThan:(NSString *)minVersion {
    return ([receiver compare:minVersion options:NSNumericSearch] != NSOrderedAscending);
}

- (void)testVersionCompare {
    NSAssert([self isVersion:@"3.1" notLowerThan:@"3.0"], @"Should be");
    NSAssert([self isVersion:@"3.1.3" notLowerThan:@"3.0"], @"Should be");
    NSAssert([self isVersion:@"3.1.3" notLowerThan:@"3.1"], @"Should be");
    NSAssert([self isVersion:@"3.1" notLowerThan:@"3.1"], @"Should be");
    NSAssert([self isVersion:@"3.1.3" notLowerThan:@"3.1.3"], @"Should be");
    NSAssert([self isVersion:@"3.0" notLowerThan:@"3.1.3"] == NO, @"Should not be");
    NSAssert([self isVersion:@"30.0" notLowerThan:@"3.1.3"], @"Should be");    
    NSAssert([self isVersion:@"30.0.1" notLowerThan:@"3.1.3"], @"Should be");   
    DLog();
}
@end
