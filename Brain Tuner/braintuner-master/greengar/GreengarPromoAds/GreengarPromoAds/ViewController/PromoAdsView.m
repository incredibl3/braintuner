//
//  PromoAdsView.m
//  GreengarPromoAds
//
//  Created by Cong Vo on 5/27/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "PromoAdsView.h"


@implementation PromoAdsView
@synthesize pilotView   = _pilotView,
            bodyView    = _bodyView,
            delegate    = _delegate; //TODO: do we need this delegate pointer?

- (id)init {
    self = [super initWithNibName:@"PromoAdsView" bundle:nil];
    if (self) {
    }
    return self;
}

- (void)dealloc {
    [_pilotView release];
    [_bodyView release];

    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

// Implement loadView to create a view hierarchy programmatically, without using a nib.
//- (void)loadView {
//    
//}

static CGFloat const kAnimationDurationPilotViewIn = 3;

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];

    //KONG: add pilot view
    self.pilotView = [[[AdsPilotView alloc] init] autorelease];
    self.view.backgroundColor = [UIColor blackColor];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(hideAdsView:) 
                                                 name:kNotificationCloseAdsView
                                               object:nil];

}

- (void)hideAdsView:(NSNotification *)notification {
    //KONG: animation in
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:kAnimationDurationPilotViewIn/2];
    _bodyView.view.alpha = 0;
    [UIView commitAnimations];
    
    
    //KONG: animation in
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:kAnimationDurationPilotViewIn];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(promoAdsViewDidHide)];
    self.view.alpha = 0;
	
    [UIView commitAnimations];
}

- (void)promoAdsViewDidHide {
//    [self dismissModalViewControllerAnimated:NO];
    [self.view removeFromSuperview];
    
//    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationAdViewHidden
//                                                        object:nil];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)show:(UIViewController *)viewController {
    DLog();
    self.view.alpha = 0;    
    [self viewWillAppear:NO];
    
    [viewController.view addSubview:self.view];

    //KONG: animation in
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:kAnimationDurationPilotViewIn];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(promoAdsViewDidShow)];
    self.view.alpha = 0.7;
	
    [UIView commitAnimations];
    [self viewDidAppear:NO];
}
     
- (void)promoAdsViewDidShow {
    DLog();
    //KONG: show pilot view
    
    CGRect pilotFrame = _pilotView.view.frame;
    pilotFrame.origin.y = self.view.frame.size.height - pilotFrame.size.height;
    _pilotView.view.frame = pilotFrame;
    [self.view addSubview:_pilotView.view];
    
    //KONG: animation in
    CGRect startingFrame = pilotFrame;
    startingFrame.origin.y = 400;
    _pilotView.view.frame = startingFrame;
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:kAnimationDurationPilotViewIn];	

    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(promoAdsPilotDidShow)];
    
    _pilotView.view.frame = pilotFrame;
	
    [UIView commitAnimations];   
}
     
- (void)promoAdsPilotDidShow {
    //KONG: update pilot view
    
    _pilotView.hintTitle.text = @"BrainTuner2";
    _pilotView.hintDescription.text = @"Stimulate your brain everyday";
    
    
    //KONG: show ads body
    
    self.bodyView = [[[AdsBodyView alloc] init] autorelease];

    [self.view addSubview:_bodyView.view];
    [self.view bringSubviewToFront:_pilotView.view];
//    CGRect pilotFrame = _pilotView.view.frame;
//    pilotFrame.origin.y = self.view.frame.size.height - pilotFrame.size.height;
//    _pilotView.view.frame = pilotFrame;
//    [self.view addSubview:_pilotView.view];
//    
//    //KONG: animation in
//    CGRect startingFrame = pilotFrame;
//    startingFrame.origin.x = 280;
//    _pilotView.view.frame = startingFrame;
//    
//    [UIView beginAnimations:nil context:nil];
//    [UIView setAnimationDuration:kAnimationDurationPilotViewIn];	
//    
//    [UIView setAnimationDelegate:self];
//    [UIView setAnimationDidStopSelector:@selector(promoAdsViewDidShow)];
//    
//    _pilotView.view.frame = pilotFrame;
//	
//    [UIView commitAnimations];  
}

- (void)cacheAd {
    [self retain];
    DLog();
}

- (void)adCached {
    DLog();
    [self autorelease];
}
@end
