//
//  AdsBodyView.m
//  GreengarPromoAds
//
//  Created by Cong Vo on 5/27/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "AdsBodyView.h"
#import <QuartzCore/QuartzCore.h>

@implementation AdsBodyView
@synthesize closeButton = _closeButton,
            adsView     = _adsView;

- (id)init {
    self = [super initWithNibName:@"AdsBodyView" bundle:nil];
    if (self) {
        
    }
    return self;
}

- (void)dealloc {
    [_closeButton release];
    [_adsView release];

    [super dealloc];
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    _adsView.layer.cornerRadius = 13;
    
    
    NSString *adsURLString = @"http://184.106.64.162/xfade/";
//    adsURLString = @"http://vnexpress.net";
    [_adsView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:adsURLString]]];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (IBAction)closeButtonPressed {
    DLog();
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationCloseAdsView object:nil];
}

@end
