//
//  PromoAdsView.h
//  GreengarPromoAds
//
//  Created by Cong Vo on 5/27/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AdsPilotView.h"
#import "AdsBodyView.h"
#import "PromoAds.h"



@interface PromoAdsView : UIViewController <AdView> {
    AdsPilotView *_pilotView;
    AdsBodyView *_bodyView;
}

@property (nonatomic, retain) AdsPilotView *pilotView;
@property (nonatomic, retain) AdsBodyView *bodyView;

- (void)show:(UIViewController *)viewController;
- (void)adCached;
@end
