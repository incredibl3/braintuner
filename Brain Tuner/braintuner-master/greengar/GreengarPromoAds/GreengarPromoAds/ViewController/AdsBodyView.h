//
//  AdsBodyView.h
//  GreengarPromoAds
//
//  Created by Cong Vo on 5/27/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

static NSString * const kNotificationCloseAdsView = @"kNotificationCloseAdsView";

@interface AdsBodyView : UIViewController {
    UIButton *_closeButton;
    UIWebView *_adsView;
}
@property (nonatomic, retain) IBOutlet UIButton *closeButton;
@property (nonatomic, retain) IBOutlet UIWebView *adsView;

- (IBAction)closeButtonPressed;

@end
