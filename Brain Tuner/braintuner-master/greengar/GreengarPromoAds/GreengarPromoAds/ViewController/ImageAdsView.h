//
//  ImageAdsView.h
//  GreengarPromoAds
//
//  Created by Cong Vo on 5/31/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EGOImageView.h"
#import "AdInfo.h"
#import "PromoAds.h"



@interface ImageAdsView : UIViewController <AdView, EGOImageViewDelegate>{
    AdInfo *_ad;
    EGOImageView *_imageView;
    
    id <AdViewDelegate> _delegate;
}

@property (nonatomic, retain) AdInfo *ad;
@property (nonatomic, retain) IBOutlet EGOImageView *imageView;


- (id)initWithAd:(AdInfo *)ad;

- (void)show:(UIViewController *)viewController;
- (void)hideAdsView;

- (IBAction)closeButtonPressed;
- (IBAction)adButtonPressed;

- (void)cacheAd;
- (void)adCached;
@end
