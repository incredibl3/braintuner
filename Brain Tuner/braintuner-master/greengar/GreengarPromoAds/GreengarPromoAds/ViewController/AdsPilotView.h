//
//  AdsPilotView.h
//  GreengarPromoAds
//
//  Created by Cong Vo on 5/27/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface AdsPilotView : UIViewController {
    UILabel *_hintTitle;
    UILabel *_hintDescription;
}

@property (nonatomic, retain) IBOutlet UILabel *hintTitle, *hintDescription;

@end
