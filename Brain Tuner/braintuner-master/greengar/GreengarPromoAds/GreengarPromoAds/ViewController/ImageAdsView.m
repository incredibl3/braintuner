//
//  ImageAdsView.m
//  GreengarPromoAds
//
//  Created by Cong Vo on 5/31/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "ImageAdsView.h"
#import "AdsData.h"

#if LITE_MODE // for Brain Tuner 1 Lite compatibility
    #define LITE 1
#endif

#if LITE
    #import "GSAdView.h"
#endif

@implementation ImageAdsView
@synthesize imageView   = _imageView,
            ad          = _ad,
            delegate    = _delegate;

- (id)initWithAd:(AdInfo *)ad {
    self = [super initWithNibName:@"ImageAdsView" bundle:nil];
    if (self) {
        // Custom initialization
        _ad = [ad retain];
    }
    return self;
}

- (void)dealloc {
    [_ad release];
    [_imageView release];
    [super dealloc];
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad {
    DLog();
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    _imageView.delegate = self;
    _imageView.imageURL = [NSURL URLWithString:_ad.imageURL];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark show/hide
static CGFloat const kAnimationDurationPilotViewIn = 1;
static CGFloat const kAnimationDurationPilotViewOut = 1;
- (IBAction)closeButtonPressed {
    [self hideAdsView];
    if (_delegate) {
        [_delegate ad:_ad clicked:NO];
    }
}


- (IBAction)adButtonPressed {
    [self hideAdsView];
    if (_delegate) {
        [_delegate ad:_ad clicked:YES];
    }
}

// This method is called to show a full-screen promo ad.
//   kAnimationDurationPilotViewIn determines the animation duration.
- (void)show:(UIViewController *)viewController {
    DLog();
//    _isAdsShown = YES;
    //self.view.alpha = 0;    
    [self viewWillAppear:NO];
 
    //Preload
    [viewController view];
    
    // Hector: resize ad view to fit the screen
    [self.view setFrame:CGRectMake(0, 0, viewController.view.frame.size.width, viewController.view.frame.size.height)];
    
    //[viewController.view addSubview:self.view];
    
    //KONG: animation in
    [UIView beginAnimations:nil context:NULL];
    
    [UIView setAnimationTransition:UIViewAnimationTransitionCurlDown forView:viewController.view cache:YES];
    
    [UIView setAnimationDuration:kAnimationDurationPilotViewIn];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(promoAdsViewDidShow)];
    
    //self.view.alpha = 1;
    
    [viewController.view addSubview:self.view];
    
    [UIView commitAnimations];
    [self viewDidAppear:NO];
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationHideGSAdsView object:nil];    
}


- (void)hideAdsView {    
    //KONG: animation in
    [UIView beginAnimations:nil context:NULL];
    
    [UIView setAnimationTransition:UIViewAnimationTransitionCurlUp forView:[self.view superview] cache:YES];
    
    [UIView setAnimationDuration:kAnimationDurationPilotViewOut];
    [UIView setAnimationDelegate:self];
    //[UIView setAnimationDidStopSelector:@selector(promoAdsViewDidHide)];
    //self.view.alpha = 0;
    
    [self.view removeFromSuperview];
	
    [UIView commitAnimations];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationShowGSAdsView object:nil];
}

- (void)promoAdsViewDidShow {
    
}

//- (void)promoAdsViewDidHide {
//    [self.view removeFromSuperview];
//}

- (void)imageViewLoadedImage:(EGOImageView*)imageView {
    DLog();
    //KONG: finished caching image
    [self adCached];
}
- (void)imageViewFailedToLoadImage:(EGOImageView*)imageView error:(NSError*)error {
    DLog();    
}

#pragma mark cacheView
- (void)cacheAd {
    DLog();
    //KONG: Force view load
    [self retain];
    [self view];
}

- (void)adCached {
    DLog();
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationAdCached object:_ad];    
    [self autorelease];
    
}

- (void)hideAd {
    [self hideAdsView];
}

@end
