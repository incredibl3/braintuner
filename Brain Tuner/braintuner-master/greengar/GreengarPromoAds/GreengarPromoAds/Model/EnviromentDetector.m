//
//  EnviromentDetector.m
//  GreengarPromoAds
//
//  Created by Cong Vo on 5/30/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "EnviromentDetector.h"
#import "IdleDetectApplication.h"
#import "Reachability.h"
#import "GSDeviceConfiguration.h"
#import "AdsData.h"

static NSString *const kPromoAdsIdleNotification = @"PromoAdsIdleNotification";

@implementation EnviromentDetector


- (BOOL)isAppInstalled:(NSString *)protocolScheme {
    NSString *testScheme = [NSString stringWithFormat:@"%@://doX", protocolScheme];
    return [[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:testScheme]];
}

#pragma mark Idle detection
static BOOL isRegisterIdleNotification;
static BOOL isRegisterEventIdleNotification;

- (void)registerIdleNotification {
    
    //KONG: observe
    if (isRegisterIdleNotification == NO) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveIdleNotification:) 
                                                     name:kPromoAdsIdleNotification object:nil];        
        isRegisterIdleNotification = YES;
    }
    
    
    //KONG: queue
    NSNotification* myNotification = [NSNotification notificationWithName:kPromoAdsIdleNotification
                                                                   object:nil];
    [[NSNotificationQueue defaultQueue] enqueueNotification:myNotification postingStyle:NSPostWhenIdle];
    
    
}

- (void)receiveIdleNotification:(NSNotification *)notification {
    DLog();
    [self performSelector:@selector(registerIdleNotification:callback:) withObject:nil afterDelay:10];
}

- (void)registerIdleNotification:(NSNotification *)notification callback:(id)callback {
    
}

- (void)registerEventIdleNotification {
    if (isRegisterEventIdleNotification == NO) {
        [[NSNotificationCenter defaultCenter] addObserver:self 
                                                 selector:@selector(receiveEventIdleNotification:) 
                                                     name:kNotificationApplicationInIdle object:nil];        
        isRegisterEventIdleNotification = YES;
    }

    
    IdleDetectApplication *application = (IdleDetectApplication *)[UIApplication sharedApplication];
    [application scheduleIdleNotification:10];
}

- (void)receiveEventIdleNotification:(NSNotification *)notification {
    DLog();
    if (_delegate) {
        [_delegate performSelector:_callback];
    }
}


- (void)registerIdle:(id)delegate callback:(SEL)callback {
    _delegate = delegate;
    _callback = callback;
    [self registerEventIdleNotification];
}


- (BOOL)isInternetReachable {
//    DLog(@"%d", [[Reachability reachabilityForInternetConnection] currentReachabilityStatus]);
    return ([[Reachability reachabilityForInternetConnection] currentReachabilityStatus] != NotReachable);
}

- (BOOL)isGreengarAdsHostReachable {
    return ([[Reachability reachabilityWithHostName:kAdsInfoHost] currentReachabilityStatus] != NotReachable);
}

#pragma mark Share Infomation

- (void)set:(id)value type:(NSString *)type toAdsPasteBoard:(NSString *)adsID {
	UIPasteboard *appPasteBoard = [UIPasteboard pasteboardWithName:@"com.greengar.promoads" 
                                                            create:YES];
	appPasteBoard.persistent = YES;
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:value];
    
	[appPasteBoard setData:data forPasteboardType:
     [NSString stringWithFormat:@"com.greengar.promoads.%@.%@", adsID, type]];
}


- (id)value:(NSString *)type fromAdsPasteBoard:(NSString *)adsID {
    UIPasteboard *appPasteBoard = [UIPasteboard pasteboardWithName:@"com.greengar.promoads" 
                                                            create:YES];
	appPasteBoard.persistent = YES;
	NSData *data = [appPasteBoard dataForPasteboardType:
            [NSString stringWithFormat:@"com.greengar.promoads.%@.%@", adsID, type]];
    if (data == nil) {
        return nil;
    }
    return [NSKeyedUnarchiver unarchiveObjectWithData:data];
}

static NSString *const kAdsDateType = @"AdsDateType";

- (void)setAdsIsShown:(NSString *)adsID {
    DLog();
    [self set:[NSDate date] type:kAdsDateType toAdsPasteBoard:adsID];
}


- (BOOL)isAds:(NSString *)adsID shownWithIn:(NSInteger)hours {
    NSDate *lastShownDate = [self value:kAdsDateType fromAdsPasteBoard:adsID];
    
    if (lastShownDate == nil || [lastShownDate isKindOfClass:[NSDate class]] == NO) {
        return NO;
    }
    
    // Return NO if lastShownDate is after current.
    //     Since lastShownDate is supposed to be in the past,
    //   this means lastShownDate is invalid, so we don't use it.
    //     This can occur if the user sees an ad when their system date is invalid,
    //   e.g. Jan 1, 2099, and then they correct their date. If we don't do this,
    //   this user would never see any promo ad in this app ever again, because
    //   lastShownDate would be in the distant future.
    if ([lastShownDate compare:[NSDate date]] == NSOrderedDescending) {
        DLog(@"lastShownDate is after current date; ignoring invalid lastShownDate.");
        return NO;
    }
    
    NSDate *nextShowDate = [lastShownDate dateByAddingTimeInterval:hours * 3600];
    DLog(@"nextShowDate %@, today: %@", nextShowDate, [NSDate date]);
    return ([nextShowDate compare:[NSDate date]] == NSOrderedDescending); // next > current => false
}

#pragma mark Condition check

- (BOOL)checkForAdDisplayCondition:(AdInfo *)ad {
    
    if (ad.platform != DevicePlatformIOS) {
        return NO;
    }
        
    //KONG: check app scheme
    if ([self isAppInstalled:ad.appScheme] == YES) {
        DLog(@"ad: %@ - app installed", ad.appName);
        return NO;
    }
    
    //KONG: check ad show in X hours
    if ([self isAds:ad.appScheme shownWithIn:ad.displayInterval]) {
        DLog(@"ad: %@ - shownRecent", ad.appName);        
        return NO;
    }
    
    if ([self isInternetReachable] == NO) {
        DLog(@"ad: %@ - isInternetReachable:NO", ad.appName);
        return NO;
    }
    
    DLog(@"current ios version: %@", [GSDeviceConfiguration iOSVersion]);
    
    if ([GSDeviceConfiguration iOSVersionLaterThan:ad.minIOS] == NO) {
        DLog(@"ad: %@ - iOSVersionLaterThan %@:NO", ad.appName, ad.minIOS);
        return NO;
    }
    
    return YES;
}

@end
