//
//  IdleDetectApplication.m
//  GreengarPromoAds
//
//  Created by Cong Vo on 5/30/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "IdleDetectApplication.h"




@implementation IdleDetectApplication

static CGFloat maxIdleTime;

+ (void)initialize {
    [super initialize];
    maxIdleTime = -1;
}


//KONG: link: http://stackoverflow.com/questions/273450/iphone-detecting-user-inactivity-idle-time-since-last-screen-touch
- (void)sendEvent:(UIEvent *)event {
//    DLog();
    [super sendEvent:event];
    
    if (maxIdleTime > 0) {
        // Only want to reset the timer on a Began touch or an Ended touch, to reduce the number of timer resets.
        NSSet *allTouches = [event allTouches];
        if ([allTouches count] > 0) {
            // allTouches count only ever seems to be 1, so anyObject works here.
            UITouchPhase phase = ((UITouch *)[allTouches anyObject]).phase;
            if (phase == UITouchPhaseBegan || phase == UITouchPhaseEnded)
                [self resetIdleTimer];
        }        
    }
    
}

static NSTimer *idleTimer;



- (void)resetIdleTimer {
//    DLog();
    [self cancelIdleNotification];
    
    idleTimer = [[NSTimer scheduledTimerWithTimeInterval:maxIdleTime target:self selector:@selector(idleTimerExceeded) userInfo:nil repeats:NO] retain];
}

- (void)idleTimerExceeded {
    NSLog(@"idle time exceeded");
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationApplicationInIdle
                                                        object:nil];
}


- (void)scheduleIdleNotification:(CGFloat)idleTime {
    maxIdleTime = idleTime;
    [self resetIdleTimer];
}

- (void)cancelIdleNotification {
    if (idleTimer) {
        [idleTimer invalidate];
        [idleTimer release];
    }   
}

@end