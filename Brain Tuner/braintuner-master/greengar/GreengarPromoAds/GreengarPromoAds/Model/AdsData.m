//
//  AdsNetworking.m
//  GreengarPromoAds
//
//  Created by Cong Vo on 5/31/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "AdsData.h"
#import "ASIHTTPRequest.h"
#import "JSON.h"
#import "EGOCache.h"
#import "GSDeviceConfiguration.h"
#import "DeviceHelper.h"

@implementation AdsData
@synthesize ads     = _ads,
            adsVersion  = _adsVersion;

- (id)init {
    self = [super init];
    if (self) {
        [[NSNotificationCenter defaultCenter] addObserver:self 
                                                 selector:@selector(adCached:) 
                                                     name:kNotificationAdCached
                                                   object:nil];
    }
    return self;
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [_ads release];
    [super dealloc];
}

- (NSString *)addToURL:(NSString *)url trackingParams:(AdInfo *)ad clicked:(BOOL)isClicked {
    NSMutableString *adsURL = [NSMutableString stringWithFormat:@"%@?ads_version=%d", 
                               url, (int)_adsVersion];
    
    if ([_ads count] > 0) {
        [adsURL appendFormat:@"&ads=[%@", [[_ads objectAtIndex:0] adID]];
        for (int i = 1; i < [_ads count]; i++) {
            [adsURL appendFormat:@",%@", [[_ads objectAtIndex:i] adID]];            
        }
        [adsURL appendString:@"]"];
    }
    
    if (ad) {
        [adsURL appendFormat:@"&shown_ad_id=%@&is_ad_clicked=%d", ad.adID, isClicked];
    }
    
    //KONG: more device info
#if (__IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_6_0)
    NSString *udid = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
#else
    NSString *udid = [DeviceHelper createNewUUID];
#endif
    
    if (udid) {
        [adsURL appendFormat:@"&udid=%@", udid];
    }
    
    //ELLIOT: this is app info
    NSString *bundleID = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleIdentifier"];
#if DEBUG || DL_DEBUG
    bundleID = [bundleID stringByAppendingString:@"DEBUG"]; // for testing
    DLog(@"CFBundleIdentifier:%@ -> %@", [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleIdentifier"], bundleID);
#endif
    if (bundleID) {
        [adsURL appendFormat:@"&bundle_id=%@", bundleID];
    }
    
    //KONG: more device info
    NSString *device = [GSDeviceConfiguration platform];
    if (udid) {
        [adsURL appendFormat:@"&device=%@", device];
    }

    [adsURL appendFormat:@"&platform=IOS"];
    
    NSString *software = [GSDeviceConfiguration iOSVersion];
    if (udid) {
        [adsURL appendFormat:@"&software=%@", software];
    }
    
    return adsURL;
}

- (void)retrieveAdsInfo:(id <AdsDataDelegate>)delegate 
               callback:(SEL)callback
                trackAd:(AdInfo *)ad 
                clicked:(BOOL)isClicked {
    
    _delegate = delegate;
    _callback = callback;
    
    NSString *adsURL = [self addToURL:kAdsInfoURL trackingParams:ad clicked:isClicked];
    
    DLog(@"adsURL: %@", adsURL);
    ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:[NSURL URLWithString:adsURL]];
    [request setDelegate:self];
    [request startAsynchronous];}



- (id)objectFromJSON:(NSString *)JSONString {
//    NSError *error = nil;
    
    // Parse
    SBJsonParser *jsonParser = [[[SBJsonParser alloc] init] autorelease];
    
    //id respondingObject = [jsonParser objectWithString:JSONString error:&error];
    
    id respondingObject = [jsonParser objectWithString:JSONString];
    
    /*
	if (error != nil) {
        return nil;
	}
    */
    return respondingObject;
}

/*
{
    ads =     (
               {
                   api = "<null>";
                   control =             {
                       "per_24h" = 24;
                       "per_launch" = 1;
                   };
                   description = "Stimulate your brain everyday!";
                   device =             (
                                         IPHONE,
                                         IPOD,
                                         IPAD
                                         );
                   expiration = "2011-06-30 09:05:09";
                   id = 1;
                   name = "Brain Tuner 2";
                   platform = IOS;
                   ratio = "3:4";
                   resolution = "640:960";
                   software = "3.0";
                   type = WEB;
                   url = "http://www.greengarstudios.com/ads/braintuner2/ad_id=1&device_id=";
                   version = "2.0";
               }
               );
    version = 1;
}
*/

- (NSInteger)parseAdsVersion:(NSString *)adsString {
    id responseObj = [self objectFromJSON:adsString]; 
    if ([responseObj isKindOfClass:[NSDictionary class]]) {
        return [[responseObj objectForKey:@"version"] intValue];
    }
    return 0;
}

- (NSArray *)parseAdsData:(NSString *)adsString {
    id responseObj = [self objectFromJSON:adsString];
    NSMutableArray *ads = [NSMutableArray array];
    if ([responseObj isKindOfClass:[NSDictionary class]]) {
        NSArray *adsInfo = [responseObj objectForKey:@"ads"];
        for (NSDictionary *adInfo in adsInfo) {
            if ([adInfo isKindOfClass:[NSDictionary class]]) {
                AdInfo *ad = [[[AdInfo alloc] initWithDictionary:adInfo] autorelease];
                if (ad) {
                    [ads addObject:ad];
                    DLog(@"added ad from adInfo = %@", adInfo);
                }
            } else if ([adInfo isKindOfClass:[NSString class]]) {
                DLog(@"error: adInfo = %@", adInfo);
            } else {
                DLog(@"error: adInfo is not an NSDictionary nor NSString");
            }
        }
    }
    return [NSArray arrayWithArray:ads];
}

static NSString *const kPreferenceAdsInfo = @"kPreferenceAdsInfo";

- (void)parseAdsString:(NSString *)adsString {
    NSInteger newAdsVersion = [self parseAdsVersion:adsString];
    NSArray *newAdsData = [self parseAdsData:adsString];
    
    if (newAdsData == nil) {
        return;
    }
    
    if ([newAdsData count] == 0) {
        
        // clear cache data
        [[EGOCache currentCache] clearCache];
        [[NSUserDefaults standardUserDefaults] setObject:nil forKey:kPreferenceAdsInfo];
        [[NSUserDefaults standardUserDefaults] synchronize];
        //[self.ads release];
        //self.ads = nil;
        // override ads in memory
        _adsVersion = newAdsVersion;
        self.ads = newAdsData;
        [self cacheAdsInfo:adsString];
        
    } else {
        
        //if (_adsVersion != newAdsVersion) {
            _adsVersion = newAdsVersion;
            self.ads = newAdsData;
            if (_delegate) {
                [_delegate didUpdateAdsData:YES];
            }
            [self cacheAdsInfo:adsString];
        //} else {
        //    if (_delegate) {
        //        [_delegate didUpdateAdsData:NO];
        //    }
        //}
        
    }
}

- (void)requestFinished:(ASIHTTPRequest *)request {
    // Use when fetching text data
    NSString *responseString = [request responseString];
    DLog(@"responseString: %@", responseString);
    if ([responseString isKindOfClass:[NSString class]]) {
        [self parseAdsString:responseString];        
    }

}

- (void)requestFailed:(ASIHTTPRequest *)request {
    
    //ELLIOT: prevent compile warning when building for Release/Distribution
    //NSError *error = [request error];
    
    DLog(@"error: %@", ([request error]));
}



- (void)cacheAdsInfo:(NSString *)adsInfo {
    //KONG: cache AdsInfo
    if (adsInfo) {
        DLog(@"cacheAdsInfo: %@", adsInfo);
        [[NSUserDefaults standardUserDefaults] setObject:adsInfo
                                                  forKey:kPreferenceAdsInfo];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}

static NSTimeInterval const kCacheInterval = 518400; //KONG: 6 days. Expired before cached image removed, 7 day, based on EGOImageLoading

- (void)loadCachedAds {
    NSString *adsString = [[NSUserDefaults standardUserDefaults] objectForKey:kPreferenceAdsInfo];
    if (adsString) {
        DLog(@"cachedAdsString: %@", adsString);   
        _adsVersion = [self parseAdsVersion:adsString];
        self.ads = [self parseAdsData:adsString];
    }
}

#pragma mark Ad Cache

- (NSString *)keyForCachingAd:(AdInfo *)ad {
    return [NSString stringWithFormat:@"%@-cached", ad.adID];
}


static NSString *const adCachedMarkString = @"1";
- (void)adCached:(NSNotification *)notification {
    DLog();
    AdInfo *ad = [notification object];
    if (ad) {
        [[EGOCache currentCache] setString:adCachedMarkString forKey:[self keyForCachingAd:ad] withTimeoutInterval:kCacheInterval];
        [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationShowCachedPromoAd object:nil];
    }
}

- (BOOL)isAdCached:(AdInfo *)ad {
    if (ad) {
        NSString *cacheInfo = [[EGOCache currentCache] stringForKey:[self keyForCachingAd:ad]];
        if ([cacheInfo isEqualToString:adCachedMarkString]) {
            return YES;
        }
    }
    return NO;
}

@end