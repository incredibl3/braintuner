//
//  GSDeviceConfiguration.h
//  GreengarAPI
//
//  Created by silvercast on 5/13/11.
//  Copyright 2011 GreenGar studios. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface GSDeviceConfiguration : NSObject {
    
}

+ (GSDeviceConfiguration *)		shareDeviceConfiguration;

+ (BOOL)isIpad;
+ (BOOL)isIpad2;

+ (BOOL)hasCamera;

+ (NSString *)iOSVersion;
+ (BOOL)iOSVersionLaterThan:(NSString *)ver;

+ (NSString *) platformString;
+ (NSString *) platform;

@end
