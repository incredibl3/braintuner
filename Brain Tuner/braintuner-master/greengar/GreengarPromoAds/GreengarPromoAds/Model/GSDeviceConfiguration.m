//
//  GSDeviceConfiguration.m
//  GreengarAPI
//
//  Created by silvercast on 5/13/11.
//  Copyright 2011 GreenGar studios. All rights reserved.
//

#import "GSDeviceConfiguration.h"
#include <sys/types.h>
#include <sys/sysctl.h>

static GSDeviceConfiguration * shareDeviceConfiguration = nil;

@implementation GSDeviceConfiguration

+ (GSDeviceConfiguration *)shareDeviceConfiguration {
	
	if (shareDeviceConfiguration == nil) {
		shareDeviceConfiguration = [[super allocWithZone:NULL] init];
	}
	
	return shareDeviceConfiguration;
}

- (id) init {
	return self;
}

+ (BOOL)isIpad {
    return ([[UIDevice currentDevice] respondsToSelector:@selector(userInterfaceIdiom)] ? [[UIDevice currentDevice] userInterfaceIdiom] : UIUserInterfaceIdiomPhone) == UIUserInterfaceIdiomPad;
}

+ (BOOL)isIpad2 {
    return [[self class] isIpad] && [[self class] hasCamera];
}

+ (BOOL)hasCamera {
    return [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera];
}

+ (NSString *)iOSVersion {
    return [[UIDevice currentDevice] systemVersion];
}

+ (BOOL)iOSVersionLaterThan:(NSString *)ver {
    NSString *currSysVer = [[UIDevice currentDevice] systemVersion];
	return ([currSysVer compare:ver options:NSNumericSearch] != NSOrderedAscending);
}

/*
 Platforms
 iPhone1,1 -> iPhone 1G
 iPhone1,2 -> iPhone 3G 
 iPod1,1   -> iPod touch 1G 
 iPod2,1   -> iPod touch 2G 
 
 iPhone Simulator == i386
 iPhone == iPhone1,1
 3G iPhone == iPhone1,2
 3GS iPhone == iPhone2,1
 1st Gen iPod == iPod1,1
 2nd Gen iPod == iPod2,1
 1st Gen iPad == iPad1,1
 iPhone 4 == iPhone3,1
 I imagine the iPod Touch 4 will be == iPod3,1
 and the 2011 next generation iPad will be == iPad2,1
 
 */

+ (NSString *) platform
{
	size_t size;
	sysctlbyname("hw.machine", NULL, &size, NULL, 0);
	char *machine = malloc(size);
    sysctlbyname("hw.machine", machine, &size, NULL, 0);
    NSString *platform = [NSString stringWithCString:machine encoding:NSUTF8StringEncoding];
	free(machine);
	return platform;
}

+ (NSString *) platformString {
    NSString *platform = [[self class] platform];
    if ([platform isEqualToString:@"i386"]) return @"Simulator";
    
    if ([platform isEqualToString:@"iPhone1,1"]) return @"iPhone 1G";
    if ([platform isEqualToString:@"iPhone1,2"]) return @"iPhone 3G (China, no WiFi possibly)";
    
    if ([platform isEqualToString:@"iPhone2,1"]) return @"iPhone 3GS";
    
    if ([platform isEqualToString:@"iPhone3,1"]) return @"iPhone 4 )";
    if ([platform isEqualToString:@"iPhone3,2"]) return @"iPhone 4 (CDMA/Verizon)";
    
    if ([platform isEqualToString:@"iPod1,1"])   return @"iPod Touch 1G";
    if ([platform isEqualToString:@"iPod2,1"])   return @"iPod Touch 2G";
    if ([platform isEqualToString:@"iPod2,2"])   return @"iPod Touch 2.5G";
    if ([platform isEqualToString:@"iPod3,1"])   return @"iPod Touch 3G";
    if ([platform isEqualToString:@"iPod4,1"])   return @"iPod Touch 4G";
    
    if ([platform isEqualToString:@"iPad1,1"])   return @"iPad 1G (wifi)";
    if ([platform isEqualToString:@"iPad1,2"])   return @"iPad 1G (3G/GSM)";
    if ([platform isEqualToString:@"iPad2,1"])   return @"iPad 2G (wifi)";
    if ([platform isEqualToString:@"iPad2,2"])   return @"iPad 2G (GSM)";
    if ([platform isEqualToString:@"iPad2,3"])   return @"iPad 2G (CDMA)";
    
    if ([platform isEqualToString:@"AppleTV2,1"])   return @"Apple TV 2G";
    
    if ([platform isEqualToString:@"i386"])      return @"iPhone Simulator";
    
    return platform;
}

+ (id)allocWithZone:(NSZone *)zone {
    return [[self shareDeviceConfiguration] retain];
}

- (id)copyWithZone:(NSZone *)zone {
    return self;
}

- (id)retain {
    return self;
}

- (NSUInteger)retainCount {
    return NSUIntegerMax;  //denotes an object that cannot be released
}

- (oneway void)release {
    //do nothing
}

- (id)autorelease
{
    return self;
}

@end
