//
//  EnviromentDetector.h
//  GreengarPromoAds
//
//  Created by Cong Vo on 5/30/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AdInfo.h"

@interface EnviromentDetector : NSObject {
    id _delegate;
    SEL _callback;
}

- (BOOL)checkForAdDisplayCondition:(AdInfo *)ad;

- (BOOL)isAppInstalled:(NSString *)protocolScheme;

//+ (BOOL)isDevice:(NSString *)deviceName;

- (void)registerIdle:(id)delegate callback:(SEL)callback;

- (BOOL)isInternetReachable;

- (BOOL)isGreengarAdsHostReachable;

- (BOOL)isAds:(NSString *)adsID shownWithIn:(NSInteger)hours;

- (void)setAdsIsShown:(NSString *)adsID;

@end
