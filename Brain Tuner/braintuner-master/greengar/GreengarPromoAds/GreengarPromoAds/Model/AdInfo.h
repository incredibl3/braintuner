//
//  AdInfo.h
//  GreengarPromoAds
//
//  Created by Cong Vo on 5/31/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
    AdTypeNone,
    AdTypeWeb,
    AdTypeImage,
} AdType;

typedef enum {
    AdDisplayNone,
    AdDisplayLaunch,
    AdDisplayIdle
    
} AdDisplayTime;

typedef enum {
    DevicePlatformNone,
    DevicePlatformIOS
} DevicePlatform;

@interface AdInfo : NSObject {
    NSString *_appName;
    NSString *_appScheme;
    NSString *_appVersion;    
    NSString *_appDescription;
    
    NSString *_adID;
    NSString  *_minIOS;    
    AdType _type;
    NSInteger _displayInterval;
    
    NSString *_imageURL;
    NSString *_downloadURL;
    DevicePlatform _platform;
    
    AdDisplayTime _displayTime;
}

@property (nonatomic, retain) NSString *appName, *appDescription, *appScheme, *appVersion;
@property (nonatomic, assign) AdType type;
@property (nonatomic, assign) AdDisplayTime displayTime;
@property (nonatomic, assign) NSString *minIOS;
@property (nonatomic, assign) NSInteger displayInterval;
@property (nonatomic, assign) DevicePlatform platform;

@property (nonatomic, retain) NSString *adID, *imageURL, *downloadURL;

- (id)initWithDictionary:(NSDictionary *)info;

@end