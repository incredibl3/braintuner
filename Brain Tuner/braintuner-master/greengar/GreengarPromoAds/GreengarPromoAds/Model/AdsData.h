//
//  AdsNetworking.h
//  GreengarPromoAds
//
//  Created by Cong Vo on 5/31/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AdInfo.h"

static NSString *const kNotificationAdCached = @"kNotificationAdCache";
static NSString *const kNotificationShowCachedPromoAd = @"kNotificationShowCachedPromoAd";
static NSString *const kNotificationHideGSAdsView = @"kNotificationHideGSAdsView";
static NSString *const kNotificationShowGSAdsView = @"kNotificationShowGSAdsView";

#if DEBUG
#   define USE_DEVELOPMENT_SERVER 0
#endif

#if USE_DEVELOPMENT_SERVER

static NSString *const kAdsInfoURL = @"http://184.106.64.162/ads/ads.php";

#else

static NSString *const kAdsInfoURL = @"http://www.greengarstudios.com/ads/ads.php";

#endif

//static NSString *const kAdsInfoURL = @"http://123.233.455.33/ads/ads.php"; //KONG: host test
static NSString *const kAdsInfoHost = @"http://www.greengarstudios.com";

@protocol AdsDataDelegate <NSObject>

- (void)didUpdateAdsData:(BOOL)isNew;

@end

@interface AdsData : NSObject {
    id <AdsDataDelegate> _delegate;
    SEL _callback;
    NSArray *_ads;    
    NSInteger _adsVersion;
}

@property (nonatomic, retain) NSArray *ads;
@property (nonatomic, assign) NSInteger adsVersion;

- (void)retrieveAdsInfo:(id <AdsDataDelegate>)delegate callback:(SEL)callback
                trackAd:(AdInfo *)ad clicked:(BOOL)isClicked;

- (NSString *)addToURL:(NSString *)url trackingParams:(AdInfo *)ad clicked:(BOOL)isClicked;

- (void)cacheAdsInfo:(NSString *)adsInfo;
- (void)loadCachedAds;

- (BOOL)isAdCached:(AdInfo *)ad;
@end
