//
//  IdleDetectApplication.h
//  GreengarPromoAds
//
//  Created by Cong Vo on 5/30/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

static NSString *const kNotificationApplicationInIdle = @"kNotificationApplicationIdle";

@interface IdleDetectApplication : UIApplication {
    
}


- (void)scheduleIdleNotification:(CGFloat)idleTime;
- (void)cancelIdleNotification;


- (void)resetIdleTimer;
@end
