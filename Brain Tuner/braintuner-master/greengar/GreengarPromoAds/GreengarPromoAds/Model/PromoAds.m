//
//  PromoAds.m
//  GreengarPromoAds
//
//  Created by Cong Vo on 5/30/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "PromoAds.h"
#import "EnviromentDetector.h"

#import "PromoAdsView.h"
#import "ImageAdsView.h"
static PromoAdsProcess _adsProcess;
@interface PromoAds()
@end

@implementation PromoAds
@synthesize adsView                 = _adsView,
            displayViewController   = _displayViewController,
            envDetector             = _envDetector,
            adsData                 = _adsData;

- (id)init {
    self = [super init];
    if (self) {
        _envDetector = [[EnviromentDetector alloc] init];
        _adsData = [[AdsData alloc] init];
        shouldShow = YES;
        shouldCache = YES;
    }
    return self;
}

- (void)dealloc {
    [_adsData release];
    [_envDetector release];
    [_adsView release];
    [_displayViewController release];

    [super dealloc];
}

+ (BOOL)isAdShown {
    return (_adsProcess == PromoAdsProcessShow);
}

- (void)updateAdsInfo:(AdInfo *)trackAd clicked:(BOOL)isClicked {
    DLog();
    if ([_envDetector isInternetReachable] == NO) {
        return;
    }
    _adsProcess = PromoAdsProcessUpdate;
    [_adsData retrieveAdsInfo:self callback:@selector(didUpdateAdsInfo) trackAd:trackAd clicked:isClicked];
}

- (void)didUpdateAdsInfo {
    
}

// static NSString *const kPreferenceAdsInfo = @"kPreferenceAdsInfo";

- (void)didUpdateAdsData:(BOOL)isNew {
    DLog(@"isNew: %d", isNew);

    //KONG: cache AdsInfo
    if (isNew && _adsData.ads) {
        [self checkAdDisplay];        
    }
}

- (BOOL)checkAdDisplay {
    
    if (_adsData.ads == nil) {
        // No ads are being shown right now.
        // This will cause the client to update ad info from the server.
        return NO;
    }
    
    BOOL willDisplayNow = NO;
    if ([_adsData.ads count] > 0) {
        for (AdInfo *chosenAd in _adsData.ads) {
            if ([_envDetector checkForAdDisplayCondition:chosenAd]) {
                
                if (chosenAd.displayTime == AdDisplayLaunch) {
                    willDisplayNow = [self checkAdDisplayLaunch:chosenAd];
                    //KONG: only 1 ads is chosen for display
                    break;
                } else if (chosenAd.displayTime == AdDisplayIdle) {
                    //KONG: not support yet
                    //[self scheduleAdsDisplayIdle:chosenAd];
                }
            }            
        }
        
    }
    return willDisplayNow;
}

- (BOOL)checkAdDisplayLaunch:(AdInfo *)ad {
    if ([_adsData isAdCached:ad]) {
        DLog(@"Case 1");
        _adsProcess = PromoAdsProcessNone;        
        [self showAd:ad inView:_displayViewController];
        return YES;
    } else if (shouldCache) {
        DLog(@"Case 2");
        _adsProcess = PromoAdsProcessNone;
        [self scheduleAdsDisplayLaunch:ad];
        shouldCache = NO;
    }
    
    return NO;
}

- (void)cacheAd:(AdInfo *)ad {
    DLog(@"ad url: %@", ad.imageURL);
    UIViewController <AdView> *adView = [self adView:ad];
    [adView cacheAd];
}

- (void)scheduleAdsDisplayLaunch:(AdInfo *)ad {
    DLog();
    [self cacheAd:ad]; 
}

- (void)scheduleAdsDisplayIdle:(AdInfo *)ad {
    DLog();
    [_envDetector registerIdle:self callback:@selector(receiveAdsDisplayNotification:)];
}

- (void)receiveAdsDisplayNotification:(NSNotification *)notification {
    DLog();
}

#pragma mark UI

- (UIViewController <AdView> *)adView:(AdInfo *)ad {
    if (ad.type == AdTypeImage) {
        return [[ImageAdsView alloc] initWithAd:ad];
    }
    return nil;
}

- (void)showAd:(AdInfo *)ad inView:(UIViewController *)viewController {
    DLog();
    //if(self.adsView) {
        
    //}
    //else {
        self.adsView = [[ImageAdsView alloc] initWithAd:ad];
        _adsView.delegate = self;
    //}
    if (_adsView && _adsProcess != PromoAdsProcessShow && shouldShow) {
        _adsProcess = PromoAdsProcessShow;
        [_adsView show:_displayViewController];
        [_envDetector setAdsIsShown:ad.appScheme];    
        shouldShow = NO;
    }
}

- (void)adHidden:(NSNotification *)notification {
    DLog();
    _adsProcess = PromoAdsProcessNone;
}

- (void)restartPromoAds {
    shouldCache = YES;
    if([PromoAds isAdShown]) {
        return;
    }
    shouldShow = YES;
    //[self.adsView release];
    //self.adsView = nil;
}

- (void)startPromoAds:(UIViewController *)displayViewController {
    DLog();
    
    if(!shouldShow)
        return;
    
    //KONG: do nothing if internet is not reachable
    if ([_envDetector isInternetReachable] == NO) {
        return;
    }
    
//    if ([_envDetector isGreengarAdsHostReachable] == NO) {
//        return;            
//    }

    _adsProcess = PromoAdsProcessLaunch;
    DLog();
    self.displayViewController = displayViewController;
    [_adsData loadCachedAds];
    
    if ([self checkAdDisplay] == NO) {
        DLog(@"No ads at launch time, update info");
        [self updateAdsInfo:nil clicked:NO];        
    }

}

- (void)ad:(AdInfo *)ad clicked:(BOOL)isClicked {
    DLog();
    if (isClicked) {
        //KONG: Open Safari
        NSString *clickURL = [_adsData addToURL:[ad downloadURL]
                                 trackingParams:ad
                                        clicked:isClicked];
        DLog(@"Open in Safari: %@", clickURL);
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:clickURL]];
        
        // Nam: restartPromoAds won't work when ad is clicked b/c we haven't called [self adHidden:nil];
        [self adHidden:nil];
        //ELLIOT: why don't we send anything to the server to track here???
        //  also, we should updateAdsInfo in case the ad has been removed or expired,
        //  or there's a new ad
        //NAM: the clicked url is also to updateAdsInfo
        
    } else {
        //KONG: send to server to track
        [self adHidden:nil];
        [self updateAdsInfo:ad clicked:isClicked];
    }
}

- (void)hideAd {
    [_adsView hideAd];
    [self adHidden:nil];
}

@end