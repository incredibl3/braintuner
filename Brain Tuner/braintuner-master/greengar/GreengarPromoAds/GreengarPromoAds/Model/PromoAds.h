//
//  PromoAds.h
//  GreengarPromoAds
//
//  Created by Cong Vo on 5/30/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "EnviromentDetector.h"
#import "AdsData.h"

typedef enum {
    PromoAdsProcessNone,
    PromoAdsProcessLaunch,
    PromoAdsProcessUpdate,
    PromoAdsProcessShow
} PromoAdsProcess;

//static NSString *const kNotificationAdViewHidden = @"NotificationAdViewHidden";

@protocol AdViewDelegate <NSObject>
- (void)ad:(AdInfo *)ad clicked:(BOOL)isClicked;
@end


@protocol AdView <NSObject>

- (void)show:(UIViewController *)viewController;
- (void)cacheAd;
@optional - (void)hideAd;

@property (nonatomic, assign) id <AdViewDelegate> delegate;
@end

@interface PromoAds : NSObject <AdsDataDelegate, AdViewDelegate> {
    AdsData *_adsData;
    EnviromentDetector *_envDetector;
    UIViewController <AdView> *_adsView;
    UIViewController *_displayViewController;    
    BOOL shouldShow; // We only show ad once per session, so we set this to NO everytime we show an ad;
    BOOL shouldCache;
}


@property (nonatomic, retain) UIViewController <AdView> *adsView;
@property (nonatomic, retain) EnviromentDetector *envDetector;
@property (nonatomic, retain) UIViewController *displayViewController;
@property (nonatomic, retain) AdsData *adsData;
- (void)restartPromoAds;
- (void)startPromoAds:(UIViewController *)displayViewController;

- (void)updateAdsInfo:(AdInfo *)trackAd clicked:(BOOL)isClicked;

- (void)scheduleAdsDisplayIdle:(AdInfo *)ad;
- (void)scheduleAdsDisplayLaunch:(AdInfo *)ad;

- (UIViewController <AdView> *)adView:(AdInfo *)ad;
- (void)showAd:(AdInfo *)ad inView:(UIViewController *)viewController;
- (void)hideAd;

- (BOOL)checkAdDisplay;
- (BOOL)checkAdDisplayLaunch:(AdInfo *)ad;

+ (BOOL)isAdShown;
@end
