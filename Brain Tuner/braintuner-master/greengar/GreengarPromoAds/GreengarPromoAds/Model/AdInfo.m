//
//  AdInfo.m
//  GreengarPromoAds
//
//  Created by Cong Vo on 5/31/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "AdInfo.h"


@implementation AdInfo
@synthesize appName         = _appName,
            appDescription  = _appDescription,
            appScheme       = _appScheme,
            appVersion      = _appVersion,
            minIOS          = _minIOS,
            type            = _type,
            displayTime     = _displayTime,
            displayInterval = _displayInterval,
            imageURL        = _imageURL,
            downloadURL     = _downloadURL,
            adID            = _adID,
            platform        = _platform;
/*
{
    "version" : 3,
    "ads" : [{
        "id" : "1",
        "name" : "Brain Tuner 2",
        "type" : "IMAGE",
        "description" : "Stimulate your brain everyday!",
        "url" : "http:\/\/www.greengarstudios.com\/ads\/1\/ads.png",
        "click_url" : "http:\/\/bit.ly\/GreenGarComBTL",
        "app" : {
            "platform" : "IOS",
            "devices" : ["IPHONE","IPOD","IPAD"],
            "min_sdk" : "3.1.3",
            "url_scheme" : "ggs-braintuner2",
            "apis" : null,
            "version" : "2.0"
        },
        "expiration" : "2011-06-30 22:05:37",
        "display" : {
            "interval" : "24",
            "time" : "launch"
        }
    }]
}
*/
- (id)initWithDictionary:(NSDictionary *)info {
    self = [super init];
    if (self) {
        _adID = [[info objectForKey:@"id"] retain];
        _appName = [[info objectForKey:@"name"] retain];
        _appDescription = [[info objectForKey:@"description"] retain];

        
        NSString *typeString = [info objectForKey:@"type"];
        if ([typeString isEqualToString:@"WEB"]) {
            _type = AdTypeWeb;
        } else if ([typeString isEqualToString:@"IMAGE"]) {
            _type = AdTypeImage;
        }
        
        NSDictionary *appInfo = [info objectForKey:@"app"];
        
        _appVersion = [[appInfo objectForKey:@"version"] retain];
        _appScheme = [[appInfo objectForKey:@"url_scheme"] retain];
        _minIOS = [[appInfo objectForKey:@"min_sdk"] retain];
        
        NSString *platformString = [appInfo objectForKey:@"platform"];
        if ([platformString isEqualToString:@"IOS"]) {
            _platform = DevicePlatformIOS;
        }
        
        NSDictionary *displayInfo = [info objectForKey:@"display"];
        _displayInterval = [[displayInfo objectForKey:@"interval"] intValue];
        NSString *displayTime = [displayInfo objectForKey:@"time"];
        if ([displayTime isEqual:@"launch"]) {
            _displayTime = AdDisplayLaunch;
        } else if ([displayTime isEqual:@"idle"]) {
            _displayTime = AdDisplayIdle;
        }


        _imageURL = [[info objectForKey:@"url"] retain];
        _downloadURL = [[info objectForKey:@"click_url"] retain];
    }
    return self;
}

- (void)dealloc {
    [_appName release];
    
    [_appScheme release];
    [_appVersion release];
    [_appDescription release];
    
    [_adID release];
    [_minIOS release];    
    
    [_imageURL release];
    [_downloadURL release];
    
    [super dealloc];
}

@end