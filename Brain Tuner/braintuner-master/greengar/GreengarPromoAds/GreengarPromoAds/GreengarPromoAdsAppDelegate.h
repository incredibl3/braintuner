//
//  GreengarPromoAdsAppDelegate.h
//  GreengarPromoAds
//
//  Created by Cong Vo on 5/27/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@class GreengarPromoAdsViewController;
#import "PromoAds.h"

@interface GreengarPromoAdsAppDelegate : NSObject <UIApplicationDelegate> {
    
    PromoAds *_promoAds;
}

@property (nonatomic, retain) IBOutlet UIWindow *window;

@property (nonatomic, retain) IBOutlet GreengarPromoAdsViewController *viewController;


- (void)testEnvironment;
- (void)testVersionCompare;
@end
