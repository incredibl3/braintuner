//
//  iPhoneConstant.m
//  BrainTuner2
//
//  Created by Hector Zhao on 5/30/11.
//  Copyright 2011 Greengar Studios. All rights reserved.
//

#import "iPhoneConstant.h"

static iPhoneConstant *shareIPhoneConstant = nil;

@implementation iPhoneConstant

+ (iPhoneConstant *) sharedIPhoneConstant {
	
	if (shareIPhoneConstant == nil) {
		shareIPhoneConstant = [[super allocWithZone:NULL] init];
	}
	
	return ((iPhoneConstant *) shareIPhoneConstant);
}

- (id) init {
    homeTitleBarLeftOffset = 20;
    homeTitleBarHeight = 100;
    homeAccountButtonLeftOffset = -95;
    homeAccountButtonBottomOffset = -85;
    homeAboutButtonLeftOffset = -80;
    homeAboutButtonBottomOffset = -135;
    homeSoundButtonLeftOffset = -95;
    homeSoundButtonBottomOffset = -190;
    homeStartButtonLeftOffset = 55;
    homeStartButtonBottomOffset = -115;
    homeRelaxButtonLeftOffset = 55;
    homeRelaxButtonBottomOffset = -185;
    homeBalloonLeftOffset = 0;
    homeBalloonBottomOffset = 90;
    homeBrainGuyX = 240;
    homeBrainGuyY = 240;

    boxBackButtonLeftOffset = -80;
    boxBackButtonBottomOffset = 122;
    boxResetButtonLeftOffset = 125;
    boxResetButtonBottomOffset = 122;
    boxPointLeftOffset = 190;
    boxPointBottomOffset = 365;
    boxClassicModeLeftOffset = -70;
    boxClassicModeBottomOffset = -10;
    boxTimeModeLeftOffset = 60;
    boxTimeModeBottomOffset = -130;
    boxInfiniteModeLeftOffset = -75;
    boxInfiniteModeBottomOffset = -140;
    boxMultiplayerModeLeftOffset = 20;
    boxMultiplayerModeBottomOffset = -40;
    boxBalloonLeftOffset = 40;
    boxBalloonBottomOffset = 60;
    boxBrainGuyX = 270;
    boxBrainGuyY = 202;
    boxPointSize = 30.0;
    
    levelBackButtonLeftOffset = -80;
    levelBackButtonBottomOffset = 132;
    levelPointLeftOffset = 190;
    levelPointBottomOffset = 365;
    levelBox1X = -100;
    levelBox1Y = 70;
    levelBox2X = -30;
    levelBox2Y = 62;
    levelBox3X = 40;
    levelBox3Y = 62;
    levelBox4X = 110;
    levelBox4Y = 65;
    levelBox5X = -85;
    levelBox5Y = -40;
    levelBox6X = 10;
    levelBox6Y = -38;
    levelBox7X = 95;
    levelBox7Y = -42;
    levelBox8X = -92;
    levelBox8Y = -145;
    levelBox9X = -10;
    levelBox9Y = -134;
    levelBox10X = 97;
    levelBox10Y = -138;
    levelBrainGuyX = 1000;
    levelBrainGuyY = 1000;
    levelPointSize = 30.0;
    
    playButtonBarHeight = 96;
    playProblemHeight = 60;
    playProblemOffset = 170;
    playProblemDimensionWidth = 280;
    playProblemDimensionHeight = 60;
    playTableBottomOffset = 0;
    playResultImageOffset = 250;
    playTitleBarHeight = 105;
    playProblemBackgroundBottomOffset = 206;
    playTitleLabelBottomOffset = 455;
    playLevelLabelLeftOffset = 90;
    playLevelLabelBottomOffset = 395;
    playBestScoreLeftOffset = 250;
    playBestScoreBottomOffset = 395;
    playBackButtonLeftOffset = 30;
    playBackButtonBottomOffset = 454;
    playWrongButtonLeftOffset = 75;
    playWrongButtonBottomOffset = 42;
    playRightButtonLeftOffset = 235;
    playRightButtonBottomOffset = 42;
    playBottomBarHeight = 66;
    playInfiniteFlagLeftOffset = 230;
    playInfiniteFlagBottomOffset = 410;
    playLabelSize = 28.0;
    playTitleSize = 35.0;
    playProblemSize = 32.0;
    
    rankingTitleBarHeight = 105;
    rankingTitleLabelBottomOffset = 455;
    rankingLevelLabelLeftOffset = 90;
    rankingLevelLabelBottomOffset = 395;
    rankingBottomBarHeight = 66;
    rankingFacebookLeftOffset = -120;
    rankingFacebookBottomOffset = -197;
    rankingTwitterLeftOffset = -50;
    rankingTwitterBottomOffset = -197;
    rankingReplayButtonLeftOffset = 60;
    rankingReplayButtonBottomOffset = -35;
    rankingBackButtonLeftOffset = -60;
    rankingBackButtonBottomOffset = -35;
    rankingNextButtonLeftOffset = 0;
    rankingNextButtonBottomOffset = -35;
    rankingRow1Col1RightOffset = 140;
    rankingRow1Col1BottomOffset = 350;
    rankingRow2Col1RightOffset = 140;
    rankingRow2Col1BottomOffset = 320;
    rankingRow3Col1RightOffset = 140;
    rankingRow3Col1BottomOffset = 290;    
    rankingRow4Col1RightOffset = 140;
    rankingRow4Col1BottomOffset = 260;
    rankingOffsetBeforeScore = 140;
    rankingOffsetAboveScore = 20;
    rankingNewStampLeftOffset = 290;
    rankingNewStampBottomOffset = 240;
    rankingPassStampLeftOffset = 290;
    rankingPassStampBottomOffset = 300;
    rankingBalloonLeftOffset = 0;
    rankingBalloonBottomOffset = -120;
    rankingBrainGuyX = 260;
    rankingBrainGuyY = 45;
    rankingLabelSize = 22.0;
    rankingTitleSize = 35.0;
    
    profileTitleBarHeight = 105;
    profileTitleLabelBottomOffset = 455;
    profileBackButtonLeftOffset = 30;
    profileBackButtonBottomOffset = 454;
    profileTitleSize = 35.0;
    
    dialogFirstButtonX = 0;
    dialogFirstButtonY = 50;
    dialogSecondButtonX = 50;
    dialogSecondButtonY = 50;
    dialogThirdButtonX = 80;
    dialogThirdButtonY = 50;
    dialogHeaderSize = 35.0;
    dialogMessageSize = 25.0;
    dialogSubMessageSize = 20.0;
    dialogExtraMessageSize = 18.0;
    dialogElementOffset = 50.0;
    dialogButtonSize = 30.0;
    
    sideBarCancelButtonLeftOffset = -135;
    sideBarCancelButtonBottomOffset = 370;
    sideBarReplayButtonLeftOffset = 40;
    sideBarReplayButtonBottomOffset = 300;
    sideBarBackButtonLeftOffset = -40;
    sideBarBackButtonBottomOffset = 300;
    sideBarSoundButtonLeftOffset = 110;
    sideBarSoundButtonBottomOffset = 360;
    sideBarTitleLeftOffset = 160;
    sideBarTitleBottomOffset = 460;
    sideBarTitleSize = 28.0;
    
	return self;
}

@end