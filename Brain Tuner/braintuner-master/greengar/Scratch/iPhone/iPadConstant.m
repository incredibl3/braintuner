//
//  iPadConstant.m
//  BrainTuner2
//
//  Created by Hector Zhao on 5/30/11.
//  Copyright 2011 Greengar Studios. All rights reserved.
//

#import "iPadConstant.h"

static iPadConstant *shareIPadConstant = nil;

@implementation iPadConstant

+ (iPadConstant *) sharedIPadConstant {
	
	if (shareIPadConstant == nil) {
		shareIPadConstant = [[super allocWithZone:NULL] init];
	}
	
	return ((iPadConstant *) shareIPadConstant);
}

- (id) init {
    homeTitleBarLeftOffset = 100;
    homeTitleBarHeight = 200;
    homeAccountButtonLeftOffset = -180;
    homeAccountButtonBottomOffset = -150;
    homeAboutButtonLeftOffset = -160;
    homeAboutButtonBottomOffset = -290;
    homeSoundButtonLeftOffset = -170;
    homeSoundButtonBottomOffset = -420;
    homeStartButtonLeftOffset = 150;
    homeStartButtonBottomOffset = -260;
    homeRelaxButtonLeftOffset = 150;
    homeRelaxButtonBottomOffset = -400;
    homeBalloonLeftOffset = -100;
    homeBalloonBottomOffset = 110;
    homeBrainGuyX = 460;
    homeBrainGuyY = 420;
    
    
    boxBackButtonLeftOffset = -180;
    boxBackButtonBottomOffset = 307;
    boxResetButtonLeftOffset = 185;
    boxResetButtonBottomOffset = 280;
    boxPointLeftOffset = 390;
    boxPointBottomOffset = 810;
    boxClassicModeLeftOffset = -170;
    boxClassicModeBottomOffset = -10;
    boxTimeModeLeftOffset = 160;
    boxTimeModeBottomOffset = -270;
    boxInfiniteModeLeftOffset = -175;
    boxInfiniteModeBottomOffset = -260;
    boxMultiplayerModeLeftOffset = 30;
    boxMultiplayerModeBottomOffset = -50;
    boxBalloonLeftOffset = 30;
    boxBalloonBottomOffset = 110;
    boxBrainGuyX = 550;
    boxBrainGuyY = 430;
    boxPointSize = 50.0;
    
    levelBackButtonLeftOffset = -180;
    levelBackButtonBottomOffset = 307;
    levelPointLeftOffset = 390;
    levelPointBottomOffset = 810;
    levelBox1X = -240;
    levelBox1Y = 150;
    levelBox2X = -80;
    levelBox2Y = 142;
    levelBox3X = 80;
    levelBox3Y = 145;
    levelBox4X = 240;
    levelBox4Y = 155;
    levelBox5X = -185;
    levelBox5Y = -40;
    levelBox6X = 10;
    levelBox6Y = -68;
    levelBox7X = 195;
    levelBox7Y = -42;
    levelBox8X = -242;
    levelBox8Y = -285;
    levelBox9X = -10;
    levelBox9Y = -264;
    levelBox10X = 247;
    levelBox10Y = -278;
    levelBrainGuyX = 2000;
    levelBrainGuyY = 2000;
    levelPointSize = 50.0;
    
    playButtonBarHeight = 96;
    playProblemHeight = 145;
    playProblemOffset = 450;
    playProblemDimensionWidth = 728;
    playProblemDimensionHeight = 120;
    playTableBottomOffset = 70;
    playResultImageOffset = 600;
    playTitleBarHeight = 210;
    playProblemBackgroundBottomOffset = 470;
    playTitleLabelBottomOffset = 975;
    playLevelLabelLeftOffset = 190;
    playLevelLabelBottomOffset = 845;
    playBestScoreLeftOffset = 600;
    playBestScoreBottomOffset = 845;
    playBackButtonLeftOffset = 60;
    playBackButtonBottomOffset = 974;
    playWrongButtonLeftOffset = 225;
    playWrongButtonBottomOffset = 84;
    playRightButtonLeftOffset = 533;
    playRightButtonBottomOffset = 84;
    playBottomBarHeight = 132;
    playInfiniteFlagLeftOffset = 560;
    playInfiniteFlagBottomOffset = 880;
    playLabelSize = 56.0;
    playTitleSize = 70.0;
    playProblemSize = 64.0;
    
    rankingTitleBarHeight = 210;
    rankingTitleLabelBottomOffset = 975;
    rankingLevelLabelLeftOffset = 180;
    rankingLevelLabelBottomOffset = 845;
    rankingBottomBarHeight = 132;
    rankingFacebookLeftOffset = -300;
    rankingFacebookBottomOffset = -427;
    rankingTwitterLeftOffset = -170;
    rankingTwitterBottomOffset = -427;
    rankingReplayButtonLeftOffset = 120;
    rankingReplayButtonBottomOffset = -125;
    rankingBackButtonLeftOffset = -120;
    rankingBackButtonBottomOffset = -125;
    rankingNextButtonLeftOffset = 0;
    rankingNextButtonBottomOffset = -125;
    rankingRow1Col1RightOffset = 270;
    rankingRow1Col1BottomOffset = 750;
    rankingRow2Col1RightOffset = 270;
    rankingRow2Col1BottomOffset = 670;
    rankingRow3Col1RightOffset = 270;
    rankingRow3Col1BottomOffset = 590;    
    rankingRow4Col1RightOffset = 270;
    rankingRow4Col1BottomOffset = 510;
    rankingOffsetBeforeScore = 300;
    rankingOffsetAboveScore = 40;
    rankingNewStampLeftOffset = 650;
    rankingNewStampBottomOffset = 590;
    rankingPassStampLeftOffset = 650;
    rankingPassStampBottomOffset = 650;
    rankingBalloonLeftOffset = -30;
    rankingBalloonBottomOffset = -240;
    rankingBrainGuyX = 500;
    rankingBrainGuyY = 100;
    rankingLabelSize = 56.0;
    rankingTitleSize = 70.0;
    
    profileTitleBarHeight = 210;
    profileTitleLabelBottomOffset = 975;
    profileBackButtonLeftOffset = 60;
    profileBackButtonBottomOffset = 974;
    profileTitleSize = 70.0;
    
    dialogFirstButtonX = 0;
    dialogFirstButtonY = 20;
    dialogSecondButtonX = 90;
    dialogSecondButtonY = 20;
    dialogThirdButtonX = 160;
    dialogThirdButtonY = 20;
    dialogHeaderSize = 70.0;
    dialogMessageSize = 50.0;
    dialogSubMessageSize = 40.0;
    dialogExtraMessageSize = 36.0;
    dialogElementOffset = 70.0;
    dialogButtonSize = 60.0;
    
    sideBarCancelButtonLeftOffset = -335;
    sideBarCancelButtonBottomOffset = 670;
    sideBarReplayButtonLeftOffset = 140;
    sideBarReplayButtonBottomOffset = 530;
    sideBarBackButtonLeftOffset = -140;
    sideBarBackButtonBottomOffset = 530;
    sideBarSoundButtonLeftOffset = 270;
    sideBarSoundButtonBottomOffset = 660;
    sideBarTitleLeftOffset = 384;
    sideBarTitleBottomOffset = 960;
    sideBarTitleSize = 56.0;
    
	return self;
}

@end