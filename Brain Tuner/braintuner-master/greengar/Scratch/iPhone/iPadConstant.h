//
//  iPadConstant.h
//  BrainTuner2
//
//  Created by Hector Zhao on 5/30/11.
//  Copyright 2011 Greengar Studios. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DeviceConstant.h"

@interface iPadConstant : DeviceConstant {
    
}

+ (iPadConstant *)	sharedIPadConstant;

- (id) init;

@end
