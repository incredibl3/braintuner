//
//  AppDelegate_iPhone.h
//  BrainTuner2
//
//  Created by Silvercast on 2/27/11.
//  Copyright 2011 GreenGar studios. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RootViewController.h"
#import "UIWindowForShake.h"

@interface AppDelegate_iPhone : NSObject <UIApplicationDelegate> {
    UIWindowForShake        *window;
    UINavigationController  *navigationController;
}

@property (nonatomic, retain) IBOutlet UIWindow                 *window;
@property (nonatomic, retain) IBOutlet UINavigationController   *navigationController;

@end

