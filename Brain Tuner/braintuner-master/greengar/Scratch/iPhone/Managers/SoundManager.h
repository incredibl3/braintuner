//
//  SoundManager.h
//  BrainTuner2
//
//  Created by Hector Zhao on 3/8/11.
//  Copyright 2011 GreenGar studios. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>

@interface SoundManager : NSObject {

	AVAudioPlayer *audioPlayer;
	NSMutableDictionary *dictionary;
}

@property (nonatomic, retain) AVAudioPlayer			*audioPlayer;
@property (nonatomic, retain) NSMutableDictionary	*dictionary;

+(SoundManager *) sharedSoundManager;
//-(AVAudioPlayer *)load:(NSString *)filename;
-(void)play:(NSString *)filename loop:(BOOL)l;
-(void)stop:(NSString *)filename;


		   
@end
