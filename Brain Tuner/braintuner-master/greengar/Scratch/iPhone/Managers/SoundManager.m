//
//  SoundManager.m
//  BrainTuner2
//
//  Created by Hector Zhao on 3/8/11.
//  Copyright 2011 GreenGar studios. All rights reserved.
//

#import "SoundManager.h"

static SoundManager *sharedSoundManager = nil;

@implementation SoundManager

@synthesize audioPlayer, dictionary;

+ (SoundManager *)sharedSoundManager {
	
	if (sharedSoundManager == nil) {
		sharedSoundManager = [[super allocWithZone:NULL] init];
	}
	
	return sharedSoundManager;
}

- (id) init {
	if (dictionary == nil) {
		dictionary = [[NSMutableDictionary alloc] init];
	}
	return self;
}

- (AVAudioPlayer *)load:(NSString *)filename loop:(BOOL)l {
	audioPlayer = [dictionary objectForKey:filename];
	if (!audioPlayer) {
		NSString *path = [NSString stringWithFormat:@"%@/%@", [[NSBundle mainBundle] resourcePath], filename];
		NSURL *url = [NSURL fileURLWithPath:path];
		
		NSError *error;
		audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:&error];
        
		if (audioPlayer == nil) {
			NSLog(@"audioPlayer error = %@", [error description]);
		} else {
			[dictionary setObject:audioPlayer forKey:filename];
			[audioPlayer release]; // dictionary retains audioPlayer
		}

	}
	
    if (l) {
        audioPlayer.numberOfLoops = -1; // loop forever
    } else {
        audioPlayer.numberOfLoops = 0;
    }    
	audioPlayer.currentTime = 0;
	
	return audioPlayer;
}

-(void)play:(NSString *)filename loop:(BOOL)l {
	audioPlayer = [self load:filename loop:l];
	[audioPlayer play];
}

- (void)stop:(NSString *)filename {
	audioPlayer = [dictionary objectForKey:filename];
	if (audioPlayer) {
		[audioPlayer stop];
		audioPlayer.currentTime = 0;
	} else {
		DLog(@"WARNING: attempting to stop nonloaded audio file: %@", filename);
	}
}

- (void)dealloc {
	[dictionary release], dictionary = nil;
	[super dealloc];
}

@end
