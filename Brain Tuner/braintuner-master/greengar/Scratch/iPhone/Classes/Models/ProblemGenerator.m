//
//  ProblemGenerator.m
//  BrainTuner2
//
//  Created by Cong Vo on 5/24/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "ProblemGenerator.h"
#import "BaseProblem.h"

#import "NumberEquityProblem.h"
#import "NumberInequityProblem.h"


@implementation ProblemGenerator


//static NSArray *classicLevels = nil;
//static NSArray *timeLevels = nil;
//static NSArray *infinityLevels = nil;

+ (NSDictionary *)levelInfo:(NSInteger)level inMode:(GameMode)mode {
    NSArray *result;
    NSString * plistPath;
    
    switch (mode) {
        case kClassicMode:
            plistPath = [[NSBundle mainBundle] pathForResource:@"ClassicBoxLevels" 
                                                                   ofType:@"plist"];
            
            result = [NSArray arrayWithContentsOfFile:plistPath];
            break;
        case kTimeMode:
            plistPath = [[NSBundle mainBundle] pathForResource:@"TimeBoxLevels" 
                                                                   ofType:@"plist"];
            result = [NSArray arrayWithContentsOfFile:plistPath];
            break;
        
        case kInfiniteMode: 
            plistPath = [[NSBundle mainBundle] pathForResource:@"InfinityBoxLevels" 
                                                        ofType:@"plist"];
                      
            result = [NSArray arrayWithContentsOfFile:plistPath];
            break;
            
        case kRelaxMode:
            plistPath = [[NSBundle mainBundle] pathForResource:@"RelaxBoxLevels" 
                                                        ofType:@"plist"];
            
            result = [NSArray arrayWithContentsOfFile:plistPath];
            break;
        default:            
            break;
    }

    return [[result objectAtIndex:level] copy];
}

+ (BaseProblem *)problem:(GameMode)mode 
                   level:(NSInteger)level 
                 current:(NSArray *)currentProblems {
    BaseProblem *problem;
    switch (mode) {
        case kClassicMode:
            problem = [[[NumberEquityProblem alloc] init] autorelease];
            [(NumberEquityProblem *) problem generate:level mode:mode];
            break;
        case kTimeMode:
            problem = [[[NumberEquityProblem alloc] init] autorelease];
            [(NumberEquityProblem *) problem generate:level mode:mode];
            break;
        case kInfiniteMode:
            problem = [self infinityProblem:currentProblems];
            break;
        case kRelaxMode:
            problem = [self relaxProblem:currentProblems];
            break;
        default:
            break;
    }
    return problem;
}


+ (BaseProblem *)problem:(GameMode)mode level:(NSInteger)level {
    BaseProblem *problem;
    switch (mode) {
        case kClassicMode:
            problem = [[[NumberEquityProblem alloc] init] autorelease];
            [(NumberEquityProblem *) problem generate:level mode:mode];
            break;
        case kTimeMode:
            problem = [[[NumberEquityProblem alloc] init] autorelease];
            [(NumberEquityProblem *) problem generate:level mode:mode];
            break;
        case kRelaxMode:
            break;
        default:
            break;
    }
    return problem;
}


#pragma mark Classic

+ (NSInteger)classicAverageTime:(NSInteger)level {
    NSDictionary *levelInfo = [self levelInfo:level inMode:kClassicMode];
    return [[levelInfo objectForKey:@"AverageTime"] intValue];
}


+ (NSInteger)timeProblemsAverageNo:(NSInteger)level {
    NSDictionary *levelInfo = [self levelInfo:level inMode:kTimeMode];
    return [[levelInfo objectForKey:@"ProblemsAverageNo"] intValue];    
}

#pragma mark Infinity

+ (BaseProblem *)infinityProblem:(NSArray *)currentProblems {
    NSInteger generatedProblems = [currentProblems count];
    NSInteger level = generatedProblems / kInfinityLevelProblemsNo;
    
    //KONG: max problem level is 10, whose index = 9
    if (level >= 10) {
        level = 9;
    }
    
    BaseProblem *problem = [[[NumberEquityProblem alloc] init] autorelease];
    [(NumberEquityProblem *) problem generate:level mode:kInfiniteMode];

    return problem;
}

+ (NSInteger)infinityLevelScore:(NSInteger)level {
    NSDictionary *levelInfo = [self levelInfo:level inMode:kInfiniteMode];
    return [[levelInfo objectForKey:@"UnitScore"] intValue];    
}

#pragma mark Relax

+ (BaseProblem *)relaxProblem:(NSArray *)currentProblems {
    
    // Hector: Relax does not require level up
    BaseProblem *problem = [[[NumberEquityProblem alloc] init] autorelease];
    [(NumberEquityProblem *) problem generate:0 mode:kRelaxMode];
    
    return problem;
}

+ (NSInteger)relaxLevelScore:(NSInteger)level {
    NSDictionary *levelInfo = [self levelInfo:level inMode:kRelaxMode];
    return [[levelInfo objectForKey:@"UnitScore"] intValue];    
}

@end
