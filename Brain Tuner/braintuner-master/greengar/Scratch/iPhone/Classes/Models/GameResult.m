//
//  GameResult.m
//  BrainTuner2
//
//  Created by Cong Vo on 5/23/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "GameResult.h"


@implementation GameResult
@synthesize time, score, incorrect, correct, skip;

- (NSString *)resultString {
    return nil;
}

- (NSString *)description {
    return [NSString stringWithFormat:@"Game Result: \n- Time: %f \n- Score: %d \n - Incorrect: %d",
            time, score, incorrect];
}

@end
