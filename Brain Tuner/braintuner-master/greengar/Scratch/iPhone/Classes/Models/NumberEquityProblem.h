//
//  NumberEquityProblem.h
//  BrainTuner2
//
//  Created by Hector Zhao on 3/2/11.
//  Copyright 2011 Greengar Studios. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EquityProblem.h"
#import "Element.h"
#import "GameUtils.h"

typedef enum {
  OperatorTypeAddition = 0,
  OperatorTypeSubtraction,
  OperatorTypeMultiplication,  
  OperatorTypeDivition
} OperatorType;

@interface NumberEquityProblem : EquityProblem {    
 
    NSInteger   _firstElement;
    NSInteger   _secondElement;
    OperatorType _operator;
}

-(void)			generateProblems;
//- (void)		generateProblems:(NSInteger)level;
-(void)         generateProblems:(int) numOfElements withMaxElement:(int)generatedNumber withMaxResult:(int)generatedResult;
-(NSString*)	getGeneratedProblemsInString;
-(ProblemResult)	check:(Element*)displayResult userInput:(BOOL)userInput;
-(BOOL)         getExpectedResult;

- (void)generate:(NSInteger)level mode:(GameMode)mode;

@end
