//
//  InEquityProblem.m
//  BrainTuner2
//
//  Created by Hector Zhao on 3/2/11.
//  Copyright 2011 Greengar Studios. All rights reserved.
//

#import "InEquityProblem.h"


@implementation InEquityProblem

- (void) generateProblems {
	
}

- (void) generateProblems:(int)numOfElements {
    
}

- (NSString *) getGeneratedProblemsInString {
	return @"";
}

-(ProblemResult)check:(Element*)displayResult userInput:(BOOL)userInput {
	return kResultUnknown;
}

-(BOOL)getExpectedResult {
    return FALSE;
}

@end
