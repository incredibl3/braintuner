//
//  BaseProblem.m
//  BrainTuner2
//
//  Created by Hector Zhao on 3/2/11.
//  Copyright 2011 Greengar Studios. All rights reserved.
//

#import "BaseProblem.h"


@implementation BaseProblem

@synthesize expectedResult, realResult, displayText;

- (void) generateProblems {

}

- (void) generateProblems:(int)numOfElements {
    
}

- (NSString *) getGeneratedProblemsInString {
	return @"";
}

-(ProblemResult)check:(Element*)displayResult userInput:(BOOL)userInput {
	return kResultUnknown;
}

-(BOOL)getExpectedResult {
    return FALSE;
}

- (void)dealloc {
    [expectedResult release];
	[realResult release];
	[displayText release];
    [super dealloc];
}

@end
