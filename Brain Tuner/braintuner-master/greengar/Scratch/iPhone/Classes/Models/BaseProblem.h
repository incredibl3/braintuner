//
//  BaseProblem.h
//  BrainTuner2
//
//  Created by Hector Zhao on 3/2/11.
//  Copyright 2011 Greengar Studios. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "Element.h"
#import "GameUtils.h"

@interface BaseProblem : NSObject {

	Element				*expectedResult;
	Element				*realResult;
	NSMutableString		*displayText;
}

@property (nonatomic, retain) Element *expectedResult;
@property (nonatomic, retain) Element *realResult;
@property (nonatomic, retain) NSMutableString *displayText;

- (void)		generateProblems;
- (void)        generateProblems:(int) numOfElements;
- (NSString *)	getGeneratedProblemsInString;
- (ProblemResult)	check:(Element *)expectedResult userInput:(BOOL)userInput;
- (BOOL)        getExpectedResult;

@end
