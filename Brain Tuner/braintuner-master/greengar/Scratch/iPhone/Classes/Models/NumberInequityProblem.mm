//
//  NumberEquityProblem.m
//  BrainTuner2
//
//  Created by Hector Zhao on 3/2/11.
//  Copyright 2011 Greengar Studios. All rights reserved.
//

#import "NumberInequityProblem.h"
#import "NumberElement.h"
#import "ExpressionEvaluator.h"
#include <stdio.h>
#include <string.h>

// random from 0 to 14, we don't want user to play with number > 15
#define kMaxGeneratedNumber 15
#define kMaxGeneratedResult 30
#define kMaxRandomSubtrahend 7

@implementation NumberInequityProblem

-(void) generateProblems:(int)numOfElements withMaxElement:(int)generatedNumber withMaxResult:(int)generatedResult withMaxRandomSubtrahend:(int)randomSubtrahend {
	int numOfElement = arc4random() % numOfElements;
	if (numOfElement < 2) {
		numOfElement = 2;
	}
    
	displayText = [[NSMutableString alloc] initWithString:@""];
	for (int j = 0; j < numOfElement; j++) {
		
		// random from 0 to kMaxGeneratedNumber-1
        int element = 0;
        if (generatedNumber > 0) {
            element = arc4random() % generatedNumber;
        }
        else {
            element = arc4random() % kMaxGeneratedNumber;
        }
		
		// since we only use +-* we don't need to eleminate divident by zero case
		/*if (element == 0) {
		 element = 1;
		 }*/
		
		[displayText appendString: [NSString stringWithFormat:@"%d", element]];
		
		// if it's not the end of the equation, append supported operant (randomized)
		if (j != numOfElement -1 ) {
			int operant = arc4random() % 3; // it should be 4 operants +-*/ but we leave / away
			switch (operant) {
				case 0:
					[displayText appendString: @" + "];
					break;
				case 1:
					[displayText appendString: @" - "];
					break;
				case 2:
					[displayText appendString: @" x "];
					break;
					/*case 3:
					 [displayText appendString: @"/"];
					 break;*/
			}
		}
	}
	
	std::string s;
	long   result;
	int    err;
	
	s = [displayText cStringUsingEncoding: NSASCIIStringEncoding];		// NSString to std:string conversion
	err = ExpressionEvaluator::calculateLong(s, result);					// evaluate the result in long format
	self.expectedResult = [[NumberElement alloc] initWithNumber:result];		// store value to self's property
	
	moreThan = (arc4random() % 2 == 0);
	if (moreThan) {
		[displayText appendString: @" > "];									// add 'greater than' at the end of the equation, right before display result
	} else {
		[displayText appendString: @" < "];									// add 'less than' at the end of the equation, right before display result
	}

	
	// next we generate display result, it could be either result or a different value
	
	BOOL useExpectedResult = (arc4random() % 2 == 0);
	
	if (useExpectedResult) {
		if (moreThan) {
            if (randomSubtrahend > 0) {
                self.realResult = [[NumberElement alloc] initWithNumber:result - (arc4random() % randomSubtrahend)];
            }
            else {
                self.realResult = [[NumberElement alloc] initWithNumber:result - (arc4random() % kMaxRandomSubtrahend)];
            }
		} else {
            if (randomSubtrahend > 0) {
                self.realResult = [[NumberElement alloc] initWithNumber:result + (arc4random() % randomSubtrahend)];
            }
            else {
                self.realResult = [[NumberElement alloc] initWithNumber:result + (arc4random() % kMaxRandomSubtrahend)];
            }
		}
	} else {
        if (generatedResult > 0) {
            self.realResult = [[NumberElement alloc] initWithNumber: arc4random() % generatedResult];
        }
        else {
            self.realResult = [[NumberElement alloc] initWithNumber: arc4random() % kMaxGeneratedResult];
        }
	}
	
	[displayText appendString: [NSString stringWithFormat:@"%@", [realResult toString]]];
	
}

-(void) generateProblems {
    [self generateProblems:2 withMaxElement:kMaxGeneratedNumber withMaxResult:kMaxGeneratedResult withMaxRandomSubtrahend:kMaxRandomSubtrahend];
}

-(NSString*) getGeneratedProblemsInString {
	return displayText;
}

-(ProblemResult)check:(Element*)realResult userInput:(BOOL)userInput {
	NumberElement* _expectedResult = (NumberElement*)self.expectedResult;
	NumberElement* _realResult = (NumberElement*)self.realResult;
	
	if (moreThan) {
		// the actual expectedResult is more than realResult
		if ([_expectedResult intValue] > [_realResult intValue]) {
			if (userInput == TRUE) {
				return kResultRight;
			}
			
			else {
				return kResultWrong;
			}

		}
		
		if ([_expectedResult intValue] <= [_realResult intValue]) {
			if (userInput == FALSE) {
				return kResultRight;
			}
			
			else {
				return kResultWrong;
			}

		}
	} else {
		// the actual expectedResult is less than realResult
		if ([_expectedResult intValue] < [_realResult intValue]) {
			if (userInput == TRUE) {
				return kResultRight;
			}
			
			else {
				return kResultWrong;
			}

		}
		
		if ([_expectedResult intValue] >= [_realResult intValue]) {
			if (userInput == FALSE) {
				return kResultRight;
			}
			
			else {
				return kResultWrong;
			}

		}
	}
	return kResultUnknown;
}

-(BOOL) getExpectedResult {
    NumberElement* _expectedResult = (NumberElement*)self.expectedResult;
	NumberElement* _realResult = (NumberElement*)self.realResult;
    
    if (moreThan) {
		// the actual expectedResult is more than realResult
		if ([_expectedResult intValue] > [_realResult intValue]) {
			return TRUE;
            
		}
		return FALSE;
	} else {
		// the actual expectedResult is less than realResult
		if ([_expectedResult intValue] < [_realResult intValue]) {
			return TRUE;
            
		}
		return FALSE;
	}
}

@end
