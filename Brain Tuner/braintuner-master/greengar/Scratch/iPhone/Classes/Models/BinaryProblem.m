//
//  BinaryProblem.m
//  BrainTuner2
//
//  Created by Hector Zhao on 3/2/11.
//  Copyright 2011 Greengar Studios. All rights reserved.
//

#import "BinaryProblem.h"


@implementation BinaryProblem

-(void) generateProblems {

}

-(NSString*) getGeneratedProblemsInString {
	return @"101010";
}

-(ProblemResult)check:(Element*)realResult userInput:(BOOL)userInput {
	return FALSE;
}

-(BOOL)getExpectedResult {
    return FALSE;
}

@end
