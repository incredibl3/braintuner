//
//  ProblemGenerator.h
//  BrainTuner2
//
//  Created by Cong Vo on 5/24/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GameUtils.h"
#import "BaseProblem.h"

static NSInteger const kInfinityLevelProblemsNo = 10;

@interface ProblemGenerator : NSObject {
    
}

+ (BaseProblem *)problem:(GameMode)mode 
                   level:(NSInteger)level 
                 current:(NSArray *)currentProblems;
+ (NSDictionary *)levelInfo:(NSInteger)level inMode:(GameMode)mode;

//KONG: classic
+ (NSInteger)classicAverageTime:(NSInteger)level;

//KONG: timing
+ (NSInteger)timeProblemsAverageNo:(NSInteger)level;

//KONG: infinity
+ (BaseProblem *)infinityProblem:(NSArray *)currentProblems;
+ (NSInteger)infinityLevelScore:(NSInteger)level;

//Hector: relax
+ (BaseProblem *)relaxProblem:(NSArray *)currentProblems;
+ (NSInteger)relaxLevelScore:(NSInteger)level;

@end
