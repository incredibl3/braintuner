//
//  BoxScoredPoint.m
//  BrainTuner2
//
//  Created by Hector Zhao on 3/8/11.
//  Copyright 2011 GreenGar studios. All rights reserved.
//

#import "BoxScoredPoint.h"


@implementation BoxScoredPoint

@synthesize totalPoint, numOfLevel, unlockedLevel, isUnlocked;

- (id)init {
    if ((self = [super init])) {
        totalPoint = 0;
        numOfLevel = 0;
        unlockedLevel = 1;
        isUnlocked = FALSE;
    }
	return self;
}

- (void) setPoint:(int)point forLevel:(int)level {
    pointForLevel[level] = point;
}

- (void) setHighScore:(int)score forLevel:(int)level {
	highScoreForLevel[level] = score;
}

- (int) getPointForLevel:(int)level {
    return pointForLevel[level];
}

- (int) getHighScoreForLevel:(int)level {
	return highScoreForLevel[level];
}
@end
