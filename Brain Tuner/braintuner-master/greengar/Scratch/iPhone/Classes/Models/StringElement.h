//
//  StringElement.h
//  BrainTuner
//
//  Created by Silvercast on 9/28/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Element.h"

@interface StringElement : Element {
	NSString *		term;
}
// override Element's toString method
-(NSString*)toString;	

// NumberElement's methods
-(StringElement*)initWithString:(NSString*)str;

@end
