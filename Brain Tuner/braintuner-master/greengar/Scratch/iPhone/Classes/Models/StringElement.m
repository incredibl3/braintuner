//
//  StringElement.m
//  BrainTuner
//
//  Created by Silvercast on 9/28/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "StringElement.h"

@implementation StringElement

-(id)initWithString:(NSString*)str
{
    if ((self = [super init])) {
        term = [NSString stringWithString:str];
    }

	return self;
}

-(NSString*)toString
{
	return [term autorelease];
}

@end
