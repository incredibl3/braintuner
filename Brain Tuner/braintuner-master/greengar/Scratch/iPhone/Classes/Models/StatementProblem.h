//
//  StatementProblem.h
//  BrainTuner2
//
//  Created by Hector Zhao on 3/2/11.
//  Copyright 2011 Greengar Studios. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseProblem.h"
#import "Element.h"
#import "GameUtils.h"

@interface StatementProblem : BaseProblem {

}

-(void)			generateProblems;
-(NSString*)	getGeneratedProblemsInString;
-(ProblemResult)	check:(Element*)displayResult userInput:(BOOL)userInput;
-(BOOL)         getExpectedResult;

@end
