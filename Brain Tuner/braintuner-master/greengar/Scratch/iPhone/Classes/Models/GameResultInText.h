//
//  GameResultInText.h
//  BrainTuner2
//
//  Created by Hector Zhao on 3/8/11.
//  Copyright 2011 Greengar Studios. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface GameResultInText : NSObject {
	NSString *title;
	NSString *message;
    
}

@property (nonatomic, retain) NSString *title;
@property (nonatomic, retain) NSString *message;

@end
