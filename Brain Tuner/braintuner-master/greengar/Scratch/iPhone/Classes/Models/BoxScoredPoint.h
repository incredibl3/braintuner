//
//  BoxScoredPoint.h
//  BrainTuner2
//
//  Created by Hector Zhao on 3/8/11.
//  Copyright 2011 GreenGar studios. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GameUtils.h"


@interface BoxScoredPoint : NSObject {
	int		totalPoint;
	int		numOfLevel;
	int		unlockedLevel;
    BOOL    isUnlocked;
    int     pointForLevel[kNumOfLevelsPerBox];
	int		highScoreForLevel[kNumOfLevelsPerBox];
}

- (void) setPoint:(int)point forLevel:(int)level;
- (void) setHighScore:(int)score forLevel:(int)level;
- (int) getPointForLevel:(int)level;
- (int) getHighScoreForLevel:(int)level;

@property (nonatomic)	int		totalPoint;
@property (nonatomic)	int		numOfLevel;
@property (nonatomic)	int		unlockedLevel;
@property (nonatomic)   BOOL    isUnlocked;

@end
