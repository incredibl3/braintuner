//
//  NumberEquityProblem.m
//  BrainTuner2
//
//  Created by Hector Zhao on 3/2/11.
//  Copyright 2011 Greengar Studios. All rights reserved.
//

#import "NumberEquityProblem.h"
#import "NumberElement.h"
#import "ExpressionEvaluator.h"
#include <stdio.h>
#include <string.h>
#include <stream.h>
#import "ProblemGenerator.h"

// random from 0 to 14, we don't want user to play with number > 15
#define kMaxGeneratedNumber 15
#define kMaxGeneratedResult 30

static NSArray *trickSelectors = nil;

@implementation NumberEquityProblem

// not used in our code currently because we apply some tricks
// see - (void)generate:(NSInteger)level mode:(GameMode)mode below
- (void)generateProblems:(int)numOfElements withMaxElement:(int)generatedNumber withMaxResult:(int)generatedResult {
    
    // generate problems with maximum numofElements elements
    // E.g. 
    // numofElements = 4, numOfElement = random from 2 to 4
    int numOfElement = arc4random() % numOfElements;
    if (numOfElement < 2) {
        numOfElement = 2;
    }
	
	displayText = [[NSMutableString alloc] initWithString:@""];
	for (int j = 0; j < numOfElement; j++) {
        
		// random from 0 to kMaxGeneratedNumber-1
        int element = 0;
        if (generatedNumber > 0) {
            element = arc4random() % generatedNumber;
        }
        else {
            element = arc4random() % kMaxGeneratedNumber;
        }
		
		// since we only use +-* we don't need to eleminate divident by zero case
		/*if (element == 0) {
		 element = 1;
		 }*/
		
		[displayText appendString: [NSString stringWithFormat:@"%d", element]];
		
		// if it's not the end of the equation, append supported operant (randomized)
		if (j != numOfElement -1 ) {
			int operant = arc4random() % 3; // it should be 4 operants +-*/ but we leave / away
			switch (operant) {
				case 0:
					[displayText appendString: @" + "];
					break;
				case 1:
					[displayText appendString: @" - "];
					break;
				case 2:
					[displayText appendString: @" x "];
					break;
					/*case 3:
					 [displayText appendString: @"/"];
					 break;*/
			}
		}
	}
    
	std::string s;
	long   result;
	int    err;
	
	s = [displayText cStringUsingEncoding: NSASCIIStringEncoding];		// NSString to std:string conversion
	err = ExpressionEvaluator::calculateLong(s, result);					// evaluate the result in long format
	self.expectedResult = [[[NumberElement alloc] initWithNumber:result] autorelease];		// store value to self's property
	[displayText appendString: @" = "];									// add equality at the end of the equation, right before display result
	// next we generate display result, it could be either result or a different value
	
	BOOL useExpectedResult = (arc4random() % 2 == 0);
	
	if (useExpectedResult) {
		self.realResult = [[[NumberElement alloc] initWithNumber:result] autorelease];
	} else {
        if (generatedResult > 0) {
            self.realResult = [[[NumberElement alloc] initWithNumber: arc4random() % generatedResult] autorelease];
        }
        else {
            self.realResult = [[[NumberElement alloc] initWithNumber: arc4random() % kMaxGeneratedResult ] autorelease];
        }
	}
    
	[displayText appendString: [realResult toString]];
	
}


-(void) generateProblems {
    [self generateProblems:2 withMaxElement:kMaxGeneratedNumber withMaxResult:kMaxGeneratedResult];
}

#pragma mark applying tricks 
#pragma mark -

static NSInteger    _level;
static GameMode     _mode;
static NSRange     _termRange;
static NSInteger   _maxResult;
static BOOL        _hasMultiply;
static NSRange     _factorRange;
static NSUInteger  _maxSubtrahend;
static BOOL        _changeOperator;
static BOOL        _switchPlace;
static BOOL        _subtractTenDigit;

+ (void)initialize {
    _mode = kUnknownMode;
    _level = -1;
    
}

- (void)setLevel:(NSInteger)level mode:(GameMode)mode{
    if (_level != level || _mode != mode) {
        NSDictionary *levelInfo = [ProblemGenerator levelInfo:level inMode:mode];
        _termRange          = NSMakeRange([[levelInfo objectForKey:@"SmallestTerm"] intValue],
                                          [[levelInfo objectForKey:@"BiggestTerm"] intValue]);
        _maxResult          = 4 * _termRange.length;
        _factorRange        = NSMakeRange(_termRange.location, (int)sqrt(_maxResult));
        _hasMultiply        = [[levelInfo objectForKey:@"HasMultiply"] boolValue];
        _maxSubtrahend      = [[levelInfo objectForKey:@"MaxSubtrahend"] intValue];
        _changeOperator     = [[levelInfo objectForKey:@"ChangeOperator"] boolValue];
        _switchPlace        = [[levelInfo objectForKey:@"SwitchPlace"] boolValue];
        _subtractTenDigit   = [[levelInfo objectForKey:@"SubtractTenDigit"] boolValue];
        _level = level;   
        _mode = mode;
        [levelInfo release];
    }

}

- (NSInteger)randomNumberInRange:(NSRange)range {
    NSAssert(range.length > 0, @"Rang length should not be 0");
    return arc4random() % range.length + range.location;
}

- (OperatorType)randomOperator {
    int operatorNo = (_hasMultiply) ? 3 : 2;
    return (OperatorType) (arc4random() % operatorNo);
}

- (NSString *)operatorString:(OperatorType)anOperator {
    switch (anOperator) {
        case 0:
            return @"+";
        case 1:
            return @"-";
        case 2:
            return @"x";
        case 3:
            return @"/";
         break;
    }
    return nil;
}

- (NSString *)operatorString {
    return [self operatorString:_operator];
}

- (void)applySubtrahend {
    NSInteger plusMinus = (arc4random() % 2 == 0)? 1 : -1;
    
    NSInteger randomSubtrahend = arc4random() % _maxSubtrahend + 1;
    
    if (realResult) {
        [realResult release];
        realResult = nil;
    }
    
    realResult = [[NumberElement alloc] initWithNumber:([(NumberElement *) self.expectedResult intValue] + plusMinus * randomSubtrahend)];    
//    DLog(@"expression: %@ - expected: %d", [self displayText], [(NumberElement *) self.expectedResult intValue]);
}

- (void)applyChangeOperator {
    
    OperatorType newOperator;
    while ((newOperator = [self randomOperator]) == _operator) {}
    
    
    NSString *newExpression = [[NSMutableString alloc] initWithFormat:@"%d %@ %d", _firstElement, 
                               [self operatorString:newOperator], _secondElement];
	std::string s;
	long   result;
	int    err;
	
	s = [newExpression cStringUsingEncoding: NSASCIIStringEncoding];
	err = ExpressionEvaluator::calculateLong(s, result);
	self.realResult = [[[NumberElement alloc] initWithNumber:result] autorelease];
    
    DLog(@"expression: %@ - expected: %d", [self displayText], [(NumberElement *) self.expectedResult intValue]);
    
    [newExpression release];
}

- (void)applySwitchPlace {
    //KONG: this trick has good effect on Subtraction & Devision
    
    NSInteger temp;
    
    switch (_operator) {
        case OperatorTypeSubtraction:
        case OperatorTypeDivition:            
            //KONG: switch 2 operant
            temp = _firstElement;
            _firstElement = _secondElement;
            _secondElement = temp;            
            self.realResult = self.expectedResult;            
            break;
        case OperatorTypeAddition:
            _operator = OperatorTypeSubtraction;
            self.realResult = [[[NumberElement alloc] initWithNumber:_secondElement] autorelease];
            _secondElement = [(NumberElement *) self.expectedResult intValue];
            break;
        case OperatorTypeMultiplication:
            //KONG: change position of 2 factors is so trivial
            // 4 * 3 = 12 -> 1 - 4 = 3
            _operator = OperatorTypeSubtraction;            
            if (_firstElement > _secondElement) {                
                temp = _firstElement;
                _firstElement = _secondElement;
                _secondElement = temp;                            
            }
            
            self.realResult = [[[NumberElement alloc] initWithNumber:_firstElement] autorelease];
            _firstElement = _secondElement - _firstElement;
            break;
            
        default:
            break;
    }    

    //KONG: because we change expression, so expectedResult is changed also
    NSString *newExpression = [[NSMutableString alloc] initWithFormat:@"%d %@ %d", _firstElement, 
                               [self operatorString], _secondElement];
	std::string s;
	long   result;
	int    err;
	
	s = [newExpression cStringUsingEncoding: NSASCIIStringEncoding];
	err = ExpressionEvaluator::calculateLong(s, result);
	self.expectedResult = [[[NumberElement alloc] initWithNumber:result] autorelease];
    
    DLog(@"expression: %@ - expected: %d", [self displayText], [(NumberElement *) self.expectedResult intValue]);
    
    [newExpression release];
}

- (void)applySubtractTenDigit {
    //KONG: This trick has good effect on expected result with 2+ digits
    NSInteger plusMinus = (arc4random() % 2 == 0)? 1 : -1;
    
    self.realResult = [[[NumberElement alloc] initWithNumber:
                       ([(NumberElement *) self.expectedResult intValue] + plusMinus * 10)] autorelease];
    
    DLog(@"expression: %@ - expected: %d", [self displayText], [(NumberElement *)self.expectedResult intValue]);

}

- (void)generateTricks {
    NSMutableArray *selectors = [NSMutableArray array];
    [selectors addObject:NSStringFromSelector(@selector(applySubtrahend))];
    
    if (_changeOperator) {
        [selectors addObject:NSStringFromSelector(@selector(applyChangeOperator))];            
    }    

    if (_switchPlace) {
        [selectors addObject:NSStringFromSelector(@selector(applySwitchPlace))];            
    }
    
    if (_subtractTenDigit) {
        [selectors addObject:NSStringFromSelector(@selector(applySubtractTenDigit))];            
    }
    [trickSelectors release];
    trickSelectors = nil;
    trickSelectors = [selectors retain];
}

- (void)applyTrick {
    NSInteger trickIndex;
    
    //KONG: choose trick
    // We can choose a random trick, or a special trick which is suitable for 1 kind of question
    // Apply a special trick make a problem harder than apply a random trick
    BOOL useSpecificTrick = (arc4random() % 4 == 3); // Only 25%
    
    if (useSpecificTrick) {        
        if (_switchPlace && 
            (_operator == OperatorTypeSubtraction || _operator == OperatorTypeDivition)) {
            
            [self applySwitchPlace];
        } else if (_subtractTenDigit 
                   && ([(NumberElement *) self.expectedResult intValue] % 10 > 0)) {
            
            [self applySubtractTenDigit];
            
        } else {
            //KONG: did not choose any special trick
            useSpecificTrick = NO;
        }
    } 
    
    if (useSpecificTrick == NO) {
        trickIndex = arc4random() % [trickSelectors count];
        [self performSelector:NSSelectorFromString([trickSelectors objectAtIndex:trickIndex])];    
    }
}

- (void)generate:(NSInteger)level mode:(GameMode)mode {
    //KONG: set level info from plist file
    [self setLevel:level mode:mode];
    [self generateTricks];
    
    // generate operation
    _operator = [self randomOperator];
    
    // generate operant

    //KONG: don't want multiply 2 big number, constrain second factor
    if (_operator == OperatorTypeMultiplication) {
        _firstElement = [self randomNumberInRange:_factorRange];     
        _secondElement = [self randomNumberInRange:_factorRange];
        
    } else {
        _firstElement = [self randomNumberInRange:_termRange];
        _secondElement = [self randomNumberInRange:_termRange];
    }


    // compose actual expression
    NSString *expression = [[NSString alloc] initWithFormat:@"%d %@ %d", _firstElement, [self operatorString], _secondElement];
	std::string s;
	long   result;
	int    err;
	
	s = [expression cStringUsingEncoding: NSASCIIStringEncoding];		// NSString to std:string conversion
	err = ExpressionEvaluator::calculateLong(s, result);					// evaluate the result in long format
	self.expectedResult = [[[NumberElement alloc] initWithNumber:result] autorelease];		// store value to self's property
	
	BOOL useExpectedResult = (arc4random() % 2 == 0);
	
	if (useExpectedResult) {
		self.realResult = [[[NumberElement alloc] initWithNumber:result] autorelease];
	} else {
        [self applyTrick];
	}

    DLog(@"%@", [self displayText]);
    
    [expression release];
}

#pragma mark -

- (NSString *)displayText {
    return [NSString stringWithFormat:@"%d %@ %d = %d", _firstElement, [self operatorString], 
            _secondElement, [(NumberElement *)self.realResult intValue]];
}

-(NSString*) getGeneratedProblemsInString {
    return displayText;
}

-(ProblemResult)check:(Element*)realResult userInput:(BOOL)userInput {
	NumberElement* _expectedResult = (NumberElement*)self.expectedResult;
	NumberElement* _realResult = (NumberElement*)self.realResult;
	
	if ([_expectedResult intValue] == [_realResult intValue]) {
		if (userInput == TRUE) {
			return kResultRight;
		} else {
			return kResultWrong;
		}
	}
	
	if ([_expectedResult intValue] != [_realResult intValue]) {
		if (userInput == FALSE) {
			return kResultRight;
		} else {
			return kResultWrong;
		}
	}
	
	return kResultUnknown;
}

-(BOOL)getExpectedResult {
    NumberElement* _expectedResult = (NumberElement*)self.expectedResult;
	NumberElement* _realResult = (NumberElement*)self.realResult;
	
	return ([_expectedResult intValue] == [_realResult intValue]);
}

@end