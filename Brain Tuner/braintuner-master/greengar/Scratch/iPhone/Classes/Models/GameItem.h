//
//  GameItem.h
//  BrainTuner2
//
//  Created by Hector Zhao on 3/8/11.
//  Copyright 2011 GreenGar studios. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseProblem.h"


@interface GameItem : NSObject {
	BaseProblem *problem;
	ProblemResult	problemResult;
}

@property (nonatomic, retain)	BaseProblem *problem;
@property (nonatomic)			ProblemResult	problemResult;

@end
