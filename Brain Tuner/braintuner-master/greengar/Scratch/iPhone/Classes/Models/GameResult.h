//
//  GameResult.h
//  BrainTuner2
//
//  Created by Cong Vo on 5/23/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GameUtils.h"

@interface GameResult : NSObject {
    /*
     Output:
     - time
     - score
     - incorrect
     - correct
     //Debug
     - expected score
     - expected time
     
     */
    CFTimeInterval time;
    
    NSInteger score;
    
    NSInteger incorrect;
    NSInteger correct;
    
    NSInteger skip;
}

@property (nonatomic, assign) CFTimeInterval time;
@property (nonatomic, assign) NSInteger score;
@property (nonatomic, assign) NSInteger incorrect, correct, skip;

- (NSString *)resultString;

@end
