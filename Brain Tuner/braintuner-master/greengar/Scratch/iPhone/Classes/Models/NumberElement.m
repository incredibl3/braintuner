//
//  NumberElement.m
//  BrainTuner
//
//  Created by Silvercast on 9/28/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "NumberElement.h"


@implementation NumberElement

-(id)initWithNumber:(int)num {
    if ((self = [super init])) {
        number = num;
    }
	return self;
}

-(int)intValue {
	return number;
}

-(NSString*)toString
{
	return [NSString stringWithFormat:@"%d", number];
}

@end
