//
//  NumberElement.h
//  BrainTuner
//
//  Created by Silvercast on 9/28/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Element.h"

@interface NumberElement : Element {
	int number;
}

// override Element's toString method
-(NSString*)toString;	

// NumberElement's methods
-(id)initWithNumber:(int)num;
-(int)intValue;
@end
