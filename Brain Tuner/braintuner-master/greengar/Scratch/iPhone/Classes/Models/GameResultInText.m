//
//  GameResultInText.m
//  BrainTuner2
//
//  Created by Hector Zhao on 3/8/11.
//  Copyright 2011 Greengar Studios. All rights reserved.
//

#import "GameResultInText.h"


@implementation GameResultInText

@synthesize title, message;

- (void)dealloc {
    [title release];
    [message release];
    [super dealloc];
}
@end
