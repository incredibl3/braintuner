//
//  EquityProblem.h
//  BrainTuner2
//
//  Created by Hector Zhao on 3/2/11.
//  Copyright 2011 Greengar Studios. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseProblem.h"
#import "Element.h"
#import "GameUtils.h"

@interface EquityProblem : BaseProblem {

}

- (void)		generateProblems;
- (void)        generateProblems:(int)numOfElements;
- (NSString *)	getGeneratedProblemsInString;
- (ProblemResult)	check:(Element *)expectedResult userInput:(BOOL)userInput;
- (BOOL)        getExpectedResult;

@end
