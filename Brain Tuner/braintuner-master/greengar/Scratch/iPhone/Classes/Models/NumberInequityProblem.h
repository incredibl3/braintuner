//
//  NumberInequityProblem.h
//  BrainTuner2
//
//  Created by Hector Zhao on 3/2/11.
//  Copyright 2011 Greengar Studios. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "InEquityProblem.h"
#import "Element.h"
#import "GameUtils.h"

@interface NumberInequityProblem : InEquityProblem {

}

-(void)			generateProblems;
-(void)         generateProblems:(int) numOfElements withMaxElement:(int)generatedNumber withMaxResult:(int)generatedResult withMaxRandomSubtrahend:(int)randomSubtrahend;
-(NSString*)	getGeneratedProblemsInString;
-(ProblemResult)	check:(Element*)displayResult userInput:(BOOL)userInput;
-(BOOL)         getExpectedResult;

@end
