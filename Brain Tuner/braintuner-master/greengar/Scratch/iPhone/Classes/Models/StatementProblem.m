//
//  StatementProblem.m
//  BrainTuner2
//
//  Created by Hector Zhao on 3/2/11.
//  Copyright 2011 Greengar Studios. All rights reserved.
//

#import "StatementProblem.h"
#import "StringElement.h"

@implementation StatementProblem

-(void) generateProblems {
	// next we generate display result, it could be either result or a different value
	displayText = [[NSMutableString alloc] initWithString:@"I'm 5. U're 4. "];
	BOOL useExpectedResult = (arc4random() % 2 == 0);
	
	NSString *result = @"I'm older than you!";
	self.expectedResult = [[StringElement alloc] initWithString:result];
	
	if (useExpectedResult) {
		self.realResult = [[StringElement alloc] initWithString:result];
	} else {
		self.realResult = [[StringElement alloc] initWithString:@"I'm younger than you!"];
	}
}

-(NSString*) getGeneratedProblemsInString {
	return displayText;
}

-(ProblemResult)check:(Element*)realResult userInput:(BOOL)userInput {
	StringElement* _expectedResult = (StringElement*)self.expectedResult;
	StringElement* _realResult = (StringElement*)self.realResult;
	
	BOOL stringEqual = [[_expectedResult toString] isEqualToString:[_realResult toString]];
	if (stringEqual) {
		if (userInput == TRUE) {
			return kResultRight;
		} else {
			return kResultWrong;
		}
	}
	
	if (!stringEqual) {
		if (userInput == FALSE) {
			return kResultRight;
		} else {
			return kResultWrong;
		}
	}
	
	return FALSE;
}

-(BOOL)getExpectedResult {
    StringElement* _expectedResult = (StringElement*)self.expectedResult;
	StringElement* _realResult = (StringElement*)self.realResult;
	
	return [[_expectedResult toString] isEqualToString:[_realResult toString]];
}

@end
