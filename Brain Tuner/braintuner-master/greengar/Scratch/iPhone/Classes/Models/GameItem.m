//
//  GameItem.m
//  BrainTuner2
//
//  Created by Hector Zhao on 3/8/11.
//  Copyright 2011 GreenGar studios. All rights reserved.
//

#import "GameItem.h"


@implementation GameItem

@synthesize problem, problemResult;

- (void)dealloc {
    [problem release];
    [super dealloc];
}

@end
