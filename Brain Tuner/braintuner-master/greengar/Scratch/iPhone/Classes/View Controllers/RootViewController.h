//
//  RootViewController.h
//  BrainTuner2
//
//  Created by Hector Zhao on 03/28/11.
//  Copyright 2011 Greengar Studios, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GameKit/GameKit.h>
#import "HomeView.h"
#import "cocos2d.h"
#import "UIWindowForShake.h"
#import "SoundManager.h"

@interface RootViewController : UIViewController <GKLeaderboardViewControllerDelegate, GKAchievementViewControllerDelegate> 
{
        
    HomeView *homeViewObj;
    UIWindowForShake *window;      
    UIImagePickerController *imagePickerController;
    int    textureLoadingIndex;
}

@property (nonatomic, retain) UIWindowForShake *window;

- (void)loadImagePickerSourceType:(UIImagePickerControllerSourceType)sourceType;

@end
