  //
//  RootViewController.m
//  BrainTuner2
//
//  Created by Hector Zhao on 03/28/11.
//  Copyright 2011 Greengar Studios, LLC. All rights reserved.
//

#import "RootViewController.h"
#import "Scratch.h"

@implementation RootViewController

@synthesize window;

- (void)loadImagePickerSourceType:(UIImagePickerControllerSourceType)sourceType {
	
	if (!imagePickerController) {
		imagePickerController = [[UIImagePickerController alloc] init];
		imagePickerController.delegate = self;
		
		//imagePickerController.allowsImageEditing = YES;
		
		//	UIBarStyleDefault          = 0,
		//	UIBarStyleBlack            = 1,
		//
		//	UIBarStyleBlackOpaque      = 1, // Deprecated. Use UIBarStyleBlack
		//	UIBarStyleBlackTranslucent = 2, // Deprecated. Use UIBarStyleBlack and set the translucent property to YES		
	}
	//DLog(@"%d ; %@ ; %d", imagePickerController.navigationBar.barStyle, imagePickerController.navigationBar.tintColor, imagePickerController.navigationBar.translucent);
	
	imagePickerController.sourceType = sourceType;
	
	if (IS_IPAD) {
		
		// dismiss popover
		Picker *picker = self.picker; //[Picker sharedPicker];
		UIPopoverController *popoverController = [picker connectPopoverController];
		if (popoverController) {
			[popoverController dismissPopoverAnimated:YES];
			[picker popoverControllerDidDismissPopover:popoverController];
		}
		[picker dismissHintPopoverAnimated:YES]; // "Tap green arrow to hide drawing tools"
		
		self.popoverController = [[UIPopoverController alloc] initWithContentViewController:imagePickerController];
		//[popoverController presentPopoverFromBarButtonItem:(UIBarButtonItem *)_picker.openButton permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
		[self.popoverController presentPopoverFromRect:CGRectOffset(_picker.openButton.frame, 0, 20) inView:_picker permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
		[self.popoverController release];
		//[popoverController release]; //Terminating app due to uncaught exception 'NSGenericException', reason: '-[UIPopoverController dealloc] reached while popover is still visible.'
	} else {
		
		[pickerViewController presentModalViewController:imagePickerController animated:YES];
		
	}
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingImage:(UIImage *)image editingInfo:(NSDictionary *)editingInfo {
	[self performSelector:@selector(loadPickedImage:) withObject:[image retain] afterDelay:0.0];
	
	if (IS_IPAD) {
		[self.popoverController dismissPopoverAnimated:YES]; // doesn't reset the state of the imagePickerController, which might be good
	} else {
		[imagePickerController dismissModalViewControllerAnimated:YES];
	}
}

- (void)loadPickedImage:(UIImage *)image {
	DLog(@"Finished picking image with width:%f height:%f", image.size.width, image.size.height);
	
	if (![drawingView loadFromSavedPhotoAlbum:image]) {
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Loading Failed"
														message:@"Unable to load image. If this error persists, contact elliot@greengarstudios.com" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[alert show];
		[alert release];
	}
	[image release];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
	// BUG FIX:
	[imagePickerController dismissModalViewControllerAnimated:YES];
}

#pragma mark -
#pragma mark View lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    SoundManager *soundManager = [SoundManager sharedSoundManager];
    [soundManager play:@"kororo.mp3" loop:YES];

    homeViewObj = [[HomeView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    [self.view addSubview:homeViewObj];
	
    CCDirector *director = [CCDirector sharedDirector];
    [director setOpenGLView:(EAGLView *)homeViewObj];
    [director enableRetinaDisplay:YES];
    [director setDeviceOrientation:kCCDeviceOrientationPortrait];
    [director setAnimationInterval:1.0/60];
    [director setDisplayFPS:NO];

	[CCTexture2D setDefaultAlphaPixelFormat:kCCTexture2DPixelFormat_RGBA8888];
    
	// Run the intro Scene
    Scratch *homeScene = [Scratch node];
	
    window = [[UIWindowForShake alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    [[CCDirector sharedDirector] runWithScene:homeScene];
    //[homeScene showWelcomeViewWithAppearingBrainGuy];
    
    [self.view setHidden:FALSE];
    
    [self setTitle:@"Kororo's magic"];
    
}

#pragma mark -
#pragma mark Memory management

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Relinquish ownership any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
    // Relinquish ownership of anything that can be recreated in viewDidLoad or on demand.
    // For example: self.myOutlet = nil;
}


- (void)dealloc {
    [super dealloc];
    SoundManager *soundManager = [SoundManager sharedSoundManager];
    [soundManager stop:@"kororo.mp3"];
}


@end

