//
//  UIWindowForShake.m
//  Whiteboard
//
//  Created by Silvercast on 11/25/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "UIWindowForShake.h"

@implementation UIWindowForShake

- (void)motionEnded:(UIEventSubtype)motion withEvent:(UIEvent *)event {
	if (motion == UIEventSubtypeMotionShake) {
		DataManager *dataManager = [DataManager sharedDataManager];
        dataManager.shakeNumber++;
        
        // Achievement: "Nervous"
        if (dataManager.shakeNumber == 3 && ![dataManager checkAchievement:17]) {
            [dataManager updateAchievement:17];
        }
	}
}

@end
