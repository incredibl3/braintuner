//
//  HomeScene.h
//  BrainTuner2
//
//  Created by Silvercast on 2/27/11.
//  Copyright 2011 GreenGar studios. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "cocos2d.h"
#import "SoundManager.h"

@interface Scratch : CCScene {
    BOOL musicOn;
    CCMenuItemToggle        *music;
    CCMenuItemSprite        *kororo;
    CCParticleSystemQuad    *magic;
    CCLabelTTF              *magicName;
}

- (void)appear:(CCNode *)node withCallback:(SEL)callBack;
- (void)disappear:(CCNode *)node withCallback:(SEL)callback;
- (void)wave:(CCNode *)node withCallback:(SEL)callback;
- (void)rain:(CCNode *)node withCallback:(SEL)callback;
        
@end
