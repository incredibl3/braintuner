//
//  HomeScene.m
//  BrainTuner2
//
//  Created by Silvercast on 2/27/11.
//  Copyright 2011 GreenGar studios. All rights reserved.
//

#import "Scratch.h"

@implementation Scratch

- (void) kororoTapped:(id)sender {
    NSLog(@"I am tapped");
    [self wave:sender withCallback:nil];
    //[self disappear:sender withCallback:nil];
    [self rain:sender withCallback:nil];
}

- (void) musicTapped:(id)sender {
    //NSLog(@"I am tapped");
    //[self wave:sender withCallback:nil];
    //[self disappear:sender withCallback:nil];
    //[self rain:sender withCallback:nil];
    //NSString *file = nil;
    if(musicOn) {
        //file = @"music_off.png";
        SoundManager *soundManager = [SoundManager sharedSoundManager];
        [soundManager stop:@"kororo.mp3"];
        musicOn = NO;
    } else {
        //file = @"music_on.jpeg";
        SoundManager *soundManager = [SoundManager sharedSoundManager];
        [soundManager play:@"kororo.mp3" loop:YES];
        musicOn = YES;
    }
    
    //music.exture = [[CCTextureCache sharedTextureCache] addImage: @"stars.png"];
}

- (id) init
{
    self = [super init];
    if (self != nil) {
        CGSize screenSize = [CCDirector sharedDirector].winSize;
        kororo = [CCMenuItemSprite itemFromNormalSprite:[CCSprite spriteWithFile:@"kororo.jpeg"]
                                         selectedSprite:[CCSprite spriteWithFile:@"kororo.jpeg"] 
                                                 target:self selector:@selector(kororoTapped:)];
        CCMenuItemImage * soundOnItem = [CCMenuItemImage itemFromNormalSprite:[CCSprite spriteWithFile:@"music_on.jpeg"]                 
                                                               selectedSprite:[CCSprite spriteWithFile:@"music_on.jpeg"]];
        CCMenuItemImage * soundOffItem = [CCMenuItemImage itemFromNormalSprite:[CCSprite spriteWithFile:@"music_off.png"]  
                                                                selectedSprite:[CCSprite spriteWithFile:@"music_off.png"]];
        music = [CCMenuItemToggle itemWithTarget:self selector:@selector(musicTapped:) items:soundOnItem, soundOffItem, nil] ;
        musicOn = YES;
        magicName = [CCLabelTTF labelWithString:@"Kororo's magic" fontName:@"Denne Kitten Heels" fontSize:33];
        //[self addChild:backgroundLayer];
        CCMenu *menu = [CCMenu menuWithItems:kororo, music, nil];
        menu.position = CGPointZero;
        kororo.position = ccp(screenSize.width/2, screenSize.height/2);
        magicName.position = ccp(screenSize.width/2, screenSize.height/2 - 200);
        music.position = ccp(25, screenSize.height - 25);
        music.scale = 0.8;
        [self addChild:menu];
        [self addChild:magicName];
        
    }
    return self;
}

// appear effect 
- (void)appear:(CCNode *)node withCallback:(SEL)callback {
    //node.visible = YES;
    id action1 = [CCFadeIn actionWithDuration:1];
    id action2 = [CCScaleBy actionWithDuration:0.6 scale:1];
    //id action3 = [CCScaleBy actionWithDuration:0.5 scale:1];
    [node runAction: [CCSpawn actions:action1, action2, nil]];
}

// disappear effect 
- (void)disappear:(CCNode *)node withCallback:(SEL)callback {
    id action1 = [CCFadeOut actionWithDuration:0.5];
    id action2 = [CCScaleBy actionWithDuration:0.5 scale:1.3];
    id action3 = [CCScaleBy actionWithDuration:0.5 scale:1];
    if (callback) {
        [node runAction:[CCSequence actions:[CCSpawn actions:action1, action2, action3, nil], [CCCallFunc actionWithTarget:self selector:callback], nil]];
    }
    else {
        [node runAction:[CCSpawn actions:action1, action2, nil]];
    }
    //node.visible = NO;
    
}

// wave effect 
- (void)wave:(CCNode *)node withCallback:(SEL)callback {
    id action = [CCWaves3D actionWithWaves:5 amplitude:20 grid:ccg(15,10) duration:3];
    [node runAction: [CCSpawn actions:action, action, nil]];
}

// rain particle
- (void)rain:(CCNode *)node withCallback:(SEL)callback {
    CGSize screenSize = [CCDirector sharedDirector].winSize;
    //[magic resetSystem];
    [magic stopSystem];
    magic = nil;
    int magicId = random() % 6;
    switch (magicId) {
        case 0:
            magic = [CCParticleRain node];
            magic.position = ccp(screenSize.width/2, screenSize.height);
            [magicName setString:[NSString stringWithFormat:@"forest's hope"]];
            break;
        case 1:
            magic = [CCParticleSnow node];
            magic.position = ccp(screenSize.width/2, screenSize.height);
            [magicName setString:[NSString stringWithFormat:@"starfall"]];
            //magic.speed += 10;
            break;
        case 2:
            magic = [CCParticleExplosion node];
            magic.position = ccp(screenSize.width/2, screenSize.height/2 + 100);
            [magicName setString:[NSString stringWithFormat:@"big bang"]];
            break;
        case 3:
            magic = [CCParticleSun node];
            magic.position = ccp(screenSize.width/2, screenSize.height/2 + 100);
            [magicName setString:[NSString stringWithFormat:@"rising sun"]];
            break;
        case 4:
            magic = [CCParticleFire node];
            magic.position = ccp(screenSize.width/2, screenSize.height/2 + 100);
            [magicName setString:[NSString stringWithFormat:@"ancient fire"]];
            break;
        case 5:
            magic = [CCParticleFlower node];
            magic.position = ccp(screenSize.width/2, screenSize.height/2 + 100);
            [magicName setString:[NSString stringWithFormat:@"cherry blossom"]];
            break;
        default:
            break;
    }
    magic.texture = [[CCTextureCache sharedTextureCache] addImage: @"stars.png"];
    [self appear:magicName withCallback:nil];
    [self addChild:magic z:10];
    
}

- (void)dealloc {
    [music release];
    [magicName release];
	[magic release];
	[super dealloc];
}

@end