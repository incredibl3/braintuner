//
//  TestStoring.h
//  BrainTuner2
//
//  Created by Hector Zhao on 5/31/11.
//  Copyright 2011 Greengar Studios. All rights reserved.
//

#import <SenTestingKit/SenTestingKit.h>
#import <UIKit/UIKit.h>
#import "DataManager.h"
#import "GameManager.h"

@interface TestStoring : SenTestCase {
    DataManager *dataManager;
    GameManager *gameManager;
}

- (void)testUpdatingAndScoringData;
- (void)printResult;
@end
