//
//  TestScoring.m
//  BrainTuner2
//
//  Created by Cong Vo on 5/24/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "TestScoring.h"
#import "GameResult.h"
#import "ProblemGenerator.h"
@implementation TestScoring
// all code under test must be linked into the Unit Test bundle


#pragma mark Classic

static NSInteger const kPenaltyPoint = 10;
static CGFloat const kFasterBonusEachTenthSec = 1;
//KONG: minus less when ppl play too slow
// this provide better score for user know their progress
static CGFloat const kSlowerMinusEachTenthSec = 0.5;

- (GameResult *)getClassicModeResult {
    //KONG: bug: why call me twice?
    GameResult *result = [[[GameResult alloc] init] autorelease];
    
    CFTimeInterval time;
    
    time = CFAbsoluteTimeGetCurrent() - startTime;
    
    // simple round    
    NSInteger tenPlayTime = round(time * 10);    
    result.time = (CFTimeInterval)tenPlayTime / 10;
    
    result.incorrect = incorrectProblems;
    
    //KONG: scoring
    result.score = 100;

    NSInteger levelAverageTime = 20 - selectedLevel;    
    
    if (incorrectProblems == 0) {  // when all right, minus/plus bonus time
        if (result.time < levelAverageTime) {
            result.score += (levelAverageTime * 10 - tenPlayTime) * kFasterBonusEachTenthSec;
        } else {
            result.score += 
            (levelAverageTime * 10 - tenPlayTime) * kSlowerMinusEachTenthSec;            
        }
        
    } else { // penalty, and minus slow time
        int penalty = kPenaltyPoint * incorrectProblems;    
        result.score -= penalty;
        
        
        if (result.time > levelAverageTime) {
            // only minus, no reward when wrong            
            result.score += (levelAverageTime * 10 - tenPlayTime) * kSlowerMinusEachTenthSec;
        }
    }
    
    //KONG: min score 10
    if (correctProblems == 0) {
        result.score = 0;
    } else {
        if (result.score < 10) {
            result.score = 10;
        }
    }
    
	return result;
}

- (void)prepareClassicTime:(CFTimeInterval)interval incorrect:(NSInteger)incorrect {
    startTime = CFAbsoluteTimeGetCurrent() - interval;    
    incorrectProblems = incorrect;
    correctProblems = 20 - incorrectProblems;
}

- (void)printResultForClassicTime:(CFTimeInterval)interval incorrect:(NSInteger)incorrect {
    [self prepareClassicTime:interval incorrect:incorrect];
    GameResult *result = [self getClassicModeResult];    
    printf("| %.2f | %2d | %4d |\n", result.time, result.incorrect, result.score);
}

- (void)testClassicLevel1 {
    
    selectedLevel = 0;
    
    [self printResultForClassicTime:20 incorrect:0];
    [self printResultForClassicTime:20 incorrect:1];
    [self printResultForClassicTime:20 incorrect:2];
    [self printResultForClassicTime:20 incorrect:3];
    [self printResultForClassicTime:20 incorrect:4];    
    [self printResultForClassicTime:19 incorrect:0];
    [self printResultForClassicTime:21 incorrect:0];
    [self printResultForClassicTime:22 incorrect:0];
    [self printResultForClassicTime:23 incorrect:0];
    [self printResultForClassicTime:24 incorrect:0];
    [self printResultForClassicTime:25 incorrect:0];    
    [self printResultForClassicTime:26 incorrect:0];    
    [self printResultForClassicTime:27 incorrect:0];        
    [self printResultForClassicTime:19.5 incorrect:0];
    [self printResultForClassicTime:20.5 incorrect:0];    
    [self printResultForClassicTime:20.5 incorrect:1];    
    [self printResultForClassicTime:20.5 incorrect:2];    
    [self printResultForClassicTime:20.5 incorrect:3];        
    [self printResultForClassicTime:30.5 incorrect:3];        
    [self printResultForClassicTime:35.5 incorrect:3];        
    [self printResultForClassicTime:40.5 incorrect:3];      
}


- (void)testClassicLevel2 {
    
    selectedLevel = 1;
    
    [self printResultForClassicTime:20 incorrect:0];
    [self printResultForClassicTime:20 incorrect:1];
    [self printResultForClassicTime:20 incorrect:2];
    [self printResultForClassicTime:20 incorrect:3];
    [self printResultForClassicTime:20 incorrect:4];    
    [self printResultForClassicTime:19 incorrect:0];
    [self printResultForClassicTime:21 incorrect:0];
    [self printResultForClassicTime:22 incorrect:0];
    [self printResultForClassicTime:23 incorrect:0];
    [self printResultForClassicTime:24 incorrect:0];
    [self printResultForClassicTime:25 incorrect:0];    
    [self printResultForClassicTime:26 incorrect:0];    
    [self printResultForClassicTime:27 incorrect:0];        
    [self printResultForClassicTime:19.5 incorrect:0];
    [self printResultForClassicTime:20.5 incorrect:0];    
    [self printResultForClassicTime:20.5 incorrect:1];    
    [self printResultForClassicTime:20.5 incorrect:2];    
    [self printResultForClassicTime:20.5 incorrect:3];        
    [self printResultForClassicTime:30.5 incorrect:3];        
    [self printResultForClassicTime:35.5 incorrect:3];        
    [self printResultForClassicTime:40.5 incorrect:3];      
}

- (void)testClassicLevel5 {
    
    selectedLevel = 4;
    
    [self printResultForClassicTime:16 incorrect:0];
    [self printResultForClassicTime:16 incorrect:1];
    [self printResultForClassicTime:16 incorrect:2];
    [self printResultForClassicTime:16 incorrect:3];
    [self printResultForClassicTime:16 incorrect:4];    
    [self printResultForClassicTime:15 incorrect:0];
    [self printResultForClassicTime:17 incorrect:0];
    [self printResultForClassicTime:18 incorrect:0];
    [self printResultForClassicTime:19 incorrect:0];
    [self printResultForClassicTime:20 incorrect:0];
    [self printResultForClassicTime:10 incorrect:0];    
    [self printResultForClassicTime:11 incorrect:0];    
    [self printResultForClassicTime:12 incorrect:0];        
    [self printResultForClassicTime:19.5 incorrect:0];
    [self printResultForClassicTime:20.5 incorrect:0];    
    [self printResultForClassicTime:20.5 incorrect:1];    
    [self printResultForClassicTime:20.5 incorrect:2];    
    [self printResultForClassicTime:20.5 incorrect:3];        
    [self printResultForClassicTime:30.5 incorrect:3];        
    [self printResultForClassicTime:35.5 incorrect:3];        
    [self printResultForClassicTime:40.5 incorrect:3];      
}

#pragma mark Time


- (GameResult *)getTimeModeResult {
    GameResult *result = [[[GameResult alloc] init] autorelease];
    result.incorrect = incorrectProblems;
    result.correct = correctProblems;
    
//    NSInteger averageCorrectProblems = [ProblemGenerator timeProblemsAverageNo:selectedLevel];
    NSInteger averageCorrectProblems = 20 + selectedLevel;
    result.score  = 100;
    if (incorrectProblems == 0) {  // when all right, minus/plus bonus problems
        if (correctProblems > averageCorrectProblems) {
            result.score += (correctProblems - averageCorrectProblems) * 10;
        } else {
            result.score -= 
            (averageCorrectProblems - correctProblems) * 5;            
        }
        
    } else { // penalty, and minus slow time
        int penalty = kPenaltyPoint * incorrectProblems;    
        result.score -= penalty;
        
        if (averageCorrectProblems > correctProblems) {
            // only minus, no reward when wrong            
            result.score -= (averageCorrectProblems - correctProblems) * 5;
        }
    }
    
    //KONG: min score 10
    if (correctProblems == 0) {
        result.score = 0;
    } else {
        if (result.score < 10) {
            result.score = 10;
        }
    }
    
    
	return result;
}


- (void)printResultForTimeCorrect:(NSInteger)correct incorrect:(NSInteger)incorrect {
    correctProblems = correct;
    incorrectProblems = incorrect;

    GameResult *result = [self getTimeModeResult];    
    printf("| %.2d | %2d | %4d |\n", result.correct, result.incorrect, result.score);
}

- (void)testTimeLevel1 {
    
    selectedLevel = 0;

    for (int i = 13; i< 25; i++) {
        [self printResultForTimeCorrect:i incorrect:0];            
    }

    
    [self printResultForTimeCorrect:13 incorrect:1];    
    [self printResultForTimeCorrect:19 incorrect:1];
    [self printResultForTimeCorrect:20 incorrect:1];    
    [self printResultForTimeCorrect:21 incorrect:1];    
    [self printResultForTimeCorrect:22 incorrect:1];        
    [self printResultForTimeCorrect:25 incorrect:1];            
    
}

- (void)testTimeLevel4 {
    
    selectedLevel = 4;
    
    for (int i = 15; i< 29; i++) {
        [self printResultForTimeCorrect:i incorrect:0];            
    }
    
    [self printResultForTimeCorrect:13 incorrect:1];    
    [self printResultForTimeCorrect:19 incorrect:1];
    [self printResultForTimeCorrect:20 incorrect:1];    
    [self printResultForTimeCorrect:21 incorrect:1];    
    [self printResultForTimeCorrect:22 incorrect:1];        
    [self printResultForTimeCorrect:25 incorrect:1];            
    
}




@end
