//
//  TestStoring.m
//  BrainTuner2
//
//  Created by Hector Zhao on 5/31/11.
//  Copyright 2011 Greengar Studios. All rights reserved.
//

#import "TestStoring.h"


@implementation TestStoring

- (void) testUpdatingAndScoringData {
    
    [dataManager readDataFromPreferences];
    [self printResult];
    
    // Data
    gameManager.gameMode = kClassicMode; // kTimeMode // kInfiniteMode;
    gameManager.selectedLevel = 1; // 1..10;
    
    [gameManager startGame];
    
    gameManager.correctProblems = 20; // 0..20;
    gameManager.incorrectProblems = 0; // 0..20;
    gameManager.startTime = CFAbsoluteTimeGetCurrent();
    
    [gameManager endGame];
    [gameManager getResult];
    
    [self printResult];
    
    [dataManager writeDataToPreferences];
    [self printResult];
    
    [dataManager resetDataFromPreferences];
    [self printResult];
}

- (void)printResult {
    
    for (int i = 0; i < kNumOfModes; i++) {
        BoxScoredPoint *boxPointObject = [dataManager.boxScoredArray objectAtIndex:i];
        printf("\nMode: %d", i);
        printf("\nIs unlocked: %@", boxPointObject.isUnlocked ? "YES" : "NO");
        printf("\nUnlocked Level: %d", boxPointObject.unlockedLevel);
        printf("\nPoint: %d", boxPointObject.totalPoint);
        for (int j = 0; j < kNumOfLevelsPerBox; j++) {
            printf("\n%d Points for Level: %d", [boxPointObject getPointForLevel:j], j);
        }
        printf("\n----------------------------------------------------------------");
    }
}

@end
