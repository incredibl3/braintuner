//
//  CCSprite+Resize.m
//  BrainTuner2
//
//  Created by silvercast on 3/9/11.
//  Copyright 2011 GreenGar studios. All rights reserved.
//

#import "CCSprite+Resize.h"


@implementation CCSprite (Resize)

-(void)resizeTo:(CGSize) theSize
{
    CGFloat newWidth = theSize.width;
    CGFloat newHeight = theSize.height;
	
	
    float startWidth = self.contentSize.width;
    float startHeight = self.contentSize.height;
	
    float newScaleX = newWidth/startWidth;
    float newScaleY = newHeight/startHeight;
	
    self.scaleX = newScaleX;
    self.scaleY = newScaleY;
	
}

@end
