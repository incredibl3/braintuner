//
//  GameUtils.h
//  BrainTuner2
//
//  Created by Hector Zhao on 3/2/11.
//  Copyright 2011 Greengar Studios. All rights reserved.
//

#import <Foundation/Foundation.h>

#define kNumOfProblemsClassicMode           20  // Limited number of problems
#define kNumOfProblemsTimeMode              -1  // Unlimited number of problems in a specific period of time
#define kNumOfProblemsInfiniteMode          -1  // Unlimited number of problems until 3 problems are evaluated as wrong
#define kLimitTimeMode                      15  // Limited time
#define kNumOfPreloadProblemsTimeMode       10  // Preload 10 problems for time mode
#define kNumOfPreloadProblemsInfiniteMode   10  // Preload 10 problems for infinite mode
#define kNumOfPreloadProblemsRelaxMode      10  // Preload 10 problems for relax mode

#define kNumOfModes                         3   // 1. Classic Mode
                                                // 2. Time Mode
                                                // 3. Infinite Mode
                                                // 4. Relax Mode is always available and does not score any Points
                                    
#define kNumOfLevelsPerBox                  10  // 10 levels per box

#define kMinPointToUnlockNewLevel           100 // At least 100 points required to unlock new level

#define kMinPointToUnlockNewBox             600 // At least 600 points required to unlock new box

#define kNumOfElements                      2   // Only allow expression with 2 operators


#define kTimeOutInfiniteMode                2   // Timeout 2 seconds for each problem on Infinite mode
#define kTimeOutInfiniteModeFirstProblem    10  // Timeout 10 seconds for the first problem on Infinite mode
#define kNumOfSkipAnswerAllowInfiniteMode   3   // Can skip answer 3 times only


#define kCountDown                          1   // 1 seconds for countdown timer

#define kOnAboutNotification                                    @"OnAboutNotification"
#define kOniOSGameCenterLeaderboardNotification                 @"OniOSLeaderboardNotification"
#define kOniOSGameCenterAchievementNotification                 @"OniOSAchievementNotification"
#define kSubmitAchievementNotification                          @"SubmitAchievementNotification"
#define kSubmitAchievementNotifyUserNotification                @"SubmitAchievementNotifyUserNotification"
#define kSubmitResetAchievementNotification                     @"SubmitResetAchievementNotification"
#define kSubmitHighScoreNotification                            @"SubmitHighScoreNotification"
#define kShowAdViewNotification                                 @"ShowAdViewNotification"
#define kHideAdViewNotification                                 @"HideAdViewNotification"

#define kPreferenceHighScoreForBoxAtLevelStringFormat           @"High_Score_Box%d_Level%d"
#define kPreferencePointForBoxAtLevelStringFormat               @"Point_Box%d_Level%d"
#define kPreferencePointForBoxStringFormat                      @"Point_Box%d"
#define kPreferenceUnlockLevelForBoxStringFormat                @"Unlock_Level_Box%d"
#define kPreferenceUnlockBoxStringFormat                        @"Unlock_Box%d"
#define kPreferenceSoundEnable                                  @"Sound_Enable"
#define kPreferenceCountdownTimerEnable                         @"Countdown_Timer_Enable"
#define kPreferenceUsername                                     @"Username"
#define kPreferencePassword                                     @"Password"
#define kPreferenceAchievementListStringFormat                  @"Achievement_Got%d"

#define kLeaderboardTitle           @"MainLeaderboard"

#define kButtonTapMusic             @"ButtonTapMusic.wav"
#define kBackgroundMusic            @"BackgroundMusic.wav"
#define KPlayMusic                  @"PlayMusic.wav"
#define kWinnerMusic                @"WinnerMusic.wav"
#define kFailureMusic               @"FailureMusic.wav"
#define kRightMusic                 @"RightMusic.wav"
#define kWrongMusic                 @"WrongMusic.wav"
#define kStampMusic                 @"StampMusic.wav"

// Play Scene
#define kPlaySceneTotalEmptyRows 4

// About Scene
#define kAboutSceneVersionHeightOffset 410
#define kAboutSceneLogoHeightOffset 200
#define kAboutSceneTitleBarHeight 60
#define kAboutSceneTitleBarOffset 450

// Brain Guy
#define kBrainGuyAnimationFrames 5

// Custome Dialog
#define kElementOffset 50
#define kShadowElementOffset 5
#define kTextFieldHeight 22

// Notification bar color: Green
#define OPAQUE_HEXCOLOR(c) [UIColor colorWithRed:((c>>16)&0xFF)/255.0 \
    green:((c>>8)&0xFF)/255.0 \
    blue:(c&0xFF)/255.0 \
    alpha:1.0]

#define KEYBOARD_ANIMATION_DURATION 0.3
#define MINIMUM_SCROLL_FRACTION 0.2
#define MAXIMUM_SCROLL_FRACTION 0.8
#define PORTRAIT_KEYBOARD_HEIGHT 216
#define LANDSCAPE_KEYBOARD_HEIGHT 162

// Achievement List
#define kNumberOfAchievement    18

#define kAchievementFormat  @"Achievement%d"
#define kAchievement1       @"Achievement1"
#define kAchievement2       @"Achievement2"
#define kAchievement3       @"Achievement3"
#define kAchievement4       @"Achievement4"
#define kAchievement5       @"Achievement5"
#define kAchievement6       @"Achievement6"
#define kAchievement7       @"Achievement7"
#define kAchievement8       @"Achievement8"
#define kAchievement9       @"Achievement9"
#define kAchievement10      @"Achievement10"
#define kAchievement11      @"Achievement11"
#define kAchievement12      @"Achievement12"
#define kAchievement13      @"Achievement13"
#define kAchievement14      @"Achievement14"
#define kAchievement15      @"Achievement15"
#define kAchievement16      @"Achievement16"
#define kAchievement17      @"Achievement17"
#define kAchievement18      @"Achievement18"

#define kAchievementTotalPoints 1000
#define kAchievement1Point  10
#define kAchievement2Point  10
#define kAchievement3Point  10
#define kAchievement4Point  10
#define kAchievement5Point  10
#define kAchievement6Point  10
#define kAchievement7Point  10
#define kAchievement8Point  20
#define kAchievement9Point  10
#define kAchievement10Point  20
#define kAchievement11Point  10
#define kAchievement12Point  20
#define kAchievement13Point  10
#define kAchievement14Point  10
#define kAchievement15Point  10
#define kAchievement16Point  20
#define kAchievement17Point  10
#define kAchievement18Point  20

typedef enum {
	kResultUnknown = 0,
	kResultWrong,
	kResultRight
} ProblemResult;

typedef enum {
    kUnknownMode = -1,
    kClassicMode = 0,
	kTimeMode,
    kInfiniteMode,
    kRelaxMode,
    kMultiplayerMode,
    kGameModeNo
} GameMode;

typedef enum {
    kNumberEquityProblem = 0,
    kNumberInequityProblem,
    kStatementProblem,
    kBinaryProblem
} ProblemMode;

// Expect to use with Greengar Ranking
// Currently not use
typedef enum {
    kBronzeChicken,
    kSilverChicken,
    kGoldChicken,
    kBronzeAxe,
    kSilverAxe,
    kGoldAxe,
    kBronzeDoubleAxe,
    kSilverDoubleAxe,
    kGoldDoubleAxe,
    kBronzeWand,
    kSilverWand,
    kGoldWand,
    kBronzeDragon,
    kSilverDragon,
    kGoldDragon
} Ranking;

// Expect to use with Greengar Achievement
// Currently not use
typedef enum {
    kRuby,
    kEmerald,
    kDiamond,
    kSapphire,
    kTanzanite,
    kAquamarine,
    kCirine,
    kGarnet,
    kAmethyst,
    kPeridot,
    kPearl,
    kTourmaline
} Achievement;

@interface GameUtils : NSObject {

}

#define kMultiplayerStartGame                                    @"OnMultiplayerStartGame"
#define kMultiplayerGameOver                                     @"OnMultiplayerGameOver"
#define kMultiplayerPeerDisconnected                             @"OnMultiplayerPeerDisconnected"

@end
