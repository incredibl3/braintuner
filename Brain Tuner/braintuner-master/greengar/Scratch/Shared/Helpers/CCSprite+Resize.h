//
//  CCSprite+Resize.h
//  BrainTuner2
//
//  Created by silvercast on 3/9/11.
//  Copyright 2011 GreenGar studios. All rights reserved.
//

#import "cocos2d.h"


@interface CCSprite (Resize) 

-(void)resizeTo:(CGSize) theSize;

@end
