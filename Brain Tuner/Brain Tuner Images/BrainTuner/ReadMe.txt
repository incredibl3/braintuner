Brain Tuner Premium 1.8.0 Update
--------------------------------
1 MB, submitted 11/03/09 @ 12:26 AM PST
Originally intended to be 1.7.2, but I thought Apple might allow me to change the App ID if I bumped the version number.
Turns out they didn't: "The binary you uploaded was invalid. The bundle identifier cannot be changed from the previous minor version."
--------------------------------
- Improved main menu design
- New splash screen with GreenGar logo
- Updated company URL to www.GreenGar.com
- Added Send Feedback button in Options menu
- New Send Email feature to share scores with friends
- Spanish localization improvements
- Bug fixes

If you have any questions or comments regarding this new update, contact us on Twitter:

twitter.com/greengar





• Nuevo Diseño: Se ha rediseñado el menú se ha mejorado la UI, especialmente cuando se usa con iPhone 3.0 o siguientes.
• Nueva Característica: ¡Ahora puedes enviar las mejores puntuaciones a tus amigos!
• Traducción al castellano.
• Los sonidos del juego ahora se paran cuando se cancela la partida.
• Se ha actualizado la URL del desarollador.
• Se han arreglado algunos errores.

Estamos preparando nuevas actualizaciones. Envíanos tus sugerencias a elliotes@greengar.com !

Gracias por jugar al Brain Tuner! Si quieres, prueba nuestras otras aplicaciones:

- Whiteboard Lite: Collaborative Drawing
- WordBreaker



Brain Tuner Lite 1.5 Update
---------------------------
- New Design: Redesigned game menu; and improved gameplay UI, especially when using iPhone OS 3.0 or later.
- New Feature: Email your best scores to your friends!
- Spanish localization (Español).
- Game sounds now stop when a game is cancelled.
- Updated company URL.
- Bug fixes.

More updates to come. Email your suggestions to elliot@greengar.com !

Thanks for playing Brain Tuner! Try our other apps:
- Whiteboard Lite: Collaborative Drawing
- WordBreaker

---- 1.7 Update ----
- Improved gameplay UI.
- Increased difficulty of higher levels.
- Improved high score submission process.
- Added option to switch Right/Wrong buttons.
- Updated company URL to www.GreenGarStudios.com .
- Fixed a bug that caused large numbers to be truncated.
- Added the ability to view high scores from the main screen.

$SECONDS$

$FORM_ACTION$
http://gengarstudios.com/scores/submitscore.php?score=58.978534&problems=20&deviceuid=F36B6157-EDEC-5681-B1FD-A3EAC18B77F2&hash=6be3bbfe0d0aad0ca241c9c64aa25a9c&version=2&d=1111110000000

$DEFAULT_NAME$

$DEFAULT_EMAIL$