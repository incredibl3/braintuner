//
//  EquityProblem.h
//  BrainTuner3
//
//  Created by Silvercast on 10/4/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Problem.h"

@interface EquityProblem : Problem {

}

- (void)randomize;

@end
