//
//  GSAudio.m
//  BrainTuner3
//
//  Created by Elliot on 10/6/10.
//  Copyright 2010 GreenGar Studios. All rights reserved.
//

#import "GSAudio.h"
#import "FlurryAPI.h"
#import <AVFoundation/AVFoundation.h>
//#import <AudioToolbox/AudioToolbox.h> // Audio Session


// NOTE: The Audio Session is set in the App Delegate.
//       Doing this properly is not as easy as it sounds!!
@implementation GSAudio

NSMutableDictionary *_dictionary = nil;

+ (NSMutableDictionary *)dictionary {
	if (_dictionary == nil) {
		_dictionary = [[NSMutableDictionary alloc] init];
	}
	return _dictionary;
}

// filename with file extension
+ (AVAudioPlayer *)load:(NSString *)filename {
	AVAudioPlayer *audioPlayer = [[GSAudio dictionary] objectForKey:filename];
	if (!audioPlayer) {
		NSString *path = [NSString stringWithFormat:@"%@/%@", [[NSBundle mainBundle] resourcePath], filename];
		NSURL *url = [NSURL fileURLWithPath:path];
		
		NSError *error;
		audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:&error];
		audioPlayer.numberOfLoops = 0;
		//audioPlayer.delegate = self;
		
		if (audioPlayer == nil) {
			NSLog(@"audioPlayer error = %@", [error description]);
			[FlurryAPI logEvent:@"audioPlayer Error" withParameters:[NSDictionary dictionaryWithObjectsAndKeys:error, @"error", [error description], @"description", nil]];
		} else {
			[_dictionary setObject:audioPlayer forKey:filename];
			[audioPlayer release]; // dictionary retains audioPlayer
		}
		
		// audioPlayer.volume = 0.5; // range [0.0 - 1.0]
	}
	
	audioPlayer.currentTime = 0;
	//[audioPlayer play]; // just load, don't play
	
	return audioPlayer;
}

// filename with file extension
+ (void)play:(NSString *)filename {
	AVAudioPlayer *audioPlayer = [self load:filename];
	[audioPlayer play];
}

+ (void)stop:(NSString *)filename {
	AVAudioPlayer *audioPlayer = [[GSAudio dictionary] objectForKey:filename];
	if (audioPlayer) {
		[audioPlayer stop];
		audioPlayer.currentTime = 0;
	} else {
		DLog(@"WARNING: attempting to stop nonloaded audio file: %@", filename);
	}
}

+ (void)dealloc {
	[_dictionary release], _dictionary = nil;
	[super dealloc]; // ???
}

@end
