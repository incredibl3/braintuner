//
//  ScoreViewController.m
//  BrainTuner3
//
//  Created by Elliot Lee on 8/12/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import "ScoreViewController.h"
#import "RootViewController.h"
#import "BrainTuner3AppDelegate.h"
#import "FlipsideViewController.h"
#import "SubmitScoreViewController.h"
#import "BTNetworkEngine.h"
#import "Reachability.h"
//#import "Score.h"
#import "SynthesizeSingleton.h"

#import <SystemConfiguration/SystemConfiguration.h>
#import <netinet/in.h>

#include <CommonCrypto/CommonDigest.h>

@interface NSString (MD5)
- (NSString *)md5sum;
@end

@implementation NSString (MD5)
- (NSString *)md5sum {
	unsigned char digest[CC_MD5_DIGEST_LENGTH], i;
	CC_MD5([self UTF8String], [self lengthOfBytesUsingEncoding:NSUTF8StringEncoding], digest);
	NSMutableString *ms = [NSMutableString string];
	for (i=0;i<CC_MD5_DIGEST_LENGTH;i++) {
		[ms appendFormat: @"%02x", (int)(digest[i])];
	}
	return [[ms copy] autorelease];
}
@end

@interface ScoreViewController ()
//NSMutableArray *arrayOfParameters;
//BOOL shouldSubmitSavedScores = YES;

- (BOOL)saveScore;
- (void)cancelConnection;
@end

@implementation ScoreViewController

SYNTHESIZE_SINGLETON_FOR_CLASS(ScoreViewController);

@synthesize playerName = _playerName;
@synthesize playerEmail = _playerEmail;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
	if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
		// Initialization code
		DLog();
		
	}
	return self;
}


// Used by singleton synthesizer
- (id)init {
	if ((self = [super initWithNibName:@"ScoreView" bundle:nil])) {
		// Initialization code
		DLog();
		//webView.detectsPhoneNumbers = NO;
		webView.dataDetectorTypes = UIDataDetectorTypeNone; // requires iOS 3.0+ // e.g. UIDataDetectorTypeLink
		webView.delegate = self;
	} else {
		DLog();
	}

	return self;
}

/*
- (void)submitSavedScoresIfNecessary {
	if ([[Reachability sharedReachability] internetConnectionStatus] == YES ||
		shouldSubmitSavedScores) {
		
		NSString *pathToFile = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:@"scores.data"];
		arrayOfParameters = [[NSMutableArray arrayWithContentsOfFile:pathToFile] retain];
		if (arrayOfParameters != nil && [arrayOfParameters count] <= 0) {
			[arrayOfParameters release];
			arrayOfParameters = nil;
			NSFileManager *fileManager = [NSFileManager defaultManager];
			[fileManager removeItemAtPath:pathToFile error:NULL];
		}
		if (arrayOfParameters != nil) {
			
			// Open question: should we check for connectivity before showing this alert?
			// 	 if ([[Reachability sharedReachability] internetConnectionStatus] == NO)
			
			UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Offline Saved Scores"
															message:[NSString stringWithFormat:@"You have %d unsubmitted score(s) that were saved offline.\n\nWordBreaker will attempt to submit them now. If this succeeds, your rank will be updated to reflect\nyour higher total score.", [arrayOfParameters count]]
														   delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
			[alert show];
			[alert release];
			
			// Array contains |parameters| dictionaries
			[BTNetworkEngine sharedBTNetworkEngine].submittingNewScore = NO;
			[[BTNetworkEngine sharedBTNetworkEngine] postToURL:submitURL withParameters:[arrayOfParameters objectAtIndex:0] target:self selector:@selector(receivedResponse:)];
			
			shouldSubmitSavedScores = NO;
			
			return;
		}
	}
}
*/

- (NSString *)scoreLabel {
	return [NSString stringWithFormat:@"Your score: %lf seconds", finalTime];
}


#define kDarknessViewTag			  1
#define kSubmitScoreControllerViewTag 2
#define kActivityIndicatorViewTag	  3
#define kLoadingLabelTag			  4
- (void)viewWillAppear:(BOOL)animated  {
	if (submitScoreViewController == nil) {
		submitScoreViewController = [[SubmitScoreViewController alloc] initWithNibName:@"SubmitScore" bundle:[NSBundle mainBundle]];
		((SubmitScoreViewController *)submitScoreViewController).scoreViewController = self;
		submitScoreViewController.view.tag = kSubmitScoreControllerViewTag;
	}
	[self.view addSubview:submitScoreViewController.view];
	// IB objects don't get created until view is added
	DLog(@"setting scoreLabel = %@", [NSString stringWithFormat:@"Your score: %lf seconds", finalTime]);
	((SubmitScoreViewController *)submitScoreViewController).scoreLabel.text = [NSString stringWithFormat:@"Your score: %lf seconds", finalTime];
	[((SubmitScoreViewController *)submitScoreViewController) setFinalTimeForSubmission:finalTime];
	
	//self.view = submitScoreController.view;
	//[submitScoreController release];
	
	//DLog(@"scoreViewController will appear");
	
	
	//[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
	
	/*
	BrainTuner3AppDelegate *app = (BrainTuner3AppDelegate *)[[UIApplication sharedApplication] delegate];
	//RootViewController *rootViewController = (RootViewController *)[app rootViewController];
	
	int problemCount = [app getProblemCountFromSegmentedControl];
	NSString *deviceuid = [[UIDevice currentDevice] uniqueIdentifier];
	
	NSString *toHash = [NSString stringWithFormat:@"%lf%d%@%@%@", finalTime, problemCount, deviceuid, @"1", @"6uphEGev"];
	NSString *hash = [toHash md5sum];
	
	//NSMutableArray *
	 = [(FlipsideViewController *)[rootViewController flipsideViewController] problemArray];
	
	NSString *desc = @"";
	
	// 1.6 changes this behavior (digit in the 11th position is NSL [no slash])
	// server-side change may be needed here
	for (int i = 0; i < OPTION_COUNT; i++) {
		desc = [desc stringByAppendingFormat:@"%d", [[app.optionsArray objectAtIndex:i] boolValue]];
	}
	
	// Version 1 uses div. Version 2 uses optionsArray.
	
	NSString *html = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"SubmitScore" ofType:@"html"]];
	NSString *path = [[NSBundle mainBundle] bundlePath];
	NSURL *baseURL = [NSURL fileURLWithPath:path];

	 //	 http://gengarstudios.com/scores/submitscore.php?score=58.978534&problems=20&deviceuid=F36B6157-EDEC-5681-B1FD-A3EAC18B77F2&hash=6be3bbfe0d0aad0ca241c9c64aa25a9c&version=2&d=1111110000000
	 
	html = [html stringByReplacingOccurrencesOfString:@"$SECONDS$" withString:[NSString stringWithFormat:@"%lf", finalTime]];
	html = [html stringByReplacingOccurrencesOfString:@"$FORM_ACTION$"
										   withString:[NSString stringWithFormat:@"http://gengarstudios.com/scores/submitscore.php?score=%lf&problems=%d&deviceuid=%@&hash=%@&version=2&d=%@",
													   finalTime, problemCount, deviceuid, hash, desc]];
	NSString *defaultName = [[NSUserDefaults standardUserDefaults] stringForKey:@"$DEFAULT_NAME$"];
	if (defaultName == nil) {
		defaultName = @"";
	}
	html = [html stringByReplacingOccurrencesOfString:@"$DEFAULT_NAME$" withString:defaultName];
	NSString *defaultEmail = [[NSUserDefaults standardUserDefaults] stringForKey:@"$DEFAULT_EMAIL$"];
	if (defaultEmail == nil) {
		defaultEmail = @"";
	}
	html = [html stringByReplacingOccurrencesOfString:@"$DEFAULT_EMAIL$" withString:defaultEmail];
	[webView loadHTMLString:html baseURL:baseURL];
	
	// Submission form as a local file (easier to style than native controls)
	// And probably easier to submit, too. I'll just pull out the submission values
	
	//NSString *urlString = [NSString stringWithFormat:
	//					   @"http://gengarstudios.com/scores/submitscore.php?score=%lf&problems=%d&deviceuid=%@&hash=%@&version=2&d=%@",
	//					   finalTime, problemCount, deviceuid, hash, desc];	
	 */
	
	/*
	NSString *html = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"SubmitScore" ofType:@"html"]];
	NSString *path = [[NSBundle mainBundle] bundlePath];
	NSURL *baseURL = [NSURL fileURLWithPath:path];
	[webView loadHTMLString:html baseURL:baseURL];
	 */
}


- (void)viewDidAppear:(BOOL)animated {
	[super viewDidAppear:animated];
	if (![self connectedToNetwork]) {
		// show info dialog
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"CONNECTION_REQUIRED", @"Score submission requires\na cellular data or\nWi-Fi connection.") message:@"Continue anyway?\n(Tap No to cancel.)" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes",nil];
		[alert show];
		[alert release];
	}
}


- (BOOL) connectedToNetwork
{
	// Create zero addy
	struct sockaddr_in zeroAddress;
	bzero(&zeroAddress, sizeof(zeroAddress));
	zeroAddress.sin_len = sizeof(zeroAddress);
	zeroAddress.sin_family = AF_INET;
	
	// Recover reachability flags
	SCNetworkReachabilityRef defaultRouteReachability = SCNetworkReachabilityCreateWithAddress(NULL, (struct sockaddr *)&zeroAddress);
	SCNetworkReachabilityFlags flags;
	
	BOOL didRetrieveFlags = SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags);
	CFRelease(defaultRouteReachability);
	
	if (!didRetrieveFlags)
	{
		printf("Error. Could not recover network reachability flags\n");
		return 0;
	}
	
	BOOL isReachable = flags & kSCNetworkFlagsReachable;
	// needsConnection if we could get connected via EDGE, but the connection hasn't been established yet
	//BOOL needsConnection = flags & kSCNetworkFlagsConnectionRequired;
	return (isReachable/* && !needsConnection*/) ? YES : NO;
}


- (void)submitWithName:(NSString *)playerName email:(NSString *)playerEmail {
	submissionSucceeded = NO;
	
	// fade in darkness
	UIView *darknessView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 460)];
	darknessView.backgroundColor = [UIColor blackColor];
	darknessView.alpha = 0.0;
	darknessView.tag = kDarknessViewTag;
	[self.view addSubview:darknessView];
	[darknessView release];
	
	#define loadingOverlayVOffset -17
	
	UIActivityIndicatorView *activityIndicatorView =
	[[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
	CGRect activityIndicatorViewFrame = CGRectMake((320 - activityIndicatorView.frame.size.width) / 2 - 44, (460 - activityIndicatorView.frame.size.height) / 2 + loadingOverlayVOffset, activityIndicatorView.frame.size.width, activityIndicatorView.frame.size.height);
	DLog(@"frame:%@", NSStringFromCGRect(activityIndicatorViewFrame));
	activityIndicatorView.frame = activityIndicatorViewFrame;
	activityIndicatorView.tag = kActivityIndicatorViewTag;
	activityIndicatorView.alpha = 0.0;
	[self.view addSubview:activityIndicatorView];
	[self.view bringSubviewToFront:activityIndicatorView];
	[activityIndicatorView startAnimating];
	[activityIndicatorView release];
	
	UILabel *loadingLabel = [[UILabel alloc] initWithFrame:CGRectMake(132, 221 + loadingOverlayVOffset, 100, 20)];
	loadingLabel.text = @"Loading...";
	loadingLabel.backgroundColor = [UIColor clearColor];
	loadingLabel.textColor = [UIColor whiteColor];
	loadingLabel.shadowColor = [UIColor blackColor];
	//loadingLabel.shadowOffset = CGRectMake(0, -1, 0, 0);
	loadingLabel.alpha = 0.0;
	loadingLabel.tag = kLoadingLabelTag;
	[self.view addSubview:loadingLabel];
	[loadingLabel release];
	
	
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationDuration:kAnimationDuration];
	
	((SubmitScoreViewController *)submitScoreViewController).submitScoreButton.alpha = 0.0;
	
	darknessView.alpha = 0.5;
	activityIndicatorView.alpha = 1.0;
	loadingLabel.alpha = 1.0;
	
	[UIView commitAnimations];	
	
	// show loading message
	//DLog(@"networkActivityIndicatorVisible = YES");
	//[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
	
	// Pull together all the data
	
	self.playerName = playerName;
	self.playerEmail = playerEmail;
	
	BrainTuner3AppDelegate *app = (BrainTuner3AppDelegate *)[[UIApplication sharedApplication] delegate];
	
	// finalTime provided by whoever loaded us
	
	// problemCount
	int problemCount = [app getProblemCountFromSegmentedControl];
	// deviceuid
	NSString *deviceuid = [[UIDevice currentDevice] uniqueIdentifier];
	// hash
	NSString *toHash = [NSString stringWithFormat:@"%lf%d%@%@%@%d", finalTime, problemCount, deviceuid, @"1", @"6uphEGev", app.rootViewController.mainViewController.difficulty];
	NSString *hash = [toHash md5sum];
	// desc
	NSString *desc = @"";
	for (int i = 0; i < OPTION_COUNT; i++) {
		desc = [desc stringByAppendingFormat:@"%d", [[app.optionsArray objectAtIndex:i] boolValue]];
	}
	
	//problemarray=%@&d=%@",
	//finalTime, problemCount, deviceuid, hash, [problemArray componentsJoinedByString:@","]
	//NSMutableArray *problemArray = [(FlipsideViewController *)[app.rootViewController flipsideViewController] problemArray];
	
	// TODO: make name and email safe for URLs
	
	/*
	NSString *submitUrl = [NSString stringWithFormat:@"http://gengarstudios.com/scores/submitscorenative.php?score=%lf&problems=%d&deviceuid=%@&hash=%@&version=2&d=%@&name=%@&email=%@",
	finalTime, problemCount, deviceuid, hash, desc,
	[playerName stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],
	[playerEmail stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
	DLog(@"%@", submitUrl);
	*/
	
	// start loading URL
	//[webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:submitUrl]]];
	
	// TODO: check for memory leaks
	if (params != nil) {
		[params release];
		params = nil;
	}
	params = [[NSDictionary dictionaryWithObjectsAndKeys:
		[NSString stringWithFormat:@"%lf", finalTime], @"score",
		[NSString stringWithFormat:@"%d", problemCount], @"problems",
		[NSString stringWithFormat:@"%d", app.rootViewController.mainViewController.difficulty], @"level",
		deviceuid, @"deviceuid",
		hash, @"hash",
		desc, @"d",
		[playerName stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding], @"name",
		[playerEmail stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding], @"email", nil] retain];
	DLog(@"params:%@", params);
	
	[[BTNetworkEngine sharedBTNetworkEngine] postToURL:submitURL withParameters:params target:self selector:@selector(receivedResponse:)];
}


// WBNetworkEngine delegate method
- (void)receivedResponse:(NSString *)response {
	DLog(@"ScoreViewController %s%@", _cmd, response);
	
	//NSURL *baseURL = nil;
	
	//NSString *path = [[NSBundle mainBundle] bundlePath];
	//NSURL *baseURL = [NSURL fileURLWithPath:path];
	
	NSURL *baseURL = [NSURL URLWithString:@"http://www.greengarstudios.com/scores/"];
	
	NSString *results;
	
	if (response == nil || [response isEqualToString:@""]) {
		// Submission failed
		// TODO: get error and notify the user
		
		results = [NSString stringWithFormat:@"<head><meta id=\"viewport\" name=\"viewport\" content=\"width=320; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;\" /></head><body style='font-family: Helvetica;'><h2>%@</h2><p>Tap the Refresh button (the circular arrow) at the top left to retry.</p></body>",
				   NSLocalizedString(@"UNABLE_TO_SUBMIT", @"Unable to submit score")];
		
		//<p><b>Error:</b> %@</p>, [sfl getError]
		
		/*
		 UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"UNABLE_TO_SUBMIT", @"Unable to submit score") message:@"Retry?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
		 //@"Unable to submit score.\nYour score has been saved\nfor later submission."
		 [alertView show];
		 [alertView release];
		 */
	} else {
		results = response;
		//results = [sfl getResults];
		submissionSucceeded = YES;
	}
	//[Engine Log:[NSString stringWithFormat:@"Upload finished: '%@'",[sfl getResults]]];
	//DLog(@"Upload finished: '%@'",[sfl getResults]);
	
	DLog(@"receivedResponse: webView loadHTMLString: baseURL:%@", baseURL);
	[webView loadHTMLString:results baseURL:baseURL];
	//[webView loadHTMLString:results baseURL:nil];
	
	//[sfl release];
	//sfl = nil;
	
	if (submissionSucceeded) {
		[UIAppDelegate.rootViewController.mainViewController submitSavedScores];
	} else {
		DLog(@"Submission failed");
		// TODO: notify the user?
	}
}


- (IBAction)tapRefresh {
#if DEBUG // remove compile warning: Unused variable 'path'
	DLog(@"webView.request = %@", webView.request);
	NSString* path=[[webView.request URL] relativePath];
	DLog(@"path = %@", path);
#endif
	if ([[webView.request URL] isFileURL] && params != nil) {
		
		[[BTNetworkEngine sharedBTNetworkEngine] postToURL:submitURL withParameters:params target:self selector:@selector(receivedResponse:)];
		//NSString *url = @"http://gengarstudios.com/scores/submitscorenative.php?
		//[webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:submitUrl]]];
	} else {
		[webView reload];
	}
}


- (void)removeSubmitScoreViewControllerView {
	UIView *submitScoreViewControllerView = [self.view viewWithTag:kSubmitScoreControllerViewTag];
	UIView *darknessView = [self.view viewWithTag:kDarknessViewTag];
	UIView *activityIndicatorView = [self.view viewWithTag:kActivityIndicatorViewTag];
	UIView *loadingLabel = [self.view viewWithTag:kLoadingLabelTag];
	
	[submitScoreViewControllerView removeFromSuperview];
	[darknessView removeFromSuperview];
	[activityIndicatorView removeFromSuperview];
	[loadingLabel removeFromSuperview];
	
	if (submitScoreViewController) {
		[submitScoreViewController release];
		submitScoreViewController = nil;
	}
}


- (void)setFinalTime:(double)score {
	finalTime = score;
}

- (IBAction)tapDone:(id)sender {
	[params release];
	params = nil;
	
	if ([[UIAppDelegate.optionsArray objectAtIndex:OPT_ADD] boolValue] == YES &&
		[[UIAppDelegate.optionsArray objectAtIndex:OPT_SUB] boolValue] == YES &&
		[[UIAppDelegate.optionsArray objectAtIndex:OPT_MUL] boolValue] == YES &&
		[UIAppDelegate.rootViewController.mainViewController onMaxDifficulty]) {
		
		float timePerProblem = finalTime / [UIAppDelegate.rootViewController.flipsideViewController getProblemCount];
		[(RootViewController *)[UIAppDelegate rootViewController] toggleViewWithTimePerProblem:timePerProblem];
	} else {
		[(RootViewController *)[UIAppDelegate rootViewController] toggleView];
	}
}


- (BOOL)webView:(UIWebView *)sender shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
	DLog(@"request:%@ navigationType:%d", request, navigationType);
	
	/**
	 * navigationType ==
	 *  1 for form submit (executes twice)
	 *  4 for refresh
	 *  5 for initial page load and custom links
	 */
	
	NSString* path=[[request URL] relativePath];
	BOOL shouldContinue=YES;
	
	//confirm("Email (optional) is blank. Press OK to continue, or Cancel to enter an email address.")
	if ([path isEqualToString:@"/_confirm_blank_email"])
	{
		shouldContinue=NO;
		
		// let's show this worked by displaying an alert
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"NO_EMAIL", @"No Email Entered")
														message:NSLocalizedString(@"EMAIL_OPTIONAL", @"Email is optional.")
													   delegate:self 
											  cancelButtonTitle:NSLocalizedString(@"ENTER_EMAIL", @"Enter Email") otherButtonTitles:NSLocalizedString(@"NO_EMAIL", @"No Email"), nil];
		
		[alert show];	
		[alert release];
	} else if ([path isEqualToString:@"/_alert_invalid_email"]) {
		shouldContinue=NO;
		
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"INVALID_EMAIL", @"Invalid Email")
														message:NSLocalizedString(@"EMAIL_INVALID_EXPLANATION", @"Email is optional, but what you entered is invalid. Correct it, or make the box blank.")
													   delegate:self
											  cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[alert show];
		[alert release];
	} else if ([[[request URL] absoluteString] isEqualToString:@"http://www.gengarstudios.com/"]) {
		//DLog([[request URL] absoluteString]);
		shouldContinue=NO;
		[[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.gengarstudios.com"]];
	} else if ([[[request URL] absoluteString] isEqualToString:@"http://www.greengarstudios.com/"]) {
		//DLog([[request URL] absoluteString]);
		shouldContinue=NO;
		[[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.greengarstudios.com"]];		
	}/* else if ([[[request URL] absoluteString] hasPrefix:@"http://gengarstudios.com/scores/submitscore.php?score="]) {
		shouldContinue = YES;
		//DLog(@"name: %@", [webView stringByEvaluatingJavaScriptFromString:@"document.frmInfo.name.value"]);
		//DLog(@"email: %@", [webView stringByEvaluatingJavaScriptFromString:@"document.frmInfo.email.value"]);
		[[NSUserDefaults standardUserDefaults] setObject:[webView stringByEvaluatingJavaScriptFromString:@"document.frmInfo.name.value"] forKey:@"$DEFAULT_NAME$"];
		[[NSUserDefaults standardUserDefaults] setObject:[webView stringByEvaluatingJavaScriptFromString:@"document.frmInfo.email.value"] forKey:@"$DEFAULT_EMAIL$"];
	}*/

	return shouldContinue;
}


- (void)webViewDidStartLoad:(UIWebView *)webView {
	DLog(@"%s", _cmd);
	
	[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
}


- (void)webViewDidFinishLoad:(UIWebView *)senderWebView {
	DLog(@"%s", _cmd);
	
	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
	
	//if ([[webView.request URL] isFileURL]) {
		// hide submitScoreViewController.view
		UIView *submitScoreViewControllerView = [self.view viewWithTag:kSubmitScoreControllerViewTag];
		UIView *darknessView = [self.view viewWithTag:kDarknessViewTag];
		UIView *activityIndicatorView = [self.view viewWithTag:kActivityIndicatorViewTag];
		UIView *loadingLabel = [self.view viewWithTag:kLoadingLabelTag];
		
		// fade out
		[UIView beginAnimations:nil context:NULL];
		[UIView setAnimationDuration:kAnimationDuration];
		//[UIView setAnimationTransition:UIViewAnimationTransitionCurlUp forView:/*submitScoreViewControllerView*/self.view cache:YES];
		
		//[self viewWillAppear:YES];
		
		//[submitScoreViewController viewWillDisappear:YES];
		//[submitScoreViewControllerView removeFromSuperview];
		//[darknessView viewWillDisappear:YES];
		//[darknessView removeFromSuperview];
		//[activityIndicatorView viewWillDisappear:YES];
		//[activityIndicatorView removeFromSuperview];
		//[loadingLabel viewWillDisappear:YES];
		//[loadingLabel removeFromSuperview];
		
		submitScoreViewControllerView.alpha = 0.0;
		darknessView.alpha = 0.0;
		activityIndicatorView.alpha = 0.0;
		loadingLabel.alpha = 0.0;
		
		//[self.view addSubview:self.view];
		
		//[submitScoreViewController viewDidDisappear:YES];
		//[self viewDidAppear:YES];
		
		[UIView commitAnimations];
		
		// remove at some future time
		[NSTimer scheduledTimerWithTimeInterval:kAnimationDuration target:self selector:@selector(removeSubmitScoreViewControllerView) userInfo:nil repeats:NO];
	//}
}


- (void)webView:(UIWebView *)senderWebView didFailLoadWithError:(NSError *)error {
	DLog(@"%s%@", _cmd, error);
	
	DLog(@"isFileURL = %d", [[webView.request URL] isFileURL]);
	
	if (!submissionSucceeded) {
		// TODO: Save for later submission
		
		/*
		NSMutableSet *scoreSet = [[[NSUserDefaults standardUserDefaults] objectForKey:@"scoreSet"] mutableCopy];
		Score *score = [[Score alloc] initWithName:self.playerName email:self.playerEmail score:finalTime];
		[scoreSet addObject:score];
		[score release];
		[[NSUserDefaults standardUserDefaults] setObject:scoreSet forKey:@"scoreSet"];
		DLog(@"[scoreSet count] == %d", [scoreSet count]);
		[scoreSet release];
		*/
		
		UIView *activityIndicatorView = [self.view viewWithTag:kActivityIndicatorViewTag];
		UIView *loadingLabel = [self.view viewWithTag:kLoadingLabelTag];
		
		[activityIndicatorView removeFromSuperview];
		[loadingLabel removeFromSuperview];
		
		//NSLocalizedString(@"SCORE_SAVED", @"Score saved for\nlater submission")
		UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"UNABLE_TO_SUBMIT", @"Unable to submit score") message:@"Retry?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
		//@"Unable to submit score.\nYour score has been saved\nfor later submission."
		[alertView show];
		[alertView release];
	}
}


// TODO: Handle above 2 errors to save score for later submission
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
	DLog(@"%@ %d", alertView.title, buttonIndex);
	//NSLocalizedString(@"SCORE_SAVED", @"Score saved for\nlater submission")
	if ([alertView.title isEqualToString:NSLocalizedString(@"CONNECTION_REQUIRED", @"Score submission requires\na cellular data or\nWi-Fi connection.")]) {
		if (buttonIndex == alertView.cancelButtonIndex) {
			// Flip back to home screen
			[(RootViewController *)[(BrainTuner3AppDelegate *)[[UIApplication sharedApplication] delegate] rootViewController] toggleView];
		} else {
			// Do nothing
		}
	} else if ([alertView.title isEqualToString:NSLocalizedString(@"UNABLE_TO_SUBMIT", @"Unable to submit score")]) {
		if (buttonIndex == alertView.cancelButtonIndex) {
			// Flip back to home screen
			[(RootViewController *)[(BrainTuner3AppDelegate *)[[UIApplication sharedApplication] delegate] rootViewController] toggleView];
		} else {
			[self tapRefresh];
		}
	} else if (alertView.tag == kCancelTag) {
		if (alertView.firstOtherButtonIndex == buttonIndex) {
			DLog(@"Saving score");
			//[[ScoreViewController sharedScoreViewController] saveScore];
			[self saveScore];
			//[[WBNetworkEngine sharedWBNetworkEngine] cancelConnection];
			[self cancelConnection];
			
			//[self doQuit];
			[self tapDone:nil];
		}
	} else {
		if (buttonIndex == 0) {
			[webView stringByEvaluatingJavaScriptFromString:@"document.frmInfo.email.focus()"];
		} else {
			[webView stringByEvaluatingJavaScriptFromString:@"document.frmInfo.submit()"];
		}
	}
}


- (BOOL)saveScore {
	if (params == nil || [params count] <= 0) {
		return NO;
	}
	NSString *pathToFile = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:@"scores.data"];
	NSMutableArray *array = [NSMutableArray arrayWithContentsOfFile:pathToFile];
	if (array == nil) {
		array = [[NSMutableArray alloc] init];
	}
	[array addObject:params];
	DLog(@"array:%@", array);
	
	[params release];
	params = nil;
	
	return ([array writeToFile:pathToFile atomically:YES]);
}


- (void)cancelConnection {
	//[sfl cancelConnection];
	[[BTNetworkEngine sharedBTNetworkEngine] cancelConnection];
}


/*
- (void) webView: (UIWebView*)webView runJavaScriptAlertPanelWithMessage: (NSString*) message initiatedByFrame: (WebFrame*) frame {
	DLog(@"runJavaScriptAlertPanelWithMessage");
}
*/

/*
 Implement loadView if you want to create a view hierarchy programmatically
- (void)loadView {
}
 */

/*
 If you need to do additional setup after loading the view, override viewDidLoad.
- (void)viewDidLoad {
}
 */


- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
	UIView *activityIndicatorView = [self.view viewWithTag:kActivityIndicatorViewTag];
	if (activityIndicatorView != nil && activityIndicatorView.alpha == 1.0) {
		// Offer to cancel
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Do you want to cancel?"
														message:@"Your score will be saved\nfor later submission."
													   delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
		alert.tag = kCancelTag;
		[alert show];
		[alert release];		
	}
	/*
	if ([[event touchesForView:overlayViewController.view] count] >= 1) {
		DLog(@"Possibly dismiss overlayViewController.view");
		
		if (overlayViewController.view.alpha != 0.0) {
			if ([overlayViewController.nibName isEqualToString:@"SubmitScore"]) {
				// Ask if the user wishes to cancel, if the view can be cancelled
				if (((ScoreViewController *)overlayViewController).canBeCancelled) {
					UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Do you want to cancel?"
																	message:@"Your score will be saved\nfor later submission."
																   delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
					alert.tag = kCancelTag;
					[alert show];
					[alert release];
				}
				
			} else if ([overlayViewController.nibName isEqualToString:@"Introduction"]) {
				// Don't allow this one to be dismissed
				
			} else {
				[UIView beginAnimations:nil context:NULL];
				[UIView setAnimationDuration:0.2];
				overlayViewController.view.alpha = 0.0;
				[UIView commitAnimations];
			}
		}
	}
	 */
	DLog(@"%s", _cmd);
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
	// Return YES for supported orientations
	return (interfaceOrientation == UIInterfaceOrientationPortrait);
}


- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning]; // Releases the view if it doesn't have a superview
	// Release anything that's not essential, such as cached data
}


- (void)dealloc {
	webView.delegate = nil;
	if (submitScoreViewController) {
		[submitScoreViewController release];
		submitScoreViewController = nil;
	}
	if (params) {
		[params release];
		params = nil;
	}
	[super dealloc];
}


@end