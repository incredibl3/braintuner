//
//  WBWebViewController.m
//  WordBreaker
//
//  Created by Elliot Lee on 7/2/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import "WBWebViewController.h"
#import "BrainTuner3AppDelegate.h"
#import "RootViewController.h"
#import "MainViewController.h"


@interface WBWebViewController ()
BOOL failed;
- (NSString *)URLString;
@end


@implementation WBWebViewController

@synthesize mainViewController;

/*
 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
        // Custom initialization
    }
    return self;
}
*/

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView {
}
*/


// see also: submitURL
#define URLFormat @"http://www.greengarstudios.com/scores/WebView.php?%@"
//#define URLFormat @"http://www.greengarstudios.com/wordbreaker/onlineranking.php?%@"
//#define URLFormat @"http://www.greengarstudios.com/wordbreaker/onlinerankingbeta.php?%@" //name=%@&email=%@&deviceuid=%@
// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
	
	DLog(@"[self URLString]:%@", [self URLString]);
	failed = NO;
	[webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[self URLString]]]];
}


- (NSString *)URLString {
	NSString *name = [[NSUserDefaults standardUserDefaults] stringForKey:@"nameTextField.text"];
	NSString *email = [[NSUserDefaults standardUserDefaults] stringForKey:@"emailTextField.text"];
	NSString *problems = [NSString stringWithFormat:@"%d", [UIAppDelegate getProblemCountFromSegmentedControl]];
	NSString *level = [NSString stringWithFormat:@"%d", UIAppDelegate.rootViewController.mainViewController.difficulty];
	NSString *deviceuid = [UIDevice currentDevice].uniqueIdentifier;
	if (name == nil) {
		name = @"";
	}
	if (email == nil) {
		email = @"";
	}
	
	NSDictionary *parameters = [NSDictionary dictionaryWithObjectsAndKeys:name, @"name",
								email, @"email",
								problems, @"problems",
								level, @"level",
								deviceuid, @"deviceuid", nil];
	
	NSMutableString *params = nil;
    //NSString *contentType = @"text/html; charset=utf-8";
    //NSURL *finalURL = url;
    if (parameters != nil)
    {
        params = [[NSMutableString alloc] init];
        for (id key in parameters)
        {
            NSString *encodedKey = [key stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            CFStringRef value = (CFStringRef)[[parameters objectForKey:key] copy];
            // Escape even the "reserved" characters for URLs 
            // as defined in http://www.ietf.org/rfc/rfc2396.txt
            CFStringRef encodedValue = CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault, 
                                                                               value,
                                                                               NULL, 
                                                                               (CFStringRef)@";/?:@&=+$,", 
                                                                               kCFStringEncodingUTF8);
            [params appendFormat:@"%@=%@&", encodedKey, encodedValue];
            CFRelease(value);
            CFRelease(encodedValue);
        }
        [params deleteCharactersInRange:NSMakeRange([params length] - 1, 1)];
    }
	
	return [NSString stringWithFormat:URLFormat, params];
}


- (IBAction)closeTapped {
	[self.parentViewController dismissModalViewControllerAnimated:YES];
}


- (IBAction)refreshTapped {
	[webView reload];
}


- (void)webView:(UIWebView *)localWebView didFailLoadWithError:(NSError *)error {
	[activityIndicatorView stopAnimating];
	failed = YES;
	[webView loadHTMLString:[NSString stringWithFormat:@"<center><font face='Helvetica'>Error: %@ (%d)<br><br>Please check your Internet connection, and try again later.</font></center>", [error localizedDescription], [error code]] baseURL:nil];
}

/* // Note: this is optional and must return a BOOL
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
	[activityIndicatorView startAnimating];
}
*/

- (void)webViewDidFinishLoad:(UIWebView *)webView {
	DLog(@"%s", _cmd);
	[activityIndicatorView stopAnimating];
	
	if (failed == NO) {
		// Loading succeeded, so update rank
		// Could also have used self.parentViewController
		//DLog(@"WBWebViewController succeeded. Calling -updateOnlineRank");
		//[self.mainViewController updateOnlineRank];
	}
}


- (void)webViewDidStartLoad:(UIWebView *)webView {
	[activityIndicatorView startAnimating];
}


/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}


- (void)dealloc {
	webView.delegate = nil;
    [super dealloc];
}


@end
