//
//  BTNetworkEngine.m
//  Brain Tuner
//
//  Created by Elliot Lee on 6/21/09.
//  Copyright 2009 GreenGar Studios. All rights reserved.
//

#import "BTNetworkEngine.h"
#import "Wrapper.h"
#import "SynthesizeSingleton.h"


@interface BTNetworkEngine (private)

Wrapper *engine = nil;
NSObject *callbackTarget = nil;
SEL callbackSelector;
NSDictionary *parameters = nil;

@end


@implementation BTNetworkEngine

SYNTHESIZE_SINGLETON_FOR_CLASS(BTNetworkEngine);

@synthesize submittingNewScore;

- (void)postToURL:(NSString *)URLString withParameters:(NSDictionary *)params target:(NSObject *)target selector:(SEL)selector {
	if (engine == nil) {
		engine = [[Wrapper alloc] init];
		engine.delegate = self;
	}
	
	// Note: Wrapper (engine) handles canceling a previous connection before starting a new one
	
	callbackTarget = target;
	callbackSelector = selector;
	
	if (parameters != nil) {
		if (self.submittingNewScore == YES) {
			// Just in case. This releases parameters
			[self saveScore];
		} else {
			[parameters release];
		}
	}
	parameters = [params retain];
	
	[engine sendRequestTo:[NSURL URLWithString:URLString] usingVerb:@"POST" withParameters:parameters];
}


- (void)cancelConnection {
	if (engine != nil) {
		[engine cancelConnection];
	}
}


#pragma mark -
#pragma mark WrapperDelegate methods

- (void)wrapper:(Wrapper *)wrapper didRetrieveData:(NSData *)data
{
    NSString *text = [engine responseAsText];
    if (text == nil) {
		DLog(@"responseAsText == nil");
	}
	
	// callbackSelector may start a new connection, so release parameters before calling it
	[parameters release];
	parameters = nil;
	
	[callbackTarget performSelector:callbackSelector withObject:text];
}

- (void)wrapperHasBadCredentials:(Wrapper *)wrapper
{
	// callbackSelector may start a new connection, so release parameters before calling it
	[parameters release];
	parameters = nil;
	
    [callbackTarget performSelector:callbackSelector withObject:nil];
	
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Bad credentials!" 
                                                    message:@"Bad credentials!"  
                                                   delegate:nil 
                                          cancelButtonTitle:@"OK" 
                                          otherButtonTitles:nil];
    [alert show];
    [alert release];
}

- (void)wrapper:(Wrapper *)wrapper didCreateResourceAtURL:(NSString *)url
{
	// callbackSelector may start a new connection, so release parameters before calling it
	[parameters release];
	parameters = nil;
	
    [callbackTarget performSelector:callbackSelector withObject:nil];
	
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Resource created!" 
                                                    message:[NSString stringWithFormat:@"Resource created at %@!", url]  
                                                   delegate:nil 
                                          cancelButtonTitle:@"OK" 
                                          otherButtonTitles:nil];
    [alert show];
    [alert release];
}


- (void)wrapper:(Wrapper *)wrapper didFailWithError:(NSError *)error
{
	// Do nothing if this wrapper was not called for the purpose of submitting a NEW score!!!
	
	NSString *message;
	if (self.submittingNewScore == YES) {
		DLog(@"Submitting new score, but it failed; so we're saving it!");
		
		// No connection
		// -1001: timed out
		// -1009: no Internet connection
		
		if ([self saveScore]) {
			//title = @"Unable to submit score";
			if ([error code] == -1009) {
				message = @"Since you have no Internet connection, ";
			} else if ([error code] == -1001) {
				message = @"Connection timed out.\n";
			}
			message = [message stringByAppendingString:@"Brain Tuner has saved your score locally, and will reattempt submitting it on the next launch."];
			if ([error code] != -1009 && [error code] != -1001) {
				message = [message stringByAppendingFormat:@"\n\nError: %@ (%d)", [error localizedDescription], [error code]];
			}
		} else {
			//title = @"Unable to submit score";
			message = [NSString stringWithFormat:@"Brain Tuner was unable to save your score locally.\n\nError: %@ (%d)", [error localizedDescription], [error code]];
		}
	} else {
		message = [NSString stringWithFormat:@"Error: %@ (%d)", [error localizedDescription], [error code]];
	}
	
	// callbackSelector may start a new connection, so release parameters before calling it
	[parameters release];
	parameters = nil;
	
	// This is detected by callbackTarget as an error because message doesn't contain |
    [callbackTarget performSelector:callbackSelector withObject:message];
}


- (BOOL)saveScore {
	if (parameters == nil || [parameters count] <= 0) {
		return NO;
	}
	NSString *pathToFile = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:@"scores.data"];
	NSMutableArray *array = [NSMutableArray arrayWithContentsOfFile:pathToFile];
	if (array == nil) {
		array = [[NSMutableArray alloc] init];
	}
	// array retains parameters
	// TODO: Test
	[array addObject:parameters];
	DLog(@"array:%@", array);
	
	// Erik: production-quality without this
	//[parameters release];
	//parameters = nil;
	
	return ([array writeToFile:pathToFile atomically:YES]);
	
	//[parameters release];
	//parameters = nil;
}


- (void)wrapper:(Wrapper *)wrapper didReceiveStatusCode:(int)statusCode
{
	// callbackSelector may start a new connection, so release parameters before calling it
	[parameters release];
	parameters = nil;
	
    [callbackTarget performSelector:callbackSelector withObject:nil];
	
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Status code not OK!" 
                                                    message:[NSString stringWithFormat:@"Status code not OK: %d!", statusCode]
                                                   delegate:nil 
                                          cancelButtonTitle:@"OK" 
                                          otherButtonTitles:nil];
    [alert show];
    [alert release];
}


- (void)dealloc {
	[parameters release];
	parameters = nil;
	
	[engine release];
	engine = nil;
	
    [super dealloc];
}


@end
