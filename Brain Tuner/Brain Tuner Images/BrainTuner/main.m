//
//  main.m
//  BrainTuner3
//
//  Created by Elliot Lee on 7/13/08.
//  Copyright GreenGar Studios 2008-2009. All rights reserved.
//

#import <UIKit/UIKit.h>

int main(int argc, char *argv[]) {
	
	NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init];
	int retVal = UIApplicationMain(argc, argv, nil, nil);
	[pool release];
	return retVal;
}
