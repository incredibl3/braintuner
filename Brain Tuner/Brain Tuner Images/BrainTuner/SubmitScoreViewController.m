//
//  SubmitScoreViewController.m
//  BrainTuner3
//
//  Created by Instructor on 2/6/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import "SubmitScoreViewController.h"
#import "BrainTuner3AppDelegate.h"

#define kFacebookAppID @"120267878022842"
#define kFacebookAPIKey @"bb3c84a97980b3339bf5828deae10614"
//#define kFaceBookAppSecret @"7cedff4db8cd0fc64d53d92ccbc05a6e"

#define kTwitterOAuthConsumerKey				@"jay5alnXE04FN1BHIZg"		
#define kTwitterOAuthConsumerSecret				@"SSPI5V0Eob3M0H9vKyScYIbJrsgL4ev77RnphDjIqw"
#define kTwitterClientName						@"Brain Tuner"
#define kTwitterClientVersion					@"1.6"
#define kBrainTunerAppURL						@"http://www.greengar.com/apps/brain-tuner/"

@implementation SubmitScoreViewController

@synthesize scoreLabel;
@synthesize nameTextField;
@synthesize emailTextField;
@synthesize submitScoreButton;
@synthesize scoreViewController;

@synthesize gameCenterButton;
@synthesize facebookButton;
@synthesize twitterButton;

@synthesize gameCenterManager;			// iOS 4.1+
@synthesize gameCenterLeaderBoardScore;	// iOS 4.1+

@synthesize gameCenterCategory;
@synthesize twitterManager;
@synthesize fbConnectManager;

// The designated initializer. Override to perform setup that is required before the view is loaded.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
        // Custom initialization
    }
    return self;
}


/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView {
}
*/

/*
- (void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];
}*/


#define OPAQUE_HEXCOLOR(c) [UIColor colorWithRed:((c>>16)&0xFF)/255.0 \
	green:((c>>8)&0xFF)/255.0 \
	blue:(c&0xFF)/255.0 \
	alpha:1.0];

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
	nameTextField.clearButtonMode = UITextFieldViewModeAlways;
	emailTextField.clearButtonMode = UITextFieldViewModeAlways;
	// Only need to do this in viewDidLoad because we don't clear the fields when the view disappears!
	nameTextField.text = [[NSUserDefaults standardUserDefaults] stringForKey:@"nameTextField.text"];
	emailTextField.text = [[NSUserDefaults standardUserDefaults] stringForKey:@"emailTextField.text"];
	
	topGreenBar.backgroundColor = OPAQUE_HEXCOLOR(0x399758);//55945B
	midGreenBar.backgroundColor = OPAQUE_HEXCOLOR(0x6CB383);//7DAF84//7EB085
	
	
	// facebook connect initialization
	fbConnectManager = [[Facebook alloc] init];
	// facebook authorization will be initialized in -(IBAction)broadcastOnFacebook
	
	// twitter engine initialization
	twitterManager = [[SA_OAuthTwitterEngine alloc] initOAuthWithDelegate: self];
	twitterManager.consumerKey = kTwitterOAuthConsumerKey;
	twitterManager.consumerSecret = kTwitterOAuthConsumerSecret;
	[twitterManager setClientName: kTwitterClientName version:kTwitterClientVersion URL:kBrainTunerAppURL token:@""];

	// we first check if Game Center is supported on the device or not
	if([GameCenterManager isGameCenterAvailable])
	{
		
//		Class GameCenterManagerClass = NSClassFromString(@"GameCenterManager");
//		if (GameCenterManagerClass) {
		
		// Game Center initialization
		self.gameCenterManager = [[[GameCenterManager alloc] init] autorelease];
		[self.gameCenterManager setDelegate: self];
		
//		}
		
	}
}


// TODO: check email validity
// must have exactly 1 @, at least 1 .
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
	DLog(@"%s", _cmd);
	if (textField == nameTextField) {
		[emailTextField becomeFirstResponder];
	} else if (textField == emailTextField) {
		[self validateInput];
	}
	
	return YES;
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
	if ([alertView.title isEqualToString:@"Name is required"]) {
		[nameTextField becomeFirstResponder];
	} else {
		if (buttonIndex == 1) {
			DLog(@"alertView: Submit score confirmed");
			[self submitScore];
		}
	}
}


- (void)validateInput {
	if ([[nameTextField.text stringByReplacingOccurrencesOfString:@" " withString:@""] isEqualToString:@""]) {
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Name is required" message:@"Enter your name." delegate:self
											  cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[alert show];
		[alert release];
		//[nameTextField becomeFirstResponder];
	} else if ([[emailTextField.text stringByReplacingOccurrencesOfString:@" " withString:@""] isEqualToString:@""]) {
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"NO_EMAIL", @"No Email Entered")
														message:NSLocalizedString(@"EMAIL_OPTIONAL", @"Email is optional.")
													   delegate:self
											  cancelButtonTitle:NSLocalizedString(@"ENTER_EMAIL", @"Enter Email") otherButtonTitles:NSLocalizedString(@"NO_EMAIL", @"No Email"), nil];
		[alert show];
		[alert release];
	} else if ([emailTextField.text rangeOfString:@"@"].location == NSNotFound || [emailTextField.text rangeOfString:@"."].location == NSNotFound) {
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"INVALID_EMAIL", @"Invalid Email")
														message:NSLocalizedString(@"EMAIL_INVALID_EXPLANATION", @"Email is optional, but what you entered is invalid. Correct it, or make the box blank.")
													   delegate:self
											  cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[alert show];
		[alert release];
	} else {
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Ready to submit your score" message:@"" delegate:self
											  cancelButtonTitle:@"Not Yet" otherButtonTitles:@"Confirm", nil];
		[alert show];
		[alert release];
		[emailTextField resignFirstResponder];
	}
}	


- (void)submitScore {
	[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
	
	[nameTextField resignFirstResponder];
	[emailTextField resignFirstResponder];
	
	// Save name and email as default
	[[NSUserDefaults standardUserDefaults] setObject:nameTextField.text forKey:@"nameTextField.text"];
	[[NSUserDefaults standardUserDefaults] setObject:emailTextField.text forKey:@"emailTextField.text"];

	[scoreViewController submitWithName:nameTextField.text email:emailTextField.text];
}


- (IBAction)cancelButtonTapped {
	[scoreViewController tapDone:nil];
}


// TODO: check email validity
- (IBAction)submitScoreButtonTapped {
	[self validateInput];
}


#pragma mark GameCenter, Facebook, Twitter notify

// called from ScoreViewController viewWillAppear
- (void)setFinalTimeForSubmission:(double)time
{
	finalTime = time;
	BrainTuner3AppDelegate *app = (BrainTuner3AppDelegate *)[[UIApplication sharedApplication] delegate];
	int problemCount = [app getProblemCountFromSegmentedControl];
	self.gameCenterCategory = [NSString stringWithFormat:@"%d", problemCount];
	

	DLog(@"setFinalTimeForSubmission: finalTime = %f problems = %d", finalTime, problemCount);
	if([GameCenterManager isGameCenterAvailable] == FALSE)
	{
		DLog(@"Game Center is unsupported!");
		gameCenterButton.hidden = YES;
		gameCenterButton.enabled = FALSE;
	}
	
	// reset user interface, ready for submission
	submissionProcessing = FALSE;
	leaderBoardSubmissionSucceeded = FALSE;
	facebookSubmissionSucceeded = FALSE;
	twitterSubmissionSucceeded = FALSE;
	[self.gameCenterButton setImage:[UIImage imageNamed:@"GameCenter_icon.jpg"] forState:UIControlStateNormal];
	[self.facebookButton setImage:[UIImage imageNamed:@"Facebook_icon.png"] forState:UIControlStateNormal];
	[self.twitterButton setImage:[UIImage imageNamed:@"Twitter_icon.png"] forState:UIControlStateNormal];
	
}

- (IBAction)broadcastOnLeaderBoard:(id)sender
{
	if (submissionProcessing) {
		return;
	}
	
	if (leaderBoardSubmissionSucceeded) {
		[self showGameCenterLeaderBoardView];
		return; // we don't want user to spam our leaderboard again and again, we could provide option to view leaderboard instead
	}
	
	submissionProcessing = TRUE;

	[self.gameCenterManager authenticateLocalUser];
	// authenticateLocalUser will call delegate method processGameCenterAuth
}

#pragma mark GameCenterDelegateProtocol Methods

- (void)processGameCenterAuth:(NSError*)error
{
	if (error == NULL)
	{
		self.gameCenterLeaderBoardScore = finalTime*100; // score conversion, Game Center uses int64_t score format
		[self.gameCenterManager reportScore:self.gameCenterLeaderBoardScore forCategory:self.gameCenterCategory];
		// reportScore will call delegate method scoreReported
	}
	else
	{
		UIAlertView* alert= [[[UIAlertView alloc] initWithTitle: @"Game Center Account Required"
														message: [NSString stringWithFormat: @"Reason: %@", [error localizedDescription]]
													   delegate: self cancelButtonTitle: @"Dismiss" otherButtonTitles: nil] autorelease];
		[alert show];
		submissionProcessing = FALSE;
	}
}

- (void) scoreReported: (NSError*) error
{
	submissionProcessing = FALSE;
	if(error == NULL)
	{
		
		[self.gameCenterManager reloadHighScoresForCategory: self.gameCenterCategory];
		leaderBoardSubmissionSucceeded = TRUE;
		[self.gameCenterButton setImage:[UIImage imageNamed:@"GameCenter_icon_ok.png"] forState:UIControlStateNormal];	
		
		[self showGameCenterLeaderBoardView];
	}
	else
	{
		UIAlertView* alert= [[[UIAlertView alloc] initWithTitle: @"Score Report Failed!" message: [NSString stringWithFormat: @"Reason: %@", [error localizedDescription]] 
													   delegate: NULL cancelButtonTitle: @"OK" otherButtonTitles: NULL] autorelease];
		[alert show];
		
	}
}

-(void)showGameCenterLeaderBoardView
{
	// use GameKit's LeaderBoard
	GKLeaderboardViewController *leaderboardController = [[GKLeaderboardViewController alloc] init];
	if (leaderboardController != NULL) 
	{
		leaderboardController.category = self.gameCenterCategory;
		leaderboardController.timeScope = GKLeaderboardTimeScopeAllTime;
		leaderboardController.leaderboardDelegate = self; 
		[self presentModalViewController: leaderboardController animated: YES];
	}
}

- (void)leaderboardViewControllerDidFinish:(GKLeaderboardViewController *)viewController
{
	[self dismissModalViewControllerAnimated: YES];
	[viewController release];
}

-(IBAction)broadcastOnFacebook:(id)sender
{
	if (submissionProcessing) {
		return;
	}
	
	if (facebookSubmissionSucceeded) {
		return;
	}
	
	NSArray * _permissions =  [[NSArray arrayWithObjects:@"publish_stream",nil] retain];
	[fbConnectManager authorize:kFacebookAPIKey permissions:_permissions delegate:self];
	// authorize will call delegate method fbDidLogin if succeed, otherwise fbDidNotLogin will be called
	[_permissions release];
}

#pragma mark FBSessionDelegate Methods

- (void)fbDidLogin
{
	// user logged in, now broadcast on FB
	SBJSON *jsonWriter = [[SBJSON new] autorelease];
	
	
	NSDictionary* actionLinks = [NSArray arrayWithObjects:[NSDictionary dictionaryWithObjectsAndKeys: 
														   @"Brain Tuner",@"text",kBrainTunerAppURL,@"href", nil], nil];
	
	NSString *actionLinksStr = [jsonWriter stringWithObject:actionLinks];
	
	NSString *caption = [NSString stringWithFormat:@"%@ %@ %@ %@ %@", 
						 @"solved ", self.gameCenterCategory, @" problems in ",[NSString stringWithFormat:@"%f", finalTime] ,@" seconds with Brain Tuner, a fun math game for Android and iOS. Can you beat this time? "];

	NSDictionary* attachment = [NSDictionary dictionaryWithObjectsAndKeys:
								@"Brain Tuner new record!", @"name",
								caption, @"caption",
								@"", @"description",
								kBrainTunerAppURL, @"href", nil];
	NSString *attachmentStr = [jsonWriter stringWithObject:attachment];
	
	NSMutableDictionary* params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
								   kFacebookAppID, @"api_key",
								   @"Tell friends on Facebook about your score",  @"user_message_prompt",
								   actionLinksStr, @"action_links",
								   attachmentStr, @"attachment",
								   nil];
	
	[fbConnectManager dialog: @"stream.publish"
				   andParams: params
				 andDelegate: self];
	submissionProcessing = FALSE;
}

/**
 * Called when the user dismiss the dialog without login
 */
- (void)fbDidNotLogin:(BOOL)cancelled
{
	submissionProcessing = FALSE;
	DLog(@" not OK!");
}

/**
 * Called when the user is logged out
 */
- (void)fbDidLogout
{
	// doing nothing here
}

#pragma mark FBDialogDelegate Methods

- (void)dialogDidComplete:(FBDialog *)dialog
{
	submissionProcessing = FALSE;
	// update user interface and prevent user from submitting again
	[self.facebookButton setImage:[UIImage imageNamed:@"Facebook_icon_ok.png"] forState:UIControlStateNormal];
	facebookSubmissionSucceeded = TRUE;
}

-(IBAction)broadcastOnTwitter:(id)sender
{
	if (submissionProcessing) {
		return;
	}
	
	if (twitterSubmissionSucceeded) {
		return;
	}
	
	
	[twitterManager clearAccessToken];	// force user to authenticate everytime he/she broadcasts on Twitter
	
	UIViewController			*controller = [SA_OAuthTwitterController controllerToEnterCredentialsWithTwitterEngine: twitterManager delegate: self];
	
	if (controller) {	
		[self presentModalViewController: controller animated: YES];
		// we wil send status update in delegate method authenticatedWithUsername , after user authorization
	} else {
		// ?? already start send status update immediately. this code never runs because we [twitterEngine clearAccessToken] everytime ?!
		//[twitterManager sendUpdate: [NSString stringWithFormat: @"Chip chip 1-2-3. %@", [NSDate date]]];
	}
}


//=============================================================================================================================
#pragma mark SA_OAuthTwitterEngineDelegate
- (void) storeCachedTwitterOAuthData: (NSString *) data forUsername: (NSString *) username {
	NSUserDefaults			*defaults = [NSUserDefaults standardUserDefaults];
	
	[defaults setObject: data forKey: @"authData"];
	[defaults synchronize];
}

- (NSString *) cachedTwitterOAuthDataForUsername: (NSString *) username {
	return [[NSUserDefaults standardUserDefaults] objectForKey: @"authData"];
}

//=============================================================================================================================
#pragma mark SA_OAuthTwitterControllerDelegate
- (void) OAuthTwitterController: (SA_OAuthTwitterController *) controller authenticatedWithUsername: (NSString *) username {
	NSLog(@"Authenicated for %@", username);
	
	NSString *caption = [NSString stringWithFormat:@"%@ %@ %@ %@ %@", 
						 @"solved ", self.gameCenterCategory, @" problems in ",[NSString stringWithFormat:@"%f", finalTime] ,@" seconds with Brain Tuner, a fun math game for Android and iOS. http://www.greengar.com/apps/brain-tuner/"];

	[twitterManager sendUpdate: caption];
}

- (void) OAuthTwitterControllerFailed: (SA_OAuthTwitterController *) controller {
	NSLog(@"Authentication Failed!");
	submissionProcessing = FALSE;
}

- (void) OAuthTwitterControllerCanceled: (SA_OAuthTwitterController *) controller {
	NSLog(@"Authentication Canceled.");
	submissionProcessing = FALSE;
}

//=============================================================================================================================
#pragma mark TwitterEngineDelegate
- (void) requestSucceeded: (NSString *) requestIdentifier {
	// update user interface and prevent user from submitting again
	NSLog(@"Request %@ succeeded", requestIdentifier);
	[self.twitterButton setImage:[UIImage imageNamed:@"Twitter_icon_ok.png"] forState:UIControlStateNormal];
	twitterSubmissionSucceeded = TRUE;
	submissionProcessing = FALSE;
}

- (void) requestFailed: (NSString *) requestIdentifier withError: (NSError *) error {
	submissionProcessing = FALSE;
	NSLog(@"Request %@ failed with error: %@", requestIdentifier, error);
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Twitter request failed" message:@"Problem when sending your request to Twitter. Please try again!" delegate:nil
										  cancelButtonTitle:@"OK" otherButtonTitles:nil];
	[alert show];
	[alert release];
}

/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning]; // Releases the view if it doesn't have a superview
    // Release anything that's not essential, such as cached data
}


- (void)dealloc {
    [super dealloc];
}


@end
