//
//  FlipsideViewController.m
//  BrainTuner3
//
//  Created by Elliot Lee on 7/13/08.
//  Copyright GreenGar Studios 2008-2009. All rights reserved.
//

#import "FlipsideViewController.h"
#import "FlurryAPI+Extensions.h"
#import <QuartzCore/QuartzCore.h>
#import "Defaults.h"
#import "EquityProblem.h"
#import "InequityProblem.h"
#import "GSAudio.h"


#define RANDOM_SEED() srandom(time(NULL))
#define LABEL_TAG 1
#define IMAGE_TAG 2

//#define kUseTimePenaltyKey @"kUseTimePenaltyKey"
#define useTimePenalty 1

#define kTotalEmptyRows 4
#define kEmptyRows		kTotalEmptyRows / 2

@implementation FlipsideViewController

//@synthesize problemCount;
@synthesize problemArray;

@synthesize problemTable;

- (void)viewDidLoad {
	app = (BrainTuner3AppDelegate *)[[UIApplication sharedApplication] delegate]; // TODO: use the #define instead
}

- (void)startGame {
	countdown.hidden = YES;
	
	//[problemTable setEnabled:NO];
	problemTable.userInteractionEnabled = NO;
	
	wrongButton.enabled = YES;
	rightButton.enabled = YES;
	
	startTimeInterval = CACurrentMediaTime();
}

- (void)viewWillAppear:(BOOL)animated {
	DLog(@"LOG_EVENT(@\"gameStarted\");"); // TODO: test
	LOG_EVENT(@"gameStarted");
	
	countdown.hidden = NO;
	countdown.text = @"";
	
	wrongButton.enabled = NO;
	rightButton.enabled = NO;
	
	//DLog(@"app.rootViewController.flipsideNavigationBar.topItem.rightBarButtonItem.style = %@", app.rootViewController.flipsideNavigationBar.topItem.rightBarButtonItem.style);
	//[app.rootViewController.flipsideNavigationBar.topItem.rightBarButtonItem setStyle:UIBarButtonSystemItemDone];
	//DLog(@"app.rootViewController.flipsideNavigationBar.topItem.rightBarButtonItem.style = %@", app.rootViewController.flipsideNavigationBar.topItem.rightBarButtonItem.style);
	
	RANDOM_SEED();
	
	currentProblem = 0;
	
	if (!blueGreenView) {
		blueGreenView = [[UIImageView alloc] initWithFrame:CGRectMake(self.view.frame.size.width-4, problemTable.frame.origin.y, 4, 0)];
		blueGreenView.image = [UIImage imageNamed:@"BlueGreenBarVert.png"];
		blueGreenView.contentMode = UIViewContentModeTop;
		blueGreenView.clipsToBounds = YES;
		[self.view addSubview:blueGreenView];
	} else {
		CGRect frame = blueGreenView.frame;
		frame.size.height = 0;
		blueGreenView.frame = frame;
	}
	
	// TODO: add to Options; add to Settings? depends on whether we like it
//	useTimePenalty = [NSDEF boolForKey:kUseTimePenaltyKey];
	
	//problemCount = app.problemCount;
	/*
	if ([self getProblemCount] < 20 || [self getProblemCount] > 100)
		[self getProblemCount] = 20;
	*/
	
	//[[NSArray arrayWithObjects:[Problem randomProblem] count:10] retain];
	
	if (problemArray == nil) {
		problemArray = [[NSMutableArray arrayWithCapacity:[self getProblemCount]] retain];
	} else {
		[problemArray removeAllObjects];
	}
	
	
	DLog(@"OPT_BIG:%d", [[app.optionsArray objectAtIndex:OPT_BIG] boolValue]);
	
	for (int i = 0; i < [self getProblemCount]; i++) {
		Problem * prob;
		//if ([[app.optionsArray objectAtIndex:OPT_INEQUITY] boolValue] == NO) {
		if (app.rootViewController.mainViewController.difficulty < 4) {
			prob = [[[EquityProblem alloc] init] autorelease];
		} else {
			BOOL useInequityProblem = (arc4random()%2 == 0);
			if (useInequityProblem) {
				prob = [[[InequityProblem alloc] init] autorelease];
			} else {
				prob = [[[EquityProblem alloc] init] autorelease];	
			}
		}

		
		[prob randomize];
		[problemArray addObject:prob];
	}
	
	[problemTable reloadData];
	
	//problemArray = [[NSArray alloc] init];
	//[problemArray
	
	//DLog(@"%@", problemArray);
	
	//currentProblem = 0;
	//tableArray = [[NSArray arrayWithObjects:@"First", @"Second", @"Third", nil] retain];
	
	//[problemTable selectRowAtIndexPath:(NSIndexPath *)currentProblem animated:NO scrollPosition:UITableViewScrollPositionMiddle];
	
	// select row
	NSIndexPath *rowPath = [NSIndexPath indexPathForRow:(currentProblem + kEmptyRows)
											  inSection:0];
	[problemTable selectRowAtIndexPath:rowPath animated:NO scrollPosition:UITableViewScrollPositionNone];
	// scroll to the row
	[problemTable scrollToRowAtIndexPath:rowPath
						atScrollPosition:UITableViewScrollPositionMiddle
								animated:YES];
	
	[GSAudio load:@"right.wav"];
	[GSAudio load:@"wrong.wav"];
	
	if ([[app.optionsArray objectAtIndex:OPT_SOUND] boolValue] == YES && [[app.optionsArray objectAtIndex:OPT_COUNTDOWN] boolValue] == YES) {
		[GSAudio load:@"blipblipblipbeep.wav"];
		[NSTimer scheduledTimerWithTimeInterval:0.85 target:self selector:@selector(startBlip:) userInfo:nil repeats:NO];
	}
	if ([[app.optionsArray objectAtIndex:OPT_COUNTDOWN] boolValue] == YES) {
		// Invalidate it first, in case the one from the previous game is still active
		[continueCountdownTimer invalidate];
		
		continueCountdownTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(continueCountdown:) userInfo:nil repeats:NO];
	} else {
		[self startGame];
	}
}


- (void)viewDidAppear:(BOOL)animated {
	[super viewDidAppear:animated];
	
	[self swapButtonsIfNecessary];
}


- (void)swapButtonsIfNecessary {
	if ([[app.optionsArray objectAtIndex:OPT_SWP] boolValue] == YES) {
		DLog(@"YES");
		rightButton.frame = CGRectMake(20, rightButton.frame.origin.y, rightButton.frame.size.width, rightButton.frame.size.height);
		wrongButton.frame = CGRectMake(168, wrongButton.frame.origin.y, wrongButton.frame.size.width, wrongButton.frame.size.height);
	} else {
		DLog(@"NO");
		rightButton.frame = CGRectMake(168, rightButton.frame.origin.y, rightButton.frame.size.width, rightButton.frame.size.height);
		wrongButton.frame = CGRectMake(20, wrongButton.frame.origin.y, wrongButton.frame.size.width, wrongButton.frame.size.height);
	}
}

- (void)startBlip:(NSTimer*)theTimer {
	[GSAudio play:@"blipblipblipbeep.wav"];
}


- (void)continueCountdown:(NSTimer*)theTimer {
	continueCountdownTimer = nil;
	if (countdown.text == @"") {
		countdown.text = [NSString stringWithFormat:@"%d", 3];
		continueCountdownTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(continueCountdown:) userInfo:nil repeats:NO];
	} else if ([countdown.text intValue] > 1) {
		countdown.text = [NSString stringWithFormat:@"%d", [countdown.text intValue] - 1];
		continueCountdownTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(continueCountdown:) userInfo:nil repeats:NO];
	} else {
		// countdown is done
		
		[self startGame];
	}
}


/*
- (void)setProblemCount:(int)newCount {
	problemCount = newCount;
}
*/


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return [problemArray count] + kTotalEmptyRows;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	static NSString *identity = @"MainCell";

	UILabel *label;
	UIImageView *imageView;
	
	//MyTableViewCell *cell = (MyTableViewCell *)[tableView dequeueReusableCellWithIdentifier:identity];
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identity];//CELL_IDENTIFIER
	
	if (cell == nil) {
	
		//cell = (MyTableViewCell *)[self cellFromNibNamed:@"MyTableViewCell"];
		CGRect frame = CGRectMake(0, 0, 320, 68);
		//cell = [[[MyTableViewCell alloc] initWithFrame:CGRectMake(0, 0, 200, 200) reuseIdentifier:identity] autorelease];
		cell = [[[UITableViewCell alloc] initWithFrame:frame reuseIdentifier:identity] autorelease];//CELL_IDENTIFIER
		
		cell.selectionStyle = UITableViewCellSelectionStyleNone;
		
		label = [[[UILabel alloc] initWithFrame:CGRectMake(12.0, 0.0, 252.0, 68.0)] autorelease];
		label.tag = LABEL_TAG;
		//label.font = [label.font fontWithSize:32.0];
		label.font = [UIFont boldSystemFontOfSize:32.0];
		
		// Test at level 101
		label.adjustsFontSizeToFitWidth = YES;
		label.minimumFontSize = 5.0;
		
		label.backgroundColor = [UIColor clearColor];
		
		[cell.contentView addSubview:label];
		
		imageView = [[[UIImageView alloc] initWithFrame:CGRectMake(248.0, 12.0, 48.0, 48.0)] autorelease];
		imageView.tag = IMAGE_TAG;
		[cell.contentView addSubview:imageView];
	} else {
		label = (UILabel *)[cell.contentView viewWithTag:LABEL_TAG];
		imageView = (UIImageView *)[cell.contentView viewWithTag:IMAGE_TAG];
	}
	
	if (indexPath.row < kEmptyRows || indexPath.row >= [problemArray count] + kEmptyRows) {
		label.text = @"";
		imageView.image = nil;
		//DLog(@"returning empty row. indexPath.row:%d [problemArray count]:%d", indexPath.row, [problemArray count]);
	} else {
		label.text = [[problemArray objectAtIndex:(indexPath.row - kEmptyRows)] text];
		// cell.text, cell setCellText:, cell.font, cell setCellFont:
		
		// if image != nil
		imageView.image = [[problemArray objectAtIndex:(indexPath.row - kEmptyRows)] image];
		// cell.image, cell setCellImage
	}
	
	return cell;
}

/*
- (UITableViewCell *)cellFromNibNamed:(NSString *)nibName {
	// laod the xib file into memory
	NSArray *array = [[NSBundle mainBundle] loadNibNamed:nibName 
												   owner:self 
												 options:nil];		
	// get the note view cell
	UITableViewCell *cell = [array objectAtIndex:1];
	
	// get the content view items
	UIView *childView = [array objectAtIndex:2]; //1
	CGRect frame = [childView frame];
	
	// finetune the position
	const int pos = 7;
	frame.origin.x += pos;
	frame.size.width -= pos;
	frame.size.height -= 1;
	[childView setFrame:frame];
	
	// get the content view child
	[cell.contentView addSubview:childView];
	
	return cell;
}
*/

- (void)updateBars:(int)numProblemsCompleted {
	float percent = (float)numProblemsCompleted / [problemArray count]; //self getProblemCount
	float points = percent * 320.0f;
	CGRect frame = blueGreenView.frame;
	frame.size.height = points;
	blueGreenView.frame = frame;
}

- (void)appendProblemCount:(int)count {
//	if (!useTimePenalty) {
#if !useTimePenalty
		// add |count| problems to the end of the array
		for (int i = 0; i < count; i++) {
			[problemArray addObject:[Problem randomProblem]];
		}
		
		// don't reload yet here
		
//	}
#endif
}

// Note: OPT_SFX was removed long ago...

- (IBAction)clickRight:(id)sender {
	
	if ([[problemArray objectAtIndex:currentProblem] isRight] == YES) {
		if ([[app.optionsArray objectAtIndex:OPT_SOUND] boolValue] == YES) {
			[GSAudio play:@"right.wav"];
		}
		[[problemArray objectAtIndex:currentProblem] setImage:[UIImage imageNamed:@"right.png"]];
	} else {
		if ([[app.optionsArray objectAtIndex:OPT_SOUND] boolValue] == YES) {
			[GSAudio play:@"wrong.wav"];
		}
		[[problemArray objectAtIndex:currentProblem] setImage:[UIImage imageNamed:@"wrong.png"]];
		
		[self appendProblemCount:3];
	}
	[problemTable reloadData];
	
	[self updateBars:currentProblem+1];
	
	if (currentProblem < [problemArray count]-1) { //self getProblemCount
		currentProblem++;
		DLog(@"%d", currentProblem);
		// path of the 'to be' selected row
		NSIndexPath *rowPath = [NSIndexPath indexPathForRow:(currentProblem + kEmptyRows)
											  inSection:0];
		[problemTable selectRowAtIndexPath:rowPath animated:NO scrollPosition:UITableViewScrollPositionNone];
		// scroll to the row
		[problemTable scrollToRowAtIndexPath:rowPath
			   atScrollPosition:UITableViewScrollPositionMiddle
					   animated:YES];
	} else {
		[self alertStatsAndReset];
	}
}


- (IBAction)clickWrong:(id)sender {
	
	if ([[problemArray objectAtIndex:currentProblem] isRight] == NO) {
		if ([[app.optionsArray objectAtIndex:OPT_SOUND] boolValue] == YES) {
			[GSAudio play:@"right.wav"];
		}
		[[problemArray objectAtIndex:currentProblem] setImage:[UIImage imageNamed:@"right.png"]];
	} else {
		if ([[app.optionsArray objectAtIndex:OPT_SOUND] boolValue] == YES) {
			[GSAudio play:@"wrong.wav"];
		}
		[[problemArray objectAtIndex:currentProblem] setImage:[UIImage imageNamed:@"wrong.png"]];
		
		[self appendProblemCount:3];
	}
	[problemTable reloadData];
	
	[self updateBars:currentProblem+1];
	
	if (currentProblem < [problemArray count]-1) { //self getProblemCount
		currentProblem++;
		NSIndexPath *rowPath = [NSIndexPath indexPathForRow:(currentProblem + kEmptyRows)
											  inSection:0];
		[problemTable selectRowAtIndexPath:rowPath animated:NO scrollPosition:UITableViewScrollPositionNone];
		// scroll to the row
		[problemTable scrollToRowAtIndexPath:rowPath
						atScrollPosition:UITableViewScrollPositionMiddle
								animated:YES];
	} else {
		[self alertStatsAndReset];
	}
}

- (void)alertStatsAndReset {
	END_TIMED_EVENT(@"gameStarted");
	
	CFTimeInterval time = CACurrentMediaTime() - startTimeInterval; // double
	
	if ([[app.optionsArray objectAtIndex:OPT_SOUND] boolValue] == YES) {
		[GSAudio play:@"trumpets fanfar.wav"];
	}
	
	correct = 0;
	incorrect = 0;
	Problem *p;
	for (int i = 0; i < [problemArray count]; i++) { //self getProblemCount
		p = [problemArray objectAtIndex:i];
		if (p.image == [UIImage imageNamed:@"right.png"]) {
			correct++;
		} else {
			incorrect++;
		}
	}
	int accuracy = (100 * correct) / [problemArray count]; //self getProblemCount
	
	int penalty = 5 * incorrect;
//	if (useTimePenalty) {
#if useTimePenalty
		finalTime = time + penalty; //double
//	} else {
#else
		finalTime = time; // important
//	}
#endif
	
	NSString *newRecord = @"";
	if (time <= 0) {
		DLog(@"Error! time is negative: %f", time);
	} else {
		newRecord = [app saveScoreIfBetter:finalTime];
	}
	
	NSString *title;
	NSString *message;
	if (finalTime >= 2.5 * [self getProblemCount]) {
		if (accuracy == 100)
			title = NSLocalizedString(@"PERFECT_SLOW",@"Perfect But Slow");
		else
			title = NSLocalizedString(@"NOT_GOOD",@"Not Very Good");
		message = NSLocalizedString(@"TRY_AGAIN",@"Get your brain in-tune\nby trying again!");
	} else if (finalTime >= 2 * [self getProblemCount]) {
		if (accuracy == 100)
			title = NSLocalizedString(@"MORE_SPEED",@"Try For More Speed");
		else
			title = NSLocalizedString(@"TRY_HARDER",@"Try Harder");
		message = NSLocalizedString(@"BETTER_PRACTICE",@"You'll get better with practice.\nTry again!");
	} else if (finalTime >= 1.5 * [self getProblemCount]) {
		title = NSLocalizedString(@"GOOD_JOB",@"Good Job");
		message = NSLocalizedString(@"GOOD_TIME",@"That's a good time! Keep improving.");
	} else if (finalTime >= [self getProblemCount]) {
		title = NSLocalizedString(@"GREAT_SPEED",@"Great Speed");
#ifdef LITE_MODE
		message = NSLocalizedString(@"NICE_JOB",@"Nice job.");
#else
		message = NSLocalizedString(@"NICE_EXPLANATION",@"Nice job. Keep playing and beat your old time!");
#endif
	} else if (finalTime >= 0.5 * [self getProblemCount]) {
		title = NSLocalizedString(@"CONGRATULATIONS",@"Congratulations!");
#ifdef LITE_MODE
		message = NSLocalizedString(@"AMAZING",@"You're amazing!");
#else
		message = NSLocalizedString(@"AMAZING_EXPLANATION",@"You're amazing! Play every day to keep your brain in-tune.");
#endif
	} else {
		title = NSLocalizedString(@"EXPERT",@"Expert");
#ifdef LITE_MODE
		message = NSLocalizedString(@"WOW",@"WOW!");
#else
		message = NSLocalizedString(@"SEND_FEEDBACK",@"Send me feedback for future Brain Tuner games!");
#endif
	}
	
	UIAlertView *alert;
	
#ifdef LITE_MODE
    
	/**
     * Brain Tuner 2 is coming soon, so let's not promote Brain Tuner Premium anymore
     *
    BOOL goodTime = (finalTime <= 1.5 * [self getProblemCount]);
    
	if (goodTime) {
		message = [NSString stringWithFormat:
				   NSLocalizedString(@"HIGH_TIME",@"%@\nWith a time like that,\nyou could be on the\nBrain Tuner Premium\nGlobal High Scores list! Would you like to see it in the App Store?"), message];
	}
     */
	
//	if (useTimePenalty) {
#if useTimePenalty
		//"SEC_PENALTY" = "%.4f seg + %d penalización =\n%.4f segundos.%@\n%d%% de aciertos.\n%@";
		message = [NSString stringWithFormat:
				   NSLocalizedString(@"SEC_PENALTY",@"%.4f sec. + %d penalty =\n%.4f seconds.%@\n%d%% Accuracy.\n%@"), time, penalty, finalTime, newRecord, accuracy, message];
//	} else {
#else
		//"SEC_PROBLEMS" = "%d problems in\n%.4f seconds.%@\n%d%% Accuracy.\n%@";
		message = [NSString stringWithFormat:
				   NSLocalizedString(@"SEC_PROBLEMS",@"%d problems in\n%.4f seconds.%@\n%d%% Accuracy.\n%@"), [problemArray count], finalTime, newRecord, accuracy, message];
//	}
#endif
	
	/*
	if (goodTime) {
		alert = [[UIAlertView alloc] initWithTitle:title
										   message:message
										  delegate:self cancelButtonTitle:@"No" otherButtonTitles:NSLocalizedString(@"YES",@"Yes"), nil];
		alert.tag = kAppStoreAlertTag;
	} else {
     */
		alert = [[UIAlertView alloc] initWithTitle:title
										   message:message
										  delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
	//}
	
#else //#ifdef LITE_MODE
	
//	if (useTimePenalty) {
#if useTimePenalty
		message = [NSString stringWithFormat:
				   NSLocalizedString(@"SEC_PENALTY",@"%.4f sec. + %d penalty =\n%.4f seconds.%@\n%d%% Accuracy.\n%@"), time, penalty, finalTime, newRecord, accuracy, message];
//	} else {
#else
		message = [NSString stringWithFormat:
				   NSLocalizedString(@"SEC_PROBLEMS",@"%d problems in\n%.4f seconds.%@\n%d%% Accuracy.\n%@"), [problemArray count], finalTime, newRecord, accuracy, message];
//	}
#endif
	
	alert = [[UIAlertView alloc] initWithTitle:title
									   message:message
									  delegate:self cancelButtonTitle:@"OK" otherButtonTitles:NSLocalizedString(@"SUBMIT_SCORE",@"Submit Score"), nil];
	
#endif
	
	[alert show];
	[alert release];
	
	// Let's try submitting the score here
}

// Moved saveScoreIfBetter because it's not loaded yet when the app first starts!

// TODO: For Premium, add code to handle Global High Score submission
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
	// Possible alerts:
	// 1. Would you like to see it in the App Store? (New Record or Not)
	// 2. Would you like to email your new record to a friend?
	// 3. OK (New Record or Not)
	
	if (alertView.tag == kAppStoreAlertTag && buttonIndex == alertView.firstOtherButtonIndex) {
		// Go to the App Store.
		// With multitasking devices, this does NOT quit the app.
        // This is a link to Brain Tuner Premium. Don't use it, though; we should use a tracked affiliate link instead.
        
        /*
		[[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://phobos.apple.com/WebObjects/MZStore.woa/wa/viewSoftware?id=288322298&mt=8"]];
		//http://gengarstudios.com/btpremium.php?r=bt14
		
		// Flip back to the Main View without animation.
		[UIAppDelegate.rootViewController toggleViewAnimated:NO];
		 */
        
		return;
	}
	
#ifdef LITE_MODE
	NSString *newRecordString = NSLocalizedString(@"NEW_RECORD",@"\n(New Record!)");
	if ([alertView.message rangeOfString:newRecordString].location != NSNotFound) {
		// This score was a new record, so let's see if they'd like to email it to a friend
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"EMAIL_FRIEND", @"Email a Friend?")
														message:NSLocalizedString(@"WOULD_EMAIL", @"Would you like to email your new record to a friend?")
													   delegate:self cancelButtonTitle:@"No" otherButtonTitles:NSLocalizedString(@"YES",@"Yes"), nil];
		alert.tag = kEmailFriendAlertTag;
		[alert show];
		[alert release];
		return;
	}
#endif
	
	if (alertView.tag == kEmailFriendAlertTag) {
		DLog(@"alertView.tag == kEmailFriendAlertTag");
		if (buttonIndex == alertView.firstOtherButtonIndex) {
			DLog(@"buttonIndex == alertView.firstOtherButtonIndex");
			
			[(RootViewController *)[app rootViewController] toggleView];
			
			[UIAppDelegate.rootViewController.mainViewController sendEmailTapped];
			
			return;
		}
	}
	
	// At this point, the user is in one of the following situations:
	// 1. Not a New Record, and said No, they do not want to see Premium in the App Store: toggle
	// 2. Not a New Record, and the score wasn't good enough to be offered Premium:        toggle
	// 3. New Record,       and said No, they do not want to email a friend:               toggle
	
#ifdef LITE_MODE
	
	// If they have Review Answers enabled, and they got more than one problem incorrect
	if ([[app.optionsArray objectAtIndex:OPT_REV] boolValue] == YES && incorrect > 0) {
		
		// Let the table be scrollable
		problemTable.userInteractionEnabled = YES;
		
		// Don't let the buttons be tappable
		[rightButton setEnabled:NO];
		[wrongButton setEnabled:NO];
		
		// Change the Cancel button to say Done
		[app.rootViewController setButtonToDone];
		
		// Tell them they can review the problems
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"REVIEW_SCROLLING",@"Review Answers:\nScrolling Enabled")
														message:[NSString stringWithFormat:NSLocalizedString(@"REVIEW_EXPLANATION",@"Since you missed %d problems,\nyou may now review your answers\nby scrolling the list of problems.\nTap 'Done' at the top when finished."), incorrect]
													   delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[alert show];
		[alert release];
		
		return;
	}
	
	[(RootViewController *)[app rootViewController] toggleView];
	
#endif
	
#ifndef LITE_MODE // Lite can't reach this branch: Global High Score submission
	
	if (alertView.cancelButtonIndex == buttonIndex) {
		
		// User selected NOT to Submit Score
		
		// ---- //
		if ([[app.optionsArray objectAtIndex:OPT_ADD] boolValue] == YES &&
			[[app.optionsArray objectAtIndex:OPT_SUB] boolValue] == YES &&
			[[app.optionsArray objectAtIndex:OPT_MUL] boolValue] == YES &&
			[app.rootViewController.mainViewController onMaxDifficulty]) {
			
			float timePerProblem = finalTime / [self getProblemCount];
			[(RootViewController *)[app rootViewController] toggleViewWithTimePerProblem:timePerProblem];
		} else {
			[(RootViewController *)[app rootViewController] toggleView];
		}
		// ---- //
		
	} else {
		
		// User selected to Submit Score
		
		[(RootViewController *)[(BrainTuner3AppDelegate *)[[UIApplication sharedApplication] delegate] rootViewController] showScoreView:finalTime];
	}
#endif
}


- (int)getProblemCount {
	return [app getProblemCountFromSegmentedControl];
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
	// Return YES for supported orientations
	return (interfaceOrientation == UIInterfaceOrientationPortrait);
}


- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning]; // Releases the view if it doesn't have a superview
	// Release anything that's not essential, such as cached data
}


- (void)dealloc {
	[super dealloc];
}


@end

