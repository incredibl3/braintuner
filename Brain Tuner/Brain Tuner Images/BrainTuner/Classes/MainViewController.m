//
//  MainViewController.m
//  BrainTuner3
//
//  Created by Elliot Lee on 7/13/08.
//  Copyright GreenGar Studios 2008-2009. All rights reserved.
//

#import "MainViewController.h"
#import "BrainTuner3AppDelegate.h"
#import "FlipsideViewController.h"
#import "RootViewController.h"
#import "OptionsViewController.h"
#import "Reachability.h"
#import "FlurryAPI+Extensions.h"

#if LITE_MODE
    #import "GSAdView.h"
#else
	#import "WBWebViewController.h"
	#import "ScoreViewController.h"
	#import "BTNetworkEngine.h"
#endif

@implementation MainViewController

@synthesize segmentedControl;
@synthesize startButton;
@synthesize iTunesURL;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
	if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) {
		DLog();
        shouldSubmitSavedScores = YES;
	}
	return self;
}

// TODO: clean this up
- (void)viewWillAppear:(BOOL)animated {
	// Does NOT get called on app launch
	// Do NOT call on app launch, or else the app will crash
	// because the Beacon (used by MyAdView) hasn't been initialized yet
	
	// don't forget to call super
	[super viewWillAppear:animated];
	
	[UIAppDelegate loadBestTimeForSegmentedControl];
}

- (void)viewDidAppear:(BOOL)animated {
#if !kDefaultPngMode
	shineTimer = [[NSTimer scheduledTimerWithTimeInterval:4.0 target:self selector:@selector(applyShine) userInfo:nil repeats:YES] retain];
#endif
	
	// shows ad
	[[UIAppDelegate rootViewController] viewDidAppear:animated]; // VERY IMPORTANT!
	// otherwise ad is hidden after playing a game
	
	
	
	[super viewDidAppear:animated];
    
    CGRect rect = [[UIApplication sharedApplication] statusBarFrame];
    [self repositionOnStatusBarHeightChange:rect.size.height];
    
    
    // call this to check if there is cached ads
    // if the promo ads is cached during the game and the game has been finished, it will be shown after going back to home view, here
    [UIAppDelegate startPromoAds];
}

- (void)promoAdCached:(NSNotification *)notification {
    DLog(@"Promo ads cached, need to show on Main View Controller because the screen is visible and idle.");
    
    if (self.view.superview) {
        // display cached ads
        [UIAppDelegate startPromoAds];        
    }
}

- (void)applyShine {
	UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(-52, startButton.frame.origin.y, 52, 45)];
	imageView.image = [UIImage imageNamed:@"Shine.png"];
	[self.view addSubview:imageView];
	[self.view bringSubviewToFront:imageView];
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationDuration:1.5];
	imageView.frame = CGRectMake(320, startButton.frame.origin.y, 52, 45);
	[UIView commitAnimations];
	[imageView release];
}

- (void)viewWillDisappear:(BOOL)animated {
	[shineTimer invalidate];
	[shineTimer release];
	shineTimer = nil;
	
	// hides ad
//	[[UIAppDelegate rootViewController] viewWillDisappear:animated];
	
	[super viewWillDisappear:animated];
}

- (void)viewDidLoad {

#ifdef LITE_MODE
	subscript.text = @"Lite";
	subscript.textColor = [UIColor colorWithRed:0.0 green:0.5 blue:0.0 alpha:1.0];
	subscript.font = [UIFont systemFontOfSize:13.0];
	
#if kDefaultPngMode
	segmentedControl.enabled = NO;
	segmentedControl.alpha = 0.5;
	
	[startButton setImage:[UIImage imageNamed:@"LoadingButtonSpanish"] forState:UIControlStateNormal];
	startButton.enabled = NO;
	
//	premiumAdButton.hidden = YES;
	[premiumAdButton setImage:nil forState:UIControlStateNormal];
	[premiumAdButton setBackgroundColor:[UIColor blackColor]];
#else
	
	premiumAdButton.frame = CGRectOffset(premiumAdButton.frame, 0, +50);
	[UIView beginAnimations:nil context:NULL];
	// assumes the banner view is offset 50 pixels so that it is not visible.
	premiumAdButton.frame = CGRectOffset(premiumAdButton.frame, 0, -50);
	[UIView commitAnimations];
	
	// select default problem count on segmented control
	[segmentedControl setSelectedSegmentIndex:0];
	
#endif
	
#endif

	// Load state of Division Switch
	/* // Replaced by OptionsView -- loaded in AppDelegate
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	BOOL divisionIsOn = [defaults boolForKey:@"divisionIsOn"];
	DLog(@"divisionIsOn = %d", divisionIsOn);
	[division setOn:divisionIsOn animated:YES];
	*/
	
	// loadBestTime moved to BrainTuner3AppDelegate.m
	
	/*
	if (kDefaultPngMode) {
		[startButton setTitle:NSLocalizedString(@"LOADING", @"Loading...") forState:UIControlStateNormal];
		[startButton setTitleColor:[UIColor colorWithRed:125.0/255.0 green:190.0/255.0 blue:48.0/255.0 alpha:1.0] forState:UIControlStateNormal];
		
		if ([startButton respondsToSelector:@selector(titleLabel)]) {
			startButton.titleLabel.font = [UIFont systemFontOfSize:24.0];
		} else {
			[startButton performSelector:@selector(setFont:) withObject:[UIFont systemFontOfSize:24.0]];
			//startButton.font = [UIFont systemFontOfSize:24.0];
		}
	}
	 */
	
#ifdef kDefaultPngMode
	[infoDark setEnabled:NO];
#endif
	
#ifdef LITE_MODE
	infoDark.frame = CGRectMake(276, 321, 44, 44);
#else
	// make the info button's active area much, much larger
	infoDark.frame = CGRectMake(243, 383, 96, 117);
	// another option is 269, 419, 44, 44
#endif
	
#ifndef LITE_MODE
	difficulty = [[NSUserDefaults standardUserDefaults] integerForKey:@"difficulty"];
	maxDifficulty = [[NSUserDefaults standardUserDefaults] integerForKey:@"maxDifficulty"];
	if (difficulty <= 0) {
		difficulty = 1;
	}
	if (maxDifficulty <= 0) {
		maxDifficulty = 1;
	}
	//maxDifficulty = 32;
	DLog(@"difficulty:%d maxDifficulty:%d", difficulty, maxDifficulty);

//#ifdef kDefaultPngMode
	loDifficultyButton.enabled = YES; //(difficulty > 1);
	hiDifficultyButton.enabled = YES; //(difficulty < maxDifficulty);
		difficultyLabel.text = [NSString stringWithFormat:NSLocalizedString(@"DIFFICULTY_LEVEL", @"Difficulty Level: %d"), difficulty];
//#endif
	
#endif
	
	speechLabel.text = NSLocalizedString(@"SELECT_WHETHER", @"Select whether an equation is right or wrong with the buttons at the bottom.\nHave fun!");

    // register to promo ads , be notified if an ads is finished downloading
    [[NSNotificationCenter defaultCenter] addObserver:self 
                                             selector:@selector(promoAdCached:) 
                                                 name:kNotificationShowCachedPromoAd
                                               object:nil];

}

#ifdef LITE_MODE
- (void)adHeightDidChange:(CGFloat)adHeight {
	DLog(@"mainViewController adHeightDidChange:%f", adHeight);
	if (adHeight == 48.0) {
		startButton.frame = CGRectMake(0, 367, 320, 45);
	} else {
		startButton.frame = CGRectMake(0, 365, 320, 45);
	}
}
#endif

#ifndef LITE_MODE

- (void)submitSavedScores {
    // Fix app crash with old reachability API
    if ([[Reachability reachabilityForInternetConnection] isReachableViaWWAN] == YES ||
	//if ([[Reachability sharedReachability] internetConnectionStatus] == YES ||
		shouldSubmitSavedScores) {
		
		NSString *pathToFile = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:@"scores.data"];
		arrayOfParameters = [[NSMutableArray arrayWithContentsOfFile:pathToFile] retain];
		if (arrayOfParameters != nil && [arrayOfParameters count] <= 0) {
			[arrayOfParameters release];
			arrayOfParameters = nil;
			NSFileManager *fileManager = [NSFileManager defaultManager];
			[fileManager removeItemAtPath:pathToFile error:NULL];
		}
		if (arrayOfParameters != nil) {
			
			// Open question: should we check for connectivity before showing this alert?
			// 	 if ([[Reachability sharedReachability] internetConnectionStatus] == NO)
			
			// If this succeeds, your rank will be updated to reflect\nyour higher total score.
			
			UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Offline Saved Scores"
															message:[NSString stringWithFormat:@"You have %d unsubmitted score(s) that were saved offline.\n\nBrain Tuner will attempt to submit them now.",
																	 [arrayOfParameters count]]
														   delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
			[alert show];
			[alert release];
			
			// Array contains |parameters| dictionaries
			//[ScoreViewController sharedScoreViewController].submittingNewScore = NO;
			[BTNetworkEngine sharedBTNetworkEngine].submittingNewScore = NO;
			//[[ScoreViewController sharedScoreViewController] postToURL:submitURL withParameters:[arrayOfParameters objectAtIndex:0] target:self selector:@selector(receivedResponse:)];
			[[BTNetworkEngine sharedBTNetworkEngine] postToURL:submitURL withParameters:[arrayOfParameters objectAtIndex:0] target:self selector:@selector(receivedResponse:)];
			
			shouldSubmitSavedScores = NO;
			
			return;
		}
	}
}


// WBNetworkEngine delegate method
- (void)receivedResponse:(NSString *)response {
	DLog(@"MainViewController %s%@", _cmd, response);
	
	//NSString *path = [[NSBundle mainBundle] bundlePath];
	//NSURL *baseURL = nil;
	//NSURL *baseURL = [NSURL fileURLWithPath:path];
	
	NSString *results;
	
	if (response == nil || [response isEqualToString:@""]) {
		// Submission failed
		// TODO: get error and notify the user
		
		results = [NSString stringWithFormat:@"<head><meta id=\"viewport\" name=\"viewport\" content=\"width=320; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;\" /></head><body style='font-family: Helvetica;'><h2>%@</h2><p>Tap the Refresh button (the circular arrow) at the top left to retry.</p></body>",
				   NSLocalizedString(@"UNABLE_TO_SUBMIT", @"Unable to submit score")];
		
		//<p><b>Error:</b> %@</p>, [sfl getError]
		
		/*
		 UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"UNABLE_TO_SUBMIT", @"Unable to submit score") message:@"Retry?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
		 //@"Unable to submit score.\nYour score has been saved\nfor later submission."
		 [alertView show];
		 [alertView release];
		 */
	} else {
		results = response;
		//results = [sfl getResults];
		submissionSucceeded = YES;
	}
	//[Engine Log:[NSString stringWithFormat:@"Upload finished: '%@'",[sfl getResults]]];
	//DLog(@"Upload finished: '%@'",[sfl getResults]);
	
	//[webView loadHTMLString:results baseURL:baseURL];
	// TODO: notify the user about the response?
	
	//[sfl release];
	//sfl = nil;
	
	if (submissionSucceeded) {
		if (arrayOfParameters.count > 0) {
			DLog(@"More objects remain; posting to %@", submitURL);
			DLog(@"parameters:%@", [arrayOfParameters objectAtIndex:0]);
			[BTNetworkEngine sharedBTNetworkEngine].submittingNewScore = NO;
			[[BTNetworkEngine sharedBTNetworkEngine] postToURL:submitURL withParameters:[arrayOfParameters objectAtIndex:0] target:self selector:@selector(receivedResponse:)];
		} else {
			[arrayOfParameters release];
			arrayOfParameters = nil;
			NSString *pathToFile = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:@"scores.data"];
			NSFileManager *fileManager = [NSFileManager defaultManager];
			[fileManager removeItemAtPath:pathToFile error:NULL];
		}
	} else {
		DLog(@"Submission failed");
		// TODO: notify the user?
	}
}

#endif // #ifndef LITE_MODE

- (IBAction)setProblemCount:(UISegmentedControl *)sender {
	int selected = [sender selectedSegmentIndex];
	DLog(@"selected %d", selected);
	BrainTuner3AppDelegate *app = (BrainTuner3AppDelegate *)[[UIApplication sharedApplication] delegate];
	//int problemCount;
	if (selected == 0)
		app.problemCount = 20;
	else if (selected == 1)
		app.problemCount = 60;
	else
		app.problemCount = 100;
	//[[[app rootViewController] flipsideViewController] setProblemCount:problemCount];

	//DLog(@"loadBestTime from setProblemCount");
	[self loadBestTime:app.problemCount];
}


// Called when the Start Game, Done, or OK is pressed.
// Flips the displayed view from the main view to the flipside view and vice-versa.
- (IBAction)toggleView {
	RootViewController *rootViewController = [(BrainTuner3AppDelegate *)[[UIApplication sharedApplication] delegate] rootViewController];
	[rootViewController toggleView];
	speechLabel.text = @"Brain Tuner offers a variety of gameplay options. Tap the ( i ) at the lower right!";
}


- (float)bestTimeForProblems:(int)numProblems {
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	
	// For accessing optionsArray
	BrainTuner3AppDelegate *app = (BrainTuner3AppDelegate *)[[UIApplication sharedApplication] delegate];
	
	// Nearly Direct copy from FlipsideViewController.m
	NSString *desc = @"";
	for (int i = 0; i < OPTION_COUNT; i++) {
		// These 3 don't affect the score being stored
		if (i != OPT_SOUND && i != OPT_COUNTDOWN && i != OPT_REV && i != OPT_NSL && i != OPT_SWP) {
			desc = [desc stringByAppendingFormat:@"%d", [[app.optionsArray objectAtIndex:i] boolValue]];
		} else if (i == OPT_NSL) {
			desc = [desc stringByAppendingFormat:@"0"];
		} else if (i != OPT_SWP) { // But just in case I do want them to later, or something
			desc = [desc stringByAppendingFormat:@"1"]; // Pretend sound and countdown are always on
			// (more flexibility for later)
		}
	}
	
	return [defaults floatForKey:[NSString stringWithFormat:@"BestTime%dOpt%@Lvl%d", numProblems, desc, difficulty]];
}


- (void)loadBestTime:(int)numProblems {
	// Workaround for Division UISwitch when app.problemCount isn't set yet
	if (numProblems == 0)
		numProblems = 20;
	
	float fBestTime = [self bestTimeForProblems:numProblems];

	NSString *sBestTime;
	if (fBestTime == 0) {
		sBestTime = NSLocalizedString(@"NONE_YET", @"None Yet");
	} else {
		sBestTime = [NSString stringWithFormat:@"%.4f %@", fBestTime, NSLocalizedString(@"SECONDS", @"seconds")];
	}
	
#ifdef kDefaultPngMode
		sBestTime = @"";
#endif
	
	bestTime.text = [NSString stringWithFormat:@"%@%@", NSLocalizedString(@"BEST_TIME", @"Best Time: "), sBestTime];
}

/* // Replaced by OptionsView
- (IBAction)tapDivision:(UISwitch *)sender {
	DLog(@"tapDivision:%d", [sender isOn]);
	BrainTuner3AppDelegate *app = (BrainTuner3AppDelegate *)[[UIApplication sharedApplication] delegate];
	[self loadBestTime:app.problemCount];
}


- (BOOL)divisionIsOn {
	return [division isOn];
}
*/


- (IBAction)touchDownRepeatLogo:(id)sender {
    
#ifdef LITE_MODE
	NSString *title = @"Brain Tuner Lite";
#else
	NSString *title = @"Brain Tuner Premium";
#endif
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
													message:[NSString stringWithFormat:@"Version %@\nbraintuner.pro.ios@greengar.com\n\nDeveloped by Elliot M. Lee\n\nSpecial thanks to Amy Lee <http://amidot.deviantart.com>,\nSimon Yang, Thuy Truong, and Silvercast Long Nguyen", [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"]]
												   delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
	[alert show];
	[alert release];	
}


#pragma mark New in 1.7


@synthesize difficulty;


- (IBAction)tapLoDifficultyButton {
	if (difficulty > 1) {
		difficulty--;
		difficultyLabel.text = [NSString stringWithFormat:NSLocalizedString(@"DIFFICULTY_LEVEL", @"Difficulty Level: %d"), difficulty];
		hiDifficultyButton.enabled = YES;
		if (difficulty == 1) {
			loDifficultyButton.enabled = NO;
		}
		// save state
		[[NSUserDefaults standardUserDefaults] setInteger:difficulty forKey:@"difficulty"];
	}
	BrainTuner3AppDelegate *app = (BrainTuner3AppDelegate *)[[UIApplication sharedApplication] delegate];
	[self loadBestTime:app.problemCount];
}


- (IBAction)tapHiDifficultyButton {
	if (difficulty < maxDifficulty) {
		difficulty++;
		difficultyLabel.text = [NSString stringWithFormat:NSLocalizedString(@"DIFFICULTY_LEVEL", @"Difficulty Level: %d"), difficulty];
		loDifficultyButton.enabled = YES;
//		if (difficulty == maxDifficulty) {
//			hiDifficultyButton.enabled = NO;
//		}
		// save state
		[[NSUserDefaults standardUserDefaults] setInteger:difficulty forKey:@"difficulty"];
	} else {
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Complete this level fast enough, and the next level will be unlocked!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[alert show];
		[alert release];
	}
	BrainTuner3AppDelegate *app = (BrainTuner3AppDelegate *)[[UIApplication sharedApplication] delegate];
	[self loadBestTime:app.problemCount];
}


- (void)unlockNextLevel {
	maxDifficulty++;
	[[NSUserDefaults standardUserDefaults] setInteger:maxDifficulty forKey:@"maxDifficulty"];
	[[NSUserDefaults standardUserDefaults] synchronize];
	hiDifficultyButton.enabled = YES;
}


- (BOOL)onMaxDifficulty {
	return difficulty == maxDifficulty;
}

#ifndef LITE_MODE

#pragma mark -
#pragma mark View High Scores

#define kNoInternetTag 986
- (IBAction)viewHighScoresTapped {
    // Fix crash with old reachability API
    if (![[Reachability reachabilityForInternetConnection] isReachable]) {
	//if ([[Reachability sharedReachability] internetConnectionStatus] == NO) {
		UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"No Internet Connection Detected"
															message:@"This function requires Internet access. Please check that you're connected to the Internet.\n\nContinue anyway?"
														   delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
		alertView.tag = kNoInternetTag;
		[alertView show];
		[alertView release];
	} else {
		WBWebViewController *webViewController = [[WBWebViewController alloc] initWithNibName:@"WBWebView" bundle:[NSBundle mainBundle]];
		webViewController.mainViewController = self;
		[self presentModalViewController:webViewController animated:YES];
		[webViewController release];
	}	
}

#endif // #ifndef LITE_MODE

- (IBAction)sendEmailTapped {
	
	if ([self bestTimeForProblems:[UIAppDelegate getProblemCountFromSegmentedControl]] <= 2.0) {
		DLog(@"No Record Yet");
        
        // KNOWN ISSUE (Premium):
        //   Player may have a record on some other level,
        //   but this function will only email their score
        //   for the currently- selected level.
        NSString *message = NSLocalizedString(@"PLAY_BRAIN", @"Play Brain Tuner, and then email your best time to your friends!");
        
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"NO_RECORD", @"No Record Yet")
														message:message
													   delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[alert show];
		[alert release];
		return;
	}
	
	// This sample can run on devices running iPhone OS 2.0 or later  
	// The MFMailComposeViewController class is only available in iPhone OS 3.0 or later. 
	// So, we must verify the existence of the above class and provide a workaround for devices running 
	// earlier versions of the iPhone OS. 
	// We display an email composition interface if MFMailComposeViewController exists and the device can send emails.
	// We launch the Mail application on the device, otherwise.
	
	Class mailClass = (NSClassFromString(@"MFMailComposeViewController"));
	if (mailClass != nil)
	{
		DLog(@"mailClass != nil");
		
		// We must always check whether the current device is configured for sending emails
		if ([mailClass canSendMail])
		{
			DLog(@"malClass canSendMail");
			
			[self displayComposerSheet];
		}
		else
		{
			[self launchMailAppOnDevice];
		}
	}
	else
	{
		[self launchMailAppOnDevice];
	}
}

#if LITE_MODE
#   define kBrainTunerURLString @"http://www.greengarstudios.com/redirect/bt1braintuner.php"
#else // Premium
#   define kBrainTunerURLString @"http://www.greengarstudios.com/redirect/bt1probraintuner.php"
#endif

- (NSString *)emailBody {
	// Fill out the email body text
#ifdef LITE_MODE
	NSString *emailBody = [NSString stringWithFormat:@"Can you beat my record?<br><a href=\"%@\">Get Brain Tuner</a>, a free iPhone game, to find out!", kBrainTunerURLString];
#else
	NSString *emailBody = [NSString stringWithFormat:@"Can you beat my record?<br><a href=\"%@\">Get Brain Tuner</a>, a fun iPhone game, to find out!", kBrainTunerURLString];
#endif
	float timeFor20 = [self bestTimeForProblems:20];
	float timeFor60 = [self bestTimeForProblems:60];
	float timeFor100 = [self bestTimeForProblems:100];
	if (timeFor20 > 2.0) {
		emailBody = [emailBody stringByAppendingFormat:@"<br><br>20 Problems: %.4f seconds", timeFor20];
	}
	if (timeFor60 > 6.0) {
		emailBody = [emailBody stringByAppendingFormat:@"<br><br>60 Problems: %.4f seconds", timeFor60];
	}
	if (timeFor100 > 10.0) {
		emailBody = [emailBody stringByAppendingFormat:@"<br><br>100 Problems: %.4f seconds", timeFor100];
	}
	
	emailBody = [NSString stringWithFormat:@"%@<br><br><a href=\"%@\">Brain Tuner</a> is a game for your iPhone, iPod touch, or iPad.", emailBody, kBrainTunerURLString];
	//emailBody = [emailBody stringByAppendingString:@"<br><br>Choose from <a href=\"http://bit.ly/BTLEmail\">Brain Tuner Lite (Free)</a> or <a href=\"http://bit.ly/BTLPremiumEmail\">Brain Tuner Premium</a>."];
	
	return emailBody;
}

#pragma mark -
#pragma mark Compose Mail

// Displays an email composition interface inside the application. Populates all the Mail fields. 
-(void)displayComposerSheet 
{
	[self viewWillDisappear:YES];
	
	MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
	picker.mailComposeDelegate = self;
	
	NSString *subject = [NSString stringWithFormat:@"My Brain Tuner record: %.4f seconds (%d problems)",
						 [self bestTimeForProblems:[UIAppDelegate getProblemCountFromSegmentedControl]],
						 [UIAppDelegate getProblemCountFromSegmentedControl]];
	
	[picker setSubject:subject];
	
	// Set up recipients
	//NSArray *toRecipients = [NSArray arrayWithObject:@"first@example.com"];
#ifdef LITE_MODE
    //ELLIOT: do not change this address
    //        (this is intentionally not a support email)
	NSArray *ccRecipients = [NSArray arrayWithObjects:@"braintuner17@greengar.com", nil];
    [picker setCcRecipients:ccRecipients];
#else
    //ELLIOT: We decided we don't want CCs from Premium users
	//NSArray *ccRecipients = [NSArray arrayWithObjects:@"braintuner.pro.ios@greengar.com", nil];
#endif
	//NSArray *bccRecipients = [NSArray arrayWithObject:@"fourth@example.com"]; 
	
	//[picker setToRecipients:toRecipients];
	
	//[picker setBccRecipients:bccRecipients];
	
	/*
	// Attach an image to the email
	NSString *path = [[NSBundle mainBundle] pathForResource:@"rainy" ofType:@"png"];
    NSData *myData = [NSData dataWithContentsOfFile:path];
	[picker addAttachmentData:myData mimeType:@"image/png" fileName:@"rainy"];
	 */
	
	[picker setMessageBody:[self emailBody] isHTML:YES];
	
	DLog(@"presentModalViewController:picker");
	[UIAppDelegate.rootViewController presentModalViewController:picker animated:YES];
    [picker release];
}


// Dismisses the email composition interface when users tap Cancel or Send. Proceeds to update the message field with the result of the operation.
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error 
{	
	//message.hidden = NO;
	// Notifies users about errors associated with the interface
	switch (result)
	{
		case MFMailComposeResultCancelled:
			//message.text = @"Result: canceled";
			LOG_EVENT(@"MailCancelled");
			break;
		case MFMailComposeResultSaved:
			//message.text = @"Result: saved";
			LOG_EVENT(@"MailSaved");
			break;
		case MFMailComposeResultSent:
			//message.text = @"Result: sent";
			LOG_EVENT(@"MailSent");
			break;
		case MFMailComposeResultFailed:
			//message.text = @"Result: failed";
			LOG_EVENT(@"MailFailed");
			break;
		default:
			//message.text = @"Result: not sent";
			LOG_EVENT(@"MailNotSent");
			break;
	}
	[UIAppDelegate.rootViewController dismissModalViewControllerAnimated:YES];
	
	[self viewWillAppear:YES];
}


#pragma mark -
#pragma mark Workaround

// Launches the Mail application on the device.
-(void)launchMailAppOnDevice
{
	NSString *subject = [NSString stringWithFormat:@"My Brain Tuner record: %.4f seconds (%d problems)",
						 [self bestTimeForProblems:[UIAppDelegate getProblemCountFromSegmentedControl]],
						 [UIAppDelegate getProblemCountFromSegmentedControl]];
#ifdef LITE_MODE
    //ELLIOT: do not change this address
    //        (this is intentionally not a support email)
	NSString *recipients = [NSString stringWithFormat:@"mailto:?cc=braintuner17@greengar.com&subject=%@", subject];
#else
    //ELLIOT: We decided we don't want CCs from Premium users
	//NSString *recipients = [NSString stringWithFormat:@"mailto:?cc=braintuner.pro.ios@greengar.com&subject=%@", subject];
    NSString *recipients = [NSString stringWithFormat:@"mailto:?subject=%@", subject];
#endif
	NSString *body = [NSString stringWithFormat:@"&body=%@", [self emailBody]];
	
	NSString *email = [NSString stringWithFormat:@"%@%@", recipients, body];
	email = [email stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
	
	[[UIApplication sharedApplication] openURL:[NSURL URLWithString:email]];
}


- (IBAction)clickLink:(id)sender {
	[[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.greengar.com/?r=mainview"]];
}

#pragma mark -
#pragma mark Affiliate Link

// Process a LinkShare/TradeDoubler/DGM URL to something iPhone can handle
- (void)openReferralURL:(NSURL *)referralURL {
	self.iTunesURL = referralURL;
	DLog(@"self.iTunesURL = %@", self.iTunesURL);
    NSURLConnection *conn = [[NSURLConnection alloc] initWithRequest:[NSURLRequest requestWithURL:referralURL] delegate:self startImmediately:YES];
    [conn release];
}

// Save the most recent URL in case multiple redirects occur
// "iTunesURL" is an NSURL property in your class declaration
- (NSURLRequest *)connection:(NSURLConnection *)connection willSendRequest:(NSURLRequest *)request redirectResponse:(NSURLResponse *)response {
	DLog(@"self.iTunesURL = %@", self.iTunesURL);
	if (self.iTunesURL) {
		self.iTunesURL = [response URL];
		if (!self.iTunesURL) {
			self.iTunesURL = [request URL];
		}
		DLog(@"self.iTunesURL = %@", self.iTunesURL);
		return request;
	}
	return nil;
}

// No more redirects; use the last URL saved
- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
	DLog(@"self.iTunesURL = %@", self.iTunesURL);
	if (self.iTunesURL)
		[[UIApplication sharedApplication] openURL:self.iTunesURL];
	[loadingAlert dismissWithClickedButtonIndex:[loadingAlert cancelButtonIndex] animated:NO];
	DLog(@"releasing loadingAlert");
	[loadingAlert release], loadingAlert = nil;
}

#define kCouldntReach @"Couldn't reach Greengar.com. Open in Safari?"
- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
	[loadingAlert dismissWithClickedButtonIndex:[loadingAlert cancelButtonIndex] animated:NO];
	DLog(@"releasing loadingAlert");
	[loadingAlert release], loadingAlert = nil;
	
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Unable to Connect" message:kCouldntReach delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
	[alert show];
	[alert release];
}

#define kLoadingTitle @"Loading..."
- (void)showLoadingAlert {
	if (!loadingAlert) { // just being defensive
		// show Loading... alert
		loadingAlert = [[UIAlertView alloc] initWithTitle:kLoadingTitle message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:@"Hide", nil];
		[loadingAlert show];
	}
}

- (IBAction)premiumAdTapped {
	LOG_EVENT(@"premiumAdTapped");
	
	NSString *iTunesLink = @"http://itunes.greengar.com/braintunerinfo.php";
	
	loadingAlert = [[UIAlertView alloc] initWithTitle:kLoadingTitle message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:@"Cancel", nil];
	[loadingAlert show];
	
	[self openReferralURL:[NSURL URLWithString:iTunesLink]];
	
//	[[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://click.linksynergy.com/fs-bin/click?id=73C4v50PGcw&subid=&offerid=146261.1&type=10&tmpid=3909&u1=BTLPremiumAd&RD_PARM1=http%3A%2F%2Fitunes.apple.com%2FWebObjects%2FMZStore.woa%2Fwa%2FviewSoftware%3Fid%3D288322298%2526mt%3D8"]];
}

- (IBAction)clickReset:(id)sender {
	BrainTuner3AppDelegate *app = (BrainTuner3AppDelegate *)[[UIApplication sharedApplication] delegate];
	if (app.problemCount == 0)
		app.problemCount = 20;
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"RESET_BEST", @"Reset Best Time?")
													message:[NSString stringWithFormat:NSLocalizedString(@"SURE_RESET", @"Do you want to reset the best time for %d problems?"), app.problemCount]
												   delegate:self cancelButtonTitle:@"No" otherButtonTitles:NSLocalizedString(@"YES", @"Yes"), nil];
	[alert show];
	[alert release];
}

- (IBAction)clickOptions {
	int optionsTapCount = [[NSUserDefaults standardUserDefaults] integerForKey:@"optionsTapCount"] + 1;
	[[NSUserDefaults standardUserDefaults] setInteger:optionsTapCount forKey:@"optionsTapCount"];
//#ifdef LITE_MODE
//	if (optionsTapCount % 2 == 0) {
//		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"BTP_AVAILABLE", @"Brain Tuner Premium\nNow Available")
//														message:NSLocalizedString(@"SEE_IT", @"Would you like to see it\nin the App Store?")
//													   delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
//		[alert show];
//		[alert release];
//	} else
//#endif
		[self switchToOptionsView];
}


- (void)switchToOptionsView {
	// Load OptionsView
	RootViewController *rootViewController = [(BrainTuner3AppDelegate *)[[UIApplication sharedApplication] delegate] rootViewController];
	[rootViewController toggleOptionsView];
}

- (void)willPresentAlertView:(UIAlertView *)alertView {
    UIAppDelegate.shouldStartPromoAds = NO;
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    UIAppDelegate.shouldStartPromoAds = YES;
    [UIAppDelegate startPromoAds];
#if LITE_MODE
	
	if ([alertView.message isEqualToString:kCouldntReach]) {
		if (buttonIndex == alertView.firstOtherButtonIndex) {
			NSString *urlString = @"http://www.greengar.com/?btlite=1";
			DLog(@"opening %@", urlString);
			[[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlString]];
		}
	} else if ([alertView.title isEqualToString:kLoadingTitle]) {
		if (buttonIndex == alertView.firstOtherButtonIndex) {
			self.iTunesURL = nil;
			[loadingAlert release], loadingAlert = nil;
		}
	} else if ([alertView.title isEqualToString:NSLocalizedString(@"BTP_AVAILABLE", @"Brain Tuner Premium\nNow Available")]) { // BUGFIX
		if (buttonIndex == 0) {
			[self switchToOptionsView];
		} else {
			[[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://phobos.apple.com/WebObjects/MZStore.woa/wa/viewSoftware?id=288322298&mt=8"]];
			//http://gengarstudios.com/btpremium.php?r=bt14
		}
		return;
	}
#else
	if (alertView.tag == kNoInternetTag) {
		if (alertView.firstOtherButtonIndex == buttonIndex) {
			WBWebViewController *webViewController = [[WBWebViewController alloc] initWithNibName:@"WBWebView" bundle:[NSBundle mainBundle]];
			webViewController.mainViewController = self;
			[self presentModalViewController:webViewController animated:YES];
			[webViewController release];
			
			LOG_EVENT(@"OnlineRankings");
		}
		
		// Do nothing otherwise
		return;
	}

#endif // #ifndef LITE_MODE

	if (buttonIndex == 0) {
		// Do nothing
	} else {
		// Reset Best Time for app.problemCount
		//NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
		BrainTuner3AppDelegate *app = (BrainTuner3AppDelegate *)[[UIApplication sharedApplication] delegate];
		
		DLog(@"saveScoreIfBetter:0");
		[app saveScoreIfBetter:0];
		
		[app loadBestTimeForSegmentedControl];
		
		return;
	}
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
	// Return YES for supported orientations
	return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)repositionOnStatusBarHeightChange:(int)newHeight {
    DLog(@"new status bar height %d", newHeight);
    
#if LITE_MODE    
    
    CGRect frame;
    
    // 330, 335, 367
    if (newHeight == 20) {
        frame = startButton.frame;
        frame.origin.y = 367;
        startButton.frame = frame;

        frame = infoDark.frame;
        frame.origin.y = 325;
        infoDark.frame = frame;
        
        frame = greengarLink.frame;
        frame.origin.y = 330;
        greengarLink.frame = frame;

        frame = greengarLink.frame;
        frame.origin.y = 330;
        greengarLink.frame = frame;        
        
        GSAdView * adView = [GSAdView sharedGSAdView];
        adView.frame = CGRectMake(0, 460 - 50 + 2, 320, 50);
    } else {

        frame = startButton.frame;
        frame.origin.y = 347;
        startButton.frame = frame;
        
        frame = infoDark.frame;
        frame.origin.y = 305;
        infoDark.frame = frame;
        
        frame = greengarLink.frame;
        frame.origin.y = 310;
        greengarLink.frame = frame;        

        GSAdView * adView = [GSAdView sharedGSAdView];
        adView.frame = CGRectMake(0, 460 - 20 - 50 + 2, 320, 50);
    }
#endif
}

- (void)didReceiveMemoryWarning {
	//[super didReceiveMemoryWarning]; // Releases the view if it doesn't have a superview
	// Release anything that's not essential, such as cached data
}


- (void)dealloc {
	[startButton release];
	
	[super dealloc];
}


@end
