//
//  RootViewController.m
//  BrainTuner3
//
//  Created by Elliot Lee on 7/13/08.
//  Copyright GreenGar Studios 2008-2009. All rights reserved.
//

#import "RootViewController.h"
#import "MainViewController.h"
#import "FlipsideViewController.h"
#import "FlurryAPI+Extensions.h"
#import "GSAudio.h"

#ifndef LITE_MODE
	#import "ScoreViewController.h"
#endif

#import "OptionsViewController.h"

//#import "AudioServices.h"
#include <AudioToolbox/AudioToolbox.h>
#include <CoreFoundation/CFURL.h>


@implementation RootViewController

@synthesize flipsideNavigationBar;
@synthesize mainViewController;
@synthesize flipsideViewController;

#ifndef LITE_MODE
	@synthesize scoreViewController;
#endif

@synthesize optionsViewController;

@synthesize buttonItem;

- (void)viewDidLoad {
#if LITE_MODE
	DLog(@"MainViewLite");
	MainViewController *viewController = [[MainViewController alloc] initWithNibName:@"MainViewLite" bundle:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showGSAdView) name:kNotificationShowGSAdsView object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(hideGSAdView) name:kNotificationHideGSAdsView object:nil];
#else
	DLog(@"MainView");
	MainViewController *viewController = [[MainViewController alloc] initWithNibName:@"MainView" bundle:nil];
#endif
	self.mainViewController = viewController;
	[viewController release];
	
	//[mainViewController viewWillAppear:NO];
	
	//[self.view insertSubview:mainViewController.view belowSubview:startButton];
	[self.view insertSubview:mainViewController.view atIndex:0];
	
	[mainViewController viewDidAppear:NO];    
}


#ifdef LITE_MODE

- (void) showGSAdView {
    [GSAdView sharedGSAdView].hidden = NO;
}

- (void) hideGSAdView {
    [GSAdView sharedGSAdView].hidden = YES;
}

- (void)initAds {
	myAdView = [[GSAdView sharedGSAdView] retain]; 

	CGFloat adHeight = IS_IPAD ? kiPadAdsMaxHeight : kiPhoneAdsMaxHeight;
	
	myAdView.frame = CGRectMake(0, self.view.frame.size.height - adHeight + 2, self.view.frame.size.width, adHeight);
	myAdView.backgroundColor = [UIColor whiteColor];
	[myAdView start];
    
    [self.view addSubview:myAdView];
}
#endif


- (void)loadFlipsideViewController {
	
	FlipsideViewController *viewController = [[FlipsideViewController alloc] initWithNibName:@"FlipsideView" bundle:nil];
	self.flipsideViewController = viewController;
	[viewController release];
	
	// Set up the navigation bar
	UINavigationBar *aNavigationBar = [[UINavigationBar alloc] initWithFrame:CGRectMake(0.0, 0.0, 320.0, 44.0)];
	aNavigationBar.barStyle = UIBarStyleBlackOpaque;
	self.flipsideNavigationBar = aNavigationBar;
	[aNavigationBar release];
	
	buttonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(confirmCancel)]; //UIBarButtonSystemItemCancel
#ifdef LITE_MODE
	UINavigationItem *navigationItem = [[UINavigationItem alloc] initWithTitle:@"Brain Tuner Lite"];
#else
	UINavigationItem *navigationItem = [[UINavigationItem alloc] initWithTitle:@"Brain Tuner Premium"];
#endif
	navigationItem.rightBarButtonItem = buttonItem;
	[flipsideNavigationBar pushNavigationItem:navigationItem animated:NO];
	[navigationItem release];
	[buttonItem release];
}

#ifndef LITE_MODE

- (void)loadScoreViewController {
	// this calls -init, not -initWithNibName:...
	self.scoreViewController = [ScoreViewController sharedScoreViewController];
	/*
	ScoreViewController *viewController = [[ScoreViewController alloc] initWithNibName:@"ScoreView" bundle:nil];
	self.scoreViewController = viewController;
	[viewController release];
	 */
}

#endif

- (void)toggleViewAnimated:(BOOL)animated {
	[self setButtonToCancel];
	
	//[self playSound:"TECHNOLOGY MULTIMEDIA MENU SCREEN BLIP 01"];
	
	// if the flipsideViewController isn't loaded
	if (flipsideViewController == nil) {
		[self loadFlipsideViewController];
	}
	
	// get a pointer to the 2 views we're working with
	UIView *mainView = mainViewController.view;
	UIView *flipsideView = flipsideViewController.view;
	
#ifndef LITE_MODE
	UIView *scoreView;
	if (scoreViewController != nil) {
		scoreView = scoreViewController.view;
	}
#endif
	
	if (animated) {
		// start animating (be sure to commitAnimations later!)
		[UIView beginAnimations:nil context:NULL];
		[UIView setAnimationDuration:kAnimationDuration];
		[UIView setAnimationTransition:([mainView superview] ? UIViewAnimationTransitionFlipFromRight : UIViewAnimationTransitionFlipFromLeft) forView:self.view cache:YES];
	}
	
	// on mainView
	if ([mainView superview] != nil) {
		[flipsideViewController viewWillAppear:YES];
		[mainViewController viewWillDisappear:YES];
		[mainView removeFromSuperview];
		
		// Start button
		//[startButton removeFromSuperview];
		
		[self.view addSubview:flipsideView];
		[self.view insertSubview:flipsideNavigationBar aboveSubview:flipsideView]; // this pairs up with the one below!
		
		if (animated) {
			[UIView commitAnimations];
		}
		
		[mainViewController viewDidDisappear:YES];
		[flipsideViewController viewDidAppear:YES];
		
		//  on flipsideView
		//} else if([flipsideView superview] != nil) {
		
		// on flipsideView or scoreView
	} else {
		[mainViewController viewWillAppear:YES];
		
		// switching from flipsideView
		if ([flipsideView superview] != nil) {
			[flipsideViewController viewWillDisappear:YES];
			[flipsideView removeFromSuperview];
			[flipsideNavigationBar removeFromSuperview]; // this pairs up with the one above!
		}
		
#ifndef LITE_MODE
		else {
			// switching from scoreView
			[scoreViewController viewWillDisappear:YES];
			[scoreView removeFromSuperview];		
		}
#endif
		
		[self.view addSubview:mainView];
//		
//#ifdef LITE_MODE
//		[myAdView moveToBottom];
//#endif
		
		// Start button
		//[self.view insertSubview:startButton aboveSubview:mainViewController.view];
		
		if (animated) {
			[UIView commitAnimations];
		}
		
		if ([flipsideView superview] != nil) {
			[flipsideViewController viewDidDisappear:YES];
		} else {
			[scoreViewController viewDidDisappear:YES];
		}
		
		[mainViewController viewDidAppear:YES];
	}
}

- (void)toggleView {
	[self toggleViewAnimated:YES];
}


- (void)toggleOptionsView {
	if (self.mainViewController == nil) {
#ifdef LITE_MODE
		MainViewController *viewController = [[MainViewController alloc] initWithNibName:@"MainViewLite" bundle:nil];
#else
		MainViewController *viewController = [[MainViewController alloc] initWithNibName:@"MainView" bundle:nil];
#endif
		self.mainViewController = viewController;
		[viewController release];
	}
	
	if (self.optionsViewController == nil) {
//#ifdef LITE_MODE
//		OptionsViewController *viewController = [[OptionsViewController alloc] initWithNibName:@"OptionsViewLite" bundle:nil];
//#else
		OptionsViewController *viewController = [[OptionsViewController alloc] initWithNibName:@"OptionsView" bundle:nil];
//#endif
		self.optionsViewController = viewController;
		[viewController release];
	}
	
	// Get pointers to the 2 views we're working with
	UIView *mainView = self.mainViewController.view;
	UIView *optionsView = self.optionsViewController.view;
	
	// Note that if [mainView superview], then we're going to hide mainView and show optionsView
	
//#ifdef LITE_MODE
//	[mainView superview] ? [myAdView moveToTop] : [myAdView moveToBottom];
//#endif
	
	// Start animating (be sure to commitAnimations later!)
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationDuration:kAnimationDuration];
	[UIView setAnimationTransition:([mainView superview] ? UIViewAnimationTransitionFlipFromRight : UIViewAnimationTransitionFlipFromLeft) forView:self.view cache:YES];
	
	if ([mainView superview]) {
		[self.optionsViewController viewWillAppear:YES];
		[self.mainViewController viewWillDisappear:YES];
		//[rootViewController.startButton removeFromSuperview];
		[mainView removeFromSuperview];
#ifdef LITE_MODE
		[self.view insertSubview:optionsView belowSubview:myAdView];
#else
		[self.view addSubview:optionsView];
#endif
		
		
#ifdef LITE_MODE
		// TODO: make sure we're using Mobclix (not AdMob)
		//[mcAdView removeFromSuperview];
		//mcAdView.frame = CGRectMake(10, 54, 300, 50);
		
		//[self.view addSubview:flipsideView];
		//[self.view insertSubview:flipsideNavigationBar aboveSubview:flipsideView];
		//[self.view insertSubview:mcAdView aboveSubview:flipsideView];
		//[self.view bringSubviewToFront:mcAdView];
		//[myAdView moveToTop];
		
		//optionsView.
#endif
		
	} else {
		// do the reverse
		[self.mainViewController viewWillAppear:YES];
		[self.optionsViewController viewWillDisappear:YES];
		// start button (if necessary)
		[optionsView removeFromSuperview];
		
#ifdef LITE_MODE
		[self.view insertSubview:mainView belowSubview:myAdView];
#else
		[self.view addSubview:mainView];
#endif
		
		
#ifdef LITE_MODE
		// TODO: make sure we're using Mobclix and not AdMob
		//[self.view addSubview:mcAdView];
		//[mcAdView removeFromSuperview];
		/*
		mcAdView.frame = CGRectMake(10, //(320 - 300) / 2
									410, // 430 - 20 // Mobclix is 2 pixels higher than AdMob
									300,
									50);
		 */
		//[self.view insertSubview:mcAdView aboveSubview:mainViewController.view];
		//[self.view bringSubviewToFront:mcAdView];
		//[myAdView moveToBottom];
#endif
		
	}
	
	[UIView commitAnimations];
	
	if ([optionsView superview]) {
		[self.mainViewController viewDidDisappear:YES];
		[self.optionsViewController viewDidAppear:YES];
	} else {
		[self.optionsViewController viewDidDisappear:YES];
		[self.mainViewController viewDidAppear:YES];
	}
}

#ifndef LITE_MODE

// This is only called when we're on the max difficulty
- (void)toggleViewWithTimePerProblem:(float)timePerProblem {
    
	
	if (timePerProblem <= 0.7 ||
		(timePerProblem <= 1.3 && mainViewController.segmentedControl.selectedSegmentIndex == 1) ||
		(timePerProblem <= 1.4 && mainViewController.segmentedControl.selectedSegmentIndex == 2) ||
		(timePerProblem <= 1.5 && mainViewController.difficulty < 5)) {
		
		BrainTuner3AppDelegate *app = (BrainTuner3AppDelegate *)[[UIApplication sharedApplication] delegate];
		if ([[app.optionsArray objectAtIndex:OPT_SOUND] boolValue] == YES) {
			[GSAudio play:@"trumpets fanfar 2.wav"];
		}
		UIAppDelegate.shouldStartPromoAds = NO;
		[mainViewController unlockNextLevel];
        
		[self toggleView];
		// because you took only %.1f seconds per problem
		UIAlertView *alert =
			[[UIAlertView alloc] initWithTitle:@"Congratulations!"
									   message:@"You've unlocked the next level. Select it now?"
									  delegate:self cancelButtonTitle:@"No"
							 otherButtonTitles:@"Yes", nil];
		[alert show];
		[alert release];
	} else {
        [self toggleView];
    }
}

#endif

- (IBAction)confirmCancel {
	DLog(@"confirmCancel");
	
	// Stop blip1 if necessary
	[GSAudio stop:@"blipblipblipbeep.wav"]; // file extension is required
	
	END_TIMED_EVENT(@"gameStarted");
	
	// Release timer
//	if (flipsideViewController.start != nil) {
//		[[flipsideViewController start] release];
//		flipsideViewController.start = nil;
//	}
	
	// Go to main menu
	//[self toggleView];
	[mainViewController toggleView];
}

- (void)willPresentAlertView:(UIAlertView *)alertView {
    UIAppDelegate.shouldStartPromoAds = NO;
}

#ifndef LITE_MODE

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    UIAppDelegate.shouldStartPromoAds = YES;
    [UIAppDelegate startPromoAds];
	if (buttonIndex == [alertView cancelButtonIndex]) {
		// No
		LOG_EVENT(([NSString stringWithFormat:@"unlocked%d", mainViewController.difficulty+1]));
        //mainViewController.shouldStartPromoAds = YES;
        
	} else {
		// Yes
		LOG_EVENT(([NSString stringWithFormat:@"unlocked%ds", mainViewController.difficulty+1]));
		
		//Select the next level
		[mainViewController tapHiDifficultyButton];
		
		/*
		// Release timer
		if (flipsideViewController.start != nil) {
			[[flipsideViewController start] release];
			flipsideViewController.start = nil;
		}
		// Go to main menu
		[self toggleView];
		 */
	}
}

- (IBAction)showScoreView:(double)score {
	
	if (scoreViewController == nil) {
		[self loadScoreViewController];
	}
	
	UIView *mainView = mainViewController.view;
	UIView *flipsideView = flipsideViewController.view;
	UIView *scoreView = scoreViewController.view;
	
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationDuration:kAnimationDuration];
	[UIView setAnimationTransition:([mainView superview] ? UIViewAnimationTransitionFlipFromRight : UIViewAnimationTransitionFlipFromLeft) forView:self.view cache:YES];
	
	[scoreViewController setFinalTime:score];
	[scoreViewController viewWillAppear:YES];
	[flipsideViewController viewWillDisappear:YES];
	[flipsideView removeFromSuperview];
	[flipsideNavigationBar removeFromSuperview];
	
	[self.view addSubview:scoreView];
	[flipsideViewController viewDidDisappear:YES];
	[scoreViewController viewDidAppear:YES];
	
	// this is important
	[UIView commitAnimations];
}

#endif

- (void)setButtonToDone {
	buttonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(confirmCancel)]; //UIBarButtonSystemItemCancel
	[flipsideNavigationBar.topItem setRightBarButtonItem:buttonItem animated:YES];
	[buttonItem release];
}


- (void)setButtonToCancel {
	buttonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(confirmCancel)];
	[flipsideNavigationBar.topItem setRightBarButtonItem:buttonItem animated:NO];
	[buttonItem release];
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
	// Return YES for supported orientations
	return (interfaceOrientation == UIInterfaceOrientationPortrait);
}


- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning]; // Releases the view if it doesn't have a superview
	// Release anything that's not essential, such as cached data
}


- (void)dealloc {
	
	// Start button
	//[startButton release];
	//[mcAdView release];
#ifdef LITE_MODE
	[myAdView release];
#endif
	
	[flipsideNavigationBar release];
	[mainViewController release];
	[flipsideViewController release];
	[super dealloc];
}

- (void)viewWillDisappear:(BOOL)animated {
#if LITE_MODE
	myAdView.hidden = YES;
#endif
	[super viewWillDisappear:animated];
	// do not call -[MainViewController viewDidDisappear:], as this would cause an infinite loop
}

- (void)viewDidAppear:(BOOL)animated {
	[super viewDidAppear:animated];
	// do not call -[MainViewController viewDidAppear:], as this would cause an infinite loop
#if LITE_MODE
	myAdView.hidden = NO;
    [self.view bringSubviewToFront:myAdView];
#endif
}


@end
