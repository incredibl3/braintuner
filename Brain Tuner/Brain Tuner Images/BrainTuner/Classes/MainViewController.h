//
//  MainViewController.h
//  BrainTuner3
//
//  Created by Elliot Lee on 7/13/08.
//  Copyright GreenGar Studios 2008-2009. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>
#import "GSAdView.h" // #ifdef LITE_MODE is inside this file

@protocol MainViewController
@end

@interface MainViewController : UIViewController <MainViewController, MFMailComposeViewControllerDelegate> {
	IBOutlet UISegmentedControl *segmentedControl;
	IBOutlet UIButton *startButton;
	IBOutlet UILabel *bestTime;
	IBOutlet UILabel *subscript;
	IBOutlet UIButton *infoDark;
	IBOutlet UIButton *greengarLink;
    
	// New in 1.7 (1 March 2009)
	IBOutlet UILabel *speechLabel;
	IBOutlet UIButton *loDifficultyButton;
	IBOutlet UILabel *difficultyLabel;
	IBOutlet UIButton *hiDifficultyButton;
	
	IBOutlet UIButton *premiumAdButton;
	
	NSTimer *shineTimer;
	int difficulty;
	int maxDifficulty;
	
#ifndef LITE_MODE
	BOOL submissionSucceeded;
#endif
	UIAlertView *loadingAlert;
	NSURL *iTunesURL;
    
  @private
#ifndef LITE_MODE
    // Used for Global High Score submission
    NSMutableArray *arrayOfParameters;
#endif
	BOOL shouldSubmitSavedScores;
}

@property (nonatomic, retain) UISegmentedControl *segmentedControl;
@property (nonatomic, retain) UIButton *startButton;
@property (nonatomic) int difficulty;
@property (nonatomic, retain) NSURL *iTunesURL;


- (IBAction)setProblemCount:(UISegmentedControl *)sender;
- (IBAction)toggleView;
- (IBAction)touchDownRepeatLogo:(id)sender;
- (IBAction)clickOptions;

#pragma mark New in 1.7

- (IBAction)clickLink:(id)sender;
- (IBAction)clickReset:(id)sender;

#ifndef LITE_MODE
	- (IBAction)tapLoDifficultyButton;
	- (IBAction)tapHiDifficultyButton;
	- (void)unlockNextLevel;
	- (BOOL)onMaxDifficulty;

	- (IBAction)viewHighScoresTapped;
	- (void)submitSavedScores;
#endif

#pragma mark -

- (void)loadBestTime:(int)numProblems;
- (void)switchToOptionsView;
- (IBAction)sendEmailTapped;
- (void)displayComposerSheet;
- (void)launchMailAppOnDevice;

#ifdef LITE_MODE
	- (IBAction)premiumAdTapped;
	- (void)adHeightDidChange:(CGFloat)adHeight;
#endif

- (void)repositionOnStatusBarHeightChange:(int)newHeight;
@end
