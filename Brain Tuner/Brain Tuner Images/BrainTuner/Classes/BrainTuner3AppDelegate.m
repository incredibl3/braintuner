//
//  BrainTuner3AppDelegate.m
//  BrainTuner3
//
//  Created by Elliot Lee on 7/13/08.
//  Copyright GreenGar Studios 2008-2009. All rights reserved.
//

#import "BrainTuner3AppDelegate.h"
#import "RootViewController.h"
#import "MainViewController.h"
#import "FlipsideViewController.h"
#import "FlurryAPI.h"

#ifndef LITE_MODE
	#import "ScoreViewController.h"
//#else
//	#import "Mobclix.h"
#endif

@implementation BrainTuner3AppDelegate

@synthesize optionsArray;

@synthesize window;
@synthesize rootViewController;
@synthesize problemCount;
@synthesize shouldStartPromoAds;

void uncaughtExceptionHandler(NSException *exception) {
    [FlurryAPI logError:@"Uncaught" message:@"Crash!" exception:exception];
}

- (void)setSession {
	// Set the AmbientSound AudioSessionCategory.
	// This allows music to play in the background.
	// It also respects the mute switch.
	//	UInt32 sessionCategory = kAudioSessionCategory_AmbientSound;
	//	AudioSessionSetProperty(kAudioSessionProperty_AudioCategory, sizeof(sessionCategory), &sessionCategory);
	
	AudioSessionInitialize(NULL, NULL, NULL, self);
//	UInt32 sessionCategory = kAudioSessionCategory_AmbientSound;
//	AudioSessionSetProperty(kAudioSessionProperty_AudioCategory, sizeof(sessionCategory), &sessionCategory);
	
	
	
	UInt32 isPlaying;
    UInt32 propertySize = sizeof(isPlaying);
    
    // Analyze: Value stored to 'status' is never read
    //OSStatus status;
	
    // check to see if their iPod music is playing
    //status = 
    AudioSessionGetProperty(kAudioSessionProperty_OtherAudioIsPlaying, &propertySize, &isPlaying);
	
    // set the session category accordingly
    if(!isPlaying) {
		DLog(@"...SoloAmbientSound");
		UInt32 sessionCategory = kAudioSessionCategory_SoloAmbientSound;
		AudioSessionSetProperty(kAudioSessionProperty_AudioCategory, sizeof(sessionCategory), &sessionCategory);
    }else{
		DLog(@"...AmbientSound");
		UInt32 sessionCategory = kAudioSessionCategory_AmbientSound;
		AudioSessionSetProperty(kAudioSessionProperty_AudioCategory, sizeof(sessionCategory), &sessionCategory);
    }
	
	AudioSessionSetActive(true);
}

- (void)startPromoAds {
    if(self.shouldStartPromoAds)
        [_promoAds startPromoAds:self.rootViewController];    
}


- (void)applicationDidFinishLaunching:(UIApplication *)application {
	//[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
	
	NSSetUncaughtExceptionHandler(&uncaughtExceptionHandler);
	
#if DEBUG
	DLog(@"DEBUG");
//#else
//	NSLog(@"RELEASE");
#endif
	
#ifdef LITE_MODE
	NSString *applicationCode = @"edfd1da6097d14ef513f0839e6c0eef4";
#else
	NSString *applicationCode = @"65b3f2ef5dd7b96b4abd4b2d45f9e336";
#endif
	
	[FlurryAPI startSession:applicationCode];
	
	// when we know the user's location, call:
	// [FlurryAPI setLocation:YOUR_UPDATED_CLLOCATION];
	
	/*** Begin Mobclix Analytics ***/
	// TODO: Use Mobclix Analytics in Premium too?
//#ifdef LITE_MODE
//	[Mobclix start];
//#endif
	/*** End Mobclix Analytics ***/
	
	[window addSubview:[rootViewController view]];
	[window makeKeyAndVisible];
	
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
		
	optionsArray = [[NSMutableArray arrayWithCapacity:OPTION_COUNT] retain];
	NSNumber *number;
	BOOL value;
	for (int i = 0; i < OPTION_COUNT; i++) {
		#ifdef LITE_MODE
			// Lite isn't allowed to have options >= OPT_BIG anymore, so set them to NO
			if (i >= OPT_BIG) {
				value = NO;
				
				//DLog(@"[%d] = %d (forced by LITE_MODE)", i, value);
				
				[optionsArray addObject:[NSNumber numberWithBool:value]];
				continue;
			}
		#endif
		number = [defaults objectForKey:[NSString stringWithFormat:@"Option%d", i]];
		if (number == nil) {
			
			//DLog(@"number is nil");
			
			value = YES;
			// These are the ones that default to NO
			// We only review problems when they get 1+ wrong, so it's OK
			if (i >= OPT_DIV/* && i != OPT_SFX && i != OPT_REV*/) {
				value = NO;
			}
			[optionsArray addObject:[NSNumber numberWithBool:value]]; // pick some default here
		} else {
			
            // OPT_BIG has been removed, so set it to NO
			if (i == OPT_BIG) {
				value = NO;
			} else {
                value = [number boolValue];
            }
            
			DLog(@"[%d] = %d", i, value);
			[optionsArray addObject:[NSNumber numberWithBool:value]];
		}
	}
	
	//DLog(@"options = %@", [optionsArray description]);
	
	// Try to transfer over the best score from the old version
	// These ignore the options... oh well
	float fBestTime = [defaults floatForKey:@"BestTime100"];
	if (fBestTime > 0) {
		// See getProblemCountFromSegmentedControl
		//[[rootViewController mainViewController] segmentedControl].selectedSegmentIndex=2;
		[self saveScoreIfBetter:fBestTime forProblemCount:100];
		[defaults setFloat:0 forKey:@"BestTime100"];
	}
	fBestTime = [defaults floatForKey:@"BestTime60"];
	if (fBestTime > 0) {
		//[[rootViewController mainViewController] segmentedControl].selectedSegmentIndex=1;
		[self saveScoreIfBetter:fBestTime forProblemCount:60];
		[defaults setFloat:0 forKey:@"BestTime60"];
	}
	fBestTime = [defaults floatForKey:@"BestTime20"];
	if (fBestTime > 0) {
		//[[rootViewController mainViewController] segmentedControl].selectedSegmentIndex=0;
		[self saveScoreIfBetter:fBestTime forProblemCount:20];
		[defaults setFloat:0 forKey:@"BestTime20"];
	}
	
	//[[rootViewController mainViewController] segmentedControl].selectedSegmentIndex=0;
	[self loadBestTimeForSegmentedControl];
	
#ifdef LITE_MODE
	#ifndef kDefaultPngMode
			[NSTimer scheduledTimerWithTimeInterval:0.0 target:rootViewController selector:@selector(initAds) userInfo:nil repeats:NO];
	#endif
#endif
	
	[self setSession];
    
    //KONG: promo Ads
    _promoAds = [[PromoAds alloc] init];
    shouldStartPromoAds = YES;
    [self startPromoAds];

}


- (void)loadBestTimeForSegmentedControl {
	int problemCountSelected = [self getProblemCountFromSegmentedControl];
	
	//DLog(@"loadBestTime from BrainTuner3AppDelegate");
	
	[[rootViewController mainViewController] loadBestTime:problemCountSelected];
}


- (int)getProblemCountFromSegmentedControl {
	int index = [[[rootViewController mainViewController] segmentedControl] selectedSegmentIndex];
	if (index == 0) {
		return 20;
	}
	else if (index == 1) {
		return 60;
	}
	else {
		return 100;
	}
}


- (NSString *)saveScoreIfBetter:(float)newScore forProblemCount:(int)problems {
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	
	//BrainTuner3AppDelegate *app = (BrainTuner3AppDelegate *)[[UIApplication sharedApplication] delegate];
	//RootViewController *rootViewController = (RootViewController *)[app rootViewController];
	//MainViewController *mainViewController = (MainViewController *)[rootViewController mainViewController];
	//DLog(@"mainViewController bestTime = %d", [mainViewController tapDivision:[[UISwitch alloc] isOn]]);
	//BOOL division = [(UISwitch *)[mainViewController division] isOn];
	
	// Modified from the one used in ScoreViewController.m -- Pay attention!
	NSString *desc = @"";
	for (int i = 0; i < OPTION_COUNT; i++) {
		if (i != OPT_SOUND && i != OPT_COUNTDOWN && i != OPT_REV && i != OPT_NSL && i != OPT_SWP) { // These two don't affect the score being stored
			// !!! boolValue !!!
			desc = [desc stringByAppendingFormat:@"%d", [[optionsArray objectAtIndex:i] boolValue]];
		} else if (i == OPT_NSL) {
			// This one defaults to NO, so pretend it's always off
			desc = [desc stringByAppendingFormat:@"0"];
		} else if (i != OPT_SWP) { // Don't change |desc| for swapping the buttons
			// But just in case I do want them to later, or something
			desc = [desc stringByAppendingFormat:@"1"]; // Pretend sound and countdown are always on
			// (more flexibility for later)
		}
	}
	
	//if (![desc isEqualToString:@"11111100000"]) {
	// Not default options
	//[[app.optionsArray objectAtIndex:OPT_DIV] boolValue] == YES) {
	
	// TODO: What about Lite?
	
	float fBestTime = [defaults floatForKey:[NSString stringWithFormat:@"BestTime%dOpt%@Lvl%d", problems, desc, rootViewController.mainViewController.difficulty]];
	
	// newScore == 0 is for the special case where we want to reset the score!
	if (newScore < fBestTime || fBestTime == 0 || newScore == 0) {
		//DLog(@"setFloat:%f forKey:%@", newScore, [NSString stringWithFormat:@"BestTime%dOpt%@", problems, desc]);
		[defaults setFloat:newScore forKey:[NSString stringWithFormat:@"BestTime%dOpt%@Lvl%d", problems, desc, rootViewController.mainViewController.difficulty]];
		return NSLocalizedString(@"NEW_RECORD",@"\n(New Record!)");
	}
	return @"";	
}


- (NSString *)saveScoreIfBetter:(float)newScore {
	DLog(@"saveScoreIfBetter:%f", newScore);
	
	// potentially save high score if it's not negative
	int problems = [self getProblemCountFromSegmentedControl];
	
	return [self saveScoreIfBetter:newScore forProblemCount:problems];
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
	/*
     Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
     If your application supports background execution, called instead of applicationWillTerminate: when the user quits.
     */
	
	[[NSUserDefaults standardUserDefaults] synchronize];
}

// In iOS 4.0 and later, this method is called as part of the transition
// from the background to the inactive state. You can use this method to
// undo many of the changes you made to your application upon entering
// the background. The call to this method is invariably followed by a
// call to the applicationDidBecomeActive: method, which then moves the
// application from the inactive to the active state.
- (void)applicationWillEnterForeground:(UIApplication *)application {
	[self setSession];
    
    [_promoAds restartPromoAds];
    
    // Don't startPromoAds if the player is in a game.
    //   mainViewController is the main gameplay screen.
    if (self.rootViewController.mainViewController.view.superview) {
        [self startPromoAds];
    }
}

- (void)applicationWillTerminate:(UIApplication *)application {
	// Switch (option) state is saved at the time it is changed
	
	[[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)application:(UIApplication *)application willChangeStatusBarFrame:(CGRect)newStatusBarFrame {
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
    [[rootViewController mainViewController] repositionOnStatusBarHeightChange:newStatusBarFrame.size.height];    
}

- (void)application:(UIApplication *)application didChangeStatusBarFrame:(CGRect)oldStatusBarFrame {
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
//    CGSize s2 = navBar.bounds.size;
//    [rootViewController.view setFrame:CGRectMake(0, [navigationController.view bounds].size.height-s2.height, s2.width, s2.height)];
//    [rootViewController.view setFrame:CGRectMake(0, 20, 320, 460)];

}

- (void)dealloc {
	[rootViewController release];
	[window release];
	[super dealloc];
}

@end
