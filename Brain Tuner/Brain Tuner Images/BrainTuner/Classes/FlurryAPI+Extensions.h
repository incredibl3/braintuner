/*
 *  FlurryAPI+Extensions.h
 *  Flashlight
 *
 *  Created by Elliot on 6/30/10.
 *  Copyright 2010 GreenGar Studios. All rights reserved.
 *
 */

#import "FlurryAPI.h"

//	[FlurryAPI logEvent:@"EVENT_NAME" timed:YES];
//	Use this version of logEvent to start timed event. 
//
//	[FlurryAPI logEvent:@"EVENT_NAME" withParameters:YOUR_NSDictionary timed:YES];
//	Use this version of logEvent to start timed event with event parameters. 
//
//	[FlurryAPI endTimedEvent:@"EVENT_NAME"];

#define START_TIMED_EVENT(x) ([FlurryAPI logEvent:x timed:YES])

#define END_TIMED_EVENT(x) ([FlurryAPI endTimedEvent:x withParameters:nil]) // updated for v2.7 (withParameters:)

#define LOG_EVENT(x) ([FlurryAPI logEvent:x])

#define LOG_EVENT_PARAMS(x, y) ([FlurryAPI logEvent:x withParameters:y])
