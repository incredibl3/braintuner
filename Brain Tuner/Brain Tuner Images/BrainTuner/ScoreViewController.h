//
//  ScoreViewController.h
//  BrainTuner3
//
//  Created by Elliot Lee on 8/12/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "SubmitScoreViewController.h"

// Moved to AppDelegate
//#define submitURL @""
//#define kSubmitUrl @"http://gengarstudios.com/scores/submitscorenative.php"

#define kCancelTag 222


@protocol ScoreViewController
@end

@interface ScoreViewController : UIViewController <ScoreViewController, UIWebViewDelegate> {
    IBOutlet UIWebView *webView;
	IBOutlet UIButton *doneButton;
	double finalTime;
	UIViewController *submitScoreViewController;
	
	NSString *_playerName;
	NSString *_playerEmail;
	
	NSDictionary *params;
	BOOL submissionSucceeded;
	//BOOL submittingNewScore;
}

@property (nonatomic, retain) NSString *playerName;
@property (nonatomic, retain) NSString *playerEmail;
//@property BOOL submittingNewScore;

+ (ScoreViewController *)sharedScoreViewController;
//- (void)postToURL:(NSString *)URLString withParameters:(NSDictionary *)params target:(NSObject *)target selector:(SEL)selector;
- (void)setFinalTime:(double)score;
- (IBAction)tapRefresh;
- (IBAction)tapDone:(id)sender;
- (void)submitWithName:(NSString *)playerName email:(NSString *)playerEmail;
- (BOOL) connectedToNetwork;

@end
