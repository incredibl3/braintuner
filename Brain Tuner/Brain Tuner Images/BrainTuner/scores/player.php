<?php

$mtime = microtime(); 
$mtime = explode(" ",$mtime); 
$mtime = $mtime[1] + $mtime[0]; 
$starttime = $mtime; 

$MIN_SCORE = 5;
if (!$_GET['problems']) {
	$problems = 20;
} else {
	$problems = strip_tags($_GET['problems']);
}

$div = strip_tags($_GET['d']);

$dbhost = 'mysql.greengarstudios.com';
$dbuser = 'greengarstudios';
$dbpass = 'pokeGr33n';

$conn = @mysql_connect($dbhost, $dbuser, $dbpass);
if (!$conn)
	die('<font face="Arial" size="3">Sorry, an error occurred. Click the <b>Reload</b> button above.<br><br>Error connecting to MySQL (152).</a>');

$dbname = 'greengarstudios';
mysql_select_db($dbname);

$time_restriction = '';

//if (strpos($_SERVER['HTTP_USER_AGENT'], 'Mobile') &&
//	strpos($_SERVER['HTTP_USER_AGENT'], 'iPhone')) {
		// Mobile Safari version
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Brain Tuner High Scores</title>
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <meta name="viewport" content="width=device-width, user-scalable=no" />
  <meta name = "format-detection" content = "telephone=no">
  <link rel = "stylesheet" href = "EdgeToEdgeNative.css" />
</head>
<body>
<div class="toolbar1">
<?php
$name = $_GET['name'];
?>
<input type=button value="Back" onClick="history.go(-1)" style="margin:0 0 0 10px;font-size:18px">
<h1 style="text-align:center;font-family:Helvetica;padding-top:0px;margin-top:0px"><?php echo strip_tags($name); ?></h1>
<?php


// ---- Get the # of score submissions ---- //
$query = sprintf (
  "SELECT COUNT(*) FROM scores WHERE name = '%s' LIMIT 1",
  mysql_real_escape_string($name)
  );
$result = mysql_query($query);
if (!$result) {
    die('Invalid query: ' . mysql_error());
}
//$num_rows = mysql_num_rows ($result);
$row = mysql_fetch_array($result);
$num_rows = intval($row[0]);
// ---- ---- //


// Make sure to get email!!!
$query = sprintf (
  "SELECT score, email, problems, date, level FROM scores WHERE name = '%s' ".
  $time_restriction." ORDER BY date DESC LIMIT 50", //score ASC
  mysql_real_escape_string($name)
  );

//echo $query;

$result = mysql_query($query);

if (!$result) {
    die('Invalid query: ' . mysql_error());
}

echo '<h2 style="text-align:center;font-family:Helvetica">'.$num_rows.' scores submitted';
if ($num_rows > 50) {
  echo '<br><small>Most recent 50 displayed</small>';
}
echo '</h2>';

?>

</div>
	<ul>
	<?php
		$rank = 1;
		$entrycount = array();
		$have_scrolled = false;
		$last_named_rank = 0;
		while (($row = mysql_fetch_assoc($result))) {
			//$name = strip_tags($row['name']);
			$shortname = substr($name,0,14);
			if (strlen($name) > 14)
				$shortname .= '...';
			$entrycount[$shortname]++;
			$entrycount[$row['email']]++;
			//echo intval($row['score']).'<br>';
			//echo $shortname.'<br>';
			if (/*$entrycount[$shortname] <= 3 && $entrycount[$row['email']] <= 3 && */intval($row['score']) > $MIN_SCORE) {
				/*
				if ($row['name'] == $_POST['name']) {
					$last_named_rank = $rank;
				}
				*/
				if (/*$row['name'] == $_POST['name'] && */($row['score'] == $_GET['score'] || number_format(strip_tags($row['score']),4) == $_GET['score'])) {
					//echo '<script>window.scroll(0,'.((45 * $rank) + 149).');</script>';
					echo '<a name="myscore"></a>';
					$have_scrolled = true;
					echo '<li class="hilite">';
				} else {
					echo '<li style="height:65px; line-height:23px">'; //57 //61
				}
				echo date('M j, y, g:ia',strtotime($row['date'])).'<span class="secondary">'.number_format(strip_tags($row['score']),4).'</span><br>';
				echo '<span style="font-weight:normal">Problems: </span>'.intval($row['problems']).'<span style="font-weight:normal"> / Level: </span>'.intval($row['level']).'<br>'; //<span class="secondary" style="font-weight:bold">
echo '<span style="font-weight:normal">';
$optionArray = preg_split('//', $row['problems'], -1, PREG_SPLIT_NO_EMPTY);
$addComma = false;
foreach($optionArray as $char) {
	if ($addComma == true) {
		// This works because the last character in $problem
		// will always be one of the cases below.
		// Later we may want to generalize this better.
		echo ', ';
	}
	$addComma = true;
	switch ($char) {
		case 'a':
			echo 'Add';
			break;
		case 's':
			echo 'Sub';
			break;
		case 'm':
			echo 'Mul';
			break;
		case 'd':
			echo 'Div';
			break;
		case 'b':
			echo 'Big';
			break;
		case 'S':
			echo 'Sml';
			break;
		case 'p':
			echo 'Pos';
			break;
		case 'n':
			echo 'Neg';
			break;
		default:
			$addComma = false;
			break;
	}
}
echo '</span>';
				echo '</li>';
				$rank++;
			}
		}
		/*
		if (!$have_scrolled && $last_named_rank > 0) {
			echo '<script>window.scroll(0,'.((45 * $last_named_rank) + 149).');</script>';
		}
		*/
		?>
	</ul>
		<div style="padding:10px;margin-bottom:100px"><input type=button value="Back" onClick="history.go(-1)" style="font-size:18px"><br><small>
		&copy; 2008 <a href="http://www.gengarstudios.com">Gengar Studios &middot; www.gengarstudios.com</a><br>
<?php
	mysql_close($conn);
	
	$mtime = microtime(); 
	$mtime = explode(" ",$mtime); 
	$mtime = $mtime[1] + $mtime[0]; 
	$endtime = $mtime; 
	$totaltime = ($endtime - $starttime); 
	echo 'Player page generated in '.round($totaltime, 3)." seconds";
?>
</small></div>
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-9214979-1");
pageTracker._trackPageview();
} catch(err) {}</script>

</body>
</html>