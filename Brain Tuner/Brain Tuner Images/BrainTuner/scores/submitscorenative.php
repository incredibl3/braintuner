<?php

$mtime = microtime(); 
$mtime = explode(" ",$mtime); 
$mtime = $mtime[1] + $mtime[0]; 
$starttime = $mtime; 

include_once 'localization.php';

if ($_POST['problems'] && $_GET['problems']) {
	die('Error occurred. Email elliot@greengarstudios.com');
}

if ($_POST['problems']) {
	$kProblems = $_POST['problems'];
} else if ($_GET['problems']) {
	$kProblems = $_GET['problems'];
} else {
	die('# of Problems missing');
}

if ($_POST['level']) {
	$level = intval($_POST['level']);
} else if ($_GET['level']) {
	$level = intval($_GET['level']);
} else {
	die("Level missing");
}

$problems = $kProblems;
//$version = intval($_POST['version']);

//if ($version == 2) {

$kD = $_POST['d'] ? $_POST['d'] : $_GET['d'];

	$kD .= ""; // Make it a string
	
	// Append asmdbpnS after the problem count
	// Add, Subtract, Multiply, Divide, Big, Positive, Negative, Small
	/*
	Indexes into OptionArray (d)
	
	#define OPT_SOUND 1
	#define OPT_COUNTDOWN OPT_SOUND + 1
	#define OPT_ADD OPT_SOUND + 2 -> 3
	#define OPT_SUB OPT_SOUND + 3 -> 4
	#define OPT_MUL OPT_SOUND + 4 -> 5
	#define OPT_DIV OPT_SOUND + 5 -> 6
	#define OPT_REV OPT_SOUND + 6
	
	#define OPT_BIG OPT_SOUND + 7 -> 8
	#define OPT_POS OPT_SOUND + 8 -> 9
	#define OPT_NEG OPT_SOUND + 9 -> 10
	#define OPT_SFX OPT_SOUND + 10
	#define OPT_SML OPT_SOUND + 11 -> 12
	*/
	if ($kD[3] == '1') {
		$problems .= 'a';
	}
	if ($kD[4] == '1') {
		$problems .= 's';
	}
	if ($kD[5] == '1') {
		$problems .= 'm';
	}
	if ($kD[6] == '1') {
		$problems .= 'd';
		//echo $problems;
	}
	if ($kD[8] == '1') {
		$problems .= 'b';
	}
	if ($kD[9] == '1') {
		$problems .= 'p';
	}
	if ($kD[10] == '1') {
		$problems .= 'n';
	}
	if ($kD[12] == '1') {
		$problems .= 'S';
	}
//}

// New score submission
if ($_POST['name'] && $_POST['score']) {
	$myhash = md5($_POST['score'] . $kProblems .
		$_POST['deviceuid'] . '1' . '6uphEGev' . $level);
	//echo $_POST['hash'] . '<br>' . $myhash . '<br>';
	if ($_POST['hash'] != $myhash) {
		die ('An error occurred. Email <a href="mailto:elliot@gengarstudios.com">elliot@gengarstudios.com</a>');
	}
	
	// save name and email in a cookie
	//setcookie  ( 'name', strip_tags($_POST['name']), time()+31536000*3, '/', '.gengarstudios.com');
	//setcookie  ( 'email', strip_tags($_POST['email']), time()+31536000*3, '/', '.gengarstudios.com');
	// don't need to anymore, but it doesn't hurt...
	
} // $_POST['name'] && $_POST['score']

$dbhost = 'mysql.greengarstudios.com';
$dbuser = 'greengarstudios';
$dbpass = 'pokeGr33n';

$conn = @mysql_connect($dbhost, $dbuser, $dbpass);
if (!$conn)
	die('<font face="Arial" size="3">Sorry, an error occurred. Click the <b>Reload</b> button above.<br><br>Error connecting to MySQL (152).</a>');

$dbname = 'greengarstudios';
mysql_select_db($dbname);

$kHours = $_GET['hours'];

if (!$kHours && $_POST['name'] && $_POST['score']) {

	// make sure name and time don't already exist
	// AND problems = '%s'
	// Not necessary - no realistic chance to get the same score with different # of problems
	// Even if they did... does it matter? This score is already recorded
	$query = sprintf("SELECT name, score FROM scores WHERE name='%s' AND score='%s'",
						mysql_real_escape_string($_POST['name']),
						mysql_real_escape_string($_POST['score'])
					);
	
	$result = mysql_query($query);
	
	if (!$result) {
		die('Invalid query: ' . mysql_error());
	}
	
	// ---------------------------- //
	
	// ---------------------------- //
	
	if (mysql_num_rows($result) == 0) {
		// ok, submit it
		
		$query = sprintf("INSERT INTO scores (name, email, score, problems, deviceuid, ip, date, hash, version, problemarray, d, level) VALUES ('%s', '%s', '%s', '%s', '%s', INET_ATON('".$_SERVER['REMOTE_ADDR']."'), NOW(), '%s', '%s', '%s', '%s' , '%s')",
			mysql_real_escape_string($_POST['name']),
			mysql_real_escape_string($_POST['email']),
			mysql_real_escape_string($_POST['score']),
			mysql_real_escape_string($problems),
			mysql_real_escape_string($_POST['deviceuid']),
			mysql_real_escape_string($_POST['hash']),
			mysql_real_escape_string('3'), // hard-coded version 3 to show it's native
			mysql_real_escape_string($_POST['problemarray']),
			mysql_real_escape_string($kD),
			mysql_real_escape_string($level)
			);
		
		$result = mysql_query($query);
		
		if (!$result) {
			die('Invalid query: ' . mysql_error());
		}
	
	}

} // $kHours

if ($kHours != 'all') {
	if (intval($kHours) > 0)
		$hours = mysql_real_escape_string($kHours);
	else
		$hours = 24;
	$time_restriction = 'AND date > DATE_SUB(NOW(), INTERVAL '.$hours.' HOUR) ';
} else {
	$time_restriction = '';
}

//if ($version == 2) {

// SELECT COUNTRY_NAME FROM <TableName> WHERE IP_FROM <= IP Number and IP_TO >= IP Number
/*
if ($_GET['leftjoin']) {
	$select_query = 'SELECT scores.name, scores.score, scores.email, countries.COUNTRY_CODE2 FROM scores LEFT JOIN countries ON (countries.IP_FROM <= scores.ip AND countries.IP_TO >= scores.ip) WHERE ';
} else {
	$select_query = 'SELECT scores.name, scores.score, scores.email, countries.COUNTRY_CODE2 FROM scores, countries WHERE (countries.IP_FROM <= scores.ip AND countries.IP_TO >= scores.ip) AND ';
}
*/

$select_query = 'SELECT id, name, score, email, ip, cc FROM scores WHERE ';

$order_by = "ORDER BY score ASC LIMIT 300";

/*
if (substr($problems, -3, 3) === 'asm') {
	$first2chars = substr($problems, 0, 2);
	if ($first2chars == 20 || $first2chars == 60) {
		$old_num_problems = $first2chars;
	} else {
		$old_num_problems = 100;
	}
	$query = sprintf($select_query."(problems = '%s' OR (problems = '%s' AND d = '')) ".
				$time_restriction.$order_by,
				$problems,
				$old_num_problems
			);
} else if (substr($problems, -4, 4) === 'asmd') {
	$first2chars = substr($problems, 0, 2);
	if ($first2chars == 20 || $first2chars == 60) {
		$old_num_problems = $first2chars;
	} else {
		$old_num_problems = 100;
	}
	$query = sprintf(
				$select_query."(problems = '%s' OR (problems = '%s' AND d = 'div')) ".
				$time_restriction.$order_by,
				$problems,
				$old_num_problems
			);
} else {
*/
	$query = sprintf($select_query."(problems = '%s' AND level = '%s')".
						$time_restriction.$order_by,
						$problems, $level
					);
//}

/*
} else {
	$query = sprintf("SELECT name, score, email FROM scores WHERE problems = '%s' AND d = '%s' ".
						$time_restriction."ORDER BY score ASC LIMIT 200",
						mysql_real_escape_string($kProblems),
						mysql_real_escape_string($kD)
					);
}
*/

// GROUP BY name

$result = mysql_query($query);

if (!$result) {
    die('Invalid query: ' . mysql_error() . '<br>Query: ' . $query);
}

//echo $query;

//	<title>iPhone Design Patterns-Edge to Edge </title>
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<?php /*	<meta name="viewport" content="width=device-width, user-scalable=no" /> */ ?>
  <meta id="viewport" name="viewport" content="width=320; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" />
	<link rel = "stylesheet" href = "EdgeToEdgeNative.css" />
<?php /*	<base href="http://gengarstudios.com/scores/">
<style>
a {
  color:black;
}
</style>
*/ ?>
</head>
<body>
<script src="smoothscroll.js" type="text/javascript"></script>
<div class="toolbar1">
<!--<h1 style="text-align:center">Global High Scores</h1>-->
<h2 style="text-align:center"><?php echo $kProblems; ?><?=LocalizedString(' Problems')?><?php if ($kD == 'div') { echo ' (with Division)'; } ?></h2>
<p style="text-align:center">Level: <?=$level?></p>
<p style="text-align:center"><?=LocalizedString('Options: ')?><?php
$optionArray = preg_split('//', $problems, -1, PREG_SPLIT_NO_EMPTY);
$addComma = false;
foreach($optionArray as $char) {
	if ($addComma == true) {
		// This works because the last character in $problem
		// will always be one of the cases below.
		// Later we may want to generalize this better.
		echo ', ';
	}
	$addComma = true;
	switch ($char) {
		case 'a':
			echo 'Add';
			break;
		case 's':
			echo 'Sub';
			break;
		case 'm':
			echo 'Mul';
			break;
		case 'd':
			echo 'Div';
			break;
		case 'b':
			echo 'Big';
			break;
		case 'S':
			echo 'Sml';
			break;
		case 'p':
			echo 'Pos';
			break;
		case 'n':
			echo 'Neg';
			break;
		default:
			$addComma = false;
			break;
	}
}

$base_url = 'http://greengarstudios.com/scores/';

$thisFilename = $base_url.'submitscorenative.php';

?></p>

<?php

$other_params = '&problems='.intval($kProblems).'&d='.$kD.'&level='.$level;

if ($kHours != 'all') { ?>
	<p style="text-align:center"><?=LocalizedString('Last')?> <?php echo $hours;
	if ($hours == 24) {
		$other_hours = 48;
	} else {
		$other_hours = 24;
	}
	
	$other_hours_URL = $thisFilename.'?hours='.$other_hours.$other_params;//.'&version='.$version;
	
	$all_time_URL = $thisFilename.'?hours=all&problems='.intval($kProblems).'&d='.$kD.$other_params;//.'&version='.$version;
	
	?> <?=LocalizedString('Hours')?> (See 
	<a class="scorelink" href="<?=$other_hours_URL?>"><?=LocalizedString('Last')?> <?=$other_hours?> <?=LocalizedString('Hours')?></a>, 
	<a class="scorelink" href="<?=$all_time_URL?>">All Time</a>)</p>
<?php } else {

$twelve_URL = $thisFilename.'?hours=24&problems='.intval($kProblems).'&d='.$kD.$other_params;//.'&version='.$version;

$twentyfour_URL = $thisFilename.'?hours=48&problems='.intval($kProblems).'&d='.$kD.$other_params;//.'&version='.$version;

?>
<p style="text-align:center">All Time (See <?=LocalizedString('Last')?> <a style="color:white;text-decoration:underline" href="<?=$twelve_URL?>">24</a>, <a style="color:white;text-decoration:underline" href="<?=$twentyfour_URL?>">48</a> <?=LocalizedString('Hours')?>)</p>
<?php } ?>
<?php if ($_POST['score']) { ?>
<p style="text-align:center"><a style="color:white;text-decoration:underline" href="#myscore"><?=LocalizedString('Jump to your score')?></a></p>
<?php } ?>
</div>
	<ul>
	<?php
		$rank = 1;
		$entrycount = array();
		$have_scrolled = false;
		$last_named_rank = 0;
		$first = true;
		while (($row = mysql_fetch_assoc($result))) {
			$name = strip_tags($row['name']);
			$shortname = substr($name,0,14);
			if (strlen($name) > 14)
				$shortname .= '...';
			$entrycount[$shortname]++;
			$entrycount[$row['email']]++;
			// I believe 6 seconds is the fastest humanly possible time, but let's say 5 just in case
			if ($entrycount[$shortname] <= 3 && $entrycount[$row['email']] <= 3 && $row['score'] > 5) {
				/*
				if ($row['name'] == $_POST['name']) {
					$last_named_rank = $rank;
				}
				*/
				if (/*$row['name'] == $_POST['name'] && */($row['score'] == $_POST['score'] || number_format(strip_tags($row['score']),4) == $_POST['score'])) {
					//echo '<script>window.scroll(0,'.((45 * $rank) + 149).');</script>';
					echo '<a name="myscore"></a>';
					$have_scrolled = true;
					echo '<li class="hilite">';
				} else {
					echo '<li>';
				}
				
				if (!$row['cc']) {
					$c_query = 'SELECT COUNTRY_CODE2 FROM countries WHERE (countries.IP_FROM <= '.$row['ip'].' AND countries.IP_TO >= '.$row['ip'].') LIMIT 1';
					$c_result = mysql_query($c_query);
					if (!$c_result)
						die('Error:'.mysql_error().'<br>Query:'.$c_query);
						
					$c_row = mysql_fetch_assoc($c_result);
					$row['cc'] = strtolower($c_row['COUNTRY_CODE2']);
					//echo '<br>country code: '.strtolower($c_row['COUNTRY_CODE2']).'<br>';
					$u_query = 'UPDATE scores SET cc = \''.$row['cc'].'\' WHERE id = '.$row['id'].' LIMIT 1';
					$u_result = mysql_query($u_query);
					if (!$u_result)
						die('Error:'.mysql_error().'<br>Query:'.$u_query);
				}

				//if (!$first) {
				  //echo $rank.'&nbsp;&nbsp;<img src="'.$base_url.'flag/'.$row['cc'].'.png" style="padding-bottom:2px"> '.stripslashes($shortname).'<span class="secondary">'.number_format(strip_tags($row['score']),4).'</span></li>';
				  
				//} else {
				  //echo $rank.'&nbsp;&nbsp;'.$row['cc'].' <a href="player.php?name='.$name.'">'.stripslashes($shortname).'</a><span class="secondary">'.number_format(strip_tags($row['score']),4).'</span></li>';
				  echo $rank.'&nbsp;&nbsp;<img src="'.$base_url.'flag/'.$row['cc'].'.png" style="padding-bottom:2px"> '.' <a href="player.php?name='.urlencode($name).'">'.stripslashes($shortname).'</a><span class="secondary">'.number_format(strip_tags($row['score']),4).'</span></li>';
				  //$first = false;
				//}
				
        
				$rank++;
			}
		}
		/*
		if (!$have_scrolled && $last_named_rank > 0) {
			echo '<script>window.scroll(0,'.((45 * $last_named_rank) + 149).');</script>';
		}
		*/
		?>
	</ul>
	<div style="padding:10px;margin-bottom:100px;font-size:12px;text-align:center">&copy; 2009 <a href="http://www.greengarstudios.com/">GreenGar Studios &middot; www.greengarstudios.com</a><br>
	<?php
	
	mysql_close($conn);

	$mtime = microtime(); 
	$mtime = explode(" ",$mtime); 
	$mtime = $mtime[1] + $mtime[0]; 
	$endtime = $mtime; 
	$totaltime = ($endtime - $starttime); 
	echo 'Global High Scores list generated in '.round($totaltime, 3)." seconds";
	?></div>

<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-9214979-1");
pageTracker._trackPageview();
} catch(err) {}</script>

</body>
</html>
<?php
/*
if ($_SERVER['REMOTE_ADDR'] == '207.151.248.106' || $_SERVER['REMOTE_ADDR'] == '76.171.7.232' || $_SERVER['REMOTE_ADDR'] == '76.171.7.247') {
  echo $defaultLanguage.'<br>';
	echo $_SERVER['QUERY_STRING'];
	echo '<br><br>problems:'.$problems.',version:'.$version;
	echo '<br><br><a href="javascript:history.go(-1)">Back</a>';
	//echo 'hey... <script>window.scroll(0,'.(200).');</script>';
}
*/