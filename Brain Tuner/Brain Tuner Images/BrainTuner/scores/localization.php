<?php
$defaultLanguage = getDefaultLanguage();
function LocalizedString($s) {
  global $defaultLanguage;
  
  if ($defaultLanguage != 'es-es')
    return $s;

  switch ($s) {
  case 'Jump to your score': return 'Ir a tu resultado';
  case 'Your score: ': return 'Tu puntuación: ';
  case ' seconds': return ' segundos';
  case 'Name:': return 'Nombre:';
  case 'Email (not shown):': return 'Email (no se mostrará):';
  case 'Double-check typing before submitting!':
    return '¡Rellena los dos campos antes de enviar!';
  case 'Submit Score': return 'Enviar puntuación';
  case "You're submitting your time to the<br>Brain Tuner <b>Global High Scores</b> chart.":
    return 'Se enviará tu tiempo a la lista de los <b>Global High Scores</b> de Brain Tuner.';
  case 'Options: ': return 'Opciones: ';
  case ' Problems': return ' Problemas';
  case 'Last': return 'Últimas';
  case 'Hours': return 'Horas';
  }
  return $s;
}
function getDefaultLanguage() {
   if (isset($_SERVER["HTTP_ACCEPT_LANGUAGE"]))
      return parseDefaultLanguage($_SERVER["HTTP_ACCEPT_LANGUAGE"]);
   else
      return parseDefaultLanguage(NULL);
}
function parseDefaultLanguage($http_accept, $deflang = "en") {
   if(isset($http_accept) && strlen($http_accept) > 1)  {
      # Split possible languages into array
      $x = explode(",",$http_accept);
      foreach ($x as $val) {
         #check for q-value and create associative array. No q-value means 1 by rule
         if(preg_match("/(.*);q=([0-1]{0,1}\.\d{0,4})/i",$val,$matches))
            $lang[$matches[1]] = (float)$matches[2];
         else
            $lang[$val] = 1.0;
      }

      #return default language (highest q-value)
      $qval = 0.0;
      foreach ($lang as $key => $value) {
         if ($value > $qval) {
            $qval = (float)$value;
            $deflang = $key;
         }
      }
   }
   return strtolower($deflang);
}
header("Vary: Accept-Language");
if ($defaultLanguage == 'es-es')
  header('Content-type: text/html; charset=UTF-8');