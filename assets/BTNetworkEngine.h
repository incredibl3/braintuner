//
//  BTNetworkEngine.h
//  Brain Tuner
//
//  Created by Elliot Lee on 6/21/09.
//  Copyright 2009 GreenGar Studios. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WrapperDelegate.h"


//@class Wrapper;

@interface BTNetworkEngine : NSObject <WrapperDelegate> {
	BOOL submittingNewScore;
}

@property BOOL submittingNewScore;

+ (BTNetworkEngine *)sharedBTNetworkEngine;
- (void)postToURL:(NSString *)URLString withParameters:(NSDictionary *)params target:(NSObject *)target selector:(SEL)selector;
- (void)cancelConnection;
- (BOOL)saveScore;

@end
