/**
 * MyAdView.m
 * Selects between AdMob and Mobclix, and provides failover when one network can't fill the request.
 *
 * Elliot Lee <elliot@greengar.com>
 * GreenGar Studios
 */

#import "GSAdView.h"

#import "FlurryAPI.h"
#import "FlurryAPI+Extensions.h"
#import "UIDevice+Hardware.h"
#import "SynthesizeSingleton.h"

#import "Wrapper.h"
#import "BrainTuner3AppDelegate.h"
#import "RootViewController.h"
#import "AdWhirlView.h"

#define alertPromoTag        888

@implementation GSAdView

@synthesize alertPromoURL = alertPromoURL_;
@synthesize promoTitle = promoTitle_;
@synthesize promoMessage = promoMessage_;
@synthesize promoCancelButtonTitle = promoCancelButtonTitle_;
@synthesize promoOtherButtonTitle = promoOtherButtonTitle_;
@synthesize iTunesURL;

BOOL TTIsStringWithAnyText(id object) {
	return [object isKindOfClass:[NSString class]] && [(NSString*)object length] > 0;
}

SYNTHESIZE_SINGLETON_FOR_CLASS(GSAdView);

// Used by singleton synthesizer
- (id)init {
	if ((self = [super initWithFrame:CGRectZero])) {
		// be sure to call -start after this (after setting delegate)
		
		mostRecentAdNetwork = [[NSMutableString alloc] init];
		[mostRecentAdNetwork setString:@"HouseAds"];                    // always start with HouseAds
		
		NSString * str = [NSString stringWithFormat:@"AdTracking.%@",@"HouseAds"];
		START_TIMED_EVENT( str );
        
        
        //KONG: check for multitasking
        UIDevice *device = [UIDevice currentDevice];
        if ([device respondsToSelector:@selector(isMultitaskingSupported)] && [device isMultitaskingSupported]) {
            [[NSNotificationCenter defaultCenter] addObserver:self 
                                                     selector:@selector(applicationWillEnterForeground:) 
                                                         name:UIApplicationWillEnterForegroundNotification 
                                                       object:nil];
        }		
	}
	return self;
}

- (void)applicationWillEnterForeground:(NSNotification *)notification {
	//[awView updateAdWhirlConfigWithDelegate:self];
	[awView updateAdWhirlConfig];
	DLog(@"updateAdWhirlConfig");
}

- (void)start {
	//[self setBackgroundColor:[UIColor whiteColor]];
	adButton = [[UIButton buttonWithType:UIButtonTypeCustom] retain];
	[adButton setImage:[UIImage imageNamed:@"HouseAd.png"] forState:UIControlStateNormal];
	
	if (IS_IPAD) {
		adButton.frame = CGRectMake(0, 0, self.bounds.size.width, 66);
	} else {
		adButton.frame = CGRectMake(0, 0, self.bounds.size.width, 50);
	}
	
	
	[adButton addTarget:self action:@selector(adButtonTouchUpInside) forControlEvents:UIControlEventTouchUpInside];
	
	[self addSubview:adButton];
	
	self.clipsToBounds = NO;
	awView = [AdWhirlView requestAdWhirlViewWithDelegate:self];
	[awView retain];
	gotAds = FALSE;
}


- (NSString *)adWhirlApplicationKey {
	return @"f72af5fef930102c96dc5b26aef5c1e9";
}

- (UIViewController *)viewControllerForPresentingModalView {
	return UIAppDelegate.rootViewController;
}


//- (NSArray *)testDevices {
//      DLog(@"test");
//      return [NSArray arrayWithObjects:
//                      @"28ab37c3902621dd572509110745071f0101b124",  // Test iPhone 3GS 3.0.1
//                      @"8cf09e81ef3ec5418c3450f7954e0e95db8ab200",  // Test iPod 2.2.1
//                      nil];
//}

- (void)adWhirlDidReceiveAd:(AdWhirlView *)adWhirlView {
	
	if (!gotAds) {
		gotAds = TRUE;
		[self addSubview:awView];
		adButton.hidden = YES;
		
		NSString * str = [NSString stringWithFormat:@"AdTracking.%@",@"HouseAds"];
		END_TIMED_EVENT( str );
		
	}
	
	if (![mostRecentAdNetwork isEqualToString:[adWhirlView mostRecentNetworkName]]) {
		
		NSString * str = [NSString stringWithFormat:@"AdTracking.%@",mostRecentAdNetwork];
		END_TIMED_EVENT( str );
		
		[mostRecentAdNetwork setString:[adWhirlView mostRecentNetworkName]];
		
		NSString * str2 = [NSString stringWithFormat:@"AdTracking.%@",[adWhirlView mostRecentNetworkName]];
		START_TIMED_EVENT(str2);
	}
	
	//      NSDictionary * adDictionary = [[NSDictionary alloc] init];
	//      
	//      [adDictionary setValue:[adWhirlView mostRecentNetworkName] forKey:@"Ad type"];
	//      [adDictionary setValue:nil forKey:@"Ad duration"];
	//      
	//      [FlurryAPI logEvent:@"Whiteboard Lite Ad tracking" withParameters:adDictionary timed:YES];
	
	DLog(@"got ads from %@ %f %f", [adWhirlView mostRecentNetworkName], self.bounds.size.width, self.bounds.size.height);
	[UIView beginAnimations:@"AdWhirlDelegate.adWhirlDidReceiveAd:"
					context:nil];
	
	[UIView setAnimationDuration:0.7];
	
	[self recalculateAdPostion];
	
	[UIView commitAnimations];
}

- (void)recalculateAdPostion {
	if (IS_IPAD) {
		adButton.frame = CGRectMake(0, 0, self.bounds.size.width, 66);
	} else {
		adButton.frame = CGRectMake(0, 0, self.bounds.size.width, 50);
	}
	
	CGSize adSize = [awView actualAdSize];
	CGRect newFrame = awView.frame;
	
	newFrame.size = adSize;
	newFrame.origin.x = (self.frame.size.width - adSize.width) / 2;
	newFrame.origin.y = (self.frame.size.height - adButton.frame.size.height); // / 2;
	
	awView.frame = newFrame;
}

- (void)adWhirlDidFailToReceiveAd:(AdWhirlView *)adWhirlView usingBackup:(BOOL)yesOrNo {
	if (yesOrNo) // if usingBackup then we do nothing
		return;
	
	// remove awView, show house ad button
	if (gotAds) {
		gotAds = FALSE;
		adButton.hidden = NO;
		[awView removeFromSuperview];
		
		//              END_TIMED_EVENT(mostRecentAdNetwork);
		//              [mostRecentAdNetwork setString:@"HouseAds"];
		//              START_TIMED_EVENT(@"HouseAds");
		
        
		NSString * str = [NSString stringWithFormat:@"AdTracking.%@",mostRecentAdNetwork];
		END_TIMED_EVENT( str );
		
		[mostRecentAdNetwork setString:@"HouseAds"];
		
		NSString * str2 = [NSString stringWithFormat:@"AdTracking.%@",@"HouseAds"];
		START_TIMED_EVENT(str2);
		
	}
}

- (void)adWhirlWillPresentFullScreenModal {
	
	//[[[[UIAppDelegate viewController] avController] captureSession] stopRunning];
	
	[awView retain];
}

- (void)adWhirlDidDismissFullScreenModal {
	//[[UIAppDelegate viewController] dismissModalViewControllerAnimated:NO];
	
	//[[[[UIAppDelegate viewController] avController] captureSession] startRunning];
	
	//[self performSelector:@selector(adWhirlDidReceiveAd:) withObject:awView afterDelay:0.7];
	
	[awView autorelease];
	//awView.delegate = self; 
}

//- (BOOL)adWhirlTestMode {
//      return YES;
//}

- (void)showPromoUsingRowURL:(BOOL)usingRowURL {
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:self.promoTitle
													message:self.promoMessage
												   delegate:self
										  cancelButtonTitle:(TTIsStringWithAnyText(self.promoCancelButtonTitle) ? self.promoCancelButtonTitle : nil)
										  otherButtonTitles:(TTIsStringWithAnyText(self.promoOtherButtonTitle) ? self.promoOtherButtonTitle : nil), nil];
	[alert show];
	[alert release];
}

- (void)wrapper:(Wrapper *)wrapper didRetrieveData:(NSData *)data {
	
	// takes ~2-5 sec for request to come back
	
	
	
	// 1.6 format (8 items [0-7]):
	// 0) iAd
	// 1) Google
	// 2) AdMob
	// 3) title
	// 4) message
	// 5) cancelButtonTitle
	// 6) otherButtonTitle
	// 7) urlString for alert promo
	
	NSArray *dataArray = [[wrapper responseAsText] componentsSeparatedByString:@"|"];
	if ([dataArray count] == 8) {
		
		DLog(@"%@", dataArray);
		
		// ignore alert if BOTH title AND message are empty
		// also, button titles and URL are REQUIRED...
		
		self.promoTitle             = [dataArray objectAtIndex:3];
		self.promoMessage           = [dataArray objectAtIndex:4];
		self.promoCancelButtonTitle = [dataArray objectAtIndex:5];
		self.promoOtherButtonTitle  = [dataArray objectAtIndex:6];
		self.alertPromoURL          = [dataArray objectAtIndex:7];              
		
		if (TTIsStringWithAnyText(self.promoTitle) || TTIsStringWithAnyText(self.promoMessage)) {
			// Don't show promo if the user is in the middle of a game
			//                      if (!UIAppDelegate.rootViewController.flipsideViewController.view.superview) {
			[self showPromoUsingRowURL:NO];
			//                      } else {
			//                              DLog(@"user is in a game, doing nothing");
			//                      }
		} else {
			DLog(@"both title and message are empty");
		}
		
	} else {
		if ([dataArray count] != 1 || ![[dataArray objectAtIndex:0] isEqualToString:@""]) {
			DLog(@"Invalid dataArray:%@", dataArray);
        }
	}
	
	[engine release], engine = nil;
}

#pragma mark -
#pragma mark Affiliate Link

// Process a LinkShare/TradeDoubler/DGM URL to something iPhone can handle
- (void)openReferralURL:(NSURL *)referralURL {
	self.iTunesURL = referralURL;
	DLog(@"self.iTunesURL = %@", self.iTunesURL);
    NSURLConnection *conn = [[NSURLConnection alloc] initWithRequest:[NSURLRequest requestWithURL:referralURL] delegate:self startImmediately:YES];
    [conn release];
}

// Save the most recent URL in case multiple redirects occur
// "iTunesURL" is an NSURL property in your class declaration
- (NSURLRequest *)connection:(NSURLConnection *)connection willSendRequest:(NSURLRequest *)request redirectResponse:(NSURLResponse *)response {
	DLog(@"self.iTunesURL = %@", self.iTunesURL);
	if (self.iTunesURL) {
		self.iTunesURL = [response URL];
		if (!self.iTunesURL) {
			self.iTunesURL = [request URL];
		}
		DLog(@"self.iTunesURL = %@", self.iTunesURL);
		return request;
	}
	return nil;
}

// No more redirects; use the last URL saved
- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
	DLog(@"self.iTunesURL = %@", self.iTunesURL);
	if (self.iTunesURL)
		[[UIApplication sharedApplication] openURL:self.iTunesURL];
	[loadingAlert dismissWithClickedButtonIndex:[loadingAlert cancelButtonIndex] animated:NO];
	DLog(@"releasing loadingAlert");
	[loadingAlert release], loadingAlert = nil;
}

#define kCouldntReach @"Couldn't reach Greengar.com. Open in Safari?"
- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
	[loadingAlert dismissWithClickedButtonIndex:[loadingAlert cancelButtonIndex] animated:NO];
	DLog(@"releasing loadingAlert");
	[loadingAlert release], loadingAlert = nil;
	
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Unable to Connect" message:kCouldntReach delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
	[alert show];
	[alert release];
}

#define kLoadingTitle @"Loading..."
- (void)showLoadingAlert {
	if (!loadingAlert) { // just being defensive
		// show Loading... alert
		loadingAlert = [[UIAlertView alloc] initWithTitle:kLoadingTitle message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:@"Hide", nil];
		[loadingAlert show];
	}
}

- (void)adButtonTouchUpInside {
	LOG_EVENT(@"premiumAdTapped");
	
	//      NSString *iTunesLink = @"http://itunes.greengar.com/braintunerinfo.php";
    
    //ELLIOT: this link should ONLY be used in Whiteboard Lite!!!
	//NSString *iTunesLink = @"http://bit.ly/wcdlitehousead"; // GOLD
    
    //ELLIOT: updated 20110620; uses bit.ly for stats, but we can change this remotely
    NSString *iTunesLink = @"http://www.greengarstudios.com/redirect/bt1litehousead.php";
    
	// Sep 17, 10 @ 4:29 PM = 905
	
	loadingAlert = [[UIAlertView alloc] initWithTitle:kLoadingTitle message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:@"Cancel", nil];
	[loadingAlert show];
	
	[self openReferralURL:[NSURL URLWithString:iTunesLink]];
}

- (void) alertView:(UIAlertView*)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
#if GOLD
	if ([alertView.message isEqualToString:kCouldntReach]) {
		if (buttonIndex == alertView.firstOtherButtonIndex) {
			NSString *urlString = @"http://www.greengar.com/?whiteboardlite=1";
			DLog(@"opening %@", urlString);
			[[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlString]];
		}
	} else if ([alertView.title isEqualToString:kLoadingTitle]) {
		if (buttonIndex == alertView.firstOtherButtonIndex) {
			self.iTunesURL = nil;
			[loadingAlert release], loadingAlert = nil;
		}
	} else
		
#endif          
		if (alertView.firstOtherButtonIndex == buttonIndex) {
			//              NSMutableDictionary *promoDict = [[[NSDEF objectForKey:@"promoDict"] mutableCopy] autorelease];
			//              [promoDict setObject:@"YES" forKey:[self.promo_id stringByAppendingString:@"response"]];
			//              [NSDEF setObject:promoDict forKey:@"promoDict"];
			[[UIApplication sharedApplication] openURL:[NSURL URLWithString:self.alertPromoURL]];
		}
}

- (void)dealloc {
	[[NSNotificationCenter defaultCenter] removeObserver:self];
	[super dealloc];
}

@end