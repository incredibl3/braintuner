//
//  SubmitScoreViewController.h
//  BrainTuner3
//
//  Created by Instructor on 2/6/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ScoreViewController.h"
#import <GameKit/GameKit.h>
#import "GameCenterManager.h"
#import "FBConnect/FBConnect.h"
#import "SA_OAuthTwitterEngine.h"
#import "SA_OAuthTwitterController.h"


// NOTE: All Game Center-related features require iOS 4.1+

@interface SubmitScoreViewController : UIViewController <
		UITextFieldDelegate,
		UIAlertViewDelegate,
		GKLeaderboardViewControllerDelegate,
		GameCenterManagerDelegate,
		FBSessionDelegate,
		FBDialogDelegate,
		SA_OAuthTwitterControllerDelegate
			> {
				
	UILabel *scoreLabel;
	UITextField *nameTextField;
	UITextField *emailTextField;
	UIButton *submitScoreButton;
	ScoreViewController *scoreViewController; // Weak reference (assign)
	
	IBOutlet UIView *topGreenBar;
	IBOutlet UIView *midGreenBar;
	
	UIButton *gameCenterButton;
	UIButton *facebookButton;
	UIButton *twitterButton;
	
	double					finalTime;
	GameCenterManager		*gameCenterManager;
	int64_t					gameCenterLeaderBoardScore;
	NSString				*gameCenterCategory;
	
	Facebook				*fbConnectManager;
	SA_OAuthTwitterEngine	*twitterManager;
	
	// interal flags for controlling gameCenterButton, facebookButton, and twitterButton
	BOOL					submissionProcessing;
	BOOL					leaderBoardSubmissionSucceeded;
	BOOL					facebookSubmissionSucceeded;
	BOOL					twitterSubmissionSucceeded;
}

@property (nonatomic, retain) IBOutlet UITextField *nameTextField;
@property (nonatomic, retain) IBOutlet UITextField *emailTextField;
@property (nonatomic, retain) IBOutlet UILabel *scoreLabel;
@property (nonatomic, retain) IBOutlet UIButton *submitScoreButton;
@property (nonatomic, retain) IBOutlet UIButton *gameCenterButton;
@property (nonatomic, retain) IBOutlet UIButton *facebookButton;
@property (nonatomic, retain) IBOutlet UIButton *twitterButton;
@property (nonatomic, assign) ScoreViewController *scoreViewController; // Weak reference (assign)

@property (nonatomic, retain) GameCenterManager					*gameCenterManager;
@property (nonatomic, assign) int64_t							gameCenterLeaderBoardScore;

@property (nonatomic, retain) NSString							*gameCenterCategory;
@property (nonatomic, retain) Facebook							*fbConnectManager;
@property (nonatomic, retain) SA_OAuthTwitterEngine				*twitterManager;

- (void)setFinalTimeForSubmission:(double)time;
- (void)submitScore;
- (IBAction)cancelButtonTapped;
- (IBAction)submitScoreButtonTapped;

- (IBAction)broadcastOnLeaderBoard:(id)sender; 

- (IBAction)broadcastOnFacebook:(id)sender;
- (IBAction)broadcastOnTwitter:(id)sender;

-(void)showGameCenterLeaderBoardView;

- (void)validateInput;

@end
