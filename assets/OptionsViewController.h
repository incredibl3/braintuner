//
//  OptionsViewController.h
//  BrainTuner3
//
//  Created by Elliot Lee on 8/23/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

//#import <UIKit/UIKit.h>
//#import "BrainTuner3AppDelegate.h"

#ifdef LITE_MODE
	#import "GSAdView.h"
#endif

#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>

@class MainViewController;

@interface OptionsViewController : UIViewController <MFMailComposeViewControllerDelegate>
{
	//IBOutlet UITableView tableView;
	IBOutlet UILabel *versionLabel;
	UITableView *_optionsTable;
	
	UISwitch *addSwitch;
	UISwitch *posSwitch;
	UISwitch *negSwitch;
}

@property (nonatomic, retain) IBOutlet UITableView *optionsTable;

- (IBAction)tapSwitch:(UISwitch *)sender;
- (IBAction)clickDone;

-(void)displayComposerSheet;
-(void)launchMailAppOnDevice;

@end
