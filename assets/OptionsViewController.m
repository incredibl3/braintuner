//
//  OptionsViewController.m
//  BrainTuner3
//
//  Created by Elliot Lee on 8/23/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import "OptionsViewController.h"
#import "RootViewController.h"
#import "MainViewController.h"
#import "FlurryAPI+Extensions.h"
#import "BrainTuner3AppDelegate.h"

#define LABEL_TAG 3
#define SWITCH_TAG 4

@implementation OptionsViewController

@synthesize optionsTable = _optionsTable;

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return ROW_COUNT; //[problemArray count];
}


#define EMAIL_CELL_IDENTIFIER @"EmailCell"
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	
	if (indexPath.row == EMAIL_ROW) {
		UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:EMAIL_CELL_IDENTIFIER];
		if (cell == nil) {
			cell = [[[UITableViewCell alloc] initWithFrame:CGRectZero reuseIdentifier:EMAIL_CELL_IDENTIFIER] autorelease];
			//cell.selectionStyle = UITableViewCellSelectionStyleBlue;
			//GreenGar Support Email
			if ([cell respondsToSelector:@selector(textLabel)]) {
				cell.textLabel.text = @"Send Feedback by Email";
			} else {
				[(id)cell setText:@"Send Feedback by Email"];
			}
			cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
		}
		return cell;
	}
	
	static NSString *identity = @"OptionCell";
	
	NSString *option;
	
	// Offset the row by 1 for backward-compatibility with Brain Tuner 1.4
	NSUInteger row = indexPath.row + 1;
	if (row == OPT_SOUND) {
		option = NSLocalizedString(@"SOUND", @"Sound");
	} else if (row == OPT_COUNTDOWN) {
		option = NSLocalizedString(@"COUNTDOWN_TIMER", @"Countdown Timer");
	} else if (row == OPT_ADD) {
		option = NSLocalizedString(@"ADDITION", @"Addition");
	} else if (row == OPT_SUB) {
		option = NSLocalizedString(@"SUBTRACTION", @"Subtraction");
	} else if (row == OPT_MUL) {
		option = NSLocalizedString(@"MULTIPLICATION", @"Multiplication");
	} else if (row == OPT_DIV) {
		option = NSLocalizedString(@"DIVISION", @"Division");
	} else if (row == OPT_REV) {
		option = NSLocalizedString(@"REVIEW", @"Review Answers");
	//} else if (row == OPT_BIG) {
	//	option = NSLocalizedString(@"BIGNUMS", @"Big Numbers");
	} else {
		// Since OPT_BIG has been removed, offset the row in order to skip ahead to the rest
		// AND we MUST modify row because it's used as an array index later!
		//NSUInteger offsetRow = row + 1;
		row += 1;
		
		if (row == OPT_POS) {
			option = NSLocalizedString(@"ALLPOS", @"All Positive");
		} else if (row == OPT_NEG) {
			option = NSLocalizedString(@"MORENEG", @"Negative Numbers");
		} else if (row == OPT_NSL) {
			option = NSLocalizedString(@"NOSLASH", @"Use ÷ for Division");
		} else if (row == OPT_SWP) {
			option = NSLocalizedString(@"REVERSEBUTTONS", @"Swap Right/Wrong");
		}
//		else if (row == OPT_INEQUITY) {
//			option = NSLocalizedString(@"INEQUITY", @"Inequity Problems");
//		} 
	}
	
    // Analyze: Function call argument is an uninitialized value
	//DLog(@"row:%d option:%@", row, option);
	
	UILabel *label;
	UISwitch *uiswitch = nil;
	//UIImageView *imageView;
	
	//MyTableViewCell *cell = (MyTableViewCell *)[tableView dequeueReusableCellWithIdentifier:identity];
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identity];//CELL_IDENTIFIER
	
	if (cell == nil) {
		
		cell = [[[UITableViewCell alloc] initWithFrame:CGRectZero reuseIdentifier:identity] autorelease];//CELL_IDENTIFIER
		cell.selectionStyle = UITableViewCellSelectionStyleNone;
		
		//#define TEXT_FIELD_OFFSET 9
		#define LEFT_COLUMN_OFFSET 10
		#define RIGHT_COLUMN_OFFSET 270
		#define ROW_WIDTH 280
		#define ROW_HEIGHT 44 - 1 // for bottom border
		#define SWITCH_WIDTH 94
		#define UPPER_ROW_TOP 8

		//#define EDITING_OFFSET 45
		
		//[super layoutSubviews];
		
		//textField.frame = frame;
		
		CGFloat boundsX = cell.contentView.bounds.origin.x;	
		CGFloat boundsY = cell.contentView.bounds.origin.y;
		//CGFloat width = ROW_WIDTH;
		CGRect frame = CGRectMake(boundsX + LEFT_COLUMN_OFFSET,
								  boundsY,
								  boundsX + RIGHT_COLUMN_OFFSET,
								  ROW_HEIGHT); //label.font.pointSize + 8
		
		label = [[[UILabel alloc] initWithFrame:frame] autorelease]; //CGRectMake(12.0, 0.0, 252.0, 68.0)
		label.tag = LABEL_TAG;
		
		#define MAIN_FONT_SIZE 16//17
		label.font = [UIFont boldSystemFontOfSize:MAIN_FONT_SIZE]; //[label.font fontWithSize:32.0];
		//label.font = [UIFont boldSystemFontOfSize:32.0];
		
		[cell.contentView addSubview:label];
		
		/*
		frame = CGRectMake(ROW_WIDTH - SWITCH_WIDTH + LEFT_COLUMN_OFFSET,
						   boundsY + UPPER_ROW_TOP,
						   ROW_WIDTH - RIGHT_COLUMN_OFFSET,
						   ROW_HEIGHT);
		
		uiswitch = [[UISwitch alloc] initWithFrame:frame];// autorelease];
		
		// Changed this to "row" for backward-compatibility with 1.4
		uiswitch.tag = SWITCH_TAG + row;
		
		[uiswitch addTarget:self action:@selector(tapSwitch:) forControlEvents:UIControlEventValueChanged];
		
		[cell.contentView addSubview:uiswitch];
		 */
		
		UISwitch *switchView = [[UISwitch alloc] init];
		switchView.tag = SWITCH_TAG + row;
		[switchView addTarget:self action:@selector(tapSwitch:) forControlEvents:UIControlEventValueChanged];
		cell.accessoryView = switchView;
		[switchView release];
		
	}
	
	// Normal cell with UISwitch
	label = (UILabel *)[cell.contentView viewWithTag:LABEL_TAG];
	// Gives us a new UISwitch with a different tag?!
	
	// Use "row" for backward-compatibility with 1.4
	//uiswitch = (UISwitch *)[cell.contentView viewWithTag:SWITCH_TAG + row];
	uiswitch = (UISwitch *)cell.accessoryView;
	uiswitch.tag = SWITCH_TAG + row;
	
	//[uiswitch addTarget:self action:@selector(tapSwitch:) forControlEvents:UIControlEventValueChanged];
	
	
	//	}
	//}
	NSMutableArray *options = [UIAppDelegate optionsArray];
	
	// Use "row" for backward-compatibility with 1.4
	[uiswitch setOn:[[options objectAtIndex:row] boolValue]];
	
	label.text = option;
	
	if (uiswitch != nil && uiswitch.tag == OPT_ADD + SWITCH_TAG) {
		DLog(@"addSwitch = uiswitch %d", uiswitch.tag);
		addSwitch = uiswitch;
	} else if (uiswitch != nil && uiswitch.tag == OPT_POS + SWITCH_TAG) {
		DLog(@"posSwitch = uiswitch %d", uiswitch.tag);
		posSwitch = uiswitch;
	} else if (uiswitch != nil && uiswitch.tag == OPT_NEG + SWITCH_TAG) {
		DLog(@"negSwitch = uiswitch %d", uiswitch.tag);
		negSwitch = uiswitch;
	}
	
	return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	
	// Apple's documentation on Managing Selections does not animate this deselection
	[tableView deselectRowAtIndexPath:indexPath animated:NO];
	
	if (indexPath.row != EMAIL_ROW) {
		return;
	}
	
	// Send an email to braintunersupport@greengar.com
	
	// This sample can run on devices running iPhone OS 2.0 or later  
	// The MFMailComposeViewController class is only available in iPhone OS 3.0 or later. 
	// So, we must verify the existence of the above class and provide a workaround for devices running 
	// earlier versions of the iPhone OS. 
	// We display an email composition interface if MFMailComposeViewController exists and the device can send emails.
	// We launch the Mail application on the device, otherwise.
	
	Class mailClass = (NSClassFromString(@"MFMailComposeViewController"));
	if (mailClass != nil)
	{
		DLog(@"mailClass != nil");
		
		// We must always check whether the current device is configured for sending emails
		if ([mailClass canSendMail])
		{
			DLog(@"malClass canSendMail");
			
			[self displayComposerSheet];
		}
		else
		{
			[self launchMailAppOnDevice];
		}
	}
	else
	{
		[self launchMailAppOnDevice];
	}	
}


#pragma mark -
#pragma mark Compose Mail

// Displays an email composition interface inside the application. Populates all the Mail fields. 
-(void)displayComposerSheet
{
	MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
	picker.mailComposeDelegate = self;
	
#ifdef LITE_MODE
	// Set up recipients
	NSArray *toRecipients = [NSArray arrayWithObject:@"braintuner.lite.ios@greengar.com"];
#else
	NSArray *toRecipients = [NSArray arrayWithObject:@"braintuner.pro.ios@greengar.com"];
#endif	
	[picker setToRecipients:toRecipients];
	
	DLog(@"presentModalViewController:picker");
	[UIAppDelegate.rootViewController presentModalViewController:picker animated:YES];
    [picker release];
}


// Dismisses the email composition interface when users tap Cancel or Send. Proceeds to update the message field with the result of the operation.
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error 
{	
	//message.hidden = NO;
	// Notifies users about errors associated with the interface
	switch (result)
	{
		case MFMailComposeResultCancelled:
			//message.text = @"Result: canceled";
			LOG_EVENT(@"SuppMailCancelled");
			break;
		case MFMailComposeResultSaved:
			//message.text = @"Result: saved";
			LOG_EVENT(@"SuppMailSaved");
			break;
		case MFMailComposeResultSent:
			//message.text = @"Result: sent";
			LOG_EVENT(@"SuppMailSent");
			break;
		case MFMailComposeResultFailed:
			//message.text = @"Result: failed";
			LOG_EVENT(@"SuppMailFailed");
			break;
		default:
			//message.text = @"Result: not sent";
			LOG_EVENT(@"SuppMailNotSent");
			break;
	}
	[UIAppDelegate.rootViewController dismissModalViewControllerAnimated:YES];
}


#pragma mark -
#pragma mark Workaround

// Launches the Mail application on the device.
-(void)launchMailAppOnDevice
{
	
#ifdef LITE_MODE
	// Set up recipients
	NSString *recipients = @"mailto:braintuner.lite.ios@greengar.com";
#else
	NSString *recipients = @"mailto:braintuner.pro.ios@greengar.com";
#endif	
	
	[[UIApplication sharedApplication] openURL:[NSURL URLWithString:recipients]];
}


#pragma mark -
#pragma mark Option Switch Actions


- (IBAction)tapSwitch:(UISwitch *)sender {
	DLog(@"tapSwitch #%d: %d", sender.tag - SWITCH_TAG, sender.isOn);
	
	NSMutableArray *options = [UIAppDelegate optionsArray];
	
	NSNumber *value = [NSNumber numberWithBool:sender.isOn];
	
	NSUInteger OPT_THIS = sender.tag - SWITCH_TAG;
	
	[options replaceObjectAtIndex:OPT_THIS withObject:value];
	//DLog(@"%@", options);
	
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	
	// Save the state of this switch
	[defaults setBool:sender.isOn forKey:[NSString stringWithFormat:@"Option%d", OPT_THIS]];
	
	// Require at least one of the operations to be enabled
	if ([[options objectAtIndex:OPT_SUB] boolValue] == NO && [[options objectAtIndex:OPT_MUL] boolValue] == NO &&
		[[options objectAtIndex:OPT_DIV] boolValue] == NO) {
		
		[options replaceObjectAtIndex:OPT_ADD withObject:[NSNumber numberWithBool:YES]];
		[defaults setBool:YES forKey:[NSString stringWithFormat:@"Option%d", OPT_ADD]];
		
		[addSwitch setOn:YES animated:YES];
		DLog(@"addSwitch setOn %d", addSwitch.tag);
	}
	
	//DLog(@"OPT_THIS:%d OPT_POS:%d OPT_NEG:%d sender.isOn:%d", OPT_THIS, OPT_POS, OPT_NEG, sender.isOn);
	// All Positive and Negative Numbers are mutually-exclusive
	if (OPT_THIS == OPT_POS && sender.isOn) {
		// Turned on All Positive, so turn off Negative Numbers (no effect if already off)
		[options replaceObjectAtIndex:OPT_NEG withObject:[NSNumber numberWithBool:NO]];
		[defaults setBool:NO forKey:[NSString stringWithFormat:@"Option%d", OPT_NEG]];
		//NSIndexPath *negIndexPath = [NSIndexPath indexPathWithIndex:(OPT_NEG - 1)];
		//UISwitch *negSwitch = (UISwitch *)([[optionsTable cellForRowAtIndexPath:negIndexPath].contentView viewWithTag:(SWITCH_TAG + OPT_NEG)]);
		[negSwitch setOn:NO animated:YES];
		DLog(@"negSwitch setOn %d", negSwitch.tag);
	} else if (OPT_THIS == OPT_NEG && sender.isOn) {
		// Turned on Negative Numbers, so turn off All Positive (no effect if already off)
		[options replaceObjectAtIndex:OPT_POS withObject:[NSNumber numberWithBool:NO]];
		[defaults setBool:NO forKey:[NSString stringWithFormat:@"Option%d", OPT_POS]];
		//NSIndexPath *posIndexPath = [NSIndexPath indexPathWithIndex:(OPT_POS - 1)];
		//UISwitch *posSwitch = (UISwitch *)([[optionsTable cellForRowAtIndexPath:posIndexPath].contentView viewWithTag:(SWITCH_TAG + OPT_POS)]);
		[posSwitch setOn:NO animated:YES];
		DLog(@"posSwitch setOn %d", posSwitch.tag);
	}
	
//	if (OPT_THIS == OPT_INEQUITY && !sender.isOn) {
//		[options replaceObjectAtIndex:OPT_INEQUITY withObject:[NSNumber numberWithBool:NO]];
//		[defaults setBool:NO forKey:[NSString stringWithFormat:@"Option%d", OPT_INEQUITY]];
//		//NSIndexPath *negIndexPath = [NSIndexPath indexPathWithIndex:(OPT_NEG - 1)];
//		//UISwitch *negSwitch = (UISwitch *)([[optionsTable cellForRowAtIndexPath:negIndexPath].contentView viewWithTag:(SWITCH_TAG + OPT_NEG)]);
//		//[negSwitch setOn:NO animated:YES];
//		
//		DLog(@"inequity set off");
//	}
	
//	if (OPT_THIS == OPT_INEQUITY && sender.isOn) {
//		[options replaceObjectAtIndex:OPT_INEQUITY withObject:[NSNumber numberWithBool:YES]];
//		[defaults setBool:YES forKey:[NSString stringWithFormat:@"Option%d", OPT_INEQUITY]];
//		//NSIndexPath *negIndexPath = [NSIndexPath indexPathWithIndex:(OPT_NEG - 1)];
//		//UISwitch *negSwitch = (UISwitch *)([[optionsTable cellForRowAtIndexPath:negIndexPath].contentView viewWithTag:(SWITCH_TAG + OPT_NEG)]);
//		//[negSwitch setOn:YES animated:YES];
//		
//		DLog(@"inequity set ON");
//	}

}


- (IBAction)clickDone {
	// we end the timed event in viewWillDisappear:
	RootViewController *rootViewController = [(BrainTuner3AppDelegate *)[[UIApplication sharedApplication] delegate] rootViewController];
	[rootViewController toggleOptionsView];
}

- (void)viewWillDisappear:(BOOL)animated {
	DLog(@"END_TIMED_EVENT(@\"optionsView\");");
	END_TIMED_EVENT(@"optionsView");
	[super viewWillDisappear:animated];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
	if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) {
		// Initialization code
	}
	return self;
}

//#ifdef LITE_MODE
- (void)viewDidLoad {
	[super viewDidLoad];
	
	versionLabel.text = [NSString stringWithFormat:@"Version %@", [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"]];
	
	self.view.backgroundColor = [UIColor groupTableViewBackgroundColor];
	/* // Create the ad in RootViewController now
	adView.adCode = @"927fc808-2f71-102c-8da0-12313a002cd2";
	adView.delegate = self;
	 */
}
//#endif

/*
- (void)loadView {
	[super loadView];
	
	NSIndexPath *rowPath = [NSIndexPath indexPathForRow:OPT_REV
											  inSection:0];
	
	[optionsTable scrollToRowAtIndexPath:rowPath
					 atScrollPosition:UITableViewScrollPositionBottom //UITableViewScrollPositionMiddle
							 animated:YES];
}
*/


- (void)viewWillAppear:(BOOL)animated {
	//DLog(@"%s", _cmd);
	
	START_TIMED_EVENT(@"optionsView");
	
	/* // Scroll the empty box off the screen
	[optionsTable reloadData];
	
	NSIndexPath *rowPath = [NSIndexPath indexPathForRow:OPT_REV
											  inSection:0];
	
	[optionsTable scrollToRowAtIndexPath:rowPath
						atScrollPosition:UITableViewScrollPositionBottom //UITableViewScrollPositionMiddle
								animated:YES];
	*/
#ifdef LITE_MODE
	[GSAdView sharedGSAdView].hidden = YES;
#endif	
}


#ifdef LITE_MODE
- (void)adHeightDidChange:(CGFloat)adHeight {
	if (adHeight == 0.0) {
		self.optionsTable.frame = CGRectMake(self.optionsTable.frame.origin.x, 44.0, self.optionsTable.frame.size.width, 320.0);
	} else if (adHeight == 48.0) {
		self.optionsTable.frame = CGRectMake(self.optionsTable.frame.origin.x, 92.0, self.optionsTable.frame.size.width, 272.0);
	} else if (adHeight == 50.0) {
		self.optionsTable.frame = CGRectMake(self.optionsTable.frame.origin.x, 94.0, self.optionsTable.frame.size.width, 270.0);
	}
}
#endif
	 

- (void)viewDidAppear:(BOOL)animated {
	[super viewDidAppear:animated];
	//DLog(@"%s", _cmd);
	
	// "You should call this method whenever you bring the scroll view to front."
	[NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(flashScrollIndicators:) userInfo:nil repeats:NO];
}


- (void)flashScrollIndicators:(NSTimer*)theTimer {
	[self.optionsTable flashScrollIndicators];
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
	// Return YES for supported orientations
	return (interfaceOrientation == UIInterfaceOrientationPortrait);
}


- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning]; // Releases the view if it doesn't have a superview
	// Release anything that's not essential, such as cached data
}


- (void)dealloc {
	[super dealloc];
}


@end
