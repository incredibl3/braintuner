//
//  WBWebViewController.h
//  WordBreaker
//
//  Created by Elliot Lee on 7/2/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "Reachability.h"


@class MainViewController;

@interface WBWebViewController : UIViewController <UIWebViewDelegate> {
	IBOutlet UIWebView *webView;
	IBOutlet UIActivityIndicatorView *activityIndicatorView;
	MainViewController *mainViewController;
}

@property (nonatomic, assign) MainViewController *mainViewController;

- (IBAction)closeTapped;
- (IBAction)refreshTapped;

@end
