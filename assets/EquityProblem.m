//
//  Problem.m
//  BrainTuner3
//
//  Created by Elliot Lee on 7/14/08.
//  Copyright 2008-2009 GreenGar Studios. All rights reserved.
//

#import "EquityProblem.h"

#import "OptionsViewController.h"
#import "BrainTuner3AppDelegate.h"
#import "RootViewController.h"
#import "MainViewController.h"

// a random number between min and max inclusive [min, max]
#define RANDOM_INT(__MIN__, __MAX__) ((__MIN__) + random() % ((__MAX__+1) - (__MIN__))) 

@implementation EquityProblem

- (void)randomize {
	
	BrainTuner3AppDelegate *app = (BrainTuner3AppDelegate *)[[UIApplication sharedApplication] delegate];
	
	//float multiplier = 1;
	int multiplier = 1;
	if ([[app.optionsArray objectAtIndex:OPT_BIG] boolValue] == YES) {
		multiplier = 4; //3;
	}/* else if ([[app.optionsArray objectAtIndex:OPT_SML] boolValue] == YES) {
	  multiplier = 0.3; //0.5;
	  }*/ // Removed Littlenums option
	
	// TODO: optimize by moving this out to the app delegate
	
	int minNumber;
	int maxNumber; //maxNumber = multiplier * 10;
	
#ifdef LITE_MODE
	minNumber = 1;
	maxNumber = 10;
#else
	int difficulty = app.rootViewController.mainViewController.difficulty;
	if (difficulty == 1) {
		minNumber = 1;
		maxNumber = 4;
	} else if (difficulty >= 2 && difficulty <= 11) {
		minNumber = difficulty - 1;
		maxNumber = 8 + difficulty;
	} else if (difficulty >= 12 && difficulty <= 21) {
		minNumber = difficulty - 11;
		maxNumber = 8 + difficulty;
		//multiplier = 2;
	} else if (difficulty >= 22 && difficulty <= 31) {
		minNumber = difficulty - 21;
		maxNumber = 8 + difficulty;
		//multiplier = 3;
	} else {
		minNumber = difficulty - 31;
		maxNumber = 8 + difficulty;
		//multiplier = 4;
	}
#endif
	
	//DLog(@"minNumber:%d maxNumber:%d", minNumber, maxNumber);
	int a = RANDOM_INT(minNumber, maxNumber);
	int b = RANDOM_INT(minNumber, maxNumber);
	int c;
	
	if ([[app.optionsArray objectAtIndex:OPT_NEG] boolValue] == YES) {
		// Only chance of first number being negative; looks weird otherwise
		// b/c negative sign looks like subtraction operator.
		if (RANDOM_INT(1, 2) == 1) {
			a = -a;
		}
	}
	
	NSString *divSign = @"/";
	if ([[app.optionsArray objectAtIndex:OPT_NSL] boolValue] == YES) {
		divSign = @"÷";
	}
	
	NSString *strOp;
	
	//BOOL division = [[[[[[UIApplication sharedApplication] delegate] rootViewController] mainViewController] division] isOn];
	//BrainTuner3AppDelegate *app = [[UIApplication sharedApplication] delegate];
	
	int numOps = [[app.optionsArray objectAtIndex:OPT_ADD] intValue] + [[app.optionsArray objectAtIndex:OPT_SUB] intValue] +
	[[app.optionsArray objectAtIndex:OPT_MUL] intValue] + [[app.optionsArray objectAtIndex:OPT_DIV] intValue];
	
	//DLog(@"Using %d kinds of operators", numOps);
	
	int op;
	//if ([[app.optionsArray objectAtIndex:OPT_DIV] boolValue] == YES) { // division == YES
	op = RANDOM_INT(1, numOps);
	/*
	 } else {
	 op = RANDOM_INT(1, 3); // 1 +, 2 -, 3 x
	 }
	 */
	
	// Find out which operator this problem shall use
	int addOp=0, subOp=0, mulOp=0, divOp=0;
	int nextOp = 1;
	if ([[app.optionsArray objectAtIndex:OPT_ADD] boolValue] == YES) { // Addition is enabled
		addOp = nextOp;
		nextOp++;
	}
	if ([[app.optionsArray objectAtIndex:OPT_SUB] boolValue] == YES) { // Subtraction is enabled
		subOp = nextOp;
		nextOp++;
	}
	if ([[app.optionsArray objectAtIndex:OPT_MUL] boolValue] == YES) {
		mulOp = nextOp;
		nextOp++;
	}
	if ([[app.optionsArray objectAtIndex:OPT_DIV] boolValue] == YES) {
		divOp = nextOp;
		nextOp++;
	}
	
	if (op == addOp) {
		strOp = @"+";
		c = a + b;
	} else if (op == subOp) {
		strOp = @"-";
		c = a - b;
		if ([[app.optionsArray objectAtIndex:OPT_POS] boolValue] == YES) {
			if (c < 0) {
				// Swap a and b
				int t = a;
				a = b;
				b = t;
				c = a - b;
			}
		}
	} else if (op == mulOp) {
		strOp = @"x";
		c = a * b;
	} else {
		strOp = divSign; //@"/"; // ÷ looks too much like +
		c = a * b;
		// Swap a and c
		int t = a;
		a = c;
		c = t;
	}
	
	/**
	 * Test Cases:
	 * ADD, SUB, MUL
	 * ADD, SUB, MUL, DIV
	 * MUL
	 * DIV
	 * ADD, DIV
	 * SUB, DIV
	 */
	
	/*
	 int opsCovered = op;
	 for (int curOp = 1; curOp <= numOps; curOp++) {
	 if ([app.optionsArray objectAtIndex:OPT_ADD] == 1 && opsCovered <= 1) {
	 strOp = @"+";
	 c = a + b;
	 opsCovered += 1;
	 } else if ([app.optionsArray objectAtIndex:OPT_SUB] == 1 && opsCovered <= 2) {
	 strOp = @"-";
	 c = a - b;
	 opsCovered += 2;
	 } else if ([app.optionsArray objectAtIndex:OPT_MUL] == 1 && opsCovered <= 3) {
	 strOp = @"x";
	 c = a * b;
	 opsCovered += 3;
	 } else if ([app.optionsArray objectAtIndex:OPT_DIV] == 1 && opsCovered <= 4) {
	 strOp = @"÷";
	 c = a * b;
	 // Swap a and c
	 int t = a;
	 a = c;
	 c = t;
	 opsCovered += 4;
	 } else {
	 DLog(@"Error: invalid operator options");
	 }
	 }
	 */
	
	int makeRight = RANDOM_INT(0, 1);
	
	if (makeRight == 1) {
		self.isRight = YES;
	} else {
		// If multiplier is 0.3, offset is [1-3]
		int offset = RANDOM_INT(1, (int)(multiplier * 10));
		// If 1, take 1 off the correct answer
		if (offset <= (multiplier * 5)) {
			c -= offset;
		} else {
			// If 2 or 3, add 2 or 3, then take 1.5 off the correct answer
			// Doesn't work for 2, because we get 0.5 < 1, getting lost in the integer conversion
			// NOTE1: Removing Littlenums option -- didn't seem useful anyway
			c += offset - (multiplier * 5);
		}
		
		// Make sure we didn't just cause c to be negative
		if ([[app.optionsArray objectAtIndex:OPT_POS] boolValue] == YES) {
			if (c < 0) {
				// Warning: Edge Case: c = 0 is actually the correct answer
				// Can only happen with subtraction, e.g. 2-2=0
				// c = a - b;
				
				// Warning: ANOTHER Edge Case: a - b == 0 BUT this is actually a division problem,
				// a / b = c, so actually this makes it RIGHT when it should be WRONG!
				// so let's only set it to 1 if it's a SUBTRACTION problem
				// Since neither a nor b can be 0, no division problem will be right with 0 as the answer
				if (a - b == 0 && op == subOp) {
					c = 1;
				} else {
					c = 0;
				}
			}
		}
		
		self.isRight = NO;
	}
	
	//instance.minimal
	
	self.text = [NSString stringWithFormat:@"%d %@ %d = %d", a, strOp, b, c];
	//instance.isRight = YES;
	//DLog(@"%d and %d", a, b);
}

- (NSString *)description {
	//return [NSString stringWithFormat: @"%@ %@", text, [NSNumber numberWithBool:isRight]];
	// Don't actually need to replace anymore, but it shouldn't hurt!
	return [[text stringByReplacingOccurrencesOfString:@" " withString:@""] stringByReplacingOccurrencesOfString:@"÷" withString:@"/"];
}

@end
