/**
 * AdMobSampleProgrammaticAdAppDelegate.h
 * AdMob iPhone SDK publisher code.
 * 
 */

#define AD_REFRESH_PERIOD 30.0 // display fresh ads once per minute

#import <UIKit/UIKit.h>

#import "AdWhirlDelegateProtocol.h"
#import "WrapperDelegate.h"

#define kiPadAdsMaxHeight 66
#define kiPhoneAdsMaxHeight 50

@interface GSAdView : UIView <WrapperDelegate, UIAlertViewDelegate, AdWhirlDelegate> {
	UIButton *adButton;
	UIAlertView *loadingAlert;
	NSURL *iTunesURL;
	
	Wrapper *engine;
	AdWhirlView *awView;
	NSMutableString *mostRecentAdNetwork;
	BOOL gotAds;
}

@property (nonatomic, retain) NSURL *iTunesURL;
@property (copy, nonatomic) NSString *alertPromoURL;
@property (copy, nonatomic) NSString *promoTitle;
@property (copy, nonatomic) NSString *promoMessage;
@property (copy, nonatomic) NSString *promoCancelButtonTitle;
@property (copy, nonatomic) NSString *promoOtherButtonTitle;

+ (GSAdView *)sharedGSAdView;

- (void)start;
- (void)recalculateAdPostion;

@end