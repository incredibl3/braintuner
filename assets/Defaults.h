/*
 *  Defaults.h
 *  BrainTuner3
 *
 *  Created by Elliot on 7/27/10.
 *  Copyright 2010 GreenGar Studios. All rights reserved.
 *
 */

// via http://io.debian.net/~tar/debian/gtamsanalyzer/gtamsanalyzer.app-0.42/Source/appDelegate.m
#define NSDEF [NSUserDefaults standardUserDefaults]
