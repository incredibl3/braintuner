//
//  GSAudio.h
//  BrainTuner3
//
//  Created by Elliot on 10/6/10.
//  Copyright 2010 GreenGar Studios. All rights reserved.
//

#import <Foundation/Foundation.h>


@class AVAudioPlayer;

@interface GSAudio : NSObject {

}

+ (AVAudioPlayer *)load:(NSString *)filename;
+ (void)play:(NSString *)filename;
+ (void)stop:(NSString *)filename;

@end
