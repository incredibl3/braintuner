//
//  UIDevice+Hardware.h
//  Whiteboard
//
//  Created by Elliot on 7/27/10.
//  Copyright 2010 GreenGar Studios. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface UIDevice (Hardware)

- (NSString *) platform;

@end
