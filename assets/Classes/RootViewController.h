//
//  RootViewController.h
//  BrainTuner3
//
//  Created by Elliot Lee on 7/13/08.
//  Copyright GreenGar Studios 2008-2009. All rights reserved.
//

#import <UIKit/UIKit.h>

#if LITE_MODE
    #import "GSAdView.h"
#endif

#include <AudioToolbox/AudioToolbox.h>
#import "BrainTuner3AppDelegate.h"
@class MainViewController;
@class FlipsideViewController;

@class ScoreViewController;
@class OptionsViewController;

@protocol RootViewController
- (MainViewController *)mainViewController;
@end

@interface RootViewController : UIViewController <RootViewController
//#ifdef LITE_MODE
//, MobclixAdViewDelegate
//#endif
> {
	MainViewController *mainViewController;
	FlipsideViewController *flipsideViewController;
	UINavigationBar *flipsideNavigationBar;
	
	ScoreViewController *scoreViewController;
	OptionsViewController *optionsViewController;
	
	UIBarButtonItem *buttonItem;
	
#if LITE_MODE
	GSAdView *myAdView;
#endif
}

@property (nonatomic, retain) MainViewController *mainViewController;
@property (nonatomic, retain) UINavigationBar *flipsideNavigationBar;
@property (nonatomic, retain) FlipsideViewController *flipsideViewController;
@property (nonatomic, retain) OptionsViewController *optionsViewController;
@property (nonatomic, retain) UIBarButtonItem *buttonItem;

#ifndef LITE_MODE
	@property (nonatomic, retain) ScoreViewController *scoreViewController;
	- (IBAction)showScoreView:(double)score;
	- (void)toggleViewWithTimePerProblem:(float)timePerProblem;
#endif

- (void)toggleView; // flipsideView (the game)
- (void)toggleOptionsView; // optionsView (the on/off switches)
- (void)setButtonToDone;
- (void)setButtonToCancel;
- (void)toggleViewAnimated:(BOOL)animated;

@end


