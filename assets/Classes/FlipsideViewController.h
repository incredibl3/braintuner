//
//  FlipsideViewController.h
//  BrainTuner3
//
//  Created by Elliot Lee on 7/13/08.
//  Copyright GreenGar Studios 2008-2009. All rights reserved.
//

#import <UIKit/UIKit.h>

#include <AudioToolbox/AudioToolbox.h>

#import "Problem.h"
#import "BrainTuner3AppDelegate.h"
#import "RootViewController.h"
#import "MainViewController.h"

#define kAppStoreAlertTag 156
#define kEmailFriendAlertTag 157

@interface FlipsideViewController : UIViewController {
	IBOutlet UILabel *countdown;
	IBOutlet UITableView *problemTable;
	IBOutlet UIButton *rightButton;
	IBOutlet UIButton *wrongButton;
	
	NSMutableArray *problemArray;
	int currentProblem;
	int correct, incorrect;
	
	NSTimer *continueCountdownTimer;
	
	CFTimeInterval startTimeInterval;
	CFTimeInterval finalTime;
	
	// We use this a lot...
	BrainTuner3AppDelegate *app; // TODO: use the #define instead
	
	UIImageView *blueGreenView;
	
//	BOOL useTimePenalty;
}

//- (UITableViewCell *)cellFromNibNamed:(NSString *)nibName;

- (IBAction)clickRight:(id)sender;
- (IBAction)clickWrong:(id)sender;

- (void)alertStatsAndReset;

- (void)swapButtonsIfNecessary;

//- (NSString *)saveScoreIfBetter:(float)finalTime;

- (int)getProblemCount;

@property (nonatomic, retain) NSMutableArray *problemArray;

@property (nonatomic, retain) UITableView *problemTable;

@end
