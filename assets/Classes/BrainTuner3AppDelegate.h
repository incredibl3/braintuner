//
//  BrainTuner3AppDelegate.h
//  BrainTuner3
//
//  Created by Elliot Lee on 7/13/08.
//  Copyright GreenGar Studios 2008-2009. All rights reserved.
//

#import <UIKit/UIKit.h>

#define submitURL @"http://www.greengarstudios.com/scores/submitscorenative.php"

// 1.4 set this at 13. Changing it is RISKY
#define OPTION_COUNT 14 //OPT_SML + 1 //13 // with OPT_SML

#define kAnimationDuration 0.78 //0.8 //0.75

//#define kDefaultPngMode 1

@class RootViewController;
#import "PromoAds.h"

@protocol BrainTuner3AppDelegate
- (RootViewController *)rootViewController;
- (NSMutableArray *)optionsArray;
@end

@interface BrainTuner3AppDelegate : NSObject <UIApplicationDelegate, BrainTuner3AppDelegate> {
	IBOutlet UIWindow *window;
	IBOutlet RootViewController *rootViewController;
	int problemCount;
	NSMutableArray *optionsArray;
    
    PromoAds *_promoAds;
    BOOL shouldStartPromoAds;
}

- (void)loadBestTimeForSegmentedControl;
- (int)getProblemCountFromSegmentedControl;
- (NSString *)saveScoreIfBetter:(float)newScore;
- (NSString *)saveScoreIfBetter:(float)newScore forProblemCount:(int)problems;

@property (nonatomic, retain) NSMutableArray *optionsArray;
@property (nonatomic) BOOL shouldStartPromoAds;
@property (nonatomic, retain) UIWindow *window;
@property (nonatomic, retain) RootViewController *rootViewController;
@property (nonatomic) int problemCount;

- (void)startPromoAds;

@end

